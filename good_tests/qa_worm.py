'''
This file performs regression testing of bitsey.
Before running it, make sure to edit the end of worm.py to uncomment the line
	regress_command_line(); quit()
Note that some of the tests -- worm_gating, worm_lab and worm_2tailed -- share
their good_worm_... files with the regular qa.py, so the good_worm_... files do
double duty. There are thus a few "blessed" differences:
- The line "Head D_K=..., tail D_K..., GJ_scale=..." is printed by
  setup_one_worm() in main(), but in regress_end() in worm.py. Thus, it is
  called in a slightly different place in the two sims, which can result in
  different values being printed out.
- Ditto for "Head gating=..." and "Tail gating=..."

For this same reason -- please DON'T FIX those three good_worm_* files!
'''
import os

def test (name, fix):
    print ('{}....'.format(name))
    os.system ('python3 ../main_worm.py ' + name + ' 5 --regress >& tmp')
    if (fix):
        os.system ('cp tmp good_' + name)
    os.system ('sdiff -s tmp good_' + name + ' | grep -v "func evals"')

fix=False
#fix=True
if (fix):
    print ('Fixing...')

test ('worm_gating', fix)
test ('worm_evolve',  fix)
test ('worm_lab', fix)
test ('worm_2tailed', fix)
test ('worm_exhaustive',  fix)
