'''
This file performs regression testing of bitsey.
Before running it, make sure to edit main.py setup_and_sim(),
ensure that plotting is turned off.
'''
import os

def test (name, fix):
    print ('{}....'.format(name))
    os.system ('python3 ../main.py ' + name + ' 5 --regress > tmp')
    if (fix):
        os.system ('cp tmp good_' + name)
    os.system ('sdiff -s tmp good_' + name)

fix=False
#fix=True
if (fix):
    print ('Fixing...')

test ('gen_decay', fix)
test ('test1', fix)
test ('test2', fix)
test ('test3', fix)
test ('magic_invs', fix)
test ('magic_xor',  fix)
test ('NN_GRN', fix)
test ('NN_QSS', fix)
test ('worm_gating', fix)
test ('worm_lab', fix)
test ('worm_2tailed', fix)
test ('lab1',  fix)
test ('lab2',  fix)
test ('lab2b', fix)
test ('lab_QSS_weighted_sum',  fix)
test ('lab_magic_xor',  fix)
test ('bistable_cell',   fix)
