import numpy as np
import RD
import sys; sys.path.append ("..")	# To get the usual Bitsey files
import sim
import eplot as eplt
import edebug as edb

#################################################################
# Functions for the the next-generation counting GRNs.
# These routines don't use the Werner model; they just spread a pattern, don't
# let it diffuse anywhere, and then try to characterize it.
#################################################################

# This tests the "direct" counting GRN. It's the one that relies on a simple,
# direct path from left to right, but still doesn't use GJs.
def run_counting_GRN_direct (n_cells, n_peaks_max):
    end_time=200
    p = sim.Params()

    # Counting to N needs 2N SIG* signals
    # The order is sig0L, sig0H, sig1L, sig1H, ...
    S_sigs = [f'S{i}{LH}' for i in range(n_peaks_max) for LH in ['L','H']]
    sigs = ['A','I'] + S_sigs

    RD.setup_common (p,.0001,True,25,50,sigs,n_cells)
    setup_counting_GRN_direct (p, n_cells, n_peaks_max)

    # Start off the computation chain with S0L=1 in cell #0 and the morphogen
    # nicely spread.
    S0L = sim.ion_i['S0L']
    A = sim.ion_i['A']
    sim.cc_cells[S0L,0] = 1
    RD.spread ("LHL", [A])

    t_shots, cc_shots = sim.sim (end_time)
    post_counting_GRN_direct (p, t_shots, cc_shots)

    # We often want a printed dump of the final simulation results.
    np.set_printoptions (formatter={'float': '{:.6g}'.format}, linewidth=90)
    #edb.dump (end_time, sim.cc_cells, edb.Units.mol_per_m3s, True)

# Build the network for our counting GRN (the direct version).
#	- 'n_peaks_max' tells how high the GRN can count to (i.e., how many
#	  peaks it can handle before saturating its count).
def setup_counting_GRN_direct (p, n_cells, n_peaks_max):
    # Get some offsets of the various signals in cc_cells[].
    S0L = sim.ion_i['S0L']
    S0L_end = S0L+2*n_peaks_max
    A = sim.ion_i['A']

    # For now, the activator A doesn't diffuse either; we seed it to a shape and
    # it will stay that way.
    sim.GJ_diffusion[A,:] = 0

    # The sig* signals travel between cells. Pick a value for GJ diffusion and
    # for their decay constant so that they have a range of about 3 cells.
    sim.GJ_diffusion[S0L:S0L_end] = 1e-13
    sig_decay = 1

    # Instantiate the GRN
    for S in range(S0L,S0L_end):
        counting_GRN_direct_S_gate (S, S0L, n_peaks_max)

# This class is for the signaling proteins.
class counting_GRN_direct_S_gate (sim.Gate):
  # Note that my_S is 7 for S0: (since Na,Cl,K,P,A,I,amDr are ahead of it).
  def __init__(self, my_S, S_start, n_peaks_max):
    super().__init__ (self.GATE_GD, my_S)
    self.my_S,self.S_start,self.N = my_S,S_start,n_peaks_max
    self.morph = sim.ion_i['A']
    #self.amDr  = sim.ion_i['amDr']
    self.decay = 1	# Decay rate of my S signal.
    self.Schm0,self.Schm1 = .1,.3

  # The function that actually computes the gating vector at runtime.
  # The main eqn is, in general, S_me = S_prev & bump_up | S_me & maintain:
  #	S0L = nothing         | [S0L & !A_SchmH & nothing]
  #	S0H = [S0L & A_SchmH] | [S0H & !A_SchmL & !S0L]
  #	S1L = [S0H & A_SchmL] | [S1L & !A_SchmH & !S0H]
  #	S1H = [S1L & A_SchmH] | [S1H & !A_SchmL & !S1L]
  # We implement OR(a,b,c) with just a wired-or
  #	out = A+B+C
  # It would seem that the wired-output could now range in [0,2] rather than
  # [0,1]. However, the two terms are always mutually exclusive.
  # We implement the AND with the product of multiple separate Hill functions.
  # For now:
  #	- they all use the same Hill exponent HN=3
  #	- term #2's prev_sig and term #3's my_sig use Km=.3
  #	- term #2 and #3's amDr uses kM=.5
  # The terminology:
  #	S_me is the signal I'm responsible for driving (each class instance
  #		drives one signal across all cells). E.g., S1H  
  #	S_prev is the "previous" signal. So if S_me is S1H, then S_prev is S1L.
  #	*conc represents a concentration. E.g., S_me_conc.
  #	*fact is a "factor," and 
  def func (self, cc, Vm_ignore, deltaV_ignore, t_ignore):
    S_me_idx = self.my_S
    S_prev_idx = S_me_idx - 1
    IamH = (S_me_idx - self.S_start) & 1 == 1 	# Am I S*H or S*L?
    HN = 3	# Hill exponent N
    S_me_conc = cc[S_me_idx]			# vector [N_CELLS]

    # Term 1: myself -- K*(S_me_idx/.5)^HN/1+""
    term1 = 0
    if (0):
     S_me_fact = (S_me_conc/.5)**HN
     # Pick kVMax so "1" is a stable point. It won't work exactly since term2
     # and term3 will add some generation, but it will be close.
     kVMax = (1+((1/.5)**HN))/((1/.5)**HN)
     decay = S_me_conc * g_pre_decay
     term1 = kVMax * S_me_fact/(1+S_me_fact)

    # Term 2 (bump up): prev_sig & A_SchmL for S*L
    #				 & A_SchmH for S*H
    term_bumpup = 0
    if (S_me_idx > self.S_start):	# S0L doesn't have a bump-up term.
        S_prev_fact = (cc[S_prev_idx]/.3)**HN	# vector [N_CELLS]
        if IamH:
            A_SchmX = (cc[self.morph]/self.Schm1)**HN	# > A_Schm1
            A_SchmX = A_SchmX/(1+A_SchmX)
        else:
            A_SchmX = (cc[self.morph]/self.Schm0)**HN	# < A_Schm0
            A_SchmX = 1/(1+A_SchmX)
        term_bumpup =  (S_prev_fact/(1+S_prev_fact)) * A_SchmX
        # This is the optional me-2 literal in the "bump-up" term.
        if (S_me_idx > self.S_start+1):
            S_prevPrev_fact = (cc[S_prev_idx-1]/.3)**HN
            # term_bumpup *= (1/(1+S_prevPrev_fact))	# Comment in or out

    # Term 3 (maintain):
    #	S_me & !A_SchmH & !S_prev	for S*L
    #	S_me & !A_SchmL & !S_prev	for S*H
    #	S_me & !A_SchmL			for S0L
    S_me_fact = (S_me_conc/.3)**HN		# vector [N_CELLS]
    if IamH:
        A_SchmX = (cc[self.morph]/self.Schm0)**HN	# !A_SchmL
        A_SchmX = A_SchmX/(1+A_SchmX)
    else:
        A_SchmX = (cc[self.morph]/self.Schm1)**HN	# !A_SchmH
        A_SchmX = 1/(1+A_SchmX)
    term_maintain =  (S_me_fact/(1+S_me_fact)) * A_SchmX
    if (S_me_idx > self.S_start):		# !prev
        term_maintain *= (1/(1+S_prev_fact))
    # This is the optional me-2 literal in the "maintain" term.
    if (S_me_idx > self.S_start+1):		# if there is a me-2 signal...
        S_prevPrev_fact = (cc[S_prev_idx-1]/.3)**HN
        #term_maintain *= (1/(1+S_prevPrev_fact))	# comment in or out.
        
    return (term1 + 2*term_bumpup + term_maintain - cc[S_me_idx]*self.decay)

def post_counting_GRN_direct (GP, t_shots, cc_shots):
    sim.GD_gates[2].func (cc_shots[-1], 0, 0, 0)
    sim.GD_gates[1].func (cc_shots[-1], 0, 0, 0)
    print ("Dumping post-sim...")
    end_shot = cc_shots[-1]
    A=sim.ion_i['A']
    S0L=sim.ion_i['S0L']
    S0H=sim.ion_i['S0H']
    S1L=sim.ion_i['S1L']
    S1H=sim.ion_i['S1H']
    edb.dump (t_shots[-1],end_shot,edb.Units.mol_per_m3s,True,ions=range(6,8))
    np.set_printoptions (formatter={'float': '{:5.3f}'.format})
    print (f'\nA = {end_shot[A]}')
    print (f'S0L = {end_shot[S0L]}')
    print (f'S0H = {end_shot[S0H]}')
    print (f'S1L = {end_shot[S1L]}')
    print (f'S1H = {end_shot[S1H]}')

    #eplt.plot_worm_times ('S0L', t_shots, cc_shots, 5,0,1)
    #eplt.plot_worm_times ('S2H', t_shots, cc_shots, 5,0,1)

    #eplt.plot_worm (t_shots[-1], end_shot, ['A'])
    #eplt.plot_worm (t_shots[-1], end_shot, ['A','S0L','S0H'])
    #eplt.plot_worm (t_shots[-1], end_shot, ['A','S0L','S0H','S1L','S1H'])
    eplt.plot_worm (t_shots[-1], end_shot, ['A','S0L','S0H','S1L','S1H','S2L','S2H'])
    eplt.plot_ion  (t_shots, cc_shots, ['S1L'],	cells=range(75,84))
    for c in range(65,85,5):
        eplt.plot_ion  (t_shots, cc_shots, ['S1H'], cells=range(c,c+6))
    import pdb; pdb.set_trace()
    eplt.plot_ion  (t_shots, cc_shots, ['S0L[79]','S0L[82]','S0L[84]'])

    for ion in ['S0L','S0H','S1L','S1H']:
        for c in range(0,100,5):
            eplt.plot_ion  (t_shots, cc_shots, ion, cells=range(c,c+5))

run_counting_GRN_direct (n_cells=200, n_peaks_max=3)
