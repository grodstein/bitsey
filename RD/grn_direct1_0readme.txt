The "direct" GRN approach, in its first form, doesn't work well. Snip1.png shows
the problem for 40 cells and an LHL pattern:
	- S0L correctly starts high and falls at the first peak.
	- S0H correctly starts low, rises at the first peak and falls at the
	  final valley.
	- S1L is wrong: while it correct rises at the final valley, it wrongly
	  starts high at cell 0 and then makes up for it by falling at the peak.
	- S1H should be forever low, but actually looks like S0H. However, this
	  turns out to be a consequence of S1L being wrong.

So what went wrong with S1L? First, lots more data:
	- Going up to 200 cells or even 500 makes no difference (experiments
	  not saved).
	- Changing HN from 3 to 10 does make the problem go away with 40 cells.
	- Adding another literal into the "maintain" term (e.g., for S1L):
		from	S1L & (A <= Schm1) & !S0H
		to	S1L & (A <= Schm1) & !S0H & !S0L
	  is helpful (Snip 2). It shows S1L now behaving mostly correctly;
	  it has some extra bumps that eventually die away before ever crossing
	  a neutral zone. Similarly, S1H starts to wrongly go high after the
	  first peak, but S0H rightly kills it. Then Snip 2b shows that S2L is
	  just a blip, and S2H never occurs. So we have a pretty good system!
	- Adding another literal into just the "bump-up" term (e.g., for S1L):
		from	S0H & (A >= Schm1)
		to	S0H & (A >= Schm1) & !S0L
	  helps even more. S1L and S1H are almost completely fixed (snip 3);
	  S2L and S2H are perfect (snip 3b).
	- Adding another literal into *both* the bump-up and maintain terms:
	  has little or no added advantage over putting it just into the bump-up
	  term (snips 4, 4b).

So it seems like the problem is that we're bumping up twice in a single cell
(or a very small number of cells), since
	- turning off "double bumping" helps dramatically.
	- even though S1H is indeed traveling left across the neutral zone,
	  turning that off via the maintain turn doesn't help as dramatically.
	- increasing the number of cells (and thus preventing diffusion across
	  the neutral zone) doesn't help at all
	- increasing HN (which shortens the interval where bump-up can occur)
	  fixes the problem.

Can we support that still more?
	- With 200 cells, here's the [A] profile on the rising slope:
		A=.10  in cell 75, at which point S0L=.92 and S0H=.09
		A=.174 in cell 81, at which point S0L=.75 and S0H=.38.
		A>.30  in cell 87, at which point S0L=.24 and S0H=.87
	- S0L starts out high in cell #0. Then each subsequent cell is slighly
	  later and slightly lower, as expected.
	- S0H starts out low in cell #0. Then it travels as a simple wave --
	  each cell starting later than the left neighbor cell. And each cell
	  cell is slighly higher until the peak in cell ??, then lower and lower
	  until the end. All as expected.
	- S1L starts out rising in cells 76-77. Then it seemingly travels both
	  leftwards and rightwards from that point.
	- S1H also starts in cell 82, and travels both ways. Specifically,
	  it does *not* wait until S1L has traveled all the way left and then
	  start from roughly cell 20.

But how could we possibly bump up twice in the same cell? Let's do the math for
	if IamH:
	    A_SchmX = (cc[self.morph]/self.Schm1)**HN	# > A_Schm1
	    A_SchmX = A_SchmX/(1+A_SchmX)
	else:
	    A_SchmX = (cc[self.morph]/self.Schm0)**HN	# < A_Schm0
	    A_SchmX = 1/(1+A_SchmX)

	A[76]=.110. So (A/.1)^3=1.3=>.43, and (A/.3)^3=.049=>.047
	A[82]=.191. So (A/.1)^3=7.0=>.12, and (A/.3)^3=.26=>.21

	So at cell 76, we should not be able to bump from L->H; and at cell 82
	we should not be able to bump from H->L.
	So something else is going on. But what?

	My hypothesis: the issue is the maintainance term. This is positive
	feedback; if we have a weak bump-up term (such as that for S1L at cell
	#76-77) that results in a low S1H, the S1H-maintain term can regenerate
	S1H up much higher. To test this, we can try modifying the expression
		S_me_fact = (S_me_conc/.3)**HN
		term_maintain =  (S_me_fact/(1+S_me_fact)) * A_SchmX
	to use HN=10 in just that location. The result (snip #8) in fact is
	as effective as adding me-2 feedback into the maintenance term. So this
	is in fact the issue.
