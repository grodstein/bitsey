Counting GRN experiments
      - This is the first set of closed-loop runs where we start with R/D to
	create a pattern of peaks, then use our counting GRN to count the number
	of peaks, then adjust the diffusion constants of A and I to nudge us
	towards the desired number of peaks. We use RD.py run_counting_GRN(),
	which runs *both* setup_counting_GRN() to set up the GRN and also
	setup_werner1() to set up the R/D.
      - If we have too many peaks, then we increase the pattern distance lamda
	(which is sqrt(Kdiff/Kdecay)) and rerun R/D. If we have too few peaks,
	then we decrease lamda -- but we don't just resim (since the too-few-
	peaks pattern would still be stable), and instead do our trick of
	setting the initial [A] linearly and initial [I]=0 before starting R/D.
      - The first experiment sets the Werner1 field length L=40, n_cells=100,
	and n_peaks_max=6. This makes setup_werner1() set D_A according to
	D_A = 1 * p.cell_r * p.GJ_len / (3*delta_L*delta_L), which on L=40
	will initially give us 4 peaks. Here are all of the experiments. The
	results are all in RD_files. A file, e.g., results100_g2_s5.txt would
	have 100 cells and a goal of 2 cells, and started with 5 cells.
      - 100 cells, s4, goal 2, lamda=3.6e-7 old
      - 200 cells, s4, goal 2, lamda 7.2e07 old (Table #1 in the paper)
      - 100 cells, s4, goal 5, lamda=3.6e-7 old
      	Continues past the goal to see interesting stuff.
      - 200 cells, s4, goal 5, lamda 7.2e-7 old (Table #2 in the paper)
	It's extended out to rep #11, even though it converged to 5 peaks at
	rep #3. It then shows some non-monotonic behavior; reps 6-11 became
	Table #3 in the paper.
      - 100 cells, s2, goal 4, lamda=7.2e-7: smooth to 4 peaks. Paper's Table 4
      - 300 cells, s5, goal 3, lamda=7.2e-7: smooth to 3 peaks. Paper's Table 5
      - 400 cells, 6 peaks: start at 7 peaks, jump down to 4 peaks, back up
	to 6. The first few iterations were interesting... we increased lamda,
	but instead of going from 7 down to 6 peaks, it stayed at 7 and the
	amplitude just kept shrinking. Then suddenly in dropped to 4 peaks with
	full amplitude.

Quantization problem with the counting-GRN experiments
      - These experiments worked well but had one problem: the resulting R/D
	patterns were not always "even" -- e.g., a LHLHLH pattern might have
	a different distance between peaks 0 and 1 than between peaks 1 and 2.
	Some examples of this are in results2/x. The debugging below showed that
	this was due to quantization error -- i.e., not enough cells in each
	pattern rep.
      - The function run_werner2() takes some of the weird R/D outputs and tries
	to reproduce them in a simpler environment. I moved worm4 and worm7
	there; both came out a bit weird but not as weird as in the
	run_counting_GRN2() environment. Not sure why. How to debug why: perhaps
	see if we can get to worm4 without going through worms 0-3 (which would
	make the debugging much faster). Or do hybrid sims; 
      - Idea: make sure that the weird worms are in fact stable (i.e.,
	perhaps we just didn't sim long enough). Do this either in run_werner2()
	or in run_counting_GRN2(). In run_werner4(), in fact all of them are
	stable.
      - Idea: is it quantization? take the final result of, say, worm4 and move
	it to its own initialization. Hopefully it will then be stable! But then
	double n_cells without changing L -- if this fixes the weirdness, then
	it's "just" a quantization problem. In fact, this is exactly the issue.
	I moved the issue to simplesim.py run_werner1d(); for each of worms4,
	6 and 7, run_werner1d() first seeds the weird-looking worm, and then
	simulates to see if the initialization stick. With n_sl=100, it sticks
	fine; but with n>sl=150 or 200, it quickly reverts to evenly-distanced
	peaks. I then ran the same experiment in RD.py run_werner4() with the
	same results. Finally, I reran run_counting_GRN2() -- but this time
	with n_cells=200. The problems all vanished! (Results in results2b).
	worm4 200 or 300 cells -> 4 even peaks
      - Idea: is it numerical integration? Try different integration algorithms
	to see if that matters. If so, then that may just mean that the initial
	conditions can indeed lead to various stable end results. I never tried
	this; the earlier experiments showed that with 100 cells, there were
	multiple possible stable outcomes at one D_A. So whether differences in
	trajectory come from different integration algorithms or from other
	sources of randomness seems irrelevant.

Werner3 breadcrumb experiments:
      - These experiments test one of the main tenets of our idea -- that once
	you have digital markers of which subregion is which, you can change
	G_GJ in one region to make that region larger or smaller. The
	experiments were successful :-)
      - LHLHLH, L=40, 100 cells
      - Where are the peaks and valleys?
	L@cell0, H@19.5, L@39.5, H@59.5, L@79.5, H@99
	This is quite uniform! And the pattern is pretty much stable by t=10
	(which isn't surprising since we initialized it with spread(LHLHLH)).
      - Now do D_A, D_I *= 1.5 and resim.
	L@cell0, H@16.4, L@39.55, H@59.55, L@79.5, H@99
	So the main effect is that peak 0 moved to the left (and peak 1 moved
	very slightly to the right).
      - Now, D_A, D_I *= 2.5 instead of 1.5
	L@cell0, H@16, L@42.4, H@61, L@80.4, H@99
	It looks like the cells in [20:40] change more slowly (i.e., lamda is
	larger). This pushes its left and right borders further away from each
	other.
      - Now, D_A, D_I /= 2.5.
	L@cell0, H@21, L@36, H@58, L@78.6, H@99
	Now the cells in [20:40] change faster (i.e., lamda is smaller), which
	pulls in that segment.
      - Now repeat this for an entire cycle (i.e., cells 20:60).
	Scale=2.5 => L@cell0, H@16, L@40, H@64.5, L@82, H@99
	As expected, we expand the entire region from 20-60; the valley at 40
	is unmoved, but H@16 has expanded to the left and H@64.5 to the right.
      - It's quite obvious, looking at the plots, that the rise and fall rates
	are very different when we change lamda. I next tried to quantify this;
	took the ratio of each successive pair of [A] values (which serves as a
	reasonable proxy for the lamda in an exponential). But it didn't work
	that well, since the [A] plots aren't that close to a true exponential.
	Specifically, when [A] was low it was fairly exponential, but when [A]
	was near a peak it wasn't at all exponential. So we'll have to live with
	just looking at the graph visually :-). This was in the function
	analyze_ratios() in RD.py.
      - Plots from these sims are in results_werner3.
