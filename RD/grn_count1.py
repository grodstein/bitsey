#!/usr/bin/env python3

#    import pdb; pdb.set_trace()
import sys; sys.path.append ("..")	# To get the usual Bitsey files

import numpy as np
import sim, eplot as eplt, edebug as edb
import werner_common as wc
#import matplotlib.pyplot as plt

#################################################################
# Functions for our cellular-automaton GRN
# This is the very first version -- quite complicated.
#################################################################

# This is our real closed-loop counting-GRN code.
def run_counting_GRN (n_cells, n_peaks_max, goal_n_peaks):
    p = sim.Params()
    sigs = ['A','I']+counting_GRN_signals(n_peaks_max)
    wc.setup_common (p,.00005,True,100,500,sigs,n_cells)
    setup_counting_GRN (p, n_cells, n_peaks_max)
    p.no_dumps = True		# turn off all dumps
    p.sim_integ_max_delt_cc = .003	# usually .001

    # Pick L=40 since it can support any pattern up to LHLHLH. This refers to
    # the "what sticks" experiments (the paper's Figure 6), where each L has a
    # range of patterns that are stable.
    L = 40 * n_cells/200
    wc.setup_werner1 (p, L, n_cells=n_cells, init_shape="")
    pre_start = sim.ion_i['pre0L']
    A = sim.ion_i['A']
    I = sim.ion_i['I']
    reac_diff_settle_time = 300
    GRN_settle_time = 500 if n_cells<201 else 1500
    
    n_loops=0; n_peaks=0

    while (n_peaks != goal_n_peaks) or (n_loops<=8):
        # On this loop, should we make lamda bigger or smaller?
        if (n_peaks > goal_n_peaks):
            # increase lamda by 10%
            sim.GJ_diffusion[A] *= 1.21
            sim.GJ_diffusion[I] *= 1.21
            print(f"{n_loops}. Increase lamda->D_A={sim.GJ_diffusion[A,0]:.2e}")
        else:	# on the first loop, n_peaks=0 and we take this branch
            # decrease lamda by 10%
            sim.GJ_diffusion[A] /= 1.21
            sim.GJ_diffusion[I] /= 1.21
            wc.linear_spread ()		# [A]=0 at tail, 1 at head; [I]=0 always
            print(f"{n_loops}. Decrease lamda->D_A="\
		   +f"{sim.GJ_diffusion[A,0]:.2e}, re-spread")
        # Make a name for plots later
        name = f"G{goal_n_peaks}-{n_loops}_DA={sim.GJ_diffusion[A,0]:.2e}"

        print ("Simulating reaction/diffusion...")
        set_pre_decay_fast()	# To clear the results of the previous GRN
        wc.set_preseed_LXH()
        t_shots, cc_shots = sim.sim (reac_diff_settle_time)
        fn = f"worm{name}_RD.jpg"
        eplt.plot_worm(t_shots[-1],cc_shots[-1],['A','I'],title=fn,filename=fn)

        #if (n_loops==4) or (n_loops>=6):
        #    import pdb; pdb.set_trace()
        #eplt.plot_worm_times ('A', t_shots, cc_shots, 10,0,1)
        #eplt.plot_ion  (t_shots, cc_shots, ['A','I'],[-1])
        #eplt.plot_worm(t_shots[-1], cc_shots[-1], ['A','sig0L','sig0H'])

        print ("Simulating counting GRN...")
        sim.cc_cells[pre_start,0] = 1	# Seed cell #0 to kick off the GRN
        set_pre_decay_normal()		# To allow GRN to work
        t_shots, cc_shots = sim.sim (GRN_settle_time)
        n_peaks = GRN_N_peaks(pre_start,n_peaks_max)
        print (f"Got {n_peaks:g} peaks (wanted {goal_n_peaks}).\n")
        #import pdb; pdb.set_trace()

        fn = f"worm{name}_{n_peaks:g}peaks.jpg"
        #eplt.plot_worm(t_shots[-1], cc_shots[-1], ['A','sig0L','sig0H','sig1L',
	#	       'sig1H','sig2L','sig2H','sig3L','sig3H','sig4L','sig4H'],
	#		filename=fn)
        #eplt.plot_worm (t_shots[-1], cc_shots[-1], ['A','I'],title=fn,filename=fn)
        #eplt.plot_worm (t_shots[-1], cc_shots[-1], ['A','pre2L','sig2L','pre2H','sig2H'])
        n_loops += 1

# This is the baby-step simple version of the counting GRN; it's not used for
# any production results. It sets up the GRN, then spreads LHLH and sims to see
# the GRN work.
def run_counting_GRN_test (n_cells, n_peaks_max):
    end_time=150
    p = sim.Params()

    # Counting to N needs 2N pre_drivers and 2N inter-cell messengers.
    # The order is pre0L, pre0H, pre1L, pre1H, ..., and then ditto for
    # sig0L, sig0H, ...
    sigs = ['A','I']+counting_GRN_signals(n_peaks_max)
    setup_common (p,.005,True,25,50,sigs,n_cells)

    setup_counting_GRN (p, n_cells, n_peaks_max)
    # Start off the computation chain with pre0L=1 in cell #0 and the morphogen
    # nicely spread.
    pre_start = sim.ion_i['pre0L']
    A = sim.ion_i['A']
    sim.cc_cells[pre_start,0] = 1
    spread ("LHLH", [A])

    t_shots, cc_shots = sim.sim (end_time)
    post_counting_GRN (p, t_shots, cc_shots)

    # We often want a printed dump of the final simulation results.
    np.set_printoptions (formatter={'float': '{:.6g}'.format}, linewidth=90)
    #edb.dump (end_time, sim.cc_cells, edb.Units.mol_per_m3s, True)

# Build the network for our counting GRN.
#	- 'n_peaks_max' tells how high the GRN can count to (i.e., how many
#	  peaks it can handle before saturating its count).
def setup_counting_GRN (p, n_cells, n_peaks_max):
    # Get some offsets of the various signals in cc_cells[].
    pre_start = sim.ion_i['pre0L']
    sig_start = pre_start + 2*n_peaks_max
    amDr = sim.ion_i['amDr']
    A = sim.ion_i['A']

    # Pre* and amDr shouldn't travel between cells.
    sim.GJ_diffusion[pre_start:pre_start+2*n_peaks_max,:] = 0
    sim.GJ_diffusion[amDr,:] = 0

    # For now, the activator A doesn't diffuse either; we seed it to a shape and
    # it will stay that way.
    sim.GJ_diffusion[A,:] = 0

    # The sig* signals travel between cells. Pick a value for GJ diffusion and
    # for their decay constant so that they have a range of about 3 cells.
    sim.GJ_diffusion[sig_start:sig_start+2*n_peaks_max,:] = 5e-14
    sig_decay = 1

    # Instantiate the gate driving amDr. It's just the simple OR of all the
    # pre* variables in any cell.
    amDr_gate (amDr, pre_start, n_peaks_max)

    for pre in range(pre_start,pre_start+2*n_peaks_max):
        # Instantiate the pre-signaling-protein GRN.
        counting_GRN_pre_gate (pre, pre_start, n_peaks_max)

        # Each signaling protein is driven from its corresponding pre*.
        # Params: gtype,out_ion,in_ion,inv,kM,N,kVMax
        # GJ_diff/decay => traveling range
        # kVMax/decay => amplitude
        sig = pre + 2*n_peaks_max	# Indices for this signal pair
        sim.Hill_gate (sim.Gate.GATE_GD, sig, pre, inv=False,
		       kM=.5,N=2,kVMax=sig_decay*1.3)
        sim.GD_const_gate (sim.Gate.GATE_GD, sig, decay=sig_decay)

def post_counting_GRN (GP, t_shots, cc_shots):
    print ("Dumping post-sim...")
    end_shot = cc_shots[-1]
    edb.dump (t_shots[-1],end_shot,edb.Units.mol_per_m3s,True,ions=range(6,15))
    A=sim.ion_i['A']
    pre0L=sim.ion_i['pre0L']
    sig0L=sim.ion_i['sig0L']
    np.set_printoptions (formatter={'float': '{:5.3f}'.format})
    #print (f'\nA = {end_shot[A]}')
    print (f'pre0L = {end_shot[pre0L]}')
    print (f'sig0L = {end_shot[sig0L]}')
    print (f'tau= {end_shot[sig0L,0:49]/ end_shot[sig0L,1:]}')

    #eplt.plot_ion  (t_shots, cc_shots, 'A', title="Plot of [A]")
    #eplt.plot_ion  (t_shots, cc_shots, ['pre0L','sig0L','amDr'], [0,1])
    #eplt.plot_ion  (t_shots, cc_shots, 'sig0L', [0,1], title="Plot of sig0L")
    #eplt.plot_ion  (t_shots, cc_shots, 'amDr', [0,1], title="Plot of amDr")
    #eplt.plot_ion  (t_shots, cc_shots, ['A','sig0L','pre0L','pre0H','amDr'], [13],title="Cell 13")
    #eplt.plot_ion  (t_shots, cc_shots, ['A','sig0L','pre0L','pre0H','amDr'], [14],title="Cell 14")
    #eplt.plot_ion  (t_shots, cc_shots, ['A','sig0L','pre0L','pre0H','amDr'], [15],title="Cell 15")
    #eplt.plot_worm (t_shots[-1], end_shot, ['A'])
    #eplt.plot_worm (t_shots[-1], end_shot, ['A','amDr','sig0L','pre0L','pre0H','sig0H'])
    #eplt.plot_worm (t_shots[-1], end_shot, ['A','amDr','sig0H','pre0H','pre1L'])
    eplt.plot_worm (t_shots[-1], end_shot, ['A','pre0L','pre0H','pre1L','pre1H'])
    #eplt.plot_worm (t_shots[-1], end_shot, ['A','amDr','sig0L','sig0H','sig1L','sig1H'])
    #eplt.plot_worm (t_shots[-1], end_shot, ['sig1L','sig1H'])

def counting_GRN_signals(n_peaks):
    pre = [f'pre{i}{LH}' for i in range(n_peaks) for LH in ['L','H']]
    sig = [f'sig{i}{LH}' for i in range(n_peaks) for LH in ['L','H']]
    return (['amDr'] + pre + sig)

# Normally the decay rate of all pre* signals is 1/s. In "fast" mode, it decays
# 10x faster. This is to break the positive-feedback loop that holds all pre*
# signals at 1.
g_pre_decay=1
def set_pre_decay_normal():
    global g_pre_decay
    g_pre_decay=1
def set_pre_decay_fast():
    global g_pre_decay
    g_pre_decay=10


# This class is for the signaling proteins.
class counting_GRN_pre_gate (sim.Gate):
  # Note that my_pre is 7 for pre0H (since Na,Cl,K,P,A,I,amDr are ahead of it).
  def __init__(self, my_pre, pre_start, n_peaks_max):
    super().__init__ (self.GATE_GD, my_pre)
    self.my_pre,self.pre_start,self.N = my_pre,pre_start,n_peaks_max
    self.morph = sim.ion_i['A']
    self.amDr  = sim.ion_i['amDr']
    self.decay = 1	# Decay rate of my pre signal.
    self.Schm0,self.Schm1 = .1,.3

  # The function that actually computes the gating vector at runtime.
  # The main eqn is 
  #	Pre0L = Pre0L | nothing                | [S0L&(M<Schm1)& !amDr]
  #	Pre0H = Pre0H | [S0L&(M>Schm1)& !amDr] | [S0H&(M>Schm0)& !amDr]
  #	Pre1L = Pre1L | [S0H&(M<Schm0)& !amDr] | [S1L&(M<Schm1)& !amDr]
  #	Pre1H = Pre1H | [S1L&(M>Schm1)& !amDr] | [S1H&(M>Schm0)& !amDr]
  # We implement OR(a,b,c) with just a wired-or
  #	out = A+B+C
  # It would seem that the wired-output could now range in [0,3] rather than
  # [0,1]. However, the !amDr input cuts off the last two terms as soon as
  # they fire.
  # We implement the AND with the product of multiple separate Hill functions.
  # For now:
  #	- they all use the same Hill exponent HN=3
  #	- term 1's self-loop uses Km=.5
  #	- term #2's prev_sig and term #3's my_sig use Km=.3
  #	- term #2 and #3's amDr uses kM=.5
  def func (self, cc, Vm_ignore, deltaV_ignore, t_ignore):
    my_sig = self.my_pre + self.N*2
    prev_sig = my_sig - 1
    IamH = (self.my_pre - self.pre_start) & 1 == 1
    HN = 3	# Hill exponent N
    amDr = (cc[self.amDr]/.5)**HN		# vector [N_CELLS]

    # Term 1: myself -- K*(my_pre/.5)^HN/1+""
    conc = cc[self.my_pre]			# vector [N_CELLS]
    factor = (conc/.5)**HN
    # Pick kVMax so "1" is a stable point. It won't work exactly since term2
    # and term3 will add some generation, but it will be close.
    kVMax = (1+((1/.5)**HN))/((1/.5)**HN)
    decay = conc * g_pre_decay
    term1 = kVMax * factor / (1 + factor)

    # Term 2: prev_sig & (M<Schm0) & !amDr for pre*L
    #			 (M>Schm1)	   for pre*H
    term2 = 0
    if (my_sig > 0):	# pre0L doesn't have a prev_sig term. ##****WRONG***
        PS = (cc[prev_sig]/.3)**HN		# previous sig; vector [N_CELLS]
        if IamH:
            M = (cc[self.morph]/self.Schm1)**HN	# (M>Schm1)
            M = M/(1+M)
        else:
            M = (cc[self.morph]/self.Schm0)**HN	# (M<Schm0)
            M = 1/(1+M)
        term2 =  (PS/(1+PS)) * M * (1/(1+amDr))

    # Term 3: my_sig  & (M<Schm1) & !amDr for pre*L
    #			(M>Schm0)         for pre*H
    MS = (cc[my_sig]/.3)**HN			# my sig; vector [N_CELLS]
    if IamH:
        M = (cc[self.morph]/self.Schm0)**HN	# (M>Schm0)
        M = M/(1+M)
    else:
        M = (cc[self.morph]/self.Schm1)**HN	# (M<Schm1)
        M = 1/(1+M)
    term3 =  (MS/(1+MS)) * M * (1/(1+amDr))
        
    return (term1 + term2 + term3 - decay)

# This class is only for the amDr signal
# Parameters:
#     -	amDr tells which cell's amDr signal I am.
#     -	pre_start and N make it easy to find that cell's pre* signals.
# The cells simulation function does
#	amDr = pre0L + pre0H + ... + all the other pre* signals in this cell
class amDr_gate (sim.Gate):
  def __init__(self, amDr, pre_start, N):
    super().__init__ (self.GATE_GD, amDr)
    self.amDr,self.pre_start,self.N = amDr,pre_start,N
    self.decay = 1

  def func (self, cc, Vm_ignore, deltaV_ignore, t_ignore):
    HN = 3	# Hill exponent N
    sum = sim.cc_cells[self.pre_start:self.pre_start+2*self.N].sum(axis=0)
    frac = (sum/.5) ** HN
    kVMax = (1+((1/.5)**HN))/((1/.5)**HN)	# So "1" is a stable point
    gen = kVMax * frac / (1+frac)
    decay = cc[self.amDr] * self.decay
    return (gen - decay)

# If we have...
#    LH -> cc_cells[-1] has sig0H, which is index 1 => return 1 peak
#    LHLH ->  cells[-1] will have sig1H, which is index 3 => return 2 peaks
# So we want to return (index+1)/2, where index is the index (relative to sig0H)
# of the highest-indexed sig* species in cc_cells[-1].
# In principle, we would like all of our patterns to have L in cc_cells[0] and
# H in cc_cells[-1]. However, that doesn't always work -- so we want an LHL
# pattern to return 1.5 peaks, which will force the algorithm to keep adjusting
# up or down until we get the desired integer number.
#    LHL ->   cells[-1] will have sig1L, which is index 2 => return 1.5 peaks
def GRN_N_peaks(pre_start, n_peaks_max):
    # Grab all 2*N pre* variables in the final (head) cell.
    last_cell = sim.cc_cells[pre_start:pre_start+2*n_peaks_max,-1]
    pre_on_indices = np.flatnonzero (last_cell > .6)
    if (pre_on_indices.size == 0):	# corner case if we've not run the GRN
        return (0)			# yet at all
    highest_pre_on = pre_on_indices.max()

    # Assume it's L on the left and H on the right.
    return ((highest_pre_on+1) / 2)

#run_counting_test (50, 2)
run_counting_GRN(n_cells=400, n_peaks_max=14, goal_n_peaks=6)
#run_counting_GRN(n_cells=300, n_peaks_max=6, goal_n_peaks=3)
