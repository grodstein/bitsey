import numpy as np
import math, operator
import matplotlib.pyplot as plt

#import pdb; pdb.set_trace()

############################################################
# Simulation infrastructure
############################################################

# This holds any relevant technology parameters (just like Bitsey). It also
# holds GP.conc[n_ions,n_slices], which is the equivalent of cc_cells -- as well
# as GP.diff (the diffusion constants) and GP.ion_i.
class Params(object):
    def __init__(self, field_L, n_slices):
        self.field_L = field_L
        self.n_slices = n_slices
        # length of our discretization slice.
        self.slice_L = self.field_L/self.n_slices
        self.slice_sa = 1	# cell surface area; moot since it cancels out
        self.slice_vol = self.slice_sa * self.slice_L

        # Simulation control. Sim_*_dump_interval is multiples of 
        # timestep. We don't do implicit integration, but we do both slow and
	# also adaptive explicit.
        self.time_step = 2e-4		# min time step for explicit integration
        self.sim_dump_interval=10	# e.g., every 10 time steps
        self.sim_long_dump_interval=100
        self.no_dumps = False		# turn off all dumps if you want to
        self.end_time = 400

        # Parameters for adaptive explicit integration. These place a limit on
        # how much any cell's ion concentration, can change in one timestep.
        self.adaptive_timestep = True	# So that these params get used.
        # .001 means that no [ion] can change by more than .1% in one timestep.
        self.sim_integ_max_delt_cc = .001

# Build GP.conc, GP.diff and GP.ion_i.
def init_big_arrays (ion_list):
    n_ions = len(ion_list)
    GP.conc = np.zeros ((n_ions, GP.n_slices))
    GP.diff = np.zeros ((n_ions))

    # Build the map from ion name -> row
    GP.ion_i = {}
    for row, ion in enumerate(ion_list):
        GP.ion_i[ion] = row		# map ion name -> its row

# Return an array [n_ions, n_slices] of (moles/m3) per sec.
# Gets called once per timepoint.
def sim_slopes (custom_func):
    # We will return conc_slew as (moles/m3) per sec. However, we first
    # accumulate most of the fluxes as (moles/m2) per sec, and then multiply
    # by the cell surface area later on.
    conc_slew = np.zeros (GP.conc.shape) # Temporarily in (moles/m2) per sec.

    # flux_from_right[i] = diffusion flux from cell i+1 to cell i, in moles/m2s
    flux_from_right = GP.diff.reshape(-1,1) * (GP.conc[:,1:]-GP.conc[:,:-1])/GP.slice_L
    conc_slew[:,:-1] += flux_from_right
    conc_slew[:,1:]  -= flux_from_right

    # The current conc_slew units are moles/(m2*s), where the m2 is m2 of
    # cell-membrane area. To convert to moles/s entering the cell, we multiply
    # by the cell's surface area. Then, to convert to moles/m3 per s entering
    # the cell, we divide by the cell volume.
    conc_slew *= (GP.slice_sa / GP.slice_vol)

    # Next, do generation, decay and any other custom stuff.
    # These are already natively in moles/(m3*s).
    gen,dec = custom_func()
    conc_slew += gen - dec
    return (conc_slew)	# Moles/m3 per second.

# Top-level simulation function, called just once.
def sim (custom_func):
    sanity_check_diffusion_rate ()

    # Save snapshots of core variables for plotting.
    global t_shots, cc_shots

    # Initialization
    t = 0
    t_shots=[]; cc_shots=[]

    # Save information for plotting at sample points. Early on (when things
    # are changing quickly) save lots of info. Afterwards, save seldom so
    # as to save memory (say 100 points before & 200 after)
    boundary=min (t+50,GP.end_time)	# when to switch to fewer snapshots
    before=boundary/100; after=(GP.end_time-boundary)/200
    last_shot=-100			# to force a snapshot at t=0
    next_dump_time = -100		# to force a dump at t=0

    # Run the simulation loop:
    time_step = GP.time_step		# seconds
    while (t < GP.end_time):
        # Dump out status occasionally during the simulation.
        if (t >= next_dump_time) and (not GP.no_dumps):
            print (f't={t:.6f}, conc={GP.conc}')
            next_dump_time = t + GP.sim_dump_interval

        # Save lots of snapshots for plotting or analysis.
        interval = (before if t<boundary else after)
        if (t > last_shot+interval):
            t_shots.append(t)
            cc_shots.append(GP.conc.copy())
            last_shot = t

        conc_slew = sim_slopes(custom_func)

        if (GP.adaptive_timestep):
            # Timestep control.
            # (moles/m3*sec) / (moles/m3) => fractional_change / sec
            frac_cc = np.absolute(conc_slew)/(GP.conc+.00001)
            max_t_cc = GP.sim_integ_max_delt_cc / (frac_cc.max())
            n_steps = max (1, int (max_t_cc / time_step))
            #print (f'At t={t}: max_t_cc={max_t_cc=} => {n_steps} steps')
        else:
            n_steps = 1

        GP.conc += conc_slew * n_steps * time_step
        t += n_steps * time_step

    return (t_shots, cc_shots)

# Sanity check: if k_d is too big, then explicit integration can oscillate
# wildly. For an ion concentration I in moles/m3, I_next = I - I*k_d*dt
# = I*(1-k_d*dt). This can explode if |1-k_d*dt| > 1. Since k_d is always>0,
# it explodes if k_d*dt > 2.
def sanity_check_decay_rate (dec):
    if (dec * GP.time_step > 2) or (dec<0):
        print ("WARNING: your degradation k_d may be too big for explicit",
		"integration at the current timestep")
        quit()

# Sanity check. Diffusion flux is D*deltaConc/deltaL; the total alteration
# over 1 timestep is then (surf_area/volume)*deltaT*D*deltaConc/deltaL.
# If this is bigger than deltaConc, we can have negative concentrations.
# The factor of 4 is because this can happen on both sides of a slice, and
# build in a 2x safety margin.
def sanity_check_diffusion_rate ():
    if (4*GP.time_step*GP.diff > GP.slice_L*GP.slice_L).any():
        print ("Warning: simulation may have unstably high diffusion rate")

############################################################
# Functions to spread out an initial concentration, and
# to look at a post-sim gradient and determine the shape.
############################################################

# Template for spreading initial ion concentrations. The first one is just
# a simple straight line; the second is taken from a 20-slice LH werner1 sim.
cc_init = np.array ([[0,.5,1], [0,0,0]])	# The first, inaccurate one.
cc_init = np.array (
	[[.0002,.0002,.0003,.0004,.0007,.0011,.0017,.0028,.0045,.0074,
	  .0120,.0196,.0320,.0523,.0856,.1401,.2292,.3723,.5317,.6119],
	[.0841,.0855,.0883,.0926,.0984,.1059,.1151,.1263,.1395,.1551,
	 .1732,.1942,.2185,.2464,.2784,.3150,.3569,.4043,.4482,.4714]])

# Spread like step 4 from werner3 results2.
def init_werner4(GP):
    print (f"Worm4 spread {GP.conc.shape[1]} cells")
    global cc_init
    cc_init = np.array (
     [[1.51361869e-03, 2.14187660e-03, 3.65916565e-03, 6.69527285e-03,
       1.25104079e-02, 2.35182841e-02, 4.42879885e-02, 8.34404688e-02,
       1.57224218e-01, 2.95911454e-01, 5.17333918e-01, 5.91048417e-01,
       5.18749514e-01, 2.98172909e-01, 1.58410453e-01, 8.40519236e-02,
       4.46008780e-02, 2.36743925e-02, 1.25806946e-02, 6.71197387e-03,
       3.63050004e-03, 2.05601956e-03, 1.33382827e-03, 1.16250975e-03,
       1.46811584e-03, 2.37226974e-03, 4.24045872e-03, 7.82967640e-03,
       1.45950775e-02, 2.72799614e-02, 5.10285446e-02, 9.54734768e-02,
       1.78639418e-01, 3.33430468e-01, 5.44739777e-01, 6.09578469e-01,
       5.34391718e-01, 3.13941962e-01, 1.66264353e-01, 8.78943246e-02,
       4.64681086e-02, 2.45695462e-02, 1.29923694e-02, 6.87115025e-03,
       3.63430663e-03, 1.92250791e-03, 1.01714130e-03, 5.38275894e-04,
       2.85033937e-04, 1.51222842e-04, 8.07511729e-05, 4.40877220e-05,
       2.58716782e-05, 1.84692783e-05, 1.87707964e-05, 2.68821684e-05,
       4.61598727e-05, 8.45992553e-05, 1.58150274e-04, 2.97335590e-04,
       5.59916972e-04, 1.05486631e-03, 1.98758911e-03, 3.74516829e-03,
       7.05700434e-03, 1.32975178e-02, 2.50565352e-02, 4.72140672e-02,
       8.89654446e-02, 1.67630105e-01, 3.15044249e-01, 5.21175549e-01,
       5.75950987e-01, 4.83189311e-01, 2.63501093e-01, 1.40153795e-01,
       7.48851765e-02, 4.06932557e-02, 2.33889500e-02, 1.57910129e-02,
       1.47463177e-02, 1.98213199e-02, 3.31221394e-02, 6.01685957e-02,
       1.12184670e-01, 2.10697729e-01, 3.84821219e-01, 3.85971694e-01,
       2.12111782e-01, 1.13150067e-01, 6.10745097e-02, 3.43444421e-02,
       2.18672667e-02, 1.84649766e-02, 2.27256280e-02, 3.64173881e-02,
       6.52223197e-02, 1.21093893e-01, 2.27110428e-01, 4.082205e-01],
      [.14697442, .15104678, .15928334, .17194332, .18934506,
       .21201641, .24049769, .27569703, .31846249, .37008205,
       .4264581 , .44645515, .42649762, .36999937, .3178323 ,
       .27454272, .23875752, .20968645, .18626438, .16812766,
       .15453045, .14536719, .1399782 , .13871516, .14101124,
       .14758444, .15769455, .1727869 , .19190264, .21730326,
       .24738277, .28589865, .33030709, .38628556, .43925766,
       .45822407, .43418758, .37634869, .31805781, .270887  ,
       .22936865, .19571255, .16625081, .14229868, .12160973,
       .10487788, .09055108, .07906626, .06954089, .06217832,
       .05631753, .05213208, .04932946, .04799533, .04787811,
       .0491194 , .05172198, .05580818, .06136926, .0686321 ,
       .07781569, .08918728, .10297209, .11960293, .1395647 ,
       .16342229, .19174588, .22538131, .26525712, .31252327,
       .36824873, .4246008 , .44364586, .42350269, .37133658,
       .32773874, .29317684, .26678233, .24771589, .23551398,
       .2298177 , .23052956, .23756386, .25119781, .27174667,
       .29988693, .33464724, .3343932 , .29861488, .26944487,
       .24764448, .23272933, .22422753, .22197199, .22582346,
       .23592657, .2525571 , .27619559, .30744192, .34463322]])
    spread ("LH", [0,1])

# Spread like step 6 from werner3 results2.
def init_werner6(GP):
    print (f"Worm6 spread {GP.conc.shape[1]} cells")
    global cc_init
    cc_init = np.array (
     [[0.00271722, 0.00384486, 0.00656811, 0.01201712, 0.02245322,
       0.04220738, 0.07947754, 0.14972914, 0.28188685, 0.50238992,
       0.57701691, 0.50245811, 0.28197569, 0.14977675, 0.07950267,
       0.04222044, 0.02245965, 0.01201957, 0.00656761, 0.0038412 ,
       0.00270888, 0.00270075, 0.00381341, 0.00650865, 0.01190496,
       0.02224182, 0.041809  , 0.07872686, 0.14831481, 0.27922861,
       0.49872786, 0.57250547, 0.49628795, 0.27626856, 0.14673257,
       0.07790018, 0.04139509, 0.0220689 , 0.01190127, 0.00667266,
       0.0042132 , 0.00350222, 0.00424464, 0.00674859, 0.0120532 ,
       0.02235987, 0.04194587, 0.07893934, 0.14869126, 0.27994514,
       0.50158624, 0.5791769 , 0.50810595, 0.28872379, 0.15339864,
       0.08141465, 0.04321539, 0.02295044, 0.01220989, 0.00653644,
       0.0035756 , 0.00209863, 0.00149259, 0.00150597, 0.00214433,
       0.00367259, 0.00672497, 0.0125682 , 0.02362722, 0.04449152,
       0.08381971, 0.15793015, 0.29722641, 0.51933841, 0.59432973,
       0.52419522, 0.30533161, 0.16229619, 0.08613413, 0.04571359,
       0.02426412, 0.01288422, 0.00685125, 0.00366155, 0.00199139,
       0.00114765, 0.00078019, 0.0007365 , 0.00099846, 0.00167477,
       0.00304611, 0.00568159, 0.01067492, 0.02009832, 0.03786251,
       0.07133959, 0.13442194, 0.25321784, 0.46870667, 0.59912825],
      [0.16877878, 0.17342337, 0.18294208, 0.19739111, 0.21749392,
       0.24335011, 0.27628747, 0.31641793, 0.3658469 , 0.42086846,
       0.44109873, 0.42085677, 0.36584037, 0.31635076, 0.27623417,
       0.24320619, 0.21739005, 0.19715133, 0.18278001, 0.17305602,
       0.16854678, 0.16823497, 0.17310557, 0.18214866, 0.19696783,
       0.21634279, 0.24279884, 0.27461951, 0.31571584, 0.36343015,
       0.42009923, 0.43848974, 0.42032514, 0.36426624, 0.31762405,
       0.2776182 , 0.2470353 , 0.2218413 , 0.20400677, 0.19075685,
       0.18373512, 0.18090721, 0.18395757, 0.19119658, 0.20469918,
       0.22276924, 0.24827564, 0.2791366 , 0.31955205, 0.36654044,
       0.42293176, 0.44101748, 0.42259935, 0.36490874, 0.3153043 ,
       0.27208546, 0.23825566, 0.20949723, 0.18779042, 0.17022519,
       0.15827489, 0.14990113, 0.1464144 , 0.14626406, 0.15088532,
       0.15890902, 0.17218714, 0.18924208, 0.21269842, 0.24063634,
       0.27694283, 0.31880604, 0.37204516, 0.42704685, 0.44898625,
       0.4275234 , 0.37257105, 0.31794135, 0.27471036, 0.23694557,
       0.20744195, 0.18231393, 0.16332004, 0.14797318, 0.13741689,
       0.13010559, 0.12683895, 0.12672581, 0.13040295, 0.13746089,
       0.14850313, 0.16350938, 0.18315455, 0.2077754 , 0.23821727,
       0.27519257, 0.3198262 , 0.37326425, 0.43593616, 0.47337473]])
    spread ("LH", [0,1])

# Spread like step 7 from werner3 results2.
def init_werner7(GP):
    print (f"Worm7 spread {GP.conc.shape[1]} cells")
    global cc_init
    cc_init = np.array (
     [[.00155378, .00233402, .00428627, .00839087, .01670892,
       .03341735, .06690623, .13399153, .26826108, .51803418,
       .60206975, .51806086, .26828727, .13400469, .06691276,
       .03342052, .01671035, .00839125, .00428581, .00233248,
       .00155041, .00154686, .00232008, .00425832, .00833487,
       .01659675, .03319268, .06645625, .1330903 , .26645994,
       .51514505, .59858415, .51330436, .26475109, .13223433,
       .06603485, .03299431, .01652181, .00834571, .0043604 ,
       .00256465, .00205675, .00258163, .00440287, .00843502,
       .01670279, .03335784, .06676349, .13369393, .26766866,
       .51802923, .60414837, .52264005, .27236345, .13605091,
       .06792997, .03391933, .01694122, .00847013, .00425229,
       .00216974, .00117673, .0007746 , .00076144, .00113064,
       .00206758, .00404275, .00804798, .0160945 , .03222284,
       .06453184, .12924494, .25875879, .50021992, .57339126,
       .47253837, .23936395, .11982712, .06043556, .03139154,
       .01811075, .01392424, .01672976, .02793611, .05317054,
       .10510442, .20978734, .41083156, .41131701, .21037773,
       .10567413, .05400558, .02945574, .01969706, .01982923,
       .02991861, .05503158, .10777848, .21461176, .41871258],
      [.15064726, .15569042, .16594555, .18175597, .20365096,
       .23236349, .26885474, .31434627, .37034778, .43618682,
       .45984554, .43618193, .37033242, .31431652, .26880956,
       .23230137, .20356982, .1816531 , .16581749, .15553291,
       .150455  , .15041385, .15540798, .16560472, .18134522,
       .20315669, .23176896, .2681403 , .31348768, .36931724,
       .4350448 , .45881344, .43536791, .37036062, .31549664,
       .27118387, .23594869, .20861299, .1882601 , .17421052,
       .16599165, .16333122, .16613649, .17450612, .18871411,
       .20924421, .23677303, .27223615, .31680231, .37197565,
       .43718057, .46045973, .43632969, .3693364 , .31164264,
       .26438814, .22596057, .19512342, .1707881 , .15220688,
       .13867508, .12984431, .12528446, .1250174 , .12880648,
       .13707664, .14971374, .16765425, .19082414, .2208858 ,
       .25767978, .30397065, .35927948, .42569782, .44865524,
       .42698132, .36842538, .32295546, .28698129, .26170028,
       .24423844, .23582127, .23443787, .24182066, .25625415,
       .28051625, .31261544, .35592904, .35462826, .31306425,
       .27884202, .25539563, .239404  , .23225421, .2322572,
       .24049953, .25645187, .28123308, .31525961, .35853727]])
    spread ("LH", [0,1])

# Spread [ion] according to the pattern string 'shape'. Roughly:
# - "LH"  would set cc_cells[ion,0:5] to cc_init[ion,0:5]
# - "LHL" would set cc_cells[ion,0:5] to cc_init[ion,[0,2,4,2,0]]
# I.e., we're assuming that the [ion]-vs-cell_number profile in cc_init[] is
# correct, and replicating it as needed to build the desired shape.
# 'Ions' is a list of which ions to spread.
def spread (shape, ions):
    # Note num_cellsI (for cc_init) vs. num_cellsW (for the worm). We can use
    # a 5-cell template to initialize a 30-cell worm!
    num_cellsI = cc_init.shape[1]

    # Indices[w] is the (potentially fractional) cc_init index for worm[w]
    # So worm_cell[w] will take its [ion] from cc_init[indices[w]]
    num_cellsW = GP.conc.shape[1]
    indices = np.empty(num_cellsW)

    # Fill in indices[]. For, e.g., LHLH, we take each of the three segments
    # LH, HL and LH from it, and fill in the corresponding part of indices[].
    for pos in range (len(shape)-1):	# One letter at a time...
        # This single segment of the pattern will go into worm cells[w0,w1].
        w0 = round (pos    *(num_cellsW-1)/(len(shape)-1))
        w1 = round ((pos+1)*(num_cellsW-1)/(len(shape)-1))
        # Are we putting a LH pattern into cells[w0,w1), or a HL?
        seg = shape[pos:pos+2]
        assert ((seg=="LH") or (seg=="HL"))
        i0 = 0 if (seg=="LH") else num_cellsI-1	# cc_init idx for worm cell[w0]
        i1 = num_cellsI-1 if (seg=="LH") else 0	# cc_init idx for worm cell[w1]
        spread = np.linspace (i0, i1, w1-w0+1)	# Spreads this entire segment.
        indices[w0:w1+1] = spread

    # Now set GP.conc accordingly.
    np.set_printoptions (formatter={'float': "{:.3f}".format})
    for ion in ions:
        GP.conc[ion] = np.interp (indices, range(num_cellsI), cc_init[ion])
        print (f"Spread {shape} set conc[{ion}] = {GP.conc[ion]}")

# Return the [A] gradient -- i.e., (max [A]) minus (min [A]).
# Assume somebody has already run a simulation.
def gradient():
    A = GP.ion_i['A']
    return (GP.conc[A].max() - GP.conc[A].min())

# Look at the profile of [A] and deduce where the worm's heads and tails are.
# Return a string such as "LH" (hopefully), or "HLH" (for a two-headed worm).
# The general idea is we walk the cells from left to right; we end a rising
# segment when we see a new [A] that is at least DELTA smaller than the
# largest value on the segment (which is usually but not always the [A] at
# the segment's start). A falling segment is similar.
def shape():
    DELTA=.02	# Ignore any changes in [A] that are less then DELTA.
    shape = ''	# Start with an empty string and add to it as we go.
    mode = 'X'	# Is the current segment falling(F), rising(R) or not set(X)?
    A = GP.ion_i['A']
    aMin = GP.conc[A,0]	# Running min & max [A] on the current segment.
    aMax = GP.conc[A,0]
    n_cells=GP.conc.shape[1]
    for cell in range (1,n_cells):	# Walk from left to right, and
        aNew = GP.conc[A,cell]	# analyze the current cell's [A]
        # If we're walking a rising segment, then we've already outputted the
        # "H" corresponding to the end of the segment that we've not reached
        # yet, but that has to come at some point. (Startup is a special
        # case... see below).

        # print (f'A[{cell}]={aNew}...')
        # Rising segment ends because we see a fall in [A]. This means that the
        # segment's H (which we've already outputted) is the cell immediately
        # to our left. We output a L, for the low cell at the end of the segment
        # we now start (we'll find the actual low cell eventually).
        if (mode=='R') and (aNew < aMax-DELTA):
            mode='F'; shape=shape+'L'
            aMin=aNew; aMax=aNew
            # print ('R->F, output L')

        if (mode=='F') and (aNew > aMin+DELTA):	# Ditto, for a falling segment
            mode='R'; shape=shape+'H'
            aMin=aNew; aMax=aNew
            # print ('F->R, output H')

        # Two special cases for the start of a worm. When we see the initial
        # rise in [A], we output both the L (for cell #0), and also the H (for
        # the eventual high that we haven't seen yet).
        # Note that we don't decide whether this initial segment is rising or
        # falling until we see >DELTA of [A] change... which may take a few
        # cells. So [A]= [.999 .987 1.012  0.999 1.001] will be LH.
        if (mode=='X') and (aNew>aMin+DELTA):
            mode='R'; shape=shape+'LH'
            aMin=aNew; aMax=aNew
            # print ('Initial LH, mode=R')

        if (mode=='X') and (aNew<aMax-DELTA):	# Ditto for an initial
            mode='F'; shape=shape+'HL'		# falling segment.
            aMin=aNew; aMax=aNew
            # print ('Initial HL, mode=F')

        aMin = min (aMin, aNew)	# Update the running min & max [A]
        aMax = max (aMax, aNew)	# along the current segment.

    return (shape)

############################################################
# Plotting
############################################################

def plot_ion (t_shots,cc_shots, ions,cell_idxs=None, title=None, filename=None):
    if (not type(ions) == list):	# If 'ions' is a scalar, turn it into a
        ions = [ions]			# single-element list for consistency.
    n_cells = cc_shots[0].shape[1]
    if cell_idxs==None:
        cell_idxs = np.arange(n_cells)

    for ion in ions:
        ion_idx = GP.ion_i[ion]
        for cell_idx in cell_idxs:
            data = [GP.conc[ion_idx,cell_idx] for cc in cc_shots]
            plt.plot(t_shots, data, linewidth=2.0, label=f'{ion}[{cell_idx}]')
    plot_common ("mol/m3", title, filename)

######################################################
# Plot one quantity (Vmem or a single [ion]) across every cell in a worm
# We only plot one single timepoint -- the x axis is cell number, not time.
# 'ions_to_plot' is a list of text string.
######################################################
def plot_worm (t, conc, ions_to_plot, title=None, filename=None):
    plt.figure()

    for ion in ions_to_plot:
        data = conc[GP.ion_i[ion]]
        plt.plot(data, linewidth=2.0, marker='.', label=ion)

    plt.xlabel('Cell # along the worm')
    plt.ylabel(f"[{','.join(ions_to_plot)}] in moles/m^3")
    plt.legend (loc="upper right")

    plot_it(title, filename)

# Graph one [ion] across the worm. But do it at multiple timepoints to see the
# curve shape evolve.
# 'Ion' is which ion to plot (text); t_shots and cc_shots are the usual summary
# of the simulation. N_views, t_start and t_end work together to pick the
# timepoints; if the sim went to t=100, then
#	(4,0,1) would plot at t=0,25,75,100
#	(3,.3,.5) would plot at t=30,40,50
def plot_worm_times (ion, t_shots, cc_shots, n_views,
			t_start=0, t_end=1, title=None, filename=None):
    plt.figure()

    # A bit of tricky coding here. First, 'times' will be the *requested*
    # timepoints (e.g., [30,40,50]). Then 'indices' will be the indices (into
    # t_shots and cc_shots) of the snapshots closest to those times.
    # We use interp(), with the PWL function given by x values in t_shots and
    # y values just counting 0,1,..., to find the index closest to desired.
    times = np.linspace (t_start, t_end, n_views) * t_shots[-1]
    indices = np.interp (times, t_shots, range(len(t_shots)))
    indices = np.rint(indices).astype(int)	# round to integers

    ion_idx = GP.ion_i[ion]
    for i in indices:
        data = cc_shots[i][ion_idx]
        plt.plot(data, linewidth=1.5, marker='.', label=f't={t_shots[i]:.3f}')

    plt.xlabel('Cell # along the worm')
    plt.ylabel('['+ion+'] (mol/m3)')
    plt.legend (loc="upper right")

    plot_it(title, filename)

def plot_common (ylabel, title=None, filename=None):
    plt.xlabel('Time (s)', fontsize = 20)
    plt.ylabel(ylabel)

    # Now the legend.
    leg = plt.legend(loc='best',ncol=2,mode="expand",shadow=True,fancybox=True)
    leg.get_frame().set_alpha(0.5)

    plot_it(title, filename)

def plot_it(title, filename):
    if (title != None):
        plt.title (title)
    if (filename is None):
        plt.show()
    else:
        plt.savefig(filename)

# Do a dump at some particular time, as follows:
#     -	starts out as a clone of sim_slopes(), computing the diffusion fluxes.
#     -	call custom_dump_func() to get gen & decay rates at the current time.
#     - then print out the above info.
def dump (t, custom_dump_func):
    # Code stolen from sim_slopes().
    # flux_from_right[i] = diffusion flux from cell i+1 to cell i, in moles/m2s
    conc_slew = np.zeros (GP.conc.shape) # Temporarily in (moles/m2) per sec.
    flux_from_right = GP.diff.reshape(-1,1) * (GP.conc[:,1:]-GP.conc[:,:-1])/GP.slice_L
    conc_slew[:,:-1] += flux_from_right		# moles/m2s
    conc_slew[:,1:]  -= flux_from_right
    conc_slew *= (GP.slice_sa / GP.slice_vol)	# moles/m3s

    gen,dec = custom_dump_func()

    # scale everything so that if N_DIGITS=1, then M*10^x10 is in [1,10].
    N_DIGITS=5
    M = max(np.abs(conc_slew).max(), np.abs(flux_from_right).max(),
	    np.abs(gen).max(), np.abs(dec).max())
    x10 = 0 if (M==0) else math.floor (math.log10(1/M)) + N_DIGITS
    tag = '*10^' + str(-x10)
    scale = 10**x10

    fmt = (lambda ar:f'{(ar*scale).astype(int)} {tag}')
    print (f"**t={t} {'*'*50}")
    for ion_name,idx in sorted (GP.ion_i.items(),key=operator.itemgetter(1)):
        print (f'{ion_name} flux through GJs: {fmt(flux_from_right[idx])}')
        print (f'{ion_name} net diff/cell mol/m3s: {fmt(conc_slew[idx])}')
        print (f'{ion_name} gen/cell mol/m3s: {fmt(gen[idx])}')
        print (f'{ion_name} dec/cell mol/m3s: {fmt(dec[idx])}\n')
    print (f"{'*'*55}")

############################################################
# First example: just simple diffusion from a source at one end of the field.
############################################################

class test: pass	# To hold parameters and such for the current test.

def setup_diff1():
    test.decay_rate_A=1				# 1/s
    test.gen_rate_A=2				# moles/(m2*s) flux from left
    test.field_L = 10				# meters
    test.diff = 1				# m2/s

    global GP
    GP = Params(test.field_L, 50)
    GP.sim_dump_interval=2000
    GP.sim_long_dump_interval=5000

    init_big_arrays (['A'])
    A=GP.ion_i['A']
    GP.diff[A]=test.diff			# diffusion constants, in m2/sec

    # Sanity check: if k_d is too big, then explicit integration can oscillate
    # wildly. For an ion concentration I in moles/m3, I_next = I - I*k_d*dt
    # = I*(1-k_d*dt). This can explode if |1-k_d*dt| > 1. Since k_d is always>0,
    # it explodes if k_d*dt > 2.
    if (test.decay_rate_A * GP.time_step > 2) or (test.decay_rate_A<0):
        print ("WARNING: your degradation k_d may be too big for explicit",
		"integration at the current timestep")

    (t_shots,cc_shots) = sim (custom_diff1)
    post_diff1 (t_shots,cc_shots)


# Return the generation & decay arrays (with units of moles/m3s) at the current
# time, using the current GP.conc. It's custom to the diff1 sim family. Its main
# purpose is the main simulation loop, but dump() uses it too.
def custom_diff1():
    A=GP.ion_i['A']

    # Simple decay everywhere. Create 'dec', a vector[n_cells]
    # Units are (moles/m3) * (1/s) = moles/(m3s)
    dec = -GP.conc[A,:] * test.decay_rate_A

    # Generation in cell #0 only
    # Units are (moles/m2s) * m2 / m3 = moles/(m3s)
    gen = np.zeros_like(dec)
    slew[0] = test.gen_rate_A * GP.slice_sa / GP.slice_vol

    return (gen,dec)		# moles/(m3*s), vector[n_cells]

def post_diff1 (t_shots, cc_shots):
    A=GP.ion_i['A']
    conc = cc_shots[-1][A]
    lamda_A = math.sqrt (GP.diff[A] / test.decay_rate_A)
    print (f'lamda_A={lamda_A}, field_L={GP.field_L}')

    # The total amount of 'A'.
    print (f'Total A = {conc}; sum={conc.sum()*GP.slice_L:6.4f} moles/m2')

    # Print every single slice.
    np.set_printoptions (formatter={'float': '{:5.4f}'.format})
    print (f'\nA = {conc}')
    quit()

    # And the graphs.
    plot_ion  (t_shots, cc_shots, 'A')
    plot_worm_ions (t_shots[-1], cc_shots[-1], ['A'])

############################################################
# The Werner sims, from the model in Werner2015
############################################################

# Special variable for werner1c sims. It forces cell[0] low and [-1] high.
g_werner1_peg_ratio = False
g_werner1_peg_ratio = False

# Return the generation & decay arrays (with units of moles/m3s) at the current
# time, using the current GP.conc. It's custom to the Werner1 sims. Its main
# purpose is for the main simulation loop, but dump() uses it too.
def custom_werner1():
    A=GP.ion_i['A']
    I=GP.ion_i['I']

    # The 1e-20 below is to avoid divide by zero.
    gen = np.zeros_like(GP.conc)
    dec = np.zeros_like(GP.conc)
    ratio = (GP.conc[I]/(GP.conc[A]+1e-20))**test.N
    if (g_werner1_peg_ratio):
        ratio[:2]=1000
        ratio[-2:]=0

    gen_func = 1 / (1 + ratio)			# unitless
    gen[A] = gen_func * test.gen_rate_A		# moles/(m3*s)
    gen[I] = gen_func * test.gen_rate_I		# moles/(m3*s)

    # Simple decay everywhere. Create 'slew', a vector[n_cells]
    # Units are (moles/m3) * (1/s) = moles/(m3s)
    dec[A] = GP.conc[A,:] * test.decay_rate_A
    dec[I] = GP.conc[I,:] * test.decay_rate_I

    return (gen, dec)		# moles/(m3*s), vector[n_cells]

# The main setup for all of the werner1 sims.
def setup_werner1 (field_L, n_slices, N, genA, decA, difA, init_shape):
    test.N=N; test.field_L=field_L; test.n_slices=n_slices
    test.gen_rate_A = genA			# moles/(m3*s)
    test.gen_rate_I = 4*genA			# moles/(m3*s)
    test.decay_rate_A = decA			# 1/s
    test.decay_rate_I = 2*decA			# 1/s
    test.D_A = difA				# m2/s
    test.D_I = 30*difA				# m2/s

    global GP
    GP = Params(test.field_L, test.n_slices)
    GP.sim_dump_interval=20
    GP.end_time = 400
    GP.adaptive_timestep = False

    init_big_arrays (['A','I'])
    A=GP.ion_i['A']; I=GP.ion_i['I']
    GP.diff[A]=test.D_A			# diffusion constants, in m2/sec
    GP.diff[I]=test.D_I			# diffusion constants, in m2/sec
    RD_D_A = test.D_A * 5.0e-6 * 15e-9 / (3*(field_L/n_slices)**2)
    print (f"Setting D_A={test.D_A:.2g}; RD version={RD_D_A:.2e}")

    sanity_check_decay_rate (test.decay_rate_A)
    sanity_check_decay_rate (test.decay_rate_I)

    test.sim_name = f'L={field_L}, {n_slices} slices, N={N}, shape_init={init_shape}'

############################################################
## First set of sims with the Werner model. Werner1a is described in 0readme
## as the "Simplesim 'what sticks' experiments".
def run_werner1a():
    for shape in ("LH", "LHL", "LHLH", "LHLHL", "LHLHLH"):
        for L in range (10,55,5):
            n_sl = (50 if L<20 else 100)
            setup_werner1 (field_L=L, n_slices=n_sl, N=10, genA=1,decA=1,difA=1,
			   init_shape=shape)
            A=GP.ion_i['A']; I=GP.ion_i['I']
            spread (shape, [A,I])
            (t_shots,cc_shots) = sim(custom_werner1)
            post_werner1a (t_shots,cc_shots)

def post_werner1a (t_shots, cc_shots):
    A=GP.ion_i['A']; I=GP.ion_i['I']

    sh = shape()
    test.sim_name += f" -> {sh}, grad={gradient():.4f}" 
    print ("Final results:", test.sim_name)
    #dump (f"end of {test.sim_name}", custom_werner1)
    lamda_A = math.sqrt (GP.diff[A] / test.decay_rate_A)
    #print (f'lamda_A={lamda_A}, field_L={GP.field_L}')

    # Print every single slice.
    np.set_printoptions (formatter={'float': '{:5.4f}'.format})
    print (f'\nA = {cc_shots[-1][A]}')
    print (f'I = {cc_shots[-1][I]}')

    # And the graphs.
    #plot_ion  (t_shots, cc_shots, 'A', title=test.sim_name)
    #plot_ion  (t_shots, cc_shots, 'I', title=test.sim_name)
    #plot_worm_ions (t_shots[-1], cc_shots[-1], ('A','I'), test.sim_name)
    #plot_worm_times ('A', t_shots, cc_shots, n_views=10,t_start=0, t_end=1,
    #		     title=test.sim_name)
    return
    plot_worm_times ('A', t_shots, cc_shots, n_views=10,t_start=0, t_end=.1,
    		     title=test.sim_name)

# The second set of Werner1 experiments is Werner1b. It's similar to the
# Werner1a experiments -- but instead of changing L, we keep L constant and
# instead change the degradation rate "dec".
# We want to pick dec so that, with constant L=20, we behave as if L=L_nom.
# So L_nom/lamda_orig = 20 / lamda,
# or lamda/lamda_orig = 20 / L_nom.
# Lamda = sqrt(dif/dec), and we're leaving diffusion alone. So to double lamda,
# we shrink dec by 4x. So in the end, dec = (L_nom/20)^2
# We also scale up generation as we scale up decay, so as to keep amplitudes
# constant.
def run_werner1b():
    for shape in ("LH", "LHL", "LHLH", "LHLHL", "LHLHLH"):
        for L_nom in range (10,55,5):	# Actual L is always 20.
            n_sl = 50 if L_nom<20 else 100
            decA = (L_nom/20)**2
            setup_werner1 (field_L=20, n_slices=n_sl, N=10,
			   genA=decA,decA=decA,difA=1,init_shape=shape)
            A=GP.ion_i['A']; I=GP.ion_i['I']
            spread (shape, [A,I])
            (t_shots,cc_shots) = sim(custom_werner1)
            test.sim_name = f'L={L_nom}, {n_sl} slices, N={10}, shape_init={shape}'
            post_werner1a (t_shots,cc_shots)
            decA *= 1.25

############################################################
## Next set of sims with the Werner model.
## Werner1c starts with a field length that's too short to hold our
## desired pattern, and iteratively increases the decay rates (i.e.,
## decreases lamda) until the desired pattern sticks.
def run_werner1c(goal_shape):
    print ("STARTING: GOAL=", goal_shape)
    test.run_numb=1
    setup_werner1 (field_L=30, n_slices=100, N=10, genA=1,decA=1,difA=1,
		   init_shape=goal_shape)
    A=GP.ion_i['A']; I=GP.ion_i['I']
    spread ('LH', [A]);  GP.conc[I]=0
    #import pdb; pdb.set_trace()
    (t_shots,cc_shots) = sim (custom_werner1)
    post_werner1c (t_shots,cc_shots)

    actual_size = len(shape())	# Results from the initial sim
    while (actual_size != len(goal_shape)):
        if actual_size > len(goal_shape):
            print ("TOO MANY LETTERS")
            # shrink decay rate = increase diffusion rate = fewer peaks
            test.decay_rate_A *= .9
            test.decay_rate_I *= .9
            (t_shots,cc_shots)=sim (custom_werner1)

        if actual_size < len(goal_shape):
            print ("TOO FEW LETTERS")
            # faster decay rate = less diffusion = more peaks
            test.decay_rate_A *= 1.1
            test.decay_rate_I *= 1.1
            sanity_check_decay_rate (test.decay_rate_A)
            sanity_check_decay_rate (test.decay_rate_I)
            spread (goal_shape, [A,I])	# call the re-init (or the I=0?)
            (t_shots,cc_shots)=sim (custom_werner1)
        test.run_numb += 1
        post_werner1c (t_shots,cc_shots)
        actual_size = len(shape())

def post_werner1c (t_shots, cc_shots):
    A=GP.ion_i['A']; I=GP.ion_i['I']
    np.set_printoptions (formatter={'float': '{:5.4f}'.format})
    print (f'\nFinal [A] = {cc_shots[-1][A]}')
    print (f'[I] = {cc_shots[-1][I]}')

    sh = shape()
    sim_name = f"{test.sim_name}->{sh} (run={test.run_numb}, KdA={test.decay_rate_A:.2f})"
    print (f"Final shape: {sim_name}, [A] gradient={gradient()}")
    return
    plot_worm_times ('A', t_shots, cc_shots, n_views=10,t_start=0, t_end=1,
    		     title=sim_name)
    plot_worm_times ('I', t_shots, cc_shots, n_views=10,t_start=0, t_end=1,
    		     title=sim_name)
    plot_worm_ions (t_shots[-1], cc_shots[-1], ('A','I'), sim_name)

# Small set of sims to see if the RD.py counting_GRN results2 worms 4,6,7 (i.e.,
# the weird worms) are only weird because they're at a reasonably low (100
# cells) resolution.
def run_werner1d():
    global GP
    L=40; n_sl=50
    test.sim_name = f'L={L}, {n_sl} slices'

    shape="worm7"
    scale = {"worm4":1.1**10, "worm6":1.1**10, "worm7":1.1**12}
    init  = {"worm4":init_werner4, "worm6":init_werner6, "worm7":init_werner7}

    setup_werner1 (field_L=L, n_slices=n_sl, N=10, genA=1,decA=1,
		   difA=1/scale[shape], init_shape=shape)
    init[shape](GP)
    GP.end_time = 200

    (t_shots,cc_shots) = sim(custom_werner1)
    plot_worm (t_shots[-1], cc_shots[-1], ('A','I'), test.sim_name)
    plot_worm_times ('A', t_shots, cc_shots, n_views=5,t_start=0,t_end=1,
    		     title=test.sim_name)

############################################################

#setup_diff1()
run_werner1d()
#run_werner1c('LH')
