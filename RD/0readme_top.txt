This file describes the many experiments in this directory. Each one then has
multiple files of its own. So, an experiment 'foo' would have foo.py with the
main code, foo_*.py with any auxiliarly code (e.g., plotting or debugging),
foo_0readme.txt with documentation/narrative.

RD: this is the code to implement simple reaction/diffusion (mostly from the
	Werner2015 paper). It also implements simple diffusion and various
	scaling experiments.

grn_count1
	The original (and quite complex) GRN to count. This is the one with
	separate Pre* and Sig* signals and an amDr.

grn_direct1
	This was our first attempt at a simpler GRN. It didn't go well.
