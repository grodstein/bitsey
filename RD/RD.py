#!/usr/bin/env python3

# Current modifications:

############################################################
# This file has reaction/diffusion routines (and a bit of simple diffusion
# code too). This code is used as a base for most everything else.
############################################################

#    import pdb; pdb.set_trace()
import sys; sys.path.append ("..")	# To get the usual Bitsey files

import werner_common as wc
import numpy as np
import re, math
import sim
import eplot as eplt
import edebug as edb
import matplotlib.pyplot as plt

############################################################
# First example: just simple diffusion from a source at one end of the field.
# These are described in 0readme.txt, under
#	"The simple one-ion generation/diffusion experiments"
############################################################

def setup_diff1(p):
    # Parameters for numerical integration and printout.
    p.adaptive_timestep = False	# So that the params above get used.
    p.use_implicit = True	# Use the implicit integrator
    p.sim_dump_interval=2000
    p.sim_long_dump_interval=5000

    n_cells=100
    n_GJs = n_cells-1

    # Create one extra ion 'A'
    sim.init_big_arrays (n_cells, n_GJs, p, ['A']) # sim.GP gets set here
    A=sim.ion_i['A']

    # Straight-line GJ connections along the 1D field. All are simple & ungated.
    sim.GJ_connects['from']  = range(n_GJs)
    sim.GJ_connects['to']    = sim.GJ_connects['from'] + 1

    # Change the gap-junction length from 100nm to 15nm.
    p.GJ_len = 15e-9

    # Set up charge and diffusion rate of A.
    # We only care about diffusion through GJs (not the cell membrane), so we
    # keep the default D[A]=0.
    D_A=1e-12		# diffusion constants, in m2/sec
    GJ_scale=1
    sim.GJ_diffusion[A] = D_A * GJ_scale
    sim.z_array[A] = 0

    # Set up generation and decay of A.
    gen_rate_A=1; decay_rate_A=1
    g = sim.GD_const_gate (sim.Gate.GATE_GD, A, 0, decay_rate_A)
    g.change_GD_const_gate (0, gen_rate_A, decay_rate_A)
    lamda_A = math.sqrt (D_A*GJ_scale / decay_rate_A)
    print (f'lamda_A={lamda_A}, R_cell={p.cell_r}')

def post_diff1 (GP, t_shots, cc_shots):
    A=sim.ion_i['A']
    np.set_printoptions (formatter={'float': '{:5.4f}'.format})
    print ('\nA = {}'.format(cc_shots[-1][A]))

    eplt.plot_ion  (t_shots, cc_shots, 'A')
    #eplt.plot_worm (t_shots[-1], cc_shots[-1], 'A')

############################################################
# Werner1
# Just lots of variations on a simple Werner2015 model.
# We mostly try out different field lengths and see what we get.

# Here are the parameters from simplesim.py:
#	test.decay_rate_A=1				# 1/s
#	test.decay_rate_I=2				# 1/s
#	test.gen_rate_A=1				# moles/(m3*s)
#	test.gen_rate_I=4				# moles/(m3*s)
#	test.D_A=1
#	test.D_I = 30*test.D_A
# We change the first one as follows:
#	- L=10, 50 slices
#	  Original delta_L=10/50=.2, D_A=1, D_I=30
#	  So we create D* = D * r * Lgj / (3*delta_L^2).
#	  For A, this is D_A* = 1 * 5e-6 * 15e-9/ (3*.2*.2) = 6.25e-13.
#	  Then D_I = 30*D_A = 270e-15.
#	- L=20, 100 slices
#	  D_I remains as above, but now use 100 cells.
############################################################

def run_werner1 ():
    end_time=200
    p = sim.Params()

    for L in range (10,55,5):
        #for shape in ("LH","LHL","LHLH","LHLHL","LHLHLH","LHLHLHL","LHLHLHLH","LHLHLHLHL","LHLHLHLHLH"):
        for shape in ("LH",):
            n_sl = (50 if L<20 else 100)
            wc.setup_common (p, .0002, True, 10,100, ['A','I'],n_cells=n_sl)
            wc.setup_werner1 (p, L=L, n_cells=n_sl, init_shape=shape)
            #wc.set_preseed_LXH()
            #wc.linear_spread ()     # [A]=0 at tail to 1 at head; [I]=0 always
            t_shots, cc_shots = sim.sim (end_time)
            post_werner1 (p, t_shots, cc_shots)
    return

    # This little snippet can more easily run a single case than the big loop
    # above, for ease of debug.
    # Parameters for numerical integration and printout.
    n_cells=100
    setup_common (p, .0002, True, 10, 100, ['A','I'],n_cells)
    wc.setup_werner1 (p, L=20, n_cells=n_cells, init_shape="LH")
    t_shots, cc_shots = sim.sim (end_time)
    eplt.plot_worm (t_shots[-1], cc_shots[-1], ['A','I'], filename="LH")

    wc.setup_werner1 (p, L=20, n_cells=n_cells, init_shape="LHLHLH")
    t_shots, cc_shots = sim.sim (end_time)
    eplt.plot_worm (t_shots[-1], cc_shots[-1], ['A','I'], filename="LHLHLH")


    # We often want a printed dump of the final simulation results.
    np.set_printoptions (formatter={'float': '{:.6g}'.format}, linewidth=90)
    edb.dump (end_time, sim.cc_cells, edb.Units.mol_per_m3s, True)

def post_werner1 (GP, t_shots, cc_shots):
    A=sim.ion_i['A']; O=sim.ion_i['I']
    np.set_printoptions (formatter={'float': '{:5.4f}'.format})
    #print ('\nA = {}'.format(cc_shots[-1][A]))
    GP.sim_name += " -> " + shape()

    print ("***" + GP.sim_name + "***")
    #eplt.plot_ion  (t_shots, cc_shots, 'A', title=GP.sim_name)
    #eplt.plot_ion  (t_shots, cc_shots, 'I', title=GP.sim_name)
    eplt.plot_worm (t_shots[-1], cc_shots[-1], ['A','I'], filename=GP.sim_name)

# Used to debug why some R/D patterns are so asymmetrical (see 0readme.txt)
# No other use.
def run_werner2():
    p = sim.Params()

    reac_diff_settle_time = 100
    L=40; n_cells=100
    wc.setup_common (p,.0002, True, 50,500, ['A','I'], n_cells)
    wc.setup_werner1 (p, L=40, n_cells=n_cells, init_shape="")
    A=sim.ion_i['A']; I=sim.ion_i['I']

    wc.linear_spread ()		# [A]=0 at tail to 1 at head; [I]=0 always

    delta_L = L/n_cells
    D_A = 1 * p.cell_r * p.GJ_len / (3*delta_L*delta_L)
    print (f"Original D_A={D_A:.2e}")
    #D_A /= (1.1**10) # worm #4, 6.02e-14
    D_A /= (1.1**12) # worm #7, 4.98e-14
    print (f'D_A={D_A:.2e}')

    sim.GJ_diffusion[A] = D_A
    sim.GJ_diffusion[I] = sim.GJ_diffusion[A]*30
    t_shots, cc_shots = sim.sim (reac_diff_settle_time)
    eplt.plot_worm (t_shots[-1], cc_shots[-1], ['A','I'])

# Can we take a nice symmetrical R/D pattern and then change D_A and D_I between
# two peaks, and then resim to get a different shape?
# These correspond to the section "Werner3 experiments" in 0readme.txt.
def run_werner3():
    p = sim.Params()

    reac_diff_settle_time = 200
    L=40; n_cells=100
    wc.setup_common (p,.0002, True, 50,500, ['A','I'], n_cells)
    wc.setup_werner1 (p, L=40, n_cells=n_cells, init_shape="LHLHLH")

    delta_L = L/n_cells
    D_A = 1 * p.cell_r * p.GJ_len / (3*delta_L*delta_L)
    print (f"Original D_A={D_A:.2e}")

    # Do the original simulation with unaltered GJ conductivities (i.e.,
    # unaltered diffusion constants).
    A=sim.ion_i['A']; I=sim.ion_i['I']
    sim.GJ_diffusion[A] = D_A
    sim.GJ_diffusion[I] = sim.GJ_diffusion[A]*30
    t_shots, cc_shots = sim.sim (reac_diff_settle_time)
    plt.plot (range(n_cells),cc_shots[-1][A],label="A_pre")
    #eplt.plot_worm (t_shots[-1], cc_shots[-1],['A','I'],title="LHLHLH 1st sim")
    #eplt.plot_worm_times ('A', t_shots, cc_shots, 10,0,.2)
    #eplt.plot_worm_times ('I', t_shots, cc_shots, 10,0,.2)
    #analyze_ratios (sim.cc_cells[A,:].copy(), delta_L)

    # Scale GJ conductivities on some selected worm segment, resim and plot.
    scale = 2.5
    sim.GJ_diffusion[A,20:60] /= scale
    sim.GJ_diffusion[I,20:60] /= scale
    t_shots, cc_shots = sim.sim (reac_diff_settle_time)
    plt.plot (range(n_cells),cc_shots[-1][A],label="A_post")
    #eplt.plot_worm (t_shots[-1], cc_shots[-1],['A','I'],title=f"D *= {scale}")
    #analyze_ratios (sim.cc_cells[A,:].copy(), delta_L)
    plt.legend ()
    plt.show()

# Plot the ratios to see if it's an exponential decay.
# Used only by run_werner3(), to analyze our purposely-skewed patterns.
# For y=exp(-x/tau), the ratio of two successive values separated by dx is
# ratio = exp(-x1/tau) / exp(-x2/tau) = exp (dx/tau). Then ln(ratio)=dx/tau
# and tau=dx/ln(ratio).
# The upshot of this plotting: the ratio between successive values is fairly
# constant when [A] is near a trough (whether it's rising or falling), but not
# at all constant when near a peak (again, regardless of whether we're rising to
# the peak or falling after it).
def analyze_ratios (concs, delta_L):
    nonce = 1e-10
    concs /= concs.max()	# Scale to the range [0,1]
    ratios1 = concs[1:] / concs[:-1]
    ratios2 = (1-concs[1:]) / (1-concs[:-1])
    good_ratios = (concs[1:]>.0001) & (concs[:-1]>.0001)
    tau1 = delta_L / np.log(ratios1)
    tau2 = delta_L / np.log(ratios2)
    cells=range(concs.size - 1)
    plt.plot (cells, np.clip(ratios1,-10,10), label="ratios1")
    plt.plot (cells, np.clip(ratios2,-10,10), label="ratios2")
    #plt.plot (cells, tau1, label="tau1")
    #plt.plot (cells, tau2, label="tau2")
    plt.plot (cells, concs[:-1],label="[A]")
    plt.legend ()
    plt.show()

# Another function (along with run_werner2) to debug why weird asymmetric worms
# appeared sometimes.
def run_werner4():
    p = sim.Params()

    reac_diff_settle_time = 200
    L=40; n_cells=200
    wc.setup_common (p,.0002, True, 50,500, ['A','I'], n_cells)
    wc.setup_werner1 (p, L=40, n_cells=n_cells, init_shape="LHLHLH")
    A=sim.ion_i['A']; I=sim.ion_i['I']

    delta_L = L/n_cells
    D_A = 1 * p.cell_r * p.GJ_len / (3*delta_L*delta_L)
    print (f"Original D_A={D_A:.2e}")

    init_werner4(); scale = 1.1**10
    #init_werner6(); scale = 1.1**10
    #init_werner7(); scale = 1.1**12
    D_A /= scale
    sim.GJ_diffusion[A] = D_A
    sim.GJ_diffusion[I] = sim.GJ_diffusion[A]*30
    print (f"New D_A={D_A:.2e}")

    # Run the sim
    sim.GJ_diffusion[A] = D_A
    sim.GJ_diffusion[I] = sim.GJ_diffusion[A]*30
    t_shots, cc_shots = sim.sim (reac_diff_settle_time)
    eplt.plot_worm (t_shots[-1], cc_shots[-1],['A','I'])
    eplt.plot_worm_times ('A', t_shots, cc_shots, 5,0,1)

# Take the final SS results from simplesim.py L=10, 50 slices -> TH; replicate
# each number twice for 100 cells here. This isn't really used much now.
def init_werner1():
    A=sim.ion_i['A']; I=sim.ion_i['I']
    # Temporarily seed A and I with the exact results from simplesim.
    sim.cc_cells[A] = np.array(\
	[.0001,.0001,.0001,.0002,.0002,.0002,.0002,.0003,.0003,.0004,
	.0005,.0006,.0008,.0009,.0011,.0014,.0017,.0020,.0025,.0030,
	.0037,.0045,.0055,.0067,.0082,.0100,.0122,.0150,.0183,.0223,
	.0272,.0332,.0406,.0496,.0605,.0739,.0902,.1101,.1345,.1642,
	.2004,.2445,.2976,.3595,.4257,.4875,.5387,.5773,.6028,.6155]).repeat(2)
    sim.cc_cells[I]=0
    '''sim.cc_cells[I] = np.array(\
	[.0840,.0842,.0847,.0854,.0863,.0874,.0888,.0904,.0922,.0943,
	.0967,.0992,.1021,.1052,.1087,.1123,.1164,.1207,.1253,.1303,
	.1356,.1413,.1474,.1538,.1607,.1679,.1757,.1839,.1926,.2018,
	.2116,.2218,.2328,.2442,.2565,.2693,.2829,.2972,.3123,.3283,
	.3451,.3628,.3814,.4006,.4195,.4367,.4510,.4620,.4694,.4731]).repeat(2)'''
    print ("Spread init_werner1() from simplesim.py results L=10 TH")

# Take the final SS results from simplesim.py L=20, 100 slices -> THTH
# This isn't really used much now.
def init_werner2():
    A=sim.ion_i['A']; I=sim.ion_i['I']
    sim.cc_cells[A] = [.0028,.0029,.0031,.0035,.0040,.0046,.0055,.0065,.0079,.0095,
	.0115,.0140,.0170,.0208,.0253,.0309,.0377,.0460,.0561,.0685,
	.0836,.1021,.1247,.1522,.1858,.2267,.2761,.3342,.3971,.4557,
	.5032,.5369,.5562,.5610,.5514,.5273,.4890,.4373,.3763,.3142,
	.2588,.2123,.1739,.1425,.1167,.0956,.0783,.0641,.0525,.0430,
	.0353,.0289,.0237,.0194,.0160,.0131,.0108,.0089,.0074,.0062,
	.0052,.0044,.0038,.0034,.0031,.0029,.0028,.0028,.0030,.0032,
	.0036,.0042,.0049,.0058,.0070,.0084,.0101,.0123,.0149,.0182,
	.0222,.0270,.0330,.0403,.0491,.0600,.0732,.0894,.1091,.1333,
	.1627,.1986,.2422,.2947,.3551,.4176,.4730,.5161,.5450,.5594]

    sim.cc_cells[I]=[.1740,.1745,.1754,.1768,.1787,.1810,.1838,.1872,.1910,.1954,
	.2001,.2056,.2114,.2180,.2250,.2328,.2409,.2500,.2595,.2700,
	.2808,.2928,.3050,.3187,.3326,.3480,.3636,.3805,.3966,.4118,
	.4234,.4325,.4370,.4387,.4357,.4300,.4198,.4072,.3912,.3750,
	.3582,.3429,.3278,.3142,.3008,.2888,.2770,.2665,.2562,.2470,
	.2381,.2302,.2226,.2158,.2094,.2038,.1985,.1939,.1896,.1861,
	.1828,.1802,.1780,.1763,.1750,.1743,.1740,.1742,.1747,.1759,
	.1774,.1794,.1819,.1849,.1884,.1924,.1969,.2019,.2075,.2136,
	.2203,.2276,.2354,.2439,.2531,.2629,.2735,.2847,.2967,.3096,
	.3232,.3377,.3531,.3692,.3859,.4020,.4160,.4269,.4343,.4380]
    print ("Spread init_werner2() from simplesim.py results L=20 THTH")

# Spread like step 4 from werner3 results2. That was an asymmetric worm; 
# We use this in run_werner4 (along with init_werner6 and init_werner7) to
# debug the asymmetry.
def init_werner4():
    A=sim.ion_i['A']; I=sim.ion_i['I']
    r = sim.cc_cells.shape[1] / 100
    print (f"Worm4 spread {sim.cc_cells.shape[1]} cells -> repeat={r}")
    sim.cc_cells[A] = np.array (
      [1.51361869e-03, 2.14187660e-03, 3.65916565e-03, 6.69527285e-03,
       1.25104079e-02, 2.35182841e-02, 4.42879885e-02, 8.34404688e-02,
       1.57224218e-01, 2.95911454e-01, 5.17333918e-01, 5.91048417e-01,
       5.18749514e-01, 2.98172909e-01, 1.58410453e-01, 8.40519236e-02,
       4.46008780e-02, 2.36743925e-02, 1.25806946e-02, 6.71197387e-03,
       3.63050004e-03, 2.05601956e-03, 1.33382827e-03, 1.16250975e-03,
       1.46811584e-03, 2.37226974e-03, 4.24045872e-03, 7.82967640e-03,
       1.45950775e-02, 2.72799614e-02, 5.10285446e-02, 9.54734768e-02,
       1.78639418e-01, 3.33430468e-01, 5.44739777e-01, 6.09578469e-01,
       5.34391718e-01, 3.13941962e-01, 1.66264353e-01, 8.78943246e-02,
       4.64681086e-02, 2.45695462e-02, 1.29923694e-02, 6.87115025e-03,
       3.63430663e-03, 1.92250791e-03, 1.01714130e-03, 5.38275894e-04,
       2.85033937e-04, 1.51222842e-04, 8.07511729e-05, 4.40877220e-05,
       2.58716782e-05, 1.84692783e-05, 1.87707964e-05, 2.68821684e-05,
       4.61598727e-05, 8.45992553e-05, 1.58150274e-04, 2.97335590e-04,
       5.59916972e-04, 1.05486631e-03, 1.98758911e-03, 3.74516829e-03,
       7.05700434e-03, 1.32975178e-02, 2.50565352e-02, 4.72140672e-02,
       8.89654446e-02, 1.67630105e-01, 3.15044249e-01, 5.21175549e-01,
       5.75950987e-01, 4.83189311e-01, 2.63501093e-01, 1.40153795e-01,
       7.48851765e-02, 4.06932557e-02, 2.33889500e-02, 1.57910129e-02,
       1.47463177e-02, 1.98213199e-02, 3.31221394e-02, 6.01685957e-02,
       1.12184670e-01, 2.10697729e-01, 3.84821219e-01, 3.85971694e-01,
       2.12111782e-01, 1.13150067e-01, 6.10745097e-02, 3.43444421e-02,
       2.18672667e-02, 1.84649766e-02, 2.27256280e-02, 3.64173881e-02,
       6.52223197e-02, 1.21093893e-01, 2.27110428e-01, 4.082205e-01]).repeat(r)
    sim.cc_cells[I]= np.array (
      [.14697442, .15104678, .15928334, .17194332, .18934506,
       .21201641, .24049769, .27569703, .31846249, .37008205,
       .4264581 , .44645515, .42649762, .36999937, .3178323 ,
       .27454272, .23875752, .20968645, .18626438, .16812766,
       .15453045, .14536719, .1399782 , .13871516, .14101124,
       .14758444, .15769455, .1727869 , .19190264, .21730326,
       .24738277, .28589865, .33030709, .38628556, .43925766,
       .45822407, .43418758, .37634869, .31805781, .270887  ,
       .22936865, .19571255, .16625081, .14229868, .12160973,
       .10487788, .09055108, .07906626, .06954089, .06217832,
       .05631753, .05213208, .04932946, .04799533, .04787811,
       .0491194 , .05172198, .05580818, .06136926, .0686321 ,
       .07781569, .08918728, .10297209, .11960293, .1395647 ,
       .16342229, .19174588, .22538131, .26525712, .31252327,
       .36824873, .4246008 , .44364586, .42350269, .37133658,
       .32773874, .29317684, .26678233, .24771589, .23551398,
       .2298177 , .23052956, .23756386, .25119781, .27174667,
       .29988693, .33464724, .3343932 , .29861488, .26944487,
       .24764448, .23272933, .22422753, .22197199, .22582346,
       .23592657, .2525571 , .27619559, .30744192, .34463322]).repeat(r)

# Spread like step 6 from werner3 results2. See the note on init_werner4 for
# what we use this for.
def init_werner6():
    A=sim.ion_i['A']; I=sim.ion_i['I']
    r = sim.cc_cells.shape[1] / 100
    print (f"Worm6 spread {sim.cc_cells.shape[1]} cells -> repeat={r}")
    sim.cc_cells[A] = np.array (
      [0.00271722, 0.00384486, 0.00656811, 0.01201712, 0.02245322,
       0.04220738, 0.07947754, 0.14972914, 0.28188685, 0.50238992,
       0.57701691, 0.50245811, 0.28197569, 0.14977675, 0.07950267,
       0.04222044, 0.02245965, 0.01201957, 0.00656761, 0.0038412 ,
       0.00270888, 0.00270075, 0.00381341, 0.00650865, 0.01190496,
       0.02224182, 0.041809  , 0.07872686, 0.14831481, 0.27922861,
       0.49872786, 0.57250547, 0.49628795, 0.27626856, 0.14673257,
       0.07790018, 0.04139509, 0.0220689 , 0.01190127, 0.00667266,
       0.0042132 , 0.00350222, 0.00424464, 0.00674859, 0.0120532 ,
       0.02235987, 0.04194587, 0.07893934, 0.14869126, 0.27994514,
       0.50158624, 0.5791769 , 0.50810595, 0.28872379, 0.15339864,
       0.08141465, 0.04321539, 0.02295044, 0.01220989, 0.00653644,
       0.0035756 , 0.00209863, 0.00149259, 0.00150597, 0.00214433,
       0.00367259, 0.00672497, 0.0125682 , 0.02362722, 0.04449152,
       0.08381971, 0.15793015, 0.29722641, 0.51933841, 0.59432973,
       0.52419522, 0.30533161, 0.16229619, 0.08613413, 0.04571359,
       0.02426412, 0.01288422, 0.00685125, 0.00366155, 0.00199139,
       0.00114765, 0.00078019, 0.0007365 , 0.00099846, 0.00167477,
       0.00304611, 0.00568159, 0.01067492, 0.02009832, 0.03786251,
       0.07133959, 0.13442194, 0.25321784, 0.46870667, 0.59912825]).repeat(r)
    sim.cc_cells[I]= np.array (
      [0.16877878, 0.17342337, 0.18294208, 0.19739111, 0.21749392,
       0.24335011, 0.27628747, 0.31641793, 0.3658469 , 0.42086846,
       0.44109873, 0.42085677, 0.36584037, 0.31635076, 0.27623417,
       0.24320619, 0.21739005, 0.19715133, 0.18278001, 0.17305602,
       0.16854678, 0.16823497, 0.17310557, 0.18214866, 0.19696783,
       0.21634279, 0.24279884, 0.27461951, 0.31571584, 0.36343015,
       0.42009923, 0.43848974, 0.42032514, 0.36426624, 0.31762405,
       0.2776182 , 0.2470353 , 0.2218413 , 0.20400677, 0.19075685,
       0.18373512, 0.18090721, 0.18395757, 0.19119658, 0.20469918,
       0.22276924, 0.24827564, 0.2791366 , 0.31955205, 0.36654044,
       0.42293176, 0.44101748, 0.42259935, 0.36490874, 0.3153043 ,
       0.27208546, 0.23825566, 0.20949723, 0.18779042, 0.17022519,
       0.15827489, 0.14990113, 0.1464144 , 0.14626406, 0.15088532,
       0.15890902, 0.17218714, 0.18924208, 0.21269842, 0.24063634,
       0.27694283, 0.31880604, 0.37204516, 0.42704685, 0.44898625,
       0.4275234 , 0.37257105, 0.31794135, 0.27471036, 0.23694557,
       0.20744195, 0.18231393, 0.16332004, 0.14797318, 0.13741689,
       0.13010559, 0.12683895, 0.12672581, 0.13040295, 0.13746089,
       0.14850313, 0.16350938, 0.18315455, 0.2077754 , 0.23821727,
       0.27519257, 0.3198262 , 0.37326425, 0.43593616, 0.47337473]).repeat(r)

# Spread like step 7 from werner3 results2. See the note on init_werner4 for
# what we use this for.
def init_werner7():
    A=sim.ion_i['A']; I=sim.ion_i['I']
    r = sim.cc_cells.shape[1] / 100
    print (f"Worm7 spread {sim.cc_cells.shape[1]} cells -> repeat={r}")
    sim.cc_cells[A] = np.array (
      [.00155378, .00233402, .00428627, .00839087, .01670892,
       .03341735, .06690623, .13399153, .26826108, .51803418,
       .60206975, .51806086, .26828727, .13400469, .06691276,
       .03342052, .01671035, .00839125, .00428581, .00233248,
       .00155041, .00154686, .00232008, .00425832, .00833487,
       .01659675, .03319268, .06645625, .1330903 , .26645994,
       .51514505, .59858415, .51330436, .26475109, .13223433,
       .06603485, .03299431, .01652181, .00834571, .0043604 ,
       .00256465, .00205675, .00258163, .00440287, .00843502,
       .01670279, .03335784, .06676349, .13369393, .26766866,
       .51802923, .60414837, .52264005, .27236345, .13605091,
       .06792997, .03391933, .01694122, .00847013, .00425229,
       .00216974, .00117673, .0007746 , .00076144, .00113064,
       .00206758, .00404275, .00804798, .0160945 , .03222284,
       .06453184, .12924494, .25875879, .50021992, .57339126,
       .47253837, .23936395, .11982712, .06043556, .03139154,
       .01811075, .01392424, .01672976, .02793611, .05317054,
       .10510442, .20978734, .41083156, .41131701, .21037773,
       .10567413, .05400558, .02945574, .01969706, .01982923,
       .02991861, .05503158, .10777848, .21461176, .41871258]).repeat(r)
    sim.cc_cells[I]= np.array (
      [.15064726, .15569042, .16594555, .18175597, .20365096,
       .23236349, .26885474, .31434627, .37034778, .43618682,
       .45984554, .43618193, .37033242, .31431652, .26880956,
       .23230137, .20356982, .1816531 , .16581749, .15553291,
       .150455  , .15041385, .15540798, .16560472, .18134522,
       .20315669, .23176896, .2681403 , .31348768, .36931724,
       .4350448 , .45881344, .43536791, .37036062, .31549664,
       .27118387, .23594869, .20861299, .1882601 , .17421052,
       .16599165, .16333122, .16613649, .17450612, .18871411,
       .20924421, .23677303, .27223615, .31680231, .37197565,
       .43718057, .46045973, .43632969, .3693364 , .31164264,
       .26438814, .22596057, .19512342, .1707881 , .15220688,
       .13867508, .12984431, .12528446, .1250174 , .12880648,
       .13707664, .14971374, .16765425, .19082414, .2208858 ,
       .25767978, .30397065, .35927948, .42569782, .44865524,
       .42698132, .36842538, .32295546, .28698129, .26170028,
       .24423844, .23582127, .23443787, .24182066, .25625415,
       .28051625, .31261544, .35592904, .35462826, .31306425,
       .27884202, .25539563, .239404  , .23225421, .2322572,
       .24049953, .25645187, .28123308, .31525961, .35853727]).repeat(r)

#run_werner1()
#run_werner3()
#run_werner4()
