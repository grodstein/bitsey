import matplotlib.pyplot as plt
import numpy as np
import re
import math
#    import pdb; pdb.set_trace()

GJ_len = 15e-9
cell_r = 5e-6

# Finish up a plot with a few matplotlib calls.
def finish (title):
    plt.title (title)
    plt.legend (loc="upper right")
    plt.ylabel ("[A]")
    plt.show()

def lamda_star (D, dec):
    DD = D * 12 * cell_r / GJ_len
    lamda = math.sqrt(DD/dec)
    return (lamda)

# Parse a single line, of the form
#	[.4,.3,.15,...,.001] # comment
# Update a dictionary with the mapping comment->array-of-numbers.
def handle_line(data_dict, line):
    #print (f'*{line}*')
    #match = re.match ("\[[^]+]\] +#.*", line)
    match = re.match ("([^#]+)#(.*)", line)
    before = match.group(1).strip(' []	')	# Before the '#': the numbers
    after = match.group(2).strip()		# after the '#': so, the comment
    numbers=[float(s) for s in before.split()]	# from comma-sep string to list
    data_dict[after] = np.array (numbers)	# from list to Numpy array
    #print (f'numbers=*{numbers}*, after=*{after}*')

# Comments have the form cells=10, D=1e-12, gen=1, dec=1, ...
# Return a dictionary of cell->10, D->1e-12, etc.
def parse_comment (comment):
    dict={}
    comment = re.sub(" ","", comment)
    pairs = re.split(',', comment)	# List of ('cells=10', 'D=1e-12',...)
    for pair in pairs:
        match = re.match ("([^=]+)=(.*)", pair)
        dict[match.group(1)] = float(match.group(2))
    return (dict)

# Open and read a data file. Assemble multiple lines into one effective line
# and call handle_line() to work on each one.
# Return a dictionary of comment->Numpy array with an entry for each effective
# line.
def read_file(fname):
    file = open (fname)
    data_dict = {}	# dictionary mapping comment => data array
    merged=""
    for line in file:
        line = line.strip()
        if (line=="" or line[0]=='#'):	# Remove comment lines and blanks.
            continue
        merged += ' ' + line		# Keep building one effective line...
        if (re.search ('\]', line)):	# until we get a ']' for end of array
            handle_line (data_dict, merged)	# Call handle_line() on it.
            merged = ""				# and start the new eff. line.
    return (data_dict)

def main(fname):
    dict1 = read_file (fname)

    # Print sums to check on total [A]
    for name,data in dict1.items():
        params = parse_comment (name)
        lamda = lamda_star (params['D'], params['dec'])
        L = data.size*2*cell_r
        print (f'{params}->lamda={lamda:5.2g}, L={L}={L/lamda:4.1f} lamda, mean={data.mean():5.3g} moles/m3')

    # Just plot them all.
    if (0):
      for name,data in dict1.items():
        L = (params['cells']-1) * 2*cell_r
        lamda = lamda_star (params['D'], params['dec'])
        plt.plot (data, label=f'{name},L={L:.2g},lamda={lamda:.2g}')
      finish ("[A] vs. cell")

    # Next, also make them all start at 1.0
    if (0):
      for name,data in dict1.items():
        plt.plot (data/data[0], label=name)
      finish ("Normalized to all start at 1")

    # Next, squish them all to an x axis of 0-1
    if (0):
      for name,data in dict1.items():
        plt.plot (np.linspace(0,1,num=data.size), data, label=name)
      finish ("Normalized to all have the same # of cells"  )

    # Next, the ratios to see if it's an exponential decay.
    # For y=exp(-x/tau), the ratio of two successive values separated by dx is
    # ratio = exp(-x1/tau) / exp(-x2/tau) = exp (dx/tau). Then ln(ratio)=dx/tau
    # and tau=dx/ln(ratio).
    if (1):
      slice_L = 2 * cell_r			# twice the cell radius
      for name,data in dict1.items():
        # assume lamda is the same for all plots
        params = parse_comment (name)
        lamda = lamda_star (params['D'],params['dec'])

        # Remove all of the really small numbers; they're inaccurate.
        zeros = (np.nonzero (data<0.001))[0]	# Index of first small number
        if (zeros.size > 0):
            data = data[:zeros[0]]		# Now data[] has only good #s
        ratios = data[:-1] / data[1:]		# Compute ratios
        taus = slice_L / np.log (ratios)
        plt.plot (taus, label=name)
      plt.plot ([0,taus.size], [lamda,lamda], label=f'lamda={lamda:5.2g}')
      finish ("Cell-by-cell lamda vs. cell")

main("x.txt")
