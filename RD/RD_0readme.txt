RD.py:
	Another file much like worm.py. It contains several experiments.
	* diff1. A long straight worm connected by GJs. Only one extra ion 'A',
	  which is uncharged. A GD_const_gate that generates A in cell #0, and
	  does simple linear decay of A everywhere.
	* werner1. The simplest Werner2015 model, with just an activator and
	  inhibitor but no enhancer/shrinker. It uses two extra ions A and I,
	  still both neutral.
	* counting_GRN. My GRN that counts the number of peaks in a reaction/
	  diffusion pattern.

simplesim.py
	This is a simple-physics simulator. It's fully self contained in one
	file, and is not based on BITSEY at all. It has no GJs; instead, it's
	just a right-cylindrical field of length L, divided into a number of
	discrete cylindrical slices of length slice_L.
	It intrinsically models diffusion and nothing else; no ion channels,
	GJs or voltage. The ions Na, K and Cl are not modeled. Each individual
	experiment adds whatever ions it likes, and gets its own "custom
	function" that typically adds generation and decay.
	* diff1: only one ion A; generated with constant flux in moles/m2s at
	  the leftmost edge. Simple linear decay everywhere.
	* werner1: two ions A and I. Simple linear decay everywhere. Generation
	  is the simple Werner2015 model.

The simple one-ion generation/diffusion experiments:
    These mostly just prove that my understanding of how generation, decay and
    diffusion all work together in scaling. The math is given in Google Drive
    research_Levin / current research / my scaling math.
    These results are from simplesim.py, using diff1(). They test a simple
    system with generation from a source at one end, combined with linear decay
    and constant diffusion.
    The comparisons of different result waveforms are done by plotting with
    RD_plot_SS.py, which takes data from RD_SS_results.txt.
	1. L=10, D=1, gen=1, dec=1, slices=100
	2. L=5,  D=1, gen=1, dec=1, slices=50
	3. L=10, D=1, gen=1, dec=1, slices=50
	4. L=20, D=1, gen=1, dec=1, slices=50
	5. L=10, D=2, gen=2, dec=2, slices=50
	6. L=20, D=4, gen=1, dec=1, slices=50
	7. L=10, D=1, gen=2, dec=1, slices=50

	1 vs. 3 shows that nothing much changes when we go to a finer grid
		interval.
	2,3,4 show the usual scaling; as the field length increases, the total
		amount of A is unchanged, and each cell has a lower
		concentration.
	3 vs. 5 shows no change from doubling gen, dec, diff
	3 vs. 6 shows if we double both lamda (by quadrupling D) and L -> plot
		of normalized [A] vs. slice # is unchanged.
	3 vs. 7 shows that doubling gen doubles everything.

    These next sims are the same setup as above, but now using RD.py rather than
    simplesim.py. They show that I can indeed transfer a sim from simplesim.py
    to RD.py without changing any results.
    And this time, the results are in RD_results.txt plotted with RD_plot.py.

	1. cells=5, D=1e-12, gen=1, dec=1
	2. cells=10, D=1e-12, gen=1, dec=1
	3. cells=20, D=1e-12, gen=1, dec=1
	4. cells=50, D=1e-12, gen=1, dec=1
	5. cells=100, D=1e-12, gen=1, dec=1
	6. cells=100, D=1e-12, gen=1, dec=2
	7. cells=100, D=1e-12, gen=1, dec=5
	8. cells=100, D=1e-12, gen=2, dec=1
	9. cells=100, D=2e-12, gen=2, dec=2
	1-5 show the general pattern of increasing cells, and that they all
		have the same total [A]. Also, that #4 and #5 are straight
		exponential decay (but not #1, #2 and #3).
	5-7 show that increasing the decay rate reduces total [A] accordingly,
		and also reduces the exponential tau.
	5 vs. 8 shows that doubling G will double the concentration in every
		cell.
	5 vs. 9 shows that doubling diffusion, gen and decay all cancel out and
		give no change at all.
	
The Werner2015 model in simplesim.py:
    These results use simplesim.py and werner1(). The different experiments
    vary the field length L and the number of slices.
    They all initialize the sim with [I]=0, and spread [A]=LH. Note that they
    spread with the simple straight line cc_init = np.array ([[0,.5,1]); if we
    instead spread with the more official pattern, then spreading LH would stay
    stable.
    The results summary is that, with this initialization setup, the result
    *looks like* it's always be as many repetitions of our basic pattern as can
    fit. So even though we will see later that a simple LH pattern *can* be
    stable on a very long field, at first glance that doesn't happen here -- a
    long field always gets something long, like LHLHLH (but caution, though
    -- it turns out later that this is not quite true, and that the results of
    this initialization is just "usually almost as many patterns as can fit").
	# L=10, slice=50 -> LH (stable by t=5)
	# L=20, slice=100 -> LHLH (forms by t=10, stable at t=200)
	# L=25, slice=100 -> LHLH (ditto)
	# L=27, slice=100 -> LHLH (ditto)
	# L=30, slice=100 -> LHLHLH (forms by t=15, stable at t=290)
	# L=30, slice=150 -> ditto
	# L=40, slice=100 -> LHLHLH (forms by t=15, stable at t=350)
	# L=40, slice=200 -> LHLHLH (forms by t=15, stable at t=400)
	# L=50, slice=100 -> LHLHLHLH (forms by t=10, stable at t=180)
    And here's very similar data from RD.py in run_werner1()
	L=10, 50 cells -> LH
	L=15, 50 cells -> LHL
	L=20, 100 cells -> LHLH
	L=25, 100 cells -> LHLH
	L=30, 100 cells -> LHLHLH
	L=35, 100 cells -> LHLHLH
	L=40, 100 cells -> LHLHLH
	L=45, 100 cells -> LHLHLHLH
	L=50, 100 cells -> LHLHLHLH

The Werner2015 model in RD.py
      - End result: the RD.py model is now giving *exactly* the same
	results as simplesim for these cases. All cases produce seemingly the
	same graph shape as simplesim, and for one case I diffed a good sampling
	of the concentrations at various times (both during the trajectory and
	at the end) and they are identical to four digits. So once again, we've
	shown that we can move results from simplesim.py to RD.py.
      -	All of these have GP.time_step=.0002, fixed integration,
	initialized with spread("LH") and cc_init = np.array ([0,.5,1]) to
	spread[A], with initial [I]=0.
      -	Start by converting the "L=10, slice=50 -> LH" case.
	Original delta_L=10/50=.2, D_A=1, D_I=30
	So we create D* = D * r * Lgj / (3*delta_L^2).
	For A, this is D_A* = 1 * 5e-6 * 15e-9/ (3*.2*.2) = 6.25e-13.
	Then D_I = 30*D_A = 270e-15.
      -	L=30, slice=100 -> LHLHLH. Now delta_L=.3, so 
	D_A* = D * r * Lgj / (3*delta_L^2)
	     = 1 * 5e-6 * 15e-9/ (3*.3*.2) = 2.77e-13.
	RD.py also gives LHLHLH
      -	L=40, slice=100-> LHLHLH so delta_L has gone from .2->.4.
	Then D_A* = 1 * 5e-6 * 15e-9/ (3*.4*.4) = 1.5625e-13
	RD.py also gives LHLHLH
      -	L=40, slice=200 -> LHLHLH. Now delta_L=.2 again, so D_A=6.25e-13.
	RD.py also gives LHLHLH
      -	L=50, slice=100 -> LHLHLHLH. Now delta_L=.5, so
	D_A* = D * r * Lgj / (3*delta_L^2)
	     = 1 * 5e-6 * 15e-9/ (3*.5*.2) = 1e-13.

Simplesim "what-sticks" experiments (Werner-1a)
      - These experiments try seeding different patterns (e.g., LH, LHL, LHLH,
	etc.) on fields of various lengths. The idea is to see which patterns
	are stable (i.e., "stick") on which field lengths. The main result will
	be that we do indeed (as the papers say) have an intrinsic pattern
	length given by lamda=sqrt(diff/decay). When we try to make a pattern
	of length < lamda, it disintegrates. However, patterns seeded at a
	length *longer* than lamda are stable.
      - The initialization details: previously (The Werner2015 model in
	simplesim.py) we just initialized with a simple	straight line, which
	resulted in patterns being quite unstable. This time, I enhanced
	simplesim spread() to use the results from a 20-point LH simplesim
	simulation, for both A and I. With this, initializations for L=10 LH
	stay pretty steady (as expected). Here are more results:
		10	15	20	25	30	35	40    45  50
	LH	.62	.63	.63	.63	.63	.63	.63   .63 .62
	LHL	.46	.58	.62	.63	.63	.63	.63   .63 .64
	LHLH	xx	.46	.56	.60	.62	.63	.63   .63 .63
	LHLHL	xx	xx	.46	.54	.58	.61	.61   .63 .63
	LHLHLH	xx	xx	xx	.45	.52	.57	.59   .61 .62
	Each table entry can be "xx" (indicating that the initialization was not
	stable, and the sim morphed the initial pattern into some other shape),
	or a number indicating (max [A] over the field) minus (min value).
      - Note that LH and LHL have roughly the same stability range (they're both
	stable at all values we tested). LHLH and LHLHL have mostly the same
	stability range. This will mean that when we run Werner-1c experiment
	#2, we won't be able to transform LHL to LH by changing the intrinsic
	pattern length lamda.
      - The plots referenced below come from RD_plot_werner1a.py. Whereas the
	plotting programs noted above keep the data in a separate file, this
	time time the data is also in RD_plot_werner1a.py (stuck into the
	variable data1a).
      - The peaks are always at places that divide the field evenly. So LHL
	always has its peak at L/2; LHLHL always at .25L and .75L. LHLH are at
	.33 and 1; LHLHLH are at .2, .6, 1.
      - When a shape has multiple peaks, they're always all the same height
	(e.g., the two peaks of LHLHL are always the same size as each other).
	However, at small L, LHLHL has smaller peaks than LHL does at the same
	L. As L gets big enough, that difference nearly vanishes.
      - The LH on every length from 10-50 all lie on top of each other when we
	line up their right sides. For LHLH and LHLHLH, when we line up the
	rightmost peaks and scale each graph to the same max height, the
	rightmost peaks also line up similarly.
      - LHL from different lengths also all line up on top of each other when
	we scale them to the same height and align L/2 on the x axis.

RD.py "what-sticks" experiments in run_werner1()
      - These experiments mostly replicate the simplesim.py ones just above,
	but go beyond them.
      - First, just replicate the experiments. Use run_werner1() in RD.py; it
	all matches perfectly. We also got more detail on what is stable,
	running patterns up to LHLHLHLHLH. Results: LH is stable everywhere;
	ditto for LHL; LHLH in [15,50];	LHLHL in [20,50]; LHLHLH in [25,50];
	LHLHLHL in [30,50]; LHLHLHLH in [35,50]; LHLHLHLHL in [40,50];
	LHLHLHLHLH in [45,50]. This is an exact match of the simplesim.py
	numbers above.
      - Now for the fun part -- let's finally answer the question: does
	set_linear_spread() truly give us the maximum number of peaks? For each
	field length L, we'll run set_linear_spread() with set_preseed_LXH()
	to see what shape we get; then compare that with our what-sticks results
	to see if any larger patter could stick.
		L=10, 50 cells: set_linear_spread: LH, max is LH
		L=15, 50 cells: set_linear_spread: LHL, max is LHLH
		L=20, 100 cells: set_linear_spread: LHLH, max is LHLH
		L=25, 100 cells: set_linear_spread: LHLH, max is LHLHLH
		L=30, 100 cells: set_linear_spread: LHLHLH, max is LHLHLH
		L=35, 100 cells: set_linear_spread: LHLHLH, max is LHLHLHLH
		L=40, 100 cells: set_linear_spread: LHLHLH, max is LHLHLHLH
		L=45, 100 cells: set_linear_spread: LHLHLHLH, max is LHLHLHLHLH
		L=50, 100 cells: set_linear_spread: LHLHLHLH, max is LHLHLHLHLH
      - So set_linear_spread() with set_preseed_LXH() is good, but not perfect.
	It worked perfectly for L=10, 20, 30, producing the maximal pattern.
	But for L=15 it surprisingly produced the illegal pattern LHL (and in
	fact LHLH) would have been stable.
	And for L=25, 35, 40, 45 and 50, it came up one pattern rep too short.

Werner-1b in simplesim.py:
      - Like 1a; but instead of increasing the field size L, we increase the
	decay rates kDA and kDI and also increase gen equivalently. This
	validates that our concept of max_pattern_repetition = field_L / lamda
	works, by showing that changing lamda is equivalent to changing field_L.
      -	Here's the argument that nothing changes.
	Let's make a new field half the original length but with the same
	pattern (i.e., the pattern is also shrunk in x). We'll keep n_slices
	unchanged; so each slice is now half the original slice size, but has
	the same concentration in it as before the shrink.
	Then since delta_L goes down by 2, diffusion flow (in moles/m3s) in
	each cell goes up 4x. If we now increase generation and decay by 4x
	each, we're back to the original numbers. So any pattern that was stable
	before is still stable.
      - That's what Werner-1b does. The numbers are in RD_plot_werner1a.py,
	stuffed into data2b. The function anal3() compares these numbers to the
	Werner-1a experiments -- they're essentially identical.

Werner-1c experiment #1 in simplesim.py:
      - This experiment starts with a field length that's too short to hold our
	desired pattern, and iteratively increases the decay rates (i.e.,
	decreases lamda) until the desired pattern sticks.
      - When the desired pattern doesn't stick, we get a pattern with fewer
	replications than desired. Even when we decrease lamda so that the
	higher-rep pattern *could* stick, the lower-rep pattern will also still
	be stable. Thus, at each iteration we must run spread() all over again.
      - We use L=10, 50 slices, N=10. Initialize with spread(LHLH); it settles
	to LH because the field is too short to hold LHLH.
      - Iterate { increase both decay constants by 1.1x, spread(LHLH), re-sim}
	until the LHLH sticks. It took 7 iterations, requiring the final
	decay_rate_A=1.77

Werner-1c experiment #2 in simplesim.py:
      - This experiment is to start with the lots of peaks (i.e., the
	initialization from "The Werner2015 model in simplesim.py", where we
	initialize [A] linearly from cell[0] to [-1], initialize [I]=0).
	Then we gradually decrease kDA and resim, and watch the number of peaks
	get smaller and smaller.
      - We just do the lots-of-peaks initializion once, and get LHLHLH. Then we
	gradually reduce the decay rates, resimming each time (but *not* calling
	spread() ever again after the first time) until hopefully we get down to
	LH.
      - Some of the results (all for L=30, 100 slices, N=10, shape_init=LH)
	LHLHLH (run=1,  kDA=1.00), [A] gradient=0.552
	LHLHLH (run=7,  kDA=0.53), [A] gradient=0.780
	LHLHL  (run=8,  kDA=0.48), [A] gradient=1.153
	LHLHL  (run=9,  kDA=0.43), [A] gradient=1.268
	LHL    (run=10, kDA=0.39), [A] gradient=1.544
	LHL    (run=27, kDA=0.06), [A] gradient=4.873
	LHL    (run=28, kDA=0.06), [A] gradient=4.871
	LHL    (run=50, kDA=0.01), [A] gradient=0.055
	LH     (run=51, kDA=0.01), [A] gradient=0.044
      - A few interesting things: first, even though we keep *increasing* the
	decay rate, the gradient actually keeps increasing until run 28 (at
	which point it steadily decreases). I don't know why -- I would have
	expected it to keep decreasing, with some jumps up when it switches
	shape. This is odd and unexplained!
      - Next, note that we hop straight from LHLHL to LHL without really passing
	through LHLH; and then pretty much get stuck at LHL. This is consistent
	with the data in the Werner-1a "what sticks" experiments, where LHLHL
	and LHL have the same stability range, and LHL and LH similarly have the
	same range. Unfortunately, that means that we don't have a good way to
	keep increasing the decay rate to get us down to LH :-(. So on to the
	next try.

Werner 1c experiment #3 in simplesim.py:
      - In this version, we add the simplesim parameter g_werner1_peg_ratio.
	It defaults to False; but when True, it modifies the generation of A
	and I in custom_werner1(). Slices [0,1] get their ratio [I]/[A] forced
	to 1000, while slices[-2,-1] peg to 0. This essentially removes the
	possibility of two-headed or two-tailed patterns, which are what gave us
	problems just above. With this done, experiment #2 becomes much better
	behaved. Note that in RD.py, the equivalent capability is called
	set_preseed_LXH().
      -	The general setup is as in experiment #2: spread() uses the simple
	linear spread for [A] and leaves [I] at 0, fixed integration at .0002,
	L=30, 100 slices, N=10, shape_init=LH. The results:
	LHLH (run=1,  kDA=1.00), [A] gradient=0.6496
	LHLH (run=2,  kDA=0.90), [A] gradient=0.7154
	....
	LHLH (run=17, kDA=0.19), [A] gradient=2.8170
	LH   (run=18, kDA=0.17), [A] gradient=3.8210
      - So it all works now! I.e., we can start with a high-rep pattern and
	keep increasing lamda until it has as few reps as we want.
