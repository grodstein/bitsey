#!/usr/bin/env python3

#    import pdb; pdb.set_trace()
import sys; sys.path.append ("..")	# To get the usual Bitsey files
import math, numpy as np
import sim

############################################################
##### Some generally-useful functions
##### They're used by every file that does the basic Werner model -- which
##### is RD.py and grn_count1.py. Grn_direct1.py doesn't use the Werner model;
##### it just characterizes whatever pattern we spread rather than trying to
##### create a pattern via reaction/diffusion.
############################################################

# Basic setup (setting integration parameters, dumping intervals,
# calling init_big_arrays() and making GJs, etc).
# It's used by all the sims in this file.
def setup_common (p, time_step, adaptive, sim_dump_interval,
		  sim_long_dump_interval, extra_ions, n_cells):
    # Parameters for numerical integration and printout.
    p.time_step = time_step
    p.adaptive_timestep = adaptive	# So that the params above get used.
    p.use_implicit = False		# Use explicit integration
    # Change from seconds to number of time_steps
    p.sim_dump_interval = int(sim_dump_interval/time_step)
    p.sim_long_dump_interval = int(sim_long_dump_interval/time_step)

    # Create extra ions 'A' and 'I' (activator and inhibitor).
    n_GJs = n_cells-1
    sim.init_big_arrays (n_cells, n_GJs, p, extra_ions)

    # Straight-line GJ connections along the 1D field. All are simple & ungated.
    sim.GJ_connects['from']  = range(n_GJs)
    sim.GJ_connects['to']    = sim.GJ_connects['from'] + 1

    p.GJ_len = 15e-9	# Change the gap-junction length from 100nm to 15nm.

# Spread [ion] according to the pattern string 'shape'.
# Assume that the [ion]-vs-cell_number profile in cc_init[] is
# correct, and replicate it as needed to build the desired shape.
# 'Ions' is a list of which ions to spread.
def spread (shape, ions):
    # Note num_cellsI (for cc_init) vs. num_cellsW (for the worm). We can use
    # a 5-cell template to initialize a 30-cell worm!
    num_cellsI = cc_init.shape[1]

    # Indices[w] is the (potentially fractional) cc_init index for worm[w]
    # So worm_cell[w] will take its [ion] from cc_init[indices[w]]
    num_cellsW = sim.cc_cells.shape[1]
    indices = np.empty(num_cellsW)

    # Fill in indices[]. For, e.g., LHLH, we take each of the three segments
    # LH, HL and LH from it, and fill in the corresponding part of indices[].
    for pos in range (len(shape)-1):	# One letter at a time...
        # This single segment of the pattern will go into worm cells[w0,w1].
        w0 = round (pos    *(num_cellsW-1)/(len(shape)-1))
        w1 = round ((pos+1)*(num_cellsW-1)/(len(shape)-1))
        # Are we putting a LH pattern into cells[w0,w1), or a HL?
        seg = shape[pos:pos+2]
        assert ((seg=="LH") or (seg=="HL"))
        i0 = 0 if (seg=="LH") else num_cellsI-1	# cc_init idx for worm cell[w0]
        i1 = num_cellsI-1 if (seg=="LH") else 0	# cc_init idx for worm cell[w1]
        spread = np.linspace (i0, i1, w1-w0+1)	# Spreads this entire segment.
        indices[w0:w1+1] = spread

    # Now set sim.cc_cells accordingly.
    np.set_printoptions (formatter={'float': "{:.3f}".format})
    for ion in ions:
        sim.cc_cells[ion] = np.interp (indices, range(num_cellsI), cc_init[ion])
        print (f"Spread {shape} set [{ion}] = {sim.cc_cells[ion]}")

# The pattern used to spread A and I. It comes from a simulation of LH.
cc_init = np.array (
	[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],	# Na
	 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],	# K
	 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],	# Cl
	 [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],	# P
	 [.0002,.0002,.0003,.0004,.0007,.0011,.0017,.0028,.0045,.0074,
	  .0120,.0196,.0320,.0523,.0856,.1401,.2292,.3723,.5317,.6119],
	 [.0841,.0855,.0883,.0926,.0984,.1059,.1151,.1263,.1395,.1551,
	  .1732,.1942,.2185,.2464,.2784,.3150,.3569,.4043,.4482,.4714]])

# Set [A] to vary linearly from [A]=0 in cell 0 (the tail) to [A]=1 in cell[-1]
# (the head). This tends to make an upcoming Werner1 reaction/diffusion sim
# create as many peaks as the field can support.
def linear_spread():
    A=sim.ion_i['A']; I=sim.ion_i['I']
    sim.cc_cells[A] = np.linspace (0, 1, sim.cc_cells.shape[1])
    sim.cc_cells[I] = 0

def set_preseed_LXH():
    # Seed to try to get L at cell 0 and H at cell[-1]
    A=sim.ion_i['A']; I=sim.ion_i['I']
    sim.cc_cells[[A,I],0:2] = 0
    sim.cc_cells[[A,I],-2:] = 1

# Look at the profile of [A] and deduce where the worm's heads and tails are.
# Return a string such as "LH" (hopefully), or "HLH" (for a two-headed worm).
# The general idea is we walk the cells from left to right; we end a rising
# segment when we see a new [A] that is at least DELTA smaller than the
# largest value on the segment (which is usually but not always the [A] at
# the segment's right-hand end). A falling segment is similar.
def shape():
    DELTA=.02	# Ignore any changes in [A] that are less then DELTA.
    shape = ''	# Start with an empty string and add to it as we go.
    mode = 'X'	# Is the current segment falling(F), rising(R) or not set(X)?
    A = sim.ion_i['A']
    aMin = sim.cc_cells[A,0]	# Running min & max [A] on the current segment.
    aMax = sim.cc_cells[A,0]
    n_cells=sim.cc_cells.shape[1]
    for cell in range (1,n_cells):	# Walk from left to right, and
        aNew = sim.cc_cells[A,cell]	# analyze the current cell's [A]
        # If we're walking a rising segment, then we've already outputted the
        # "H" corresponding to the end of the segment that we've not reached
        # yet, but that has to come at some point. (Startup is a special
        # case... see below).

        # print (f'A[{cell}]={aNew}...')
        # Rising segment ends because we see a fall in [A]. This means that the
        # segment's H (which we've already outputted) is the cell immediately
        # to our left. We output a L, for the low cell at the end of the segment
        # we now start (we'll find the actual low cell eventually).
        if (mode=='R') and (aNew < aMax-DELTA):
            mode='F'; shape=shape+'L'
            aMin=aNew; aMax=aNew
            # print ('R->F, output L')

        if (mode=='F') and (aNew > aMin+DELTA):	# Ditto, for a falling segment
            mode='R'; shape=shape+'H'
            aMin=aNew; aMax=aNew
            # print ('F->R, output H')

        # Two special cases for the start of a worm. When we see the initial
        # rise in [A], we output both the L (for cell #0), and also the H (for
        # the eventual high that we haven't seen yet).
        # Note that we don't decide whether this initial segment is rising or
        # falling until we see >DELTA of [A] change... which may take a few
        # cells. So [A]= [.999 .987 1.012  0.999 1.001] will be LH.
        if (mode=='X') and (aNew>aMin+DELTA):
            mode='R'; shape=shape+'LH'
            aMin=aNew; aMax=aNew
            # print ('Initial LH, mode=R')

        if (mode=='X') and (aNew<aMax-DELTA):	# Ditto for an initial
            mode='F'; shape=shape+'HL'		# falling segment.
            aMin=aNew; aMax=aNew
            # print ('Initial HL, mode=F')

        aMin = min (aMin, aNew)	# Update the running min & max [A]
        aMax = max (aMax, aNew)	# along the current segment.

    return (shape)

# The main setup routine for any sim that uses the Werner2015 model.
# Take L and n_cells; compute delta_L = L/n_cells. Then, starting with the
# simplesim.py usage of D_A=1, compute our own D_A with the formula
#    D_A = simplesim_D_A * p.cell_r * p.GJ_len / (3*delta_L*delta_L)
def setup_werner1(p, L, n_cells, init_shape):
    # Physics parameters gen, decay and Hill model
    gen_rate_A=1;   gen_rate_I=4	# moles/(m3*s)
    decay_rate_A=1; decay_rate_I=2	# 1/s
    Hill_N=10
    p.sim_name = f'L={L}, {n_cells} cells, N={Hill_N}, shape_init={init_shape}'

    # GJ diffusion rate
    delta_L = L/n_cells
    D_A = 1 * p.cell_r * p.GJ_len / (3*delta_L*delta_L)
    print (f'D_A={D_A:.2e}')
    #D_A=6.25e-13			# m2/s. For delta_L=.2
    #D_A=2.77777777e-13			# m2/s. For delta_L=.3
    #D_A=1.5625e-13			# m2/s. For delta_L=.4
    #D_A=1e-13				# m2/s. For delta_L=.5
    D_I=30*D_A

    A=sim.ion_i['A']; I=sim.ion_i['I']
    if (init_shape != ""):
        spread (init_shape, [A,I])
    #init_werner1()

    # Set up charge and diffusion rate of A and I.
    # We only care about diffusion through GJs (not the cell membrane), so we
    # keep the default D[A]=0 and D[I]=0
    sim.GJ_diffusion[A] = D_A
    sim.GJ_diffusion[I] = D_I
    sim.z_array[[A,I]] = 0

    # Generation and decay of A
    # Params are out, in1,in2, N, kVMax for kVMax/(1+(in2/in1)^N)
    Hill_ratio_gate (A,A,I,Hill_N,gen_rate_A)		# generation of A
    sim.GD_const_gate (sim.Gate.GATE_GD, A,0,decay_rate_A)	# decay of A
    Hill_ratio_gate (I,A,I,Hill_N,gen_rate_I)		# generation of I
    sim.GD_const_gate (sim.Gate.GATE_GD, I,0,decay_rate_I)	# decay of I

    lamda_A = math.sqrt (D_A / decay_rate_A)
    print (f'lamda_A={lamda_A:.2e}, R_cell={p.cell_r}')

############################################################
##### Gating functions for the model in Werner2015, which is used
##### by pretty much every sim in this file (other than the simple-diffusion
##### sims above).
############################################################

# The "G" in Werner2015. Roughly, generate when [activator] > [inhibitor].
# We'll instantiate one of these to generate A and one for I. Each will have a
# different kVMax.
class Hill_ratio_gate (sim.Gate):
  # in_ion1 will be A and in_ion2 will be I.
  def __init__(self, out_ion,in_ion1,in_ion2,N,kVMax):
    super().__init__ (self.GATE_GD, out_ion)
    self.in_ion1,self.in_ion2 = in_ion1,in_ion2	# which ligands control the gate
    self.params = np.zeros ((2,self.width))	# (2 params) x (# cells or GJs)
    self.params[0],self.params[1] = N,kVMax

  # The function that actually computes the gating vector at runtime.
  # We compute kVMax / (1 + (I/A)**N)
  def func (self, cc, Vm_ignore, deltaV_ignore, t_ignore):
    # self.params[] is [2, # cells or GJs]. Assign each of its rows (i.e., a
    # vector of one parameter across all cells) to one variable.
    N,kVMax = self.params 	#Each var is a vector (one val per cell)
    ratio = (cc[self.in_ion2]/(cc[self.in_ion1]+1e-20)) ** N
    out = kVMax / (1 + ratio)
    return (out)
