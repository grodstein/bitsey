Worm.py has a fitness function that tries to measure how well a worm (or really,
a set of worm parameters) builds and maintains a gradient. These notes describe
how to make fitness a continuous function of the parameters.

Here's the gist of the original function:

	- If the simulation doesn't finish, fitness= -1. If it does...
	- A no-gradient worm gets 0-40 points for how long it takes to collapse.
	- A good worm gets 50 points for living, another 0-10 for how big a
	  spread they have, and another 0-40 for how many of 20 segments
	  can regenerate.

The details:

- If the simulation doesn't finish, fitness= -1.
  "Doesn't finish" means that Worm.sim() returns None, which happens when
  sim.sim() throws a Value_Error, which in turn happens when
  scipy.integrate.solve_ivp() returns a "bunch" object whose bunch.success is
  False.

  This is a very binary situation that is clearly discontinuous! There may be a
  sharp boundary between two parameter regions, where the simulation succeeds on
  one side of the boundary and fails on the other, and that would imply a sudden
  discontinuous increase in fitness across the boundary.

  How might we get around it? The simplest way is to just remove this criterion
  completely; it's just worth one point. We might be able to keep it if
  solve_ivp() returns some indication of, e.g., how long the simulation lasted
  before crashing and turn that into a number. We would then use a logistic
  function to map the range "crashes at t=0" to "stays crash-free forever" to,
  e.g., the range [0-1] and add that to the fitness function.

- A no-gradient worm gets 0-40 points for how long it takes to collapse.
  Sim.sim() returns data to us (in the form of cc_shot[] and t_shot[]) at lots
  of points that sim.sim() would make a pretty plot. We go through every one of
  these cc_shot snapshots, and find the first such snapshot where the gradient
  has collapsed (i.e., [M]_head <= 1.6 [M]_tail). If the time of that snapshot
  is 't', we return t*40/SIM_TIME.

  This is *almost* continuous. To make it so, we would just have to use
  interpolation to find the correct-according-to-interpolation t rather than the
  first discrete t that is collapsed.

- A worm that does have a gradient gets 50 points for living another and 0-10
  for how big a gradient they have
  This is just a map from a gradient of, say, 0 to +75mV to a score of, say,
  0 to 25. It is thus continuous and gets added to the total score. Gradient<0
  also gets 0 points.

- A worm that does have a gradient gets another 0-40 for how many of 20
  segments can regenerate. Specifically, if n_good is the number of children
  that create "sufficient" gradient, we have a adder of n_good*scale_factor.

  This is clearly discontinuous; the term n_good is discrete. Instead, we could
  give each of the 20 children a *continuous* score based on how big of a
  gradient they built (the map of 0 to +75mV to a score of 0 to 25 above). We
  then just add up the scores of all 20 children.

  def fitness (self, only_TH=True):
      if (self._fitness >= 0):	# As a worm moves from one generation to
          return (self._fitness)	# the next, only compute fitness once.

      # Seed the worm with [Na], [M], etc.
      # Sim for 10K seconds. Does it hold a ΔV?
      print ("Initial fitness sim...")
      spread_M ("TH")

      global GRADIENT; GRADIENT=.030	# Tell sim() how much gradient is enough
      status = self.sim (SIM_TIME)
      if (status == None):	# Unsuccessful simulation => fitness = -1
          return (-1)

      if (status == False):	# Sim finished, but not enough gradient
          # The full worm doesn't hold its spread. Return a score in [0,40]
          # based on how long it took for the original Δ[M] shrink away. We
          # don't need a new sim; we have the old data at numerous timepoints. 
          print ("Worm died")
          M=sim.ion_i['M']
          for idx,cc in enumerate(self.cc_shots):
              if (cc[M][-1] <= cc[M][0]*1.6):	# The [M] gradient has shrunk
                   t = self.t_shots[idx]
                   self._fitness = t * 40.0 / SIM_TIME
                   return (self._fitness)
          self._fitness = 40.0		# gradient never shrunk much.
          return (self._fitness)	# We get the full 40 points.

      # The original sim finished and had a good gradient.
      # 50 points for that, and another 0-10 points for how big delta_Vm is.
      # We'll let 75 mV get the entire 10 extra points.
      self._fitness = 50 + self.gradient()*10/.075
      #self._fitness = min (60, 50 + self.gradient()*10/.075)

      shape = self.shape()
      if (only_TH and (shape != "TH")):
          return (self._fitness)	# Don't even try any children.

      # From how many locations can you regrow a small piece? Each
      # location will start with a narrow spread of [M]. Instead of 0-2,
      # we'll divide the worm into 5 pieces of [0-.4], [.4-.8], [.8-1.2],
      # [1.2-1.6] and [1.6-2].
      n_good = 0
      for piece in range(NUM_PIECES):
          print ("\nSim for piece #",piece)
          spread_M ("TH", piece, NUM_PIECES)
          status = self.sim (SIM_TIME)
          shape = self.shape()
          if (status and ((not only_TH) or (shape=="TH"))):
              print ('Piece #',piece,"-> success")
              n_good = n_good + 1
      print (n_good, "/", NUM_PIECES, "children lived")
      self._n_good_children = n_good
      self._fitness = self._fitness + n_good*40/NUM_PIECES
      return (self._fitness)

