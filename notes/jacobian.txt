def Jac_sp ():
    import scipy.sparse as sp
    global cc_cells, z_array
    n_state_vars = cc_cells.size
    Jac = sp.dok_matrix ((n_state_vars, n_state_vars))
    # ion channels. The IC flux for ion i in cell c is of course only nonzero if
    # i actually passes through ICs! If so, then it depends on Vmem,c. It also
    # depends on the [i] in c. Finally, it depends on any IC gating.
    for (i,c) in np.transpose (Dm_array.nonzero()):	# each relevant i,c
        add_conc_to_Jac (Jac,i,c, i, c)	# depends on [i] in cell c
        add_Vmem_to_Jac (Jac,i,c, c)		# depends on Vmem[c]
        add_gating_to_Jac (Jac,i,c, IC_gates[i,c]) # depends on IC_gates[i,c]

    # GJ_diffusion[n_ions] gives the basic diffusion constant for each
    # ion type through GJs.
    for (idx, (GJfrom,GJto,GJscale,GJtype)) in enumerate(GJ_connects):
        ions = np.flatnonzero (GJ_diffusion[:,GJtype])
        for i in ions:	# for each ion that diffuses through this GJ
            for end in [GJfrom, GJto]:
                # diffusion; depends on [i] at each end
                add_conc_to_Jac (Jac,i,end, i, GJfrom)
                add_conc_to_Jac (Jac,i,end, i, GJto)
                if (z_array[i]!=0):	# drift: depends on Vmem at each end
                    add_Vmem_to_Jac (Jac,i,end, GJfrom)
                    add_Vmem_to_Jac (Jac,i,end, GJto)
                add_gating_to_Jac (Jac,i,end, GJ_gates[idx])

    # The generation rate for ion i in cell c is
    # gen_cells[i,c] * eval_gate(gen_gating[i,c]).
    for (i,c) in np.transpose (gen_cells.nonzero()): # for each i,c with generation
        add_gating_to_Jac (Jac,i,c, gen_gating[i,c])

    # The decay rate for ion i in cell c is [ion] * decay_rates[ion].
    for i in decay_rates.nonzero():	# foreach ion with nonzero decay rate...
        for c in range(cc_cells.shape[1]):	# for each cell
            add_conc_to_Jac (Jac,i,c, i, c)	# Depends on its own conc.

    return (Jac)

# If ion i in cell c depends on (Vmem in cell2)...
#	then it depends on conc[i2,cell2] for all charged ions i2
def add_Vmem_to_Jac (Jac, i, c, cell2):
    for i2 in z_array.nonzero():	# foreach charged ion...
        add_conc_to_Jac (Jac,i,c, i2,cell2)

def add_gating_to_Jac (Jac,i,c, M):
    (Mtype,kM,N,Mcell,Mion,Mcell2) = M
    if Mtype==1:	# Vmem gating
        if (Mcell > -1):
            add_Vmem_to_Jac (Jac,i,c, Mcell)
        if (Mcell2 > -1):
            add_Vmem_to_Jac (Jac,i,c, Mcell2)

    elif ((Mtype==2) or (Mtype==3)):
        add_conc_to_Jac (Jac,i,c, Mion, Mcell)

def add_conc_to_Jac (Jac,i,c, i2, c2):
    global cc_cells
    n_cells = cc_cells.shape[1]
    Jac[i*n_cells+c, i2*n_cells+c2] = 1

def Jac_full (t, y):
    import scipy.sparse as sp
    global cc_cells
    print ("Called Jac_full(t=",t,')')
    num_ions=cc_cells.shape[0]; num_cells=cc_cells.shape[1]
    cc_cells = y.reshape((num_ions,num_cells)) # moles/(m3*s)

    n_state_vars = cc_cells.size
    Jac = sp.dok_matrix ((n_state_vars, n_state_vars))
    time_step = .005
    cc_cells_save = cc_cells.copy()
    slew_cc_save = sim_slopes (0, cc_cells).copy()

    for i in range (cc_cells.shape[0]):
        for c in range (cc_cells.shape[1]):
            dx = slew_cc_save[i,c] * time_step
            if (dx==0):
                dx = .0001
            cc_cells_2 = cc_cells_save.copy()
            cc_cells_2[i,c] += dx
            slew_cc_2 = sim_slopes (0, cc_cells_2)
            Jac_col = (slew_cc_2 - slew_cc_save) / dx
            Jac_col = Jac_col.reshape ((n_state_vars))
            which_col = i*cc_cells.shape[1] + c
            for idx in np.flatnonzero(Jac_col):
                Jac[idx,which_col] = Jac_col[idx]
                #Jac[idx,which_col] = (1 if (Jac_col[idx]!=0) else 0)
    #np.set_printoptions (formatter={'float':'{:.2g}'.format},linewidth=150, threshold=10000)
    #print (Jac.toarray())
    return (Jac)
