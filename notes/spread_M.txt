This file is from trying to make worm.py work with our worm-electrical issues
(C1, J2, F2, C3a and C3b). The specific issue is the spread_M function.
Our first pass simply:
	- spread [M] between [0,2]
	- added an equal amount of [Na] to each cell as the current [M], so
	  as to reach charge neutrality.
There was a problem, though. Yes, the nodes all started at Vmem=0 at t=0.
However, right after the simulation started, the node voltages would go crazy
for a short time before (sometimes) settling out into a nice TH pattern.

To eliminate that initial craziness, I tried setting [Na] not simply to create
Vmem=0 in all cells, but in fact to precisely tune [Na] so that it produced an
initial Vmem that matched the [M], which in fact was set to mirror the hoped-for
final simulation outcome. That way (as the idea went), the simulation would just
smoothly adjust itself slightly to the final outcome.

It didn't work. There were two problems:
	- Even if Vmem and [M] were reasonably close to their consistent final
	  values, [Na], [K] and [Cl] were not. Thus, we still had large startup
	  transients; i.e., the sim still started off "crazy."
	- The C1 tests comprised multiple simulations. The first one started
	  with default concentrations (which had [M]=0 in every cell) and spread
	  a TH pattern of [M]. This was OK. But then C1's second sim would start
	  from that final pattern, re-seed [M] to HTH and resimulate. That
	  exposed a bug; since the result of the first sim was not [M]=0
	  everywhere, we also had the sum of [Na], [K] and [Cl] was nonzero.
	  Thus, setting [Na] to match [M] did *not* bring us to charge
	  neutrality, but often left us at extreme voltages. In fact, this bug
	  was also present in the first failed attempt, but I hadn't noticed it.
The code is in solution #2 below.

Solution #3 tries to be more subtle.
	- [M] is spread as usual.
	- The desired Vmem follows [M] as before, with [M]=0 meaning the tail
	  at -80mV, and [M]=2 meaning the head at Vmem=0.
	- [Cl] follows simply, since its Vnernst must match Vmem and we know
	  the external [Cl].
	- [Na] is harder. Spread_M() must be given an array that has various
	  Vmem -> [Na] pairs; we interpolate our [Na] for each cell from this
	  array. Where could the table come from? The exact final values for
	  [Na] at a given Vmem will differ based on the parameters on any given
	  simulation. Thus, we cannot do this perfectly. Instead, we just do as
	  well as we can. Originally, the table will just come from the values
	  for test1(), which is a lot of cells with no GJs. But for future sims,
	  we just take results from the first C1() simulation, which is a
	  simple TH worm.
	- Finally, we set [K] to get the desired Vmem based on charge balance.

***************************************************************
Solution #2:
def spread_M (shape, M_min=0, M_max=2*GD_FVAL):
    Na=sim.ion_i['Na']; M=sim.ion_i['M']
    num_cells = sim.cc_cells.shape[1]
    M_valence = sim.z_array[M]

    print ("Entering spread_M: Vm(V)=",sim.compute_Vm(sim.cc_cells))
    print ("cc_cells=", sim.cc_cells)

    for pos in range (len(shape)-1):
        i0 = round (pos    *(num_cells-1)/(len(shape)-1))
        i1 = round ((pos+1)*(num_cells-1)/(len(shape)-1))
        seg = shape[pos:pos+2]
        assert ((seg=="TH") or (seg=="HT"))
        M0 = M_min if (seg=="TH") else M_max
        M1 = M_max if (seg=="TH") else M_min
        spread = np.linspace (M0, M1, i1-i0+1)
        sim.cc_cells[M, i0:i1+1] = spread

    print ("Spread M to", sim.cc_cells[M])
    print ("And now Vm(V)=",sim.compute_Vm(sim.cc_cells))
    # how many moles/m3 we need to make 1C of charge.
    p = sim.GP
    mm3_per_C = 1 / (p.F * p.cell_vol)	# 1 / (C/mole * m^3), or moles/m^3 per C

    cMem = p.cell_sa * p.cm		# cMem in Farads -- or in Coul/V

    # M from [0,2] will match Vmem [-80mV, 0]
    Vmem = (sim.cc_cells[M,:] * .040) - .080
    print ("Desired final Vmem=", Vmem)

    # Now compute the desired net charge in each cell (in mole/m^3)
    mm3 = Vmem * cMem * mm3_per_C

    # We were balanced with [M]=0.
    # Each cell already has unbalanced charge from its [M] != 0.
    mm3_M = sim.cc_cells[M,:] * M_valence

    # Now adjust [Na] to give us the desired Vmem in each cell.
    # Why just [Na] and not also [K] and [Cl]? Because we're moving around such
    # small amounts of charge that we don't affect the macroscopic [Na], [K] or
    # [Cl] anyway, so it doesn't really matter which we use.
    # Also, assume that the net charge was pretty much neutral before.
    # Finally, we also have to add enough extra Na to make up for the M we added
    sim.cc_cells[Na,:] -= mm3_M
    print ("After canceling M, Vm(V)=",sim.compute_Vm(sim.cc_cells))
    sim.cc_cells[Na,:] += mm3
    #sim.cc_cells[Na,:] += (mm3 - mm3_M)
    print (sim.cc_cells)
    print ("After Na, Vm(V)=",sim.compute_Vm(sim.cc_cells))
***************************************************************
