def run_transfer_curve (func, ion_in, cell_in, ion_out, cell_out, min, max):
    GP = sim.Params()
    end_time = 10
    in_concs=[]; out_concs=[]
    for inp in np.linspace(min, max, 15):
        converged = False
        while (not converged):
            print ('Starting sim for ['+ion_in+']=', inp, 'to time=',end_time)
            eval ('setup_'+func+'(GP)')
            GP.no_dumps = True
            sim.cc_cells[sim.ion_i[ion_in],cell_in]=inp
            Vm = sim.compute_Vm (sim.cc_cells, GP)
            assert (Vm<.5).all()	# I.e.,roughly charge neutral
            t_shots, cc_shots = sim.sim (end_time, GP)

            # Did it converge?
            io = sim.ion_i[ion_out]
            x2 = cc_shots[-1][io,cell_out]
            for i in range (len(t_shots)-1,-1,-1):
                if (t_shots[i] < .9*end_time):
                    x1 = cc_shots[i][io,cell_out]
                    print ('t=',t_shots[i],'=>',x1,'; t=',end_time,'=>',x2)
                    converged = (abs(x1-x2)<.01) and (abs(x1-x2)/(x1+x2)<.01)
                    break
            if (not converged):
                end_time = end_time*2
                print('not converged')
        in_concs.append(inp)
        out_concs.append(x2)
    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(in_concs, out_concs, linewidth=2.0)
    plt.show()

