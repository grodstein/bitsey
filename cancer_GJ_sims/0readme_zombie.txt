Big lessons learned:
- Two outer cells can always (given a big enough Kd) flip an inner cell and turn
  it into a zombie (surround_flip sims). And the behavior is very
  attractor-point-like; it only takes very small differences in GJ_scale to
  totally flip how a sim behaves.
- A rectangular tumor in a 2D field is weakest at the corners. Why?
  All cells have four neighbors. Our first (naive) analysis says that for a cell
  on the tumor border, three of those neighbors are in the tumor and one is
  outside. So it will be quite hard to flip that border cell. However, that's
  not quite correct.

  The problem is that *all* of the cells on the tumor border are being attacked.
  So a tumor's two on-border-and-in-tumor neighbors are also being attacked, and
  aren't really available to help out the tumor cell. In fact, *almost* all of
  the tumor-boundary cells engage in a 1-vs-1 fight with their across-the-border
  neighbor.

  There is one exception, though -- the cells at the four corners of the tumor.
  These cells have two in-tumor neighbors (both of which are being attacked) and
  two out-of-tumor neighbors. Thus, those corner cells are each fighting two
  attackers, and typically lose. They are a 2D tumor's weak spot.

- Fixing a tumor (steps 2 & 3 of zombie_sims()) is easy in 2D, but may be
  impossible in 1D. The first step is always finding Kd_collapse, the largest Kd
  that doesn't collapse a pre-built border. Then comes the binary search for the
  lowest Kd that can fix a tumor, in the range of [0,Kd_collapse].

  But if Kd_collapse *didn't* collapse a border, then why would it smother a
  tumor? In 2D the reason is that the background first attacks a tumor at the
  corners, where the tumor is weak. So a Kd that cannot collapse a solid border
  can easily fix a tumor.

  In 1D, however, there are no corners. So we often cannot find any Kd that can
  fix a tumor but won't collapse a border. The only way it can work is if the
  tumor is small enough that the background field overwhelms it.

- When the cells of a tumor band together via extra GJs, it's helpful for
  invasion but may not be enough. It only helps if the tumor is large enough.
  Basically, the tumor gets attacked at its boundary cells; and the tumor's
  interior "volume" is what fights back. So the tumor can only live if its
  volume is greater than its surface area -- which happens if the tumor is big.

****************************************************************************
			The big picture

Part 1: zombie flipping and how GJ gating helps border sharpness
 a. measure_spread() sims show that lowering Kd sharpens the border. But
    why not make Kd=0, and thus have the border is perfectly sharp? Because...
 b. zombie_sims_border_sharpness() sims of a tumor on a neutral background
    (A=B=50 everywhere), with all cells initialized to OA=100 except for the
    blot, show that Kd must be big enough to fix the blot.
 c. zombie_sims_border_sharpness() sims of a neutral background (again, A=B=50
    everywhere) with a border (left-side cells at OA=100 and right side to OA=0)
    show that Kd must be small enough to not clobber the border.
 d. zombie_sims_border_sharpness() sims with gated GJs show an advantage, but
    not a huge one:
    (1) the border sharpness at the minimum Kd that fixes a 1x1 blot is
	fairly similar with and without GJ gating.
    (2) ditto for the maximum Kd that doesn't collapse, and for the geometric
	mean of the two.
    (3) the ratio of max/min Kd may be pretty similar with and without gating.
 e. zombie_sims_border_sharpness() sims show that applying gating *after* we
    make a border is very effective at sharpening the border.
 f. surround_flip() shows that cells can be flipped and that, once flipped, they
    are undistinguishable from the other cells on their new team.

Part 2: tumor isolation
 - zombie_tumor_isolate() sims show that a small clump of errant cells in
   no-man’s land gets assimilated. They also show that if it's isolated with
   gated GJs and a different Vmem, it can resist. This is just the fact that the
   gated version of any param set has a higher minimum Kd than the ungated
   version.
 - zombie_tumor_isolate() also shows that small tumors get assimilated more
   easily than large tumors. This is not because of the surface-area-to-volume
   issue, since these sims don't overexpress GJs and hence don't have a tumor
   acting as a syncytium. Instead, it's because the outer layer of the tumor
   is attacked by the innermost healthy-tissue layer and defended by the
   immediately-more-inner tumor layer -- and the ratio between the size of these
   two layers is more reasonable for larger tumors.
   number of cells in each of 

Part 3: metastasis
 - zombie_sims_metastasis() shows that a big tumor can proselytize its neighbors
   better than a small one.
 - zombie_sims_metastasis() shows that a tumor can increase its internal
   connectivity to make it a syncytium, giving it some “oomph.”
 - If I am a tumor, and I want to proselytize my neighbor N, then my best
   strategy is to remove N’s connection to his healthy neighbors. If I can
   convince his VMem to be more like mine, then job done. So a good tumor needs
   to not only know how to alter its own VMem, but also to spread that VMem to
   its neighbors.
 - Zombie() demonstrates the good-zombie effect, where a flipped cell becomes
   indistinguishable from its neighbors. Surround_flip() show this as well.
   It might be nice to also set up a sim where the cells are simple non-bistable
   Hill inverters, to show that the good-zombie effect doesn't occur there.
   Or make an even better demonstration of the good-zombie effect - the trail
   of changed VMem that Mafe and Cervera demonstrate (though it has nothing to
   do with sharp borders).
 - Show that a higher dimensionality of connectivity improves robustness. More
   dimensions means more neighbors, which means you can fix more errant cells.
   In a 2D sim with a one-layer bistable zone, if the bistable zone has one bad
   cell then its two good neighbors can fix it -- but two consecutive bad cells
   in the bistable layer stay broken.

Isolation sims
- Start with the Kd_min simulation that removes a spot. Then either decrease
  spot D_GJ, or add gating, or both. And now rerun the Kd_min search to find
  the new Kd_min; it should be much higher (note that the search will only
  change D_GJ *outside* of the tumor).

Metastasis sims
- Start with a spot and with Kd just below Kd_min, so the spot isn't consumed.
  Then raise the spot's D_GJ and it's Vmem, and show that it grows. This
  requires raising its Vmem without changing its [OA], a new trick!
- Start with a spot and with Kd just below Kd_min, so the spot isn't consumed.
  Then raise the spot's D_GJ and keep its Vmem low, but have it's *neighbors*
  change to the tumor Vmem (another new trick). Again, show that it grows.


Summary: cancer is when cells change from a team-player phenotype to a lone-wolf
phenotype. All cells know how to do that in the embryo, and so we don't really
need mutations. But if there is indeed a gene-expression component to some
cancers, reverting to embryonic behavior, then it's likely to involve state
nodes (since that's how embryonic differentiation probably works).

****************************************************************************
		set_Vmem_class1(), set_Vmem_class2()
	These tests aren't part of my narrative, but helped me set up my model.

These tests help set up class set_Vmem_from_conc, which lets [OA] and [OB]
control Vmem via ion-channel gating. We want to build a gating where
      -	[OA]>>[OB] leads to depolarized Vmem
      -	[OA]<<[OB] leads to hyperpolarized Vmem
      - [OA]==[OB] is the knee of the curve, with a sharp response.
Why? Because we will later define our border between regions as the cell where
[OA] and [OB] flip, and we want this region to have a big Vmem transition,
which will help in turning off GJs there.

The first experiment use set_Vmem_class1().
We set up 17 disconnected cells, with
      - DNa is always the default 1e-18
      - DK ranges from 1e-18 to 160e-18 by steps of 10. The Vmem results are
	[-15 -53 -63 -68 -71 -73 -75 -76 -77 -78 -78 -79 -79 -79 -80 -80 -80]mV.
The idea is that the gating would be the Hill function
	ratio = ([OA]/[OB])**N
	Hill = ratio / (1+ratio)
	gating = 1 + Hill*160
So [OA]<<[OB] would result in DK=1e-18, and [OA]>>[OB] in DK=161e-18
On the good side, we have a nice wide swing of 65mV. But the clear knee is at DK
in the range [1,11], which is way at the extreme end. Furthermore, while sims
converged nice and fast when DK>50e-18, they ran slower and slower the smaller
Dk got, and took about 1000s to converge for DK=1e-18.
So this model didn't work well.

Next, do the same but now with DNa=10e-18. Here's our new Vmem result:
	[29 -10 -20 -24 -27 -29 -30 -31 -31 -32 -32 -32 -32 -32 -32 -32 -32]
This is worse, not better! Our range is down from 65mV to 61mV. And our knee is
no better located. And finally, positive Vmem isn't really biologically
realistic.
The bad results aren't surprising, though. VNernst,K = -72mV and
VNernst,Na=+28mV. So making the Na channel stronger can give positive Vmem, as
we see. And having the Na channel (which is still an *ungated* leak channel)
stronger means that the K channel (which is where we still have the gating) is
relatively weaker, so we get less range.

Next, try the double range of DK in [1,160]*1e-18 and DNa in [160,1]*1e-18.
Our new Vmem vs. cell# is 
	[58  18  9  6  4  3  2  1  0  -1  -2  -3  -4  -7 -12 -28 -80]
Our range is, unsurprisingly, huge at 135mV. But the highly-positive Vmem isn't
too biologically reasonable, and our knees are still at the ends.

Finally, let's try DK in [1,30]*1e-18 and DNa=1e-18. This gives Vmem of
	[-15 -30 -38 -44 -49 -52 -55 -57 -59 -60 -62 -63 -64 -65 -66 -67 -68]
which is getting pretty reasonable. The final ion concentrations are

cc_cells= (moles/m3)
	[6.0  6.8  7.2  7.4  7.6  7.7  7.9  7.9  8.0  8.1  8.1] Na
	[131  126  123  122  121  120  119  119  119  118  118] K
	[ 57   52   50   49   48   48   47   47   47   46   46] Cl
	[ 80   80   80   80   80   80   80   80   80   80   80] P
	[-15  -35  -45  -51  -56  -59  -61  -63  -65  -66  -68] Vmem (mV)
	[  1    4    7   10   13   16   19   22   25   28   31] Dk / 1e-18
Note the substantial changes from the initial Na=10, K=125, Cl=55.

-----------------

The next experiments use set_Vmem_class2(). They try out different Hill
functions in set_Vmem_from_conc.func() to find one that has the knee of the
Vmem-vs-([OA]/[OB]) curve to have the steep portion around [OA]=[OB].

We instantiate 11 disconnected cells. Set [OA] and [OB] in each cell manually,
with [OA] varying linearly over [0-100], and [OB]=100-[OA]. Each cell then
instantiates a set_Vmem_from_conc instance to gate its K channels.

Our first try for class set_Vmem_from_conc (sim.Gate):
	ratio = (cc[self.hypIon]/(.001+cc[self.depIon]))**4
	Hill = ratio / (1+ratio)
	gating = 1 + Hill*160
This produces
	[-15 -15 -21 -44 -67 -76 -79 -80 -80 -80 -80] Vmem
	[  0   1   2   3   4   5   6   7   8   9  10] cell
and the knee of the curve is at cell #3, which has [OA]=30 and [OB]=70. We
wanted the knee to be at [OA]=[OB]=50, so this isn't great.

Next try: 
	gating = 1 + Hill*30
This gives
	[-15 -15 -16 -24 -43 -59 -66 -68 -68 -68 -68] Vmem
	[  0   1   2   3   4   5   6   7   8   9  10] cell
with the knee at cell 4 ([OA]=40 and [OB]=60). At [OA]=[OB]=50, Vmem=-59mV,
which is pretty close to the final -68. So better, but not great.

Next try: 
	gating = 1 + Hill*20
This gives
	[-15 -15 -16 -21 -37 -53 -61 -63 -63 -63 -63]
	[  0   1   2   3   4   5   6   7   8   9  10]
Not really helping...

Let's try a different tack. From the set_Vmem_class1 experiments, we had
	[-15  -35  -45  -51  -56  -59  -61  -63  -65  -66  -68] Vmem (mV)
	[  1    4    7   10   13   16   19   22   25   28   31] Dk / 1e-18
The midpoint of the range [-15,-68]mV is -41mV. And we get -41mV at about
Dk=6e-18. Then a bit of algebra says we can build
	ratio = ([OA]/[OB])**N
	Dk = (30 * ratio) / (4 + ratio)
and it will give ratio=1 => DK=6e-18, which will give a nice midrange Vmem.
Does it actually work? That's what these set_Vmem_class2() experiments tell us.

In fact, using 
	Dk = (30 * ratio) / (1 + ratio) gives ratio=1 => Vmem=-58mV
	Dk = (30 * ratio) / (2 + ratio) gives ratio=1 => Vmem=-53mV
	Dk = (30 * ratio) / (3 + ratio) gives ratio=1 => Vmem=-49mV
	Dk = (30 * ratio) / (4 + ratio) gives ratio=1 => Vmem=-45mV
	Dk = (30 * ratio) / (5 + ratio) gives ratio=1 => Vmem=-43mV
We'll pick "(4 + ratio)". I'm not sure what the biological justification
for this model is -- but it certainly works well :-).

****************************************************************************
			Basic model analysis

We next analyze our cross-coupled NOR gates with char_NOR2().
For each NAB/NR/plus combination, we first find the stable point, then the
metastable point, and finally compute how far the metastable point is from the
midpoint of stable (OA+OB)/2. The data is below. We notice a few obvious things:

- Increasing NR greatly spreads the stable OA:OB apart, as expected. I'll call
  this "increasing the range."
- Increasing NAB and delta also increases the range, as explained above.

A few correlation coefficients:
- NR -> range  = .36
- NAB -> range = .57
- plus -> range= .63
- alpha -> range=-.82
- 50*beta->range=-.96

If we define mid = (stable OA + stable OB)/2, then we get
- 100/alpha -> mid = .76

The more interesting results is for the metastable-point location:
- Meta is always in [44,50]
- Increasing NR always pushes the metastable point towards 50.
And some correlation coefficients:
- 50*beta->meta = -.93
- range -> meta = .96
- NR -> meta    = .26

This is lousy. We want the metastable point to lie right in the middle of the
stable range; that would mean that when a 1-cell and a 0-cell fight, the fight
is fair. But in our case, we often have meta = mid + 11, meaning that the
metastable point is fairly close to the 1-cell stable value. This means that
a 1-cell can almost never convince a 0-cell to flip, but a 0-cell has a really
easy time flipping a 1-cell. That's not fair to tumors :-).
The mean value of meta-mid = 141/42=3.5, with a peak of 11.

NAB=3, NR=3, plus=15. Stable=35:56, meta=45=mid-0
NAB=3, NR=3, plus=20. Stable=28:65, meta=46=mid-0
NAB=3, NR=3, plus=25. Stable=24:71, meta=47=mid-1
NAB=3, NR=5, plus=5. Stable=30:55, meta=44=mid+1
NAB=3, NR=5, plus=10. Stable=21:63, meta=45=mid+3
NAB=3, NR=5, plus=15. Stable=16:69, meta=46=mid+4
NAB=3, NR=5, plus=20. Stable=12:73, meta=47=mid+4
NAB=3, NR=5, plus=25. Stable=10:77, meta=48=mid+4
NAB=3, NR=10, plus=5. Stable=18:57, meta=46=mid+8
NAB=3, NR=10, plus=10. Stable=8:63, meta=47=mid+11
NAB=3, NR=10, plus=15. Stable=4:69, meta=48=mid+11
NAB=3, NR=10, plus=20. Stable=2:73, meta=48=mid+11
NAB=3, NR=10, plus=25. Stable=1:77, meta=49=mid+9
NAB=5, NR=3, plus=10. Stable=31:61, meta=46=mid-0
NAB=5, NR=3, plus=15. Stable=22:74, meta=47=mid-1
NAB=5, NR=3, plus=20. Stable=18:81, meta=48=mid-2
NAB=5, NR=3, plus=25. Stable=16:86, meta=49=mid-2
NAB=5, NR=5, plus=5. Stable=23:61, meta=45=mid+3
NAB=5, NR=5, plus=10. Stable=14:71, meta=47=mid+4
NAB=5, NR=5, plus=15. Stable=9:79, meta=48=mid+4
NAB=5, NR=5, plus=20. Stable=7:84, meta=49=mid+3
NAB=5, NR=5, plus=25. Stable=5:88, meta=49=mid+2
NAB=5, NR=10, plus=5. Stable=10:62, meta=47=mid+11
NAB=5, NR=10, plus=10. Stable=3:71, meta=48=mid+11
NAB=5, NR=10, plus=15. Stable=1:79, meta=49=mid+9
NAB=5, NR=10, plus=20. Stable=1:84, meta=49=mid+7
NAB=5, NR=10, plus=25. Stable=0:88, meta=49=mid+5
NAB=10, NR=3, plus=5. Stable=30:63, meta=46=mid-0
NAB=10, NR=3, plus=10. Stable=17:83, meta=48=mid-2
NAB=10, NR=3, plus=15. Stable=14:91, meta=49=mid-3
NAB=10, NR=3, plus=20. Stable=13:95, meta=50=mid-4
NAB=10, NR=3, plus=25. Stable=12:97, meta=50=mid-5
NAB=10, NR=5, plus=5. Stable=13:72, meta=47=mid+4
NAB=10, NR=5, plus=10. Stable=6:86, meta=49=mid+3
NAB=10, NR=5, plus=15. Stable=4:93, meta=49=mid+1
NAB=10, NR=5, plus=20. Stable=4:97, meta=50=mid-0
NAB=10, NR=5, plus=25. Stable=3:98, meta=50=mid-1
NAB=10, NR=10, plus=5. Stable=2:72, meta=48=mid+11
NAB=10, NR=10, plus=10. Stable=0:86, meta=49=mid+6
NAB=10, NR=10, plus=15. Stable=0:93, meta=50=mid+3
NAB=10, NR=10, plus=20. Stable=0:97, meta=50=mid+1
NAB=10, NR=10, plus=25. Stable=0:98, meta=50=mid+1

------------------------

Next we used the new NOR2 function
	alpha = 1 + (50/(50+g_plus))**g_NAB
	beta = alpha ** (1/g_NR)
	out = alpha*100 / (1 + (50/(50+g_plus))**g_NAB + (OA/(50/beta))**g_NR)
This gave us much nicer results:
- the metastable point is now always at 50:50.
- the stable [OB] ranges from 99 to 100
- the stable [OA] ranges from 0 to 11, but that's still better than before.
- on average, meta = mid - 90/42 or about mid-2, with a max of mid-5.5.
- we'll use this one :-).

NAB=3, NR=3, plus=15. Stable=11:99, meta=50=mid-5
NAB=3, NR=3, plus=20. Stable=11:99, meta=50=mid-5
NAB=3, NR=3, plus=25. Stable=11:99, meta=50=mid-5
NAB=3, NR=5, plus=5. Stable=3:100, meta=50=mid-2
NAB=3, NR=5, plus=10. Stable=3:100, meta=50=mid-2
NAB=3, NR=5, plus=15. Stable=3:100, meta=50=mid-2
NAB=3, NR=5, plus=20. Stable=3:100, meta=50=mid-2
NAB=3, NR=5, plus=25. Stable=3:100, meta=50=mid-2
NAB=3, NR=10, plus=5. Stable=0:100, meta=50=mid-0
NAB=3, NR=10, plus=10. Stable=0:100, meta=50=mid-0
NAB=3, NR=10, plus=15. Stable=0:100, meta=50=mid-0
NAB=3, NR=10, plus=20. Stable=0:100, meta=50=mid-0
NAB=3, NR=10, plus=25. Stable=0:100, meta=50=mid-0
NAB=5, NR=3, plus=10. Stable=11:99, meta=50=mid-5
NAB=5, NR=3, plus=15. Stable=11:99, meta=50=mid-5
NAB=5, NR=3, plus=20. Stable=11:99, meta=50=mid-5
NAB=5, NR=3, plus=25. Stable=11:99, meta=50=mid-5
NAB=5, NR=5, plus=5. Stable=3:100, meta=50=mid-2
NAB=5, NR=5, plus=10. Stable=3:100, meta=50=mid-2
NAB=5, NR=5, plus=15. Stable=3:100, meta=50=mid-2
NAB=5, NR=5, plus=20. Stable=3:100, meta=50=mid-2
NAB=5, NR=5, plus=25. Stable=3:100, meta=50=mid-2
NAB=5, NR=10, plus=5. Stable=0:100, meta=50=mid-0
NAB=5, NR=10, plus=10. Stable=0:100, meta=50=mid-0
NAB=5, NR=10, plus=15. Stable=0:100, meta=50=mid-0
NAB=5, NR=10, plus=20. Stable=0:100, meta=50=mid-0
NAB=5, NR=10, plus=25. Stable=0:100, meta=50=mid-0
NAB=10, NR=3, plus=5. Stable=11:99, meta=50=mid-5
NAB=10, NR=3, plus=10. Stable=11:99, meta=50=mid-5
NAB=10, NR=3, plus=15. Stable=11:99, meta=50=mid-5
NAB=10, NR=3, plus=20. Stable=11:99, meta=50=mid-5
NAB=10, NR=3, plus=25. Stable=11:99, meta=50=mid-5
NAB=10, NR=5, plus=5. Stable=3:100, meta=50=mid-2
NAB=10, NR=5, plus=10. Stable=3:100, meta=50=mid-2
NAB=10, NR=5, plus=15. Stable=3:100, meta=50=mid-2
NAB=10, NR=5, plus=20. Stable=3:100, meta=50=mid-2
NAB=10, NR=5, plus=25. Stable=3:100, meta=50=mid-2
NAB=10, NR=10, plus=5. Stable=0:100, meta=50=mid-0
NAB=10, NR=10, plus=10. Stable=0:100, meta=50=mid-0
NAB=10, NR=10, plus=15. Stable=0:100, meta=50=mid-0
NAB=10, NR=10, plus=20. Stable=0:100, meta=50=mid-0
NAB=10, NR=10, plus=25. Stable=0:100, meta=50=mid-0

****************************************************************************
			Measure_spread experiments

Here are the results of the measure_spread() experiments.
We look, over a range of NR=3,5,10 and three values of GJ-conductivity Kd,
at the sharpness of the border.
To find the "sharpness," we build a straight 1D chain of 12 cells connected by
ungated GJs. Six cells on the right are initialized to "one" and the rest are
all initialized to "zero." Border sharpness is defines as
	([OA] in cell 6) / ([OA] in cell 5).
We then do the same thing, but for gated GJs, and with cells depolarizing iff
their [OA]>[OB].
The results are in measure_spread.txt. Here's a sample

	NR=3, Kd_GJ=5e-15: border sharpness is 6.339 ungated and 8.123 gated
	NR=3, Kd_GJ=1e-14: border sharpness is 5.063 ungated and 7.713 gated
	NR=3, Kd_GJ=1e-13: border sharpness is 1.715 ungated and 4.738 gated

	NR=5, Kd_GJ=5e-15: border sharpness is 15.95 ungated and 27.84 gated
	NR=5, Kd_GJ=1e-14: border sharpness is 10.9 ungated and 24.28 gated
	NR=5, Kd_GJ=1e-13: border sharpness is 2.634 ungated and 9.376 gated

	NR=10, Kd_GJ=5e-15: border sharpness is 33.78 ungated and 168.1 gated
	NR=10, Kd_GJ=1e-14: border sharpness is 18.07 ungated and 93.98 gated
	NR=10, Kd_GJ=1e-13: border sharpness is 3.275 ungated and 14.31 gated

- Obviously, increasing Kd_GJ hurts border sharpness, for obvious reasons.
- Increasing NR increases border sharpness; it means that the cells push back
  harder against any deviation from their "normality."
- Gating increases border sharpness as well. Since we use the same Kd values
  for both gated and ungated sims, the gated sims simply have an extra resistor
  separating the two halves.

Here's a fuller data set for the case of NR=5, Kd_GJ=1e-14:

	NR=5, Kd_GJ=1e-14: border sharpness is 10.9 ungated and 24.28 gated
	[100 100 100 100 100  95   9   3   3   3   3   3] OA ungated
	[100 100 100 100 100  99   4   3   3   3   3   3] OA gated
	[  3   3   3   3   3   9  95 100 100 100 100 100] OB ungated
	[  3   3   3   3   3   4  99 100 100 100 100 100] OB gated
	[-15 -15 -15 -15 -15 -18 -67 -68 -68 -68 -68 -68] Vmem ungated
	[-15 -15 -15 -15 -15 -15 -68 -68 -68 -68 -68 -68] Vmem gated
	[  0   1   2   3   4   5   6   7   8   9  10  11] cell #

The most surprising thing is that, while the [OA] shows a substantial
improvement from gating, Vmem shows very little change. In both cases, we're
good enough pre-gating that there's not much improvement to be made.

	NR=5, Kd_GJ=1e-13: border sharpness is 2.634 ungated and 9.376 gated
	[100 100  99  98  93  76  29  11   6   4   3   3] OA ungated
	[100 100 100 100  98  94  10   5   4   3   3   3] OA gated
	[  3   3   4   6  11  29  76  93  98  99 100 100] OB ungated
	[  3   3   3   4   5  10  94  98 100 100 100 100] OB gated
	[-15 -15 -15 -15 -15 -18 -67 -68 -68 -68 -68 -68] Vmem ungated
	[-15 -15 -15 -15 -15 -15 -68 -68 -68 -68 -68 -68] Vmem gated
	[  0   1   2   3   4   5   6   7   8   9  10  11] cell #

Jumping up to Kd_GJ=1e-13 now shows a much bigger improvement in [OA] -- but
still not much Vmem improvement. The reason is that when the code changes the
GJ diffusivity of OA and OB, it *does not* change the GJ diffusivity of Na,
K and Cl. And I suspect those values are already low enough to keep voltages
pretty sharp.

****************************************************************************
		Zombie_sims_border_sharpness sims

Next come the zombie_sims_border_sharpness sims. These sims differ from the
measure_spread() sims in that, rather than looking at gated-vs-ungated at the
*same* Kd, attempt to level the playing field by looking at border gain at
"appropriate" Kd values for each case. Here are the results for 1D (saved in
zombie_sims_border_sharpness_1D.txt).

	1x6 tumor in a 1x25 field
	NR=3. Final useful Kd range is 1.94e-13/6.5e-13=3.357x ug, 9.16e-13/1.97e-12=2.152x g,
		bGain min=1.46/2.23/3.74, bGain mid=1.32/1.98/3.01, bGain max=1.24/1.75/2.43 ug/g/pg
	NR=5. Final useful Kd range is 4.08e-13/1e-12=2.451x ug, 2.19e-12/4.83e-12=2.210x g,
		bGain min=1.54/2.36/4.56, bGain mid=1.41/2.06/3.72, bGain max=1.34/1.8/3.06 ug/g/pg
	NR=10. Final useful Kd range is 9.37e-13/2.04e-12=2.172x ug, 3.62e-12/7.82e-12=2.159x g,
		bGain min=1.45/2.29/3.8, bGain mid=1.35/2.02/3.21, bGain max=1.29/1.76/2.74 ug/g/pg

The "useful Kd range" of, e.g., 1.94e-13/6.5e-13=3.357x, means that a Kd of
1.94e-13 was the minimum Kd needed to cure the tumor, and that Kd=6.5e-13 was
the maximum Kd that didn't collapse the entire field, and that 6.88e-13/1.94e-13
is 3.357x. That data is replicated for the ungated (ug) and gated (g) setup.

The data also shows border-gain results at three Kd values: min (i.e.,
1.94e-13), max (i.e., 6.5e-13) and mid (the geographic mean of the two). It
gives ungated border gain at the three ungated-specific Kd values; gated border
gain at the three gated-specific Kd values; and also post-gated border gain
(set up without gating, using the three ungated-specific Kd values and then add
gating after the sim settles).

Looking at the 1D results, we see that
- the useful range is always larger (by about 1.5-2x) for ungated than gated.
  I don't know of any good intuitive reason why; that's just what the sims show.
- the border gain is always better (by about 30%) for gated than ungated (even
  with each taken at its own relevant Kd values). Again, I don't know of any
  intuitive reason why. 
- the border gain gets smaller as Kd gets larger. This is obvious, as noted in
  the measure_spread data above.
- changing NR doesn't affect things much. This is different than what we
  observed in the measure_spread data, and is because we're now taking all of
  our data at the relevant Kd for each NR.
- The actual Kd values for ungated are always lower than for gated; this is
  partly because a gated GJ never conducts at 100%, and partly because gating
  allows the tumor to isolate itself.
- Most importantly: post-gating is the clear winner. This is because it lets us
  take advantage of the lower Kd values for ungated setup, and then gets the
  isolation effect from gating.

6x6 tumor in a 25x25 field
NR=3. Final useful Kd range is 1.94e-14/6.5e-13=33.488x ug, 1.13e-13/9.99e-13=8.809x g,
	bGain min=3.73/3.75/6.66, bGain mid=1.66/2.38/3.69, bGain max=1.24/1.57/1.81 ug/g/pg
NR=5. Final useful Kd range is 5.65e-14/1e-12=17.732x ug, 2.81e-13/2.62e-12=9.320x g,
	bGain min=3.64/4.15/10.1, bGain mid=1.8/2.47/4.41, bGain max=1.34/1.61/2.24 ug/g/pg
NR=10. Final useful Kd range is 1.33e-13/2.04e-12=15.284x ug, 5.18e-13/4.54e-12=8.761x g
	bGain min=2.82/3.74/8.32, bGain mid=1.67/2.33/3.63, bGain max=1.29/1.55/2.04 ug/g/pg

We now notice that
- the useful Kd ranges have gotten much larger than in 1D. The upper end
  (determined by border collapse) is unchanged, since the 2D problem is
  symmetrically identical to 1D. However, the Kd needed to remove a tumor is now
  much smaller, since it can attack the tumor at a corner.
- the observation about gated GJs having a smaller range is unchanged.
- with the lower Kd values, we also get larger border gains. However, the
  general patterns are relatively unchanged.

***********************************************************************
			Surround_flip experiments

Next: the surround_flip() experiments (with output to zombie_surround_flip
for the original experiments and zombie_surround_flip_chain for the chain
experiments described below).
These analyses see how hard it is to flip a cell. For each reasonable
parameter set, we start with one cell initialized to OA=100, OB=0. We surround
it with two cells, then three cells, etc., all initialized to OA=0, OB=100.
We do an initial sim with all cells disconnected to let the cells settle to
their stable values. Then, for each number of cells around it, we gradually up
the GJ size until the inner cell flips and *all* cells reach the same final
value as the disconnected outer cells initially did.

Most importantly, there are zero cases of "weird binary search." No matter what
NR is, two outer cells can *always* turn an inner cell into a zombie. I.e., two
surrounding cells *always* succeeded at flipping the central cell, and doing so
*completely*. By completely, we mean that every single cell reached the same
end OA:OB values as the exterior cells started at. This is a powerful
demonstration of the zombie effect.

Next, there are clearly attractors at work here. Let's look at the case of
NR=5, surround=2. We have
	GJ_scale=0	    => 100:3 3:100 3:100 
	GJ_scale=2.5e-14    => 74:30 14:91 14:91
	GJ_scale=2.6343e-14 => 69:34 14:91 14:91
	GJ_scale=2.6367e-14 => 4:99  4:100 4:100
I.e., tiny changes in GJ_scale result in tidal-wave changes in the phenotype.

Obviously, the more surrounding cells, the easier it is to flip the central
cell. For example:
	NR=5, surround=2: final GJ_scale=2.64e-14
	NR=5, surround=3: final GJ_scale=1.66e-14
	NR=5, surround=4: final GJ_scale=1.21e-14
	NR=5, surround=5: final GJ_scale=9.54e-15

Doing our math, 2.637*2=5.274; 1.660*3=4.980; 1.212*4=4.828; .0954*5=4.77
I.e., it's reasonably close to requiring a constant amount of current to flip
the inner cell.

We expect to see that the higher NR is, the harder a cell is to flip. Let's see:
	NR=3, surround=2: final GJ_scale=1.02e-14
	NR=5, surround=2: final GJ_scale=2.64e-14
	NR=10, surround=2: final GJ_scale=5.28e-14
And that's exactly what we see -- quite strongly.

Next, our small-signal model is notoriously poor at predicting the final
GJ_scale. Let's look at NR=10.
	surround=2: final V0=67.551, V1=16.322,Vth0=100.000, Vth1=0.098, R_dec0=1.91e+15, R_dec1=9.55e+14, R_GJ=3.02e+15
	surround=3: final V0=67.590, V1=10.901,Vth0=100.000, Vth1=0.098, R_dec0=1.91e+15, R_dec1=6.37e+14, R_GJ=3.34e+15
	surround=4: final V0=67.597, V1=8.198, Vth0=100.000, Vth1=0.098, R_dec0=1.91e+15, R_dec1=4.77e+14, R_GJ=3.5e+15
	surround=5: final V0=67.612, V1=6.575, Vth0=100.000, Vth1=0.098, R_dec0=1.91e+15, R_dec1=3.82e+14, R_GJ=3.6e+15
We see again that R_GJ (which is the effective GJ resistance in the small-signal
model) is roughly equal in all four cases. But we also see that the final
V0=67 and V1<=16 does not reach the metastable point of 50:50, and thus "should"
not flip the cell! It obviously does -- because out small-signal model is not
great at predicting large-signal behavior. In this case, we have four
interacting state nodes (OA and OB both the inner and the merge-outer cells),
which is more complex than our simple two-element metastability model.

Finally, look at zombie_surround_flip_chain. This is done by setting
central_node=False in surround_flip_sims(). Rather than putting the single
100:0 node in the hub of a star, it puts the 100:0 node at the left of a
straight chain. Some results from the same NR=5 case:

	NR=5, surround=2: final GJ_scale=5.91e-14
	NR=5, surround=3: final GJ_scale=5.89e-14
	NR=5, surround=4: final GJ_scale=5.88e-14
	NR=5, surround=5: final GJ_scale=5.88e-14

Comparing this to our original
	NR=5, surround=2: final GJ_scale=2.64e-14

we see first that for surround=2, it takes over 2x as much GJ strength as before
to flip the 100:0 node. This isn't surprising: our two 0:100 cells are now in
series rather than parallel.
Next, making the chain longer is of very little help fliping the 100:0 node.
All the extra cells do is help prop up the leftmost 0:100 node when the 100:0
node is fighting it. And the fight is probably over before a distant cell can
help much. Here's the final non-zombie sim result for n_surround=5 for the same
NR=5 case:
	68:36 19:86 7:97  4:99  3:100 3:100 
Clearly cell[3] is not helping a lot.

The most notable thing about the chain sims is that, again, every single
parameter combination resulted in a zombie flip of the "bad" node.

****************************************************************************
			Isolation experiments.

These are done with zombie_tumor_isolate(). They look at how hard it is to "fix"
a tumor, when the tumor uses various strategies to isolate itself from being
fixed.
They also show that larger tumors are harder to fix. This is not a SA/volume
thing (these sims don't overexpress GJs, and so the tumor doesn't act as a
syncytium), but for slightly more complex reasons explained below.

They have (in the 1D version) a 1x6 tumor in a 1x25 field of 0s. With each
isolation strategy, we merely find the minimum Kd that fixes the tumor. With
Kd=0, the tumor obviously remains as is; with large enough Kd, the fact that
there are many more 0 cells than 1 cells implies that the field will become a
uniform zero.

The tumor has 4 strategies:
1. no tricks; GJs are ungated, and GJ expression level is identical throughout
   the field.
2. Vmem gating: tumor cells are depolarized and GJs are Vmem gated. This creates
   a GJ border around the tumor for isolation.
3. Underexpression: tumors underexpress GJs. This is implemented by having any
   GJ that touches a tumor cell cut its Kd down by half.
4. Both strategy #2 and #3.

Here are results for the 1x6 tumor in a 1x25 field (from zombie_tumor_isolate_1D).
    NR=3: no tricks/Vmem-gating/underexp/both: 1.94e-13/9.16e-13/9.3e-14/1.33e-12
    NR=5: no tricks/Vmem-gating/underexp/both: 4.08e-13/2.18e-12/2.81e-13/3.26e-12
    NR=10: no tricks/Vmem-gating/underexp/both: 9.38e-13/3.62e-12/6.97e-13/5.37e-12
The four numbers for each NR value are the minimum Kd that will fix the tumor.

We see that
- Adding Vmem gating is a hugely effective isolation strategy; it improves
  resistance to Kd by about 4x.
- GJ underexpression is a lousy strategy -- it hurts a bit rather than helps!
  Why? Consider the outermost, border-adjacent tumor cell. Yes, it is now less
  connected to its normal neighbor. But it's also less connected to its friendly
  tumor interior. Furthermore, the tumor is less able to act as a syncytium than
  the healthy tissue can, which is a very bad thing.

Moving to 2D, though, the story for an 6x6 tumor in a 25x25 field is different
(from zombie_tumor_isolate_2D)
    NR=3: no tricks/Vmem-gating/underexp/both: 1.94e-14/2.32e-13/3.47e-14/4.08e-13
    NR=5: no tricks/Vmem-gating/underexp/both: 6.24e-14/6.9e-13/1.02e-13/1.13e-12
    NR=10: no tricks/Vmem-gating/underexp/both: 1.52e-13/1.15e-12/2.19e-13/1.83e-12

With this size, underexpression is now a help rather than a hindrance. The
difference? With a 1D field, the tumor's single border cell has one good
neighbor and one tumor neighbor. Thus, its survival is a matter of the border
cell drawing help from its friends, and having the tumor behave as less of a
syncytium than the healthy tissue is fatal.
However, with a 2D field, the tumor corners are now weak points. In this case,
saving the corner cell is all about preventing the two healthy cells from
ganging up on the tumor corner cell. Thus, weakening its ties to both the tumor
and the tissue does help.

Finally, a 6x6x6 tumor in a 25x25x25 field (zombie_tumor_isolate_6x6x6)
    NR=3: no tricks/Vmem-gating/underexp/both: 1.15e-14/1.27e-13/2.12e-14/2.36e-13
    NR=5: no tricks/Vmem-gating/underexp/both: 3.17e-14/3e-13/5.76e-14/5.48e-13
    NR=10: no tricks/Vmem-gating/underexp/both: 6.92e-14/4.82e-13/1.16e-13/8.58e-13

More results from zombie_tumor_isolate_2D:
	  1x1       2x2     3x3       4x4     5x5     6x6       7x7     8x8
  NR=3  9.51e-14 1.93e-13 2.65e-13 3.25e-13 3.72e-13 4.08e-13 4.39e-13 4.72e-13
  NR=5  2.30e-13 4.62e-13 6.26e-13 7.74e-13 8.89e-13 9.73e-13 1.04e-12 1.13e-12
  NR=10 3.93e-13 7.54e-13 1.01e-12 1.23e-12 1.41e-12 1.55e-12 1.67e-12 1.83e-12
Each column shows a different tumor size, all in the usual 25x25 field. Each
is the minimum Kd value needed to fix the tumor (with both voltage gating and
GJ underexpression).
The take-away is that bigger tumors are more difficult to fix. Partially, this
is because of the usual surface-area-to-volume argument. But not really; these
tumors have underexpressed GJs, and are thus not functioning as syncytiums.
Instead, remember that the fight here is at the border. An nxn tumor has 4n-4
border cells, with 4n good cells surrounding it. The larger the tumor, the
closer this is to (relatively) a fair fight -- the less the tumor is at a
disadvantage. Thus, the good tissue requires a higher Kd to defeat the tumor.

****************************************************************************
			Metastasis experiments

These experiments examine what it takes for a tumor to metastasize -- by which
we mean that it can invade *neighboring* cells. We use zombie_sims_metastasis(),
which creates a 1-spot tumor on a 0 background (with [A]=[B]=50 throughout).

The trick is that we'll use a GJ gating function, "class tumor_power," to
increase the GJ strength in the tumor. This turns the tumor into a big mass that
can then overwhelm/convert neighbors one by one. Class tumor_power computes the
ratio=([OA]/[OB])**10 in each cell. It then computes the Hill function
factor = 1 + (self.power_amt-1)*ratio/(1+ratio), so that cells with [OA]>>[OB]
have a gating factor of power_amt, and cells with [OA]<<[OB} have a gating
factor of 1.
Of course, we want gating factors for *GJs*, not for cells. Thus, each GJ looks
at the two cells it spans, and computes
	my_factor = min_factor + (max_factor-min_factor)*GJ_NCMM
(where GJ_NCMM stands for GJ neighbor-channel min/max).

In actuality, we don't use class tumor_power, but instead use the pair class
tumor_power2_gen_decay and class tumor_power2_GJ_gate. Details in a bit...

Here's the 831 (Aug 31 2024) data
    First, GJ_power vs. tumor size
         1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19
      1 [0   0   0   0   0   0   0   0  12  12  12  12  12  12  12  12  12  21  24]
     10 [0   0  43  76 103 125 133 155 172 174 184 184 192 200 209 217 228 236 240]
    100 [0   0  67 111 131 149 164 180 196 203 217 228 233 234 236 242 250 256 260]
    So the big jump is from power=1 to power=10.
    But I would have thought that benefit much more from big power than
    smaller tumors do; that's not what we see.

    Next, GJ_NCMM vs. GJ_power
              1  10   100
	  0 [51 650   654]
	 .5 [51 1131 1310]
	  1 [51 1090 1393]
    So NCMM=1 doesn't really do better than .5 (and occasionally does worse!).

    Next, tau vs. tumor size
         1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19
    .01 [0   0  25  41  55  63  67  76  90  92  97 101 103 105 105 109 115 122 124]
     .1 [0   0  25  41  55  65  67  78  90  92 100 103 103 105 106 111 117 122 124]
      1 [0   0  25  45  57  69  74  82  94  99 105 105 109 109 115 117 123 130 132]
     10 [0   0  35  60  67  77  89  99 106 106 111 115 122 127 131 134 135 139 144]
    So tau isn't a big hammer.

    Next, tau vs. GJ_NCMM
	       0 0.5   1
	.01 [319 581 590]
	 .1 [325 585 594]
	  1 [343 620 627]
	 10 [368 706 723]
    Tau looks a bit better here. But we can probably remove tau=.01.

-----------------------------------------------
Cool metastasis case: oscillation.
    This comes from metastasis_case_2036. It's 2D, NR=10, 1x4x4 tumor,
    Kd_field=2.32e-15, GJ_power=100, GJ_NCMM=1, GJ_tau=10, neighbor-depol
    It uses zombie.py22, which has a single 'm' that drives both GJ growth and
    depolarization of healthy cells. Its neighbor-depol scheme is that after a
    cell has been a tumor for about 10 seconds, it forcibly depolarizes its
    neighbors. The narrative for why we oscillate:
      -	With the 3x3 tumor, cells[0:3,0:3] are tumor and other cells aren't.
	GJs at the tumor-healthy border start out 100x strong; the healthy
	border (cells[0:3,4], cells[4,0:3], cells[4,4]) are all depolarized.
	Params give R_decay=1.9e15; R_GJ_healthy=1.4e17, R_GJ_tumor=1.4e15.
      - At the sim start, the corner tumor cell[3,3] is immediately overwhelmed
	by its two healthy neighbors ([3,4] and [4,3]) and quickly becomes 
	healthy.
      - 10 seconds later, the GJs connecting [3,3] to [3,4] and [4,3] become
	weak (1x rather than 100x), and off (since [3,4] and [4,3] now
	hyperpolarize again.
      - Then, since [3,3] is still supported by tumor cells [2,3] and [3,2], it
	flips back to be a tumor.
      - The whole cycle then repeats again!
    The key in this example is that the powered-up tumor GJs are still only as
    strong as the GRN, but not stronger -- which means that syncytiums don't
    really happen. And so the battle is waged only at borders.

-----------------------------------------------
A case to show why bigger tumors invade better
    Let's start with NR=5, not gated.
    And tumor_power=100, GJ_NCMM=1, both of which are very pro-tumor.
    With a 4x4 tumor in a 6x6 field (so there's only a one-cell-thick layer
    around the tumor), the tumor is still squashed! It turns out that it's a
    SA/vol-ratio issue -- here's why:
     At t=.0001, we have
	z=...    0     1     2     3     4     5   
	x=0,y=0 9:79  9:79  9:79  9:79  9:79  9:79  
	x=0,y=1 9:79  79:9  79:9  79:9  79:9  9:79  
	x=0,y=2 9:79  79:9  79:9  79:9  79:9  9:79  
	x=0,y=3 9:79  79:9  79:9  79:9  79:9  9:79  
	x=0,y=4 9:79  79:9  79:9  79:9  79:9  9:79  
	x=0,y=5 9:79  9:79  9:79  9:79  9:79  9:79  
     At t=.1, it's
	z=...    0     1     2     3     4     5   
	x=0,y=0 9:79  30:59 34:55 34:55 30:59 9:79  
	x=0,y=1 30:59 48:41 56:33 56:33 48:41 30:59 
	x=0,y=2 34:55 56:33 68:20 68:20 56:33 34:55 
	x=0,y=3 34:55 56:33 68:20 68:20 56:33 34:55 
	x=0,y=4 30:59 48:41 56:33 56:33 48:41 30:59 
	x=0,y=5 9:79  30:59 34:55 34:55 30:59 9:79  
    It's an even fight in most of the array, but the untouched corner cells at
    [0,0], [0,5], [5,0] and [5,5] eventually win the fight and the tumor
    vanishes. In the big picture, this is not surprising. Our tumor is 4x4 and
    has 16 cells. But it's immediate surroundings, fighting it, are four
    boundaries of four cells each -- or 16 cells. So essentially its linear
    surface area and interior "volume" are both 16. We need a bigger tumor, to
    have a larger volume/surface ratio.

-----------------------------------------------

This case shows why, when we newly flip a healthy cell to be tumor-like, we
don't want to grow big GJs from that cell until it's well and truly flipped.
In this example, class tumor_power connects a cell to its neighbor the very
instant that a cell has [OA]>[OB]. But at that point, the cell is not yet well
and truly flipped. So it allows a cell that's only partially converted to being
a tumor to start overexpressing GJs to the nearby healthy tissue -- which can
then flip the cell back to being healthy!

    We'll use a 1x10 1D field, with a 3-cell tumor at the very left. The params:
	Kd=4e-13, GJ_power=100, GJ_NCMM=0.5, decay=0.01
    Here is a time sequence:
	t=0.0001...
	z=...    0     1     2     3     4     5     6     7     8     9   
	x=0,y= 0 79:9  79:9  78:10 10:78 9:79  9:79  9:79  9:79  9:79  9:79  
	Most common gatings are [1.0]
	GJ from (0,0,0)=[79:9] to (0,0,1)=[79:9], gatings=[100.0]
	GJ from (0,0,1)=[79:9] to (0,0,2)=[78:10], gatings=[100.0]
	GJ from (0,0,2)=[78:10] to (0,0,3)=[10:78], gatings=[50.5]
    So we start as expected: three "1" tumor cells on the left, interconnected
    with two 100x GJs, and a 50x GJ connecting them to their intended victim.
    Everything else is interconnected with 1x GJs.

     t=0.0052...
	z=...    0     1     2     3     4     5     6     7     8     9   
	x=0,y= 0 76:12 72:16 62:26 36:52 9:79  9:79  9:79  9:79  9:79  9:79  
	Most common gatings are [1.0]
	GJ from (0,0,0)=[76:12] to (0,0,1)=[72:16], gatings=[100.0]
	GJ from (0,0,1)=[72:16] to (0,0,2)=[62:26], gatings=[99.992]
	GJ from (0,0,2)=[62:26] to (0,0,3)=[36:52], gatings=[51.674]
	GJ from (0,0,3)=[36:52] to (0,0,4)=[9:79], gatings=[2.182]
    The three tumor cells are winning: cell[2] has dropped by 14, but cell[3]
    rises by 27. Good so far.
     t=0.0103...
	z=...    0     1     2     3     4     5     6     7     8     9   
	x=0,y= 0 71:17 66:21 58:29 44:43 15:73 9:79  9:79  9:79  9:79  9:79  
	Most common gatings are [1.0]
	GJ from (0,0,0)=[71:17] to (0,0,1)=[66:21], gatings=[99.999]
	GJ from (0,0,1)=[66:21] to (0,0,2)=[58:29], gatings=[99.944]
	GJ from (0,0,2)=[58:29] to (0,0,3)=[44:43], gatings=[78.8]
	GJ from (0,0,3)=[44:43] to (0,0,4)=[15:73], gatings=[29.355]
    Now we're starting to see trouble. Cell[3] has become a weak "one" (44:43).
    This is still enough to crank up the GJ between cells #3 and #4 -- which
    will now let the good cells gang up and quash the tumor:
      t=0.056
	z=...    0     1     2     3     4     5     6     7     8     9   
	x=0,y= 0 50:38 49:38 48:40 46:42 42:46 28:60 9:78  9:79  9:79  9:79  
	Most common gatings are [1.0]
	GJ from (0,0,0)=[50:38] to (0,0,1)=[49:38], gatings=[93.592]
	GJ from (0,0,1)=[49:38] to (0,0,2)=[48:40], gatings=[89.699]
	GJ from (0,0,2)=[48:40] to (0,0,3)=[46:42], gatings=[78.739]
	GJ from (0,0,3)=[46:42] to (0,0,4)=[42:46], gatings=[48.975]
	GJ from (0,0,4)=[42:46] to (0,0,5)=[28:60], gatings=[14.149]
	GJ from (0,0,5)=[28:60] to (0,0,6)=[9:78], gatings=[1.021]
    Cells #3 and #4 are now virtually identical, since their connecting GJ is
    50x strong. That will let them fight the tumor:
      t=0.1071...
	z=...    0     1     2     3     4     5     6     7     8     9   
	x=0,y= 0 45:42 45:43 45:43 43:44 42:46 37:51 14:73 9:79  9:79  9:79  

    How do we fix this? By adding a new GJ gating tumor_power2_GJ_gate(). When a
    cell flips to 1, this gating doesn't pump up the cells connecting GJs
    immediately. Instead, it leaves a built-in delay. In the case here, the
    delay will allow cells #0-2 to totally flip cell #3 before upping the
    3-to-4 GJ strength. In fact, it works fine :-)

----------------------------------------
1x6 tumors
   For all of these cases, R_decay=1.9e+15.
   hyperpolarized:
	68 NR=3, 74 NR=5, 72 NR=10
	Now look only at NR=5
	Kd	   R_field    Results
	1e-16	   3.2e+18    36 const
	4.81e-16   6.6e+17    36 const
	2.32e-15   1.4e+17    28 const,  8 grew (power=100,GJ_NCMM=.5/1,tau=*)
	1.12e-14   2.8e+16    28 const,  8 grew (power=100,GJ_NCMM=.5/1,tau=*)
	5.37e-14   5.9e+15    20 const, 16 grew (po=10/100,GJ_NCMM=.5/1,tau=*)
	2.59e-13   1.2e+15    12 const, 24 grew (power=10/100, GJ_NCMM=*, tau=*)
	1.25e-12   2.5e+14    18 shrunk (power=1,   GJ_NCMM=*, tau=*)
					(power=100, GJ_NCMM=.5/1, tau=.01/.1/1)
			      18 grew   (power=10,  GJ_NCMM=*, tau=*)
					(power=100, GJ_NCMM=0, tau=*)
					(power=100, GJ_NCMM=.5/1, tau=10)
	6.00e-12   5.3e+13    36 shrunk

   depolarized:
	How many grew? 84 NR=3, 98 NR=5, 89 NR=10.
	Now look only at NR=5
	1e-16	     36 const (same as hypol)
	4.81e-16     36 const (same as hypol)
	2.32e-15     28 const,  8 grew (same as hypol)
	1.12e-14     28 const,  8 grew (same as hypol)
	5.37e-14     20 const, 16 grew (same as hypol)
	2.59e-13     36 grew
	1.25e-12     6 shrunk (power=100, GJ_NCMM=.5/1,tau=.01/.1/1)
		     30 grew (power=1/10, GJ_NCMM=*, tau=*)
			     (power=100, GJ_NCMM=0, tau=*)
			     (power=100, GJ_NCMM=.5/1, tau=10)
	6.00e-12     36 shrunk (same as hypol)

   Takeaways:
      -	At low Kd, the cells are isolated and everything stays constant.
      -	At high Kd, we always collapse to a single syncytium and the tumor is
	absorbed. Why did a large Kd preclude nearly all metastases, even with
	power=100? Because once Kd is such that the GJ conductance is much
	larger than the decay conductance, the field has become a syncytium,
	and it's a larger syncytium than the tumor. The depol strategy might
	be able to beat this, since it disconnects the nearest healthy cells
	from their syncytium. But it's only a partial disconnect.
      - At fairly low Kd, where the GJ conductance is still weaker than the GRN
	conductance, there's only one route to growing. First, since the border
	GJ isn't strong enough to overwrite a healthy cell, we need GJ_NCMM>0.
	Second, since even GJ_NCMM=1 can only make the border GJ as strong as
	the (powered) GJs in the tumor, this case can only work if GJ_power is
	big enough so that GJ_conductance * GJ_power is big enough to overwrite
	the GRN -- so GJ_power must be pretty big as well as GJ_NCMM>0.
	So the ability to grow without needing GJ_power=100 only happens when
	Kd_field is getting fairly big.
      -	GJ_tau almost never matters. It only matters for Kd_field=1.25e-12,
	which is the highest Kd_field that's not a syncytium and which is thus
	*almost* a syncytium. So it's a case where the healthy tissue really can
	clobber the tumor if we allow it to, which is why GJ_tau matters.
	Specifically, it's for power=100;GJ_NCMM=.5/1, where GJ_tau<10 shrinks
	and >=10 grows. This is the usual issue of a fast tau meaning that
	we connect a barely-switched cell to the mass of healthy tissue before
	it's fully tumor-zombied, and thus give the healthy tissue a route to
	convert the tumor.
      -	We *almost* never grow at power=1. But there are a few exceptions:
	Depol/Kd_field=1.25e-12/power=1/GJ_NCMM=*/GJ_tau=*.
	Kd_field=1.25e-12 is the largest Kd_field that doesn't turn the healthy
	tissue into a syncytium, and is the only such Kd_field that gives us a
	GJ conductance that is large enough to overwrite a GRN. That's why it
	can work at GJ_power=1. And with GJ_power=1, GJ_NCMM becomes totally
	irrelevant.
------------------------
Now look at 6x6 tumors (from zombie_results/zombie_metas_6x6_hypolTumor):
   Again, R_decay=1.9e+15 for all of these cases.

	41 NR=3, 47 NR=5, 40 NR=10
	Now look only at NR=5
	Kd	   R_field	Results
	1e-16	     3.2e+18	36 const
	4.81e-16     6.6e+17	36 const
	2.32e-15     1.4e+17	28 const,  8 grew (power=100,GJ_NCMM=.5/1,tau=*)
	1.12e-14     2.8e+16	24 const,  4 shru (power=10,GJ_NCMM=1,tau=*)
				  	   8 grew (power=100,GJ_NCMM=.5/1, tau=*)
	5.37e-14     5.9e+15	20 const,  2 shrunk (po=100,GJ_NCMM=1,tau=.01/.1)
				  	   14 grew (power=10, GJ_NCMM=.5/1, tau=*)
				  		   (power=100, GJ_NCMM=.5, tau=*)
				  		   (power=100, GJ_NCMM=1, tau=1/10)
	2.59e-13     1.2e+15	19 shrunk (power=1, GJ_NCMM=*, tau=*)
					  (power=100,GJ_NCMM=.5,tau=.01/.1/1)
					  (power=100,GJ_NCMM=1,tau=*)
				17 grew   (power=10,GJ_NCMM=*,tau=*)
					  (power=100,GJ_NCMM=0,tau=*)
					  (power=100,GJ_NCMM=.5,tau=10)
	1.25e-12     2.5e+14	36 shrunk
	6.00e-12     5.3e+13	36 shrunk

	For 1.12e-14, why did power=10/GJ_NCMM=1/tau=* all shrink but power=1 all const?
        Was tau=10 not big enough?

   Depolarized (from zombie_results/zombie_metas_6x6_depolTumor):

	50 NR=3, 48 NR=5, 45 NR=10
	Now look only at NR=5
	Kd	   R_field	Results
=	1e-16	     3.2e+18	36 const
=	4.81e-16     6.6e+17	36 const
=	2.32e-15     1.4e+17	28 const,  8 grew (power=100,GJ_NCMM=.5/1,tau=*)
=	1.12e-14     2.8e+16	24 const,  4 shru (power=10,GJ_NCMM=1,tau=*)
					   8 grew (power=100,GJ_NCMM=.5/1,tau=*)
+1	5.37e-14     5.9e+15	20 const, 15 grew (po=10,GJ_NCMM=.5/1,tau=*)
					  (power=100,GJ_NCMM=.5,tau=*)
					  (power=100,GJ_NCMM=1,tau=.1/1/10)
=	2.59e-13     1.2e+15	19 shrunk (power=1, GJ_NCMM=*, tau=*)
					  (power=100,GJ_NCMM=1,tau=*)
				17 grew   (power=10,GJ_NCMM=*,tau=*)
					  (power=100,GJ_NCMM=0,tau=*)
					  (power=100,GJ_NCMM=.5,tau=10)
=	1.25e-12     2.5e+14	36 shrunk
=	6.00e-12     5.3e+13	36 shrunk

   Takeaways are the same as above, except:
      -	Now there are zero cases of growing at GJ.power=1.
---------------
Finally, look at 6x6x6 tumors
For hyper: NR=5 has 30 grew, 131 const, 118 shrunk
288 cases (NR=5)*(8 Kd_field=)*(GJ_power=1,10,100)*(GJ_NCMM=0,.5,1)*(GJ_tau=.01,.1,1,10)
We actually have only 279; we're missing the last 9.
The 8 Kd_field are 1e-16,4.81e-16,2.32e-15,1.12e-14,5.37e-14,2.59e-13,1.25e-12,6e-12

	Kd	   R_field	Results
	1e-16	     3.2e+18	36 const
	4.81e-16     6.6e+17	32 const, 4 shrunk (power=100, GJ_NCMM=1,tau=*)
	2.32e-15     1.4e+17	28 const,  8 grew (power=100,GJ_NCMM=.5/1,tau=*)
	1.12e-14     2.8e+16	23 const,  5 shru (power=10,GJ_NCMM=.5,tau=10)
						  (power=10,GJ_NCMM=1,tau=*)
					   8 grew (power=100,GJ_NCMM=.5/1,tau=*)
	5.37e-14     5.9e+15	 8 const, 10 grew (po=10,GJ_NCMM=.5/1,tau=*)
					  (power=100,GJ_NCMM=.5/1,tau=10)
				18 shrunk (power=1,GJ_NCMM=0/.5/1,tau=*)
					  (power=100,GJ_NCMM=.5/1,tau=.01/.1/1)
	2.59e-13     1.2e+15	4 const (power=10, GJ_NCMM=0, tau=*)
				28 shrunk
				4 grew   (power=100,GJ_NCMM=0,tau=*)
	1.25e-12     2.5e+14	36 shrunk
	6.00e-12     5.3e+13	36 shrunk

For depol: NR=5 has 34 grew, 127 const, 125 shrunk, 2 stack dump
288 cases (NR=5)*(8 Kd_field=)*(GJ_power=1,10,100)*(GJ_NCMM=0,.5,1)*(GJ_tau=.01,.1,1,10)
The 8 Kd_field are 1e-16,4.81e-16,2.32e-15,1.12e-14,5.37e-14,2.59e-13,1.25e-12,6e-12
	Kd	   R_field	Results
=	1e-16	     3.2e+18	36 const
=	4.81e-16     6.6e+17	32 const, 4 shrunk (power=100, GJ_NCMM=1,tau=*)
=	2.32e-15     1.4e+17	28 const,  8 grew (power=100,GJ_NCMM=.5/1,tau=*)
=	1.12e-14     2.8e+16	23 const,  5 shru (power=10,GJ_NCMM=.5,tau=10)
						  (power=10,GJ_NCMM=1,tau=*)
					   8 grew (power=100,GJ_NCMM=.5/1,tau=*)
=	5.37e-14     5.9e+15	 8 const, 10 grew (po=10,GJ_NCMM=.5/1,tau=*)
					  (power=100,GJ_NCMM=.5/1,tau=10)
				18 shrunk (power=1,GJ_NCMM=0/.5/1,tau=*)
					  (power=100,GJ_NCMM=.5/1,tau=.01/.1/1)
+4	2.59e-13     1.2e+15	8 grew (power=10/100, GJ_NCMM=0, tau=*)
=				28 shrunk
=	1.25e-12     2.5e+14	36 shrunk
=	6.00e-12     5.3e+13	36 shrunk


-----------------------------
More metastasis results. We have
	2D_3/5/10_(hy/de)pol_831 the latest
		Last N=10 5x5 hypol grew at Kd_field=2.59e-13
	2D_10bX (Sep 20) 40x40 field, 1x1-19x19 tumor, tau=.01-10, 5472 cases.
	2D_hypol_3/5/10 (Aug 3-12) 25x25 field, 1x1-9x9 tumor, tau="", 2592 cases
		Last N=5 5x5 grew at Kd_field=2.59e-13
		Last N=10 5x5 grew at Kd_field=2.59e-13
	2D_depol_3/5/10 (Aug 3-12) 40x40 field, 1x1-19x19 tumor, cut short at 3070 cases.
	results/_6x6X(hy/de)polTumor (Jun 27) 25x25 field, 5x5 tumor, NR=3/5/10, the old NAB/plus
		(3/5/10) * 8 Kd * 3 power=1/10/100 * 3 NCMM *  4tau =864

**********************************************************************
		Bigger-is-better-for-metastasis experiments

    Now for the zombie_sims_metastasis() sims showing that a big tumor can
    proselytize its neighbors better than a small one. This data comes from
    running plot_big_tumor.py, using the function plot_success_vs_XY(). The x
    axis is tumor size; the y axis is Kd. The number (for each size/Kd pair)
    shows how many of the tumors with that parameter pair grew. It uses the
    data set from the six files zombie_metas_2D_(3/5/10)_(hy/de)pol.

    Kd        1 2  3  4  5  6  7   8   9  10  11  12  13  14  15  16  17  18  19
    1.00e-16: 0 0  0  0  0  0  0   0   0   0   0   0   0   0   0   0   0   0   0
    4.81e-16: 0 0  8  8  8  8  8   8   8   8   8   8   8   8   8   8   8   8   8
    2.32e-15: 0 0 24 32 32 32 32  32  32  32  32  32  32  32  32  32  32  32  32
    1.12e-14: 0 0 42 52 64 64 64  64  64  64  64  64  64  64  64  64  64  64  64
    5.37e-14: 0 0 34 62 78 87 92  92 100 104 108 108 108 108 108 108 108 108 108
    2.59e-13: 0 0  2 33 51 69 78 101 126 127 132 140 140 140 140 140 142 153 160
    1.25e-12: 0 0  0  0  1 14 23  38  50  54  69  72  78  80  85  92 104 112 112
    6.00e-12: 0 0  0  0  0  0  0   0   0   0   0   0   7  14  20  27  32  36  40

    We see that Kd=1e-16 gave no growth. Kd=4.8e-16 through 5.37e-14 show
    exactly the behavior we expect; better growth with larger tumors up to a
    certain tumor size, and then no more improvement. Then Kd=2.59e-13 and up
    show continued improvement with tumor size all the way through 19x19 tumors.

    We also see that as Kd gets larger,
    - often the minimum tumor size needed for growth also gets larger.
    - the "saturation point" where larger tumors don't aid growth keeps
      increasing.

    Can we explain any of this? The saturation-point increase is simple.
    Saturation occurs because as the tumor size grows, the RC-delay time for
    cells in the tumor core to help the boundary cells grows quadratically, but
    the GRN response time in a local cell stays constant. And while the "C" part
    of the tumor response time stays constant, the "R" part is directly
    proportional to 1/Kd. So a larger Kd means a faster response time from the
    tumor core, which means that the tumor can get bigger before its core
    response arrives too late.

    The increase in minimum tumor size vs. Kd is a bit harder to explain.
    Roughly, larger Kd means that the cells are more tightly tied; as noted
    above, there are more cells just outside the tumor than inside, and this
    ratio improves with larger tumors. That's easy -- but the hard part is
    why the 4.8e-16 through 2.59e-13 all have the same 3x3 minimum size. My
    guess is it's because there are confounding factors, but what are they?

    Next, look at growth vs. tumor size and GJ_power:
         1 2  3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19
      1: 0 0  0   0   0   0   0   0  12  12  12  12  12  12  12  12  12  21  24
     10: 0 0 43  76 103 125 133 155 172 174 184 184 192 200 209 217 228 236 240
    100: 0 0 67 111 131 149 164 180 196 203 217 228 233 234 236 242 250 256 260

    And for GJ_NCMM
        1 2  3  4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19
     0: 0 0  4 24  30  49  55  71  82  84  89  90  99 104 107 112 114 119 122
    .5: 0 0 48 83 102 113 119 135 148 150 162 164 166 168 172 179 188 195 200
     1: 0 0 58 80 102 112 123 129 150 155 162 170 172 174 178 180 188 199 202

    For GJ_tau
         1 2  3  4  5  6  7  8   9  10  11  12  13  14  15  16  17  18  19
    .01: 0 0 25 41 55 63 67 76  90  92  97 101 103 105 105 109 115 122 124
     .1: 0 0 25 41 55 65 67 78  90  92 100 103 103 105 106 111 117 122 124
      1: 0 0 25 45 57 69 74 82  94  99 105 105 109 109 115 117 123 130 132
     10: 0 0 35 60 67 77 89 99 106 106 111 115 122 127 131 134 135 139 144

    For strategy
         1 2  3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19
    dep: 0 0 62 105 124 146 156 176 202 207 217 222 229 233 239 247 255 271 278
    hyp: 0 0 48  82 110 128 141 159 178 182 196 202 208 213 218 224 235 242 246
    

First start with the 2D results from zombie_metas_2D_3/5/10. The mechanics of
this are "grep final zombie_metas_2D_* > zombie.out", and then run
plot_big_tumor.py on zombie.out.

There are a total of 7776 cases:
	(NR=3,5,10) x (Kd_field=8 values) x (GJ_power=1,10,100)
		    x (GJ_NCMM=0,.5,1) x (GJ_tau=.01,.1,1,10) x (9 tumor sizes)
Note that only use the hyperpolarizing strategy, not depolarizing.

In total: (0.,   0.,  48.,  82., 110., 128., 141., 159., 178.]),
With our 8 values of Kd_field
	[1e-16,4.81e-16,2.32e-15,1.12e-14,5.37e-14,2.59e-13,1.25e-12,6e-12],
	we have [0, 28, 108, 207, 240, 205, 58, 0] tumors grow.
Overall, the growth by tumor size is
	0.,   0.,  48.,  82., 110., 128., 141., 159., 178
With GJ_power=1, nothing grows
With GJ_power=10, it's
	0.,  0., 17., 31., 49., 59., 64., 72., 82
With GJ_power=100, it's
	0.,  0., 31., 51., 61., 69., 77., 87., 96

**********************************************************************
zombie_metas_2D_hypol_3,5,10:  hypol 1x1-9x9 in 25x25 field, 2592 cases
	(8 Kd_field) * (3 power) * (3 GJ_NCMM) * (4 tau) * (9 tumor sizes) =2592
zombie_metas_2D_depol_3/5/10: depol 1x1-19x19 in 40x40 field, 3070/3429/4057 cases
	(8 Kd_field) * (3 power) * (3 GJ_NCMM) * (4 tau) * (19 tumor sizes)=5472

zombie_metas_2D_10X  started 8/29 4pm match
zombie_metas_2D_10bX started 8/30 8am 3045/4056, match

zombie_metas_2D_3/5/10_depol/hypol started 8/31 6pm
Hypol sims cannot compare (different field size, not numbered).
5471. 2D_3_hypol_831.
5471. 2D_5_hypol_831.
5471. 2D_10_hypol_831.

5172. 2D_3_depol_831 (vs 2D_3_depol): same-ish everywhere checkable.
	At 6e-12,10,1
5471. 2D_5_depol_831 (vs 2D_5_depol):
	worse 1977,1996; better at 2433-2488. Very small delta from the old sim; just crossed a line.
	3327: 3x3 Kd_field=5.37e-14, GJ_power=100, GJ_NCMM=0.5, GJ_tau=10
       	3428: 9x9 Kd_field=2.59e-13, GJ_power=1, GJ_NCMM=0, GJ_tau=0.01
	In both these, we just got to grow at one tumor size smaller.
5471. 2D_10_depol_831 (vs 2D_10_depol):
	worse 2036-51 (explained in metastasis_case_2036).
	 4x4-19x19 tumors Kd_field=2.32e-15, GJ_power=100, GJ_NCMM=1, GJ_tau=10
*************
The 1018 sims use the Mafe-Cervera GJ model.
zombie_metas_2D_5_dumb_all_1018
zombie_metas_2D_5_hypol_all_1018 done 	0-5472
zombie_metas_2D_5_depol_all_1018 done	0-5472

************
Algorithm-compare sims

Metastasis sims to quantify how well depol and hypol work vs. dumb.
The "best" algorithm is the one that needs the smallest value of GJ_power to
succeed at metastasis. So:
    for each combination of Kd_field, GJ_tau, GJ_NCMM, ...
  1. Do a tumor-size search. For each of the algorithms dumb, depol &
     hypol, it finds the range of "acceptable" tumor sizes where power=1
     fails to metastasize, but power=100 succeeds. The goal of this is
     to find a common set of conditions for...
  2. Based on the results of the tumor-size search, try to find a single
     tumor size where, for all three algorithms, power=1 doesn't
     metastasize but power=100 does. Then do a power search:
	for each algorithm dumb, depol and hypol
	    do a binary search to find the borderline power that
	    separates failure from success
	print the borderline power value for each algorithm.

These sims are from metas_algo_compare_0_64. It runs 64 cases:
(8 values of Kd_field in 1e-16...6e-12)x(GJ_tau=.01,.1,1.10)x(GJ_NCMM in .5,1)
All are at NR=5.
Kd_field	tau	NCMM	dumb	hypol	depol	cases
2.32e-15	*	.5	80	54	53	16-23 even
2.32e-15	*	1	40	29	28	16-23 odd
1.12e-14	*	.5	31	12	11	24-31 even
1.12e-14	*	1	16	 7	 6	24-31 odd
5.37e-14	*	.5	11	2.7	1.7	32-39 even
5.37e-14	*	1	6.3-.5	2.0	1.4	32-39 odd tau dependence
2.59e-13	*	.5	4.7	1.3-.5	--	40-47 even "	"
2.59e-13	*	1	3.1-.2	1.3-.4	--	40-47 odd  "	"
1.25e-12	10	.5	2.6	2.0	1.5	54
1.25e-12	10	.5	2.2	2.0	1.6	55

The results (all at NR=5), slightly reordered:
16. Kd_field=2.32e-15, GJ_tau=.01, GJ_NCMM=.5=> Pdumb=80.23,Pdepol=53.26,Phypol=54.37
18. Kd_field=2.32e-15, GJ_tau=.1 , GJ_NCMM=.5=> Pdumb=80.23,Pdepol=53.26,Phypol=54.37
20. Kd_field=2.32e-15, GJ_tau=1,   GJ_NCMM=.5=> Pdumb=80.23,Pdepol=53.26,Phypol=54.37
22. Kd_field=2.32e-15, GJ_tau=10,  GJ_NCMM=.5=> Pdumb=80.23,Pdepol=53.26,Phypol=54.37
17. Kd_field=2.32e-15, GJ_tau=.01, GJ_NCMM=1 => Pdumb=40.32,Pdepol=28.36,Phypol=28.99
19. Kd_field=2.32e-15, GJ_tau=.1,  GJ_NCMM=1 => Pdumb=40.32,Pdepol=28.36,Phypol=28.99
21. Kd_field=2.32e-15, GJ_tau=1,   GJ_NCMM=1 => Pdumb=40.32,Pdepol=28.36,Phypol=28.99
23. Kd_field=2.32e-15, GJ_tau=10,  GJ_NCMM=1 => Pdumb=40.35,Pdepol=28.36,Phypol=28.99

24. Kd_field=1.12e-14, GJ_tau=.01, GJ_NCMM=.5=> Pdumb=31.28,Pdepol=10.52,Phypol=11.63
26. Kd_field=1.12e-14, GJ_tau=.1,  GJ_NCMM=.5=> Pdumb=31.28,Pdepol=10.52,Phypol=11.63
28. Kd_field=1.12e-14, GJ_tau=1,   GJ_NCMM=.5=> Pdumb=31.28,Pdepol=10.52,Phypol=11.63
30. Kd_field=1.12e-14, GJ_tau=10,  GJ_NCMM=.5=> Pdumb=31.21,Pdepol=10.52,Phypol=11.63
25. Kd_field=1.12e-14, GJ_tau=.01, GJ_NCMM=1 => Pdumb=16.24,Pdepol=6.003,Phypol=6.626
27. Kd_field=1.12e-14, GJ_tau=.1,  GJ_NCMM=1 => Pdumb=16.24,Pdepol=6.003,Phypol=6.626
29. Kd_field=1.12e-14, GJ_tau=1,   GJ_NCMM=1 => Pdumb=16.24,Pdepol=6.003,Phypol=6.626
31. Kd_field=1.12e-14, GJ_tau=10,  GJ_NCMM=1 => Pdumb=16.12,Pdepol=6.006,Phypol=6.626

32. Kd_field=5.37e-14, GJ_tau=.01, GJ_NCMM=.5=> Pdumb=11.22,Pdepol=1.675,Phypol=2.742
34. Kd_field=5.37e-14, GJ_tau=.1,  GJ_NCMM=.5=> Pdumb=11.23,Pdepol=1.675,Phypol=2.743
36. Kd_field=5.37e-14, GJ_tau=1,   GJ_NCMM=.5 =>Pdumb=11.22,Pdepol=1.675,Phypol=2.743
38. Kd_field=5.37e-14, GJ_tau=10,  GJ_NCMM=.5=> Pdumb=11.21,Pdepol=1.677,Phypol=2.746
33. Kd_field=5.37e-14, GJ_tau=.01, GJ_NCMM=1 => Pdumb=6.493,Pdepol=1.361,Phypol=1.968
35. Kd_field=5.37e-14, GJ_tau=.1,  GJ_NCMM=1 => Pdumb=6.487,Pdepol=1.361,Phypol=1.968
37. Kd_field=5.37e-14, GJ_tau=1,   GJ_NCMM=1 => Pdumb=6.372,Pdepol=1.362,Phypol=1.97
39. Kd_field=5.37e-14, GJ_tau=10,  GJ_NCMM=1 => Pdumb=6.275,Pdepol=1.362,Phypol=1.973

40. Kd_field=2.59e-13, GJ_tau=.01, GJ_NCMM=.5=> Pdumb=4.65,Phypol=1.312
42. Kd_field=2.59e-13, GJ_tau=.1,  GJ_NCMM=.5=> Pdumb=4.653,Phypol=1.312
44. Kd_field=2.59e-13, GJ_tau=1,   GJ_NCMM=.5=> Pdumb=4.659,Phypol=1.335
46. Kd_field=2.59e-13, GJ_tau=10,  GJ_NCMM=.5=> Pdumb=4.701,Phypol=1.468
41. Kd_field=2.59e-13, GJ_tau=.01, GJ_NCMM=1 => Pdumb=3.242,Phypol=1.301
43. Kd_field=2.59e-13, GJ_tau=.1,  GJ_NCMM=1 => Pdumb=3.242,Phypol=1.301
45. Kd_field=2.59e-13, GJ_tau=1,   GJ_NCMM=1 => Pdumb=3.236,Phypol=1.357
47. Kd_field=2.59e-13, GJ_tau=10,  GJ_NCMM=1 => Pdumb=3.076,Phypol=1.441

54. Kd_field=1.25e-12, GJ_tau=10,  GJ_NCMM=.5=> Pdumb=2.6,Pdepol=1.489,Phypol=2.033
55. Kd_field=1.25e-12, GJ_tau=10,  GJ_NCMM=1 => Pdumb=2.153,Pdepol=1.592,Phypol=1.985

Observations: 
      *	The first 16 cases are Kd_field=1e-16 and 4.81e-16. With our tumor size
	capped at 20, no tumor ever grew.
      *	Cases [16,24) have Kd_field=2.3e-15; this is barely big enough to flip
	a cell, and thus needs pretty high power (Pdumb=80 for NCMM=.5 and
	Pdumb=40 for NCMM=1). Depol and hypol each reduce the power needed by
	about 30%
      *	Cases [24,32) have Kd_field=1.1e-14. Now Pdumb=31 for NCMM=.5 and 16 for
	NCMM=1. Depol and hypol are hugely effective, reducing power by 65%.
      *	Cases [32,40) have Kd_field=5.4e-14. Now Pdumb=11 for NCMM=.5 and 6 for
	NCMM=1. Depol reduces power by 84%, and hypol by "only" 75%.
      * Cases [40,48) have Kd_field=2.6e-13. Now Pdumb=4.6 for NCMM=.5 and 3.2
	for NCMM=1. Depol doesn't work at all (see below for why). And hypol
	is unaffected by NCMM, and "only" helps by 60-72% vs. dumb.
      * Cases [48,56) have Kd_field=1.2e-12. Only two cases grew, both with
	GJ_tau=10. And depol was best, and hypol wasn't much better than dumb.
      *	Cases [56,64) are Kd_field=6e-12. With this huge connectivity, no
	tumor every grew. So we are indeed looking at the full reasonable range
	of Kd_field.
      *	In most cases, the hypol and depol algorithms did roughly equally well.
	There were some exceptions, though. 
	Cases 32-39 (all Kd_field=5.37e-14) had depol do much better than hypol.
	Case >=40 (which is Kd_field=2.59e-13 and above), depol was never
	feasible. We had weird "inversions" where, e.g.,
		depol => [pass100,fail1] range is [15,4]
		I.e., at power=100, size=14 shrinks and 15 grows.
		But at power=1, size=4 shrinks and size=5 grows.
	So there was no size where power=1 shrinks and power=100 grows. With
	extra power evidently hurting rather than helping, the metric of judging
	an algorithm by the minimum power for metastasis isn't meaningful.
	This is the weird case where the Kd_field is large enough that the
	healthy tissue is always a syncytium. Then, no matter how big you make
	power, the healthy tissue is a bigger syncytium than the tumor, and
	can thus normalize the tumor. So the bigger we make GJ_power, the more
	this normalization can happen. The only exception is at GJ_tau=10
	(where we almost have a workable sim).
	Chris' hypothesized that larger Kd_field would make neighbor-depol the
	only effective strategy, since it disconnects the healthy border from	
	its backup. But the problem is that GJ switching doesn't create a huge
	delta between ON and OFF, and isn't enough to overcome the healthy-
	tissue syncytium.
      *	When I made Table Algo1, I used GJ_NCMM=.5, 
