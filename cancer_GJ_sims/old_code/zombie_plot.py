# This file analyzes the results of the zombie() sims.
# It gets lots of lines of the form
#	NAB/NR=10/10, delta=5 (gated) field=1x16x21.
#		final useful Kd range is 1.11e-13/1.25e-13:1.81e-12, or 14.4x;
#		border=5.51/-1,2.69/-1,1.57/-1
# The 10/10, delta=5 (gated) field=1x16x21 is hopefully obvious. For the rest...
# In terms of the names, it's
#	final useful Kd range is Kd_min0/Kd_min1:Kd_max, or Kd_ratx;
#	border=bordOrgMin/bordWgtMin,bordOrgMid/bordWgtMid,bordOrgMax/bordWgtMax
#
# The decoder ring is:
#     -	Kd_min0 is the minimum Kd that fixed a 1-spot on a 0 background
#     - Kd_min1 is similar, but for a 0-spot on a 1 background
#     - Kd_max is the largest Kd that didn't collapse a simple border
#     -	Kd_rat is Kd_max / max(Kd_min0,Kd_min1)
#     -	bord* is the border sharpness in six different conditions. By border
#	sharpness, we basically mean the ratio of [OA] on one side of the border
#	boundary divided by [OA] on the other side (and if the boundary is, say,
#	12 cells tall, then we average the 12 ratios).
#	Next, each of the three pairs is at a different Kd -- the first at
#	max(Kd_min0,Kd_min1), the third at Kd_max, and the second at the
#	geometric mean of those two Kd values.
#	For each pair, we first have the results from this sim ("Org") and then
#	("Wgt") the results from adding gating post-sim (or -1, if the original
#	sims were already gated).

import re, statistics
import matplotlib, matplotlib.pyplot as plt
#import pdb; pdb.set_trace()

################################################
# Classes and globals
################################################

class sim_results:
  def __init__ (self, NAB, NR, delta, gated, spot_size):
    self.NAB = NAB
    self.NR = NR
    self.delta = delta
    self.gated = gated
    self.spot_size = spot_size
  def str(self):
    return (f"NAB={self.NAB}, NR={self.NR}, delta={self.delta}, " \
	  + f"gated={self.gated}, {self.spot_size}")

# Each sim-result line that we read from the file becomes one instance of
# class sim_results, and one item in sr_list.
sr_list = []

################################################
# Small utilities
################################################

# Given a list, round all its elements and return a string.
# Works for both ints and floats.
def lround(list):
    if (type(list[0]) == int):
        return (", ".join ([str(round(list[i],-2)) for i in range(len(list))]))
    else:
        return (", ".join ([f"{list[i]:.2g}" for i in range(len(list))]))

# The main routine to parse an input file and build sr_list.
def parse (file, spot_size, use_gated=True):
    print ("Opening file",file)
    infile = open (file, "r")
    global sr_list
    for line in infile:
        if (re.search ("Final", line) == None):
            continue
        #print (f"\nHave line *{line}*")
        rint = "(-?[0-9]+)"
        rflt = "([0-9e+-.]+)"

        # NAB/NR=10/10
        NAB_NR = re.match (f"NAB/NR={rint}/{rint},", line)
        (NAB, NR) = re_to_numbers (NAB_NR, True)

        # delta=5 (gated) field
        delta = re.search (f"delta={rint} ", line)
        delta = int (delta.group(1))
        gated = (re.search (" \(gated\) field", line) != None)
        if (gated and not use_gated):
            continue

        sr = sim_results (NAB, NR, delta, gated, spot_size)

        # Kd range is 1.11e-13/1.25e-13:1.81e-12, or 14.4x
        Kd_rng  =re.search(f"Kd range is {rflt}/{rflt}:{rflt}, or {rflt}x",line)
        (sr.Kd_min0,sr.Kd_min1,sr.Kd_max,sr.Kd_rat) =re_to_numbers(Kd_rng,False)

        # border=5.51/-1,2.69/-1,1.57/-1        
        bord=re.search(f"border={rflt}/{rflt},{rflt}/{rflt},{rflt}/{rflt}",line)
        (sr.bordOrgMin,sr.bordWgtMin,sr.bordOrgMid,sr.bordWgtMid,
		sr.bordOrgMax,sr.bordWgtMax) = re_to_numbers(bord,False)
        if (not gated):
            sr.bordMinRat = sr.bordWgtMin/sr.bordOrgMin
            sr.bordMidRat = sr.bordWgtMin/sr.bordOrgMid
            sr.bordMaxRat = sr.bordWgtMin/sr.bordOrgMax

        #print(f"NAB/R={sr.NAB}/{sr.NR}, delta={sr.delta}: gated if {sr.gated}")
        #print(f"Kd={sr.Kd_min0}/{sr.Kd_min1}:{sr.Kd_max} => ratio={sr.Kd_rat}")
        #print(f"border={sr.bordOrgMin}/{sr.bordWgtMin},{sr.bordOrgMid}/" \
	#     +f"{sr.bordWgtMid},{sr.bordOrgMax}/{sr.bordWgtMax}")
        sr_list.append (sr)
    print (f"Parse({file},{spot_size}) created {len(sr_list)} sr objects")

# Small helper for parse().
# Given a match result that typically comprises multiple () groups: convert each
# group in the match to a number and return the list of numbers.
def re_to_numbers (match, is_int):
    n_params = len (match.groups())	# How many groups are in the match.
    if (is_int):
        params = [int(match.group(i)) for i in range(1,n_params+1)]
    else:
        params = [float(match.group(i)) for i in range(1,n_params+1)]
    return (tuple (params))


################################################
# First main analysis/plot routine.
# For each discrete value of 'x_cat', analyze the category 'y_cat' 
# So, e.g., analyze Kd_min for each value of NAB (3, 5 and 10).
# The analysis includes the average value of Kd_min for each of those three
# categories, std deviation, etc.
# We can also turn out a bar graph.
# The goal is typically to see how well x_cat predicts the value of y_cat.
################################################

def anal_by_cat (x_cat, y_cat):
    global sr_list
    values_map = {}
    # Start with sr_list (a list of one object per "Final" line). Build
    # values_map, which maps each discrete value of x_cat to a list of all
    # values of y_cat for 
    for sr in sr_list:
        x = sr.__dict__[x_cat]		# E.g., 3, 5 or 10
        y = sr.__dict__[y_cat]		# E.g., the resultant Kd
        if not (x in values_map.keys()):
            values_map[x] = []		# New entry, no data yet
        values_map[x].append (y)	# Add to existing entry
        #print (f"Processed record with {x},{y} => {values_map[x]}")

    # x_list is a list of the values of x_cat (e.g., [3,5,10]).
    x_list = sorted([*values_map.keys()])	# The keys, as a list.
    # Avg_list would have 3 elements. The first would have the average (e.g.,)
    # Kd_min over all sims with NAB=3.
    avg_list = [(sum(values_map[x]) / len(values_map[x])) for x in x_list]
    min_list = [min(values_map[x]) for x in x_list]
    max_list = [max(values_map[x]) for x in x_list]
    std_list = [statistics.pstdev(values_map[x]) for x in x_list]

    print (f'{x_cat}=({x_list}) => avg {y_cat}=({lround(avg_list)})')
    print (f'{x_cat}=({x_list}) => std {y_cat}=({lround(std_list)})')
    print (f'{x_cat}=({x_list}) => min {y_cat}=({lround(min_list)})')
    print (f'{x_cat}=({x_list}) => max {y_cat}=({lround(max_list)})')

    return
    plt.bar (x_list, avg_list, yerr=std_list)
    plt.xlabel(f'Values of {x_cat}')
    plt.ylabel(f'Mean value of {y_cat}')
    #plt.legend(loc='best')
    plt.title (f'{y_cat} vs. {x_cat} for each {x_cat} value')
    plt.show()
    
################################################
# Second main analysis/plot routine.
# It again is trying to see how well one parameter predicts the results of
# simulations.
# We start by going through sr_list and collecting sets of sim_results objects
# that have identical parameters except for group_param. So if
# group_param=="NAB", then we're looking for objects that have the same
# NR, delta, gated and spot_size but different NAB. For NAB, it would typically
# have three items (the results for NAB=3, NAB=5 and NAB=10).
# For each such set, we
#     -	see if the values check_param are monotonically increasing in the values
#	of group_param.
#     -	ditto, but monotonically decreasing
#     -	compute the average of adjacent ratios. In this case, that would be the
#	average of (check_param@NAB=5/check_param@NAB=3, ditto@NAB=10/@NAB=5)
# Report how many of the sets were monotonically increasing and how many were
# monotonically decreasing.
# Report the average of all the average ratios.
################################################

def anal_groups(group_param, check_param):
    groups = group (group_param)
    cnt = 0
    for grp in groups:
        for sr in grp:
            print (f"\t{sr.str()} => {check_param}={sr.__dict__[check_param]}")
            cnt = cnt+1
        print()
    print (f"Grouped {cnt} sr items by {group_param}")

    n_all_le=0; n_all_ge=0
    ratio_sum = 0		# To get an avg across all groups.
    n_groups_used = 0		# Number of groups with >1 element
    for grp in groups:
        if (len(grp)==1):	# A group of length one isn't really all
            continue		# ascending or all descending.
        n_groups_used += 1
        ratio_sum_in_grp=0	# To get an average ratio in a group
        all_le = True		# Ascending: each item is <= its follower
        all_ge = True		# Descending: each item is >= its follower
        for i in range(len(grp)-1):
            a = grp[i].__dict__[check_param]
            b = grp[i+1].__dict__[check_param]
            if (a < b):
                all_ge = False
            if (a > b):
                all_le = False
            ratio_sum_in_grp += (b/a)
        if (all_ge):
            n_all_ge += 1
        if (all_le):
            n_all_le += 1
        ratio_avg_in_grp = ratio_sum_in_grp / (len(grp)-1)
        ratio_sum += ratio_avg_in_grp

    ratio_avg = ratio_sum / n_groups_used
    print (f"Of {len(groups)} groups, {n_all_le} have {check_param} ascending "\
	  +f" and {n_all_ge} are all descending")
    print (f"The average in-group ratio is {ratio_avg:.2g}")

# Finds items in sr_list that are identical except for 'param_to_group'
# and puts them into a group.
# Returns a list of such groups
def group (param_to_group):
    groups=[]		# Each item is a list of "similar" sr objects
    #for i in range(len(sr_list)):
    #    print (f"{i}. {sr_list[i].str()}")

    sr_copy = sr_list.copy()
    while (len(sr_copy) > 0):
        grp = [0]	# Start with sr_copy[0] in this group.
        for i in range(1,len(sr_copy)):
            if (match_except (sr_copy[0], sr_copy[i], param_to_group)):
                grp.append (i)
                #print (f"Adding {sr_copy[i].str()}")
        group = [sr_copy[i] for i in grp]
        groups.append (group)
        for i in reversed(grp):	# reverse so we don't invalidate other indices
            del sr_copy[i]
    return (groups)

def match_except (sr1, sr2, param):
    return (((param=="NAB") or (sr1.NAB==sr2.NAB)) \
        and ((param=="NR")  or (sr1.NR ==sr2.NR)) \
        and ((param=="delta")  or (sr1.delta ==sr2.delta)) \
        and ((param=="gated")  or (sr1.gated ==sr2.gated)) \
        and ((param=="spot_size") or (sr1.spot_size ==sr2.spot_size)))

################################################
# Special-purpose analysis
################################################

# Look at what conditions improve/hurt border sharpness.

################################################

def main():
    use_gated = False
    #parse ("xx", "1x1", use_gated)
    parse ("zombie_1x1", "1x1", use_gated)
    parse ("zombie_2x2", "2x2", use_gated)
    parse ("zombie_4x4", "4x4", use_gated)
    #anal_groups("spot_size", "Kd_min0")
    #quit()

    result_cat = "Kd_min0"
    result_cat = "Kd_rat"
    result_cat = "bordMaxRat"
    anal_by_cat ("gated", result_cat)
    anal_by_cat ("NR", result_cat)
    anal_by_cat ("NAB", result_cat)
    anal_by_cat ("delta", result_cat)

main()
