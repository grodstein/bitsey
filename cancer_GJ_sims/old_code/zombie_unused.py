################################################################
# The first part of the file: characterizing simple cross-coupled inverters.
# This analysis isn't part of the cancer narrative, but it does have a section
# in 0readme_zombie.
# Simply characterize a single cross-coupled-inverter latch (A=!B and B=!A).
# Hill functions for the two inverters and linear decay. Just one cell, no GJs.
# Do the characterization for a range of kM and N. What we characterize is
# - find the two stable points via two sims of the cross-coupled-inv latch.
# - find its resilience. I.e., disturb the latch by injecting a constant
#   generation rate of A, and see how much the stable values change. In some
#   cases, we may even flip the cell when we do this.
# - finally, find the metastable point (and re-find all stable points) by a
#   binary-search technique using an open-loop circuit.
################################################################

def char_latch():		# The top-level entry point.
    GP = sim.Params()
    GP.no_dumps = True

    for N in [2,3,5,10]:
        for kM in [20,35,50,65,80]:
            # Find the steady-state operating point, and measure resilience.
            # We set up two inverters A=!B and B=!A.
            closed_loop_sims (GP, N, kM)

            # Now find all stable & metastable points. We use the open-loop
            # circuit B=!A, C=!B.
            setup_latch_openloop (GP, N, kM)	# Build the network.
            startup_init_Vmem()
            (A,B,C) = ions ('A','B','C')

            # Find zero-crossing points and then use binary search to narrow in.
            sign_old = openloop_sim (Ainit=0)
            for Anew in range (1, kVMax+1):
                sign_new = openloop_sim (Ainit=Anew)
                if (sign_new != sign_old):
                    (Aloop,_) = binSearch (openloop_sim, Anew-1, Anew)
                    openloop_sim (Ainit=Aloop)
                    type = "metastable" if (sign_new==1) else "stable"
                    print (f"\tFixed {type} point with {sim.cc_cells[A,0]:.3f} "
                           f"-> {sim.cc_cells[B,0]:.3f} -> {sim.cc_cells[C,0]:.3f}")
                sign_old = sign_new


# Build the network for the "closed-loop" configuration, where A=!B and B=!A.
def setup_latch (p, N, kM, genA):
    sim.init_big_arrays (n_cells=1, n_GJs=0, p=p, extra_ions=['A', 'B'])
    (A,B) = ions ('A','B')

    # gtype, out_ion, in_ion, inv=None,kM=1,N=1,kVMax=1,offset=0.0):
    gAB=sim.Hill_gate (sim.Gate.GATE_GD,B,A, inv=True,kM=kM,N=N,kVMax=kVMax)
    #gAB=sim.Hill_gate (sim.Gate.GATE_GD,B,A, inv=True,kM=50,N=N,kVMax=kVMax)
    gBA=sim.Hill_gate (sim.Gate.GATE_GD,A,B, inv=True,kM=kM,N=N,kVMax=kVMax)
    dA = sim.GD_const_gate (sim.Gate.GATE_GD, A, gen=genA, decay=1)
    dB = sim.GD_const_gate (sim.Gate.GATE_GD, B, decay=1)

    # Set up initial concentrations
    sim.cc_cells[A,0] = 0
    sim.cc_cells[B,0] = kVMax

# Set up and run two simulations on the "closed-loop" configuration, where
# A=!B and B=!A.
# Simulation #1: just simulate it and note the final values for A and B.
# Simulation #2: add an additional source of A at 1 moles/(m3*s), resim, and
# note how much [A] actually increased at steady state.
# Print the results, but don't return any values
def closed_loop_sims (GP, N, kM):
    end_time=100
    setup_latch (GP, N, kM, genA=0)
    startup_init_Vmem()
    t_shots, cc_shots = sim.sim (end_time)
    (A,B) = ions ('A','B')
    final_A0 = cc_shots[-1][A][0]
    final_B0 = cc_shots[-1][B][0]

    setup_latch (GP, N, kM, genA=1)
    startup_init_Vmem()
    t_shots, cc_shots = sim.sim (end_time)
    final_A1 = cc_shots[-1][A][0]
    final_B1 = cc_shots[-1][B][0]
    deltaA = f'deltaA={(final_A1-final_A0):.3f}'
    if (final_A1 > final_B1):
        deltaA = "deltaA flipped the cell"
    if (final_A0+.05 > final_B0):
        deltaA = "no convergence"
    print (f'kM={kM}, N={N}: A={final_A0:.3f}, B={final_B0:.3f}, {deltaA}')

# Open-loop simulation: B=!A, C=!B.
# Each inverter is a Hill gate, with gen/decay on its output to provide decay.
# The input A has no gen/decay; someone initializes it in cc_cells[A,0] and it
# drives the simulation with that constant value.
# The purpose of this network (and the sims that follow) is simply to find
# Vsw of the inverters.
def setup_latch_openloop (p, N, kM):
    sim.init_big_arrays (n_cells=1, n_GJs=0, p=p, extra_ions=['A', 'B','C'])
    (A,B,C) = ions ('A','B','C')

    # First the two Hill gates for B=!A and C=!B.
    # gtype, out_ion, in_ion, inv=None,kM=1,N=1,kVMax=1,offset=0.0):
    gAB=sim.Hill_gate (sim.Gate.GATE_GD,B,A, inv=True,kM=kM,N=N,kVMax=kVMax)
    gBC=sim.Hill_gate (sim.Gate.GATE_GD,C,B, inv=True,kM=kM,N=N,kVMax=kVMax)

    # And the gen/decay gates (just to do decay, no generation).
    dB = sim.GD_const_gate (sim.Gate.GATE_GD, B, decay=1)
    dC = sim.GD_const_gate (sim.Gate.GATE_GD, C, decay=1)

    # Set up initial concentrations
    sim.cc_cells[A,0] = 0	# Someone will likely overwrite this.

# Initialize sim.cc_cells[A,0] = Ainit to drive our simulation.
# Then run a sim, look at the final output C and return the sign of
# (final_C - Ainit). This will be used to find zero-crossings. The idea is that
# when final_C==Ainit, we have a stable point (be it stable or metastable).
# This is the inverter Vsw.
def openloop_sim (Ainit):
    (A,C) = ions ('A','C')
    sim.cc_cells[A,:] = Ainit
    end_time=200
    t_shots, cc_shots = sim.sim (end_time)
    final_C = cc_shots[-1][C,0]
    return (-1 if (final_C<Ainit) else 1)

def startup_init_Vmem():
    Vm = sim.compute_Vm (sim.cc_cells)
    assert (np.abs(Vm)<.5).all(), \
            "Your initial voltages are too far from charge neutral"

#######################################################
# The predict_bistab() sims. They're not part of the cancer narrative, but do
# have a section in 0readme_zombie.
#
# This set of functions tries to understand the behavior of a simple GRN.
# The GRN is cross-coupled NOR2 gates. The two non-cross-coupled inputs are
# morphogens A and B, which are complementary in that they come from opposing
# gradients.
# The big-picture goal is to find which parameters result in nice behavior:
#     -	When A is very high and B is very low (or vice versa), the GRN has only
#	one possible state.
#     - When A and B are both kind of midrange, the GRN is bistable.
#
# Our actual strategy is that, for numerous parameter sets,
#     -	build a straight-line network of cells that are not connected
#     -	set up the opposing gradients of A and B, so that each cell represents
#	the GRN's response to a particular [A]
#     - initialize all cross-coupled structures to 1:0 and sim
#     - initialize all cross-coupled structures to 0:1 and sim
#
# For each parameter set, the characterization produces/prints a string with
# one character per cell, where
#     -	"0" means that OA >> OB0, independent of initialization.
#     -	"1" means that OA << OB, independent of initialization.
#     - "B" means that both initial values are stable
#     - "X" means anything else.
# It also compares the string to a prediction via a simple algorithm.
#
# The NOR gate is implemented as
#	kVmax / (1 + (M/kM)^NM + (R/kR)^NR)
# where M is a morphogen (A or B), and R is a repressor (the cross-coupling).
#######################################################

# The top-level function to loop through many parameter sets, call
# setup_basic_network() to build a network with a single parameter set,
# simulate and print results.
def predict_bistab():
    GP = sim.Params()
    GP.no_dumps = True

    n_cells=20
    kVMax=100

    # Tons of parameter sets
    #for NAB in [2,3,5,10]:
    #   for NR in [2,3,5,10]:
    #       for kMA in [20,35,50,65,80]:
    #           for kMB in [20,35,50,65,80]:
    #              for kMR in [20,35,50,65,80]:

    # More "plus" values of the "reasonable" parameter sets.
    for plus in [0,5,10,15]:
        for NAB in [2,3,5,10]:
            for NR in [2,3,5,10]:
                     kMA = 50+plus
                     kMB = 50+plus
                     kMR = 50
                     # Build the network.
                     #(NAB,NR,kMA,kMB,kMR) = (10,10,10,65,50,20)
                     setup_basic_network (GP, NR, 1,1,n_cells,NAB=NAB,plus=plus)

                     # Do both sims and analyze the results.
                     title = f'NAB/R={NAB}/{NR}, kMA/B/R={kMA}/{kMB}/{kMR}'
                     expect = expects (kMA, kMB, n_cells)
                     predict_bistab_run_both_sims(GP, NAB, NR, plus, kMA,kMB,
                                             n_cells, title, expect)
                     #quit()

# Given a single specific parameter combo, run both sims and interpret/print
# the results. We print lines like
#	NAB/R=10/10, kMA/B/R=80/80/65, expect 11111BBBBBBBBBBBB0000,
#			#5:B->1,#15:B->0,#16e:B->0
# where NAB and NR are the Hill exponents, kMA, kMB, kMR are the Michaelis-
# Menton kM values, "expect ..." is the results from our simple prediction
# algorithm, and "#5:B->1" means that cell #5 was predicted to have B but
# simulated to "1", the "#16e" means that cell #16 was an "edge;" its [A]
# matched kMA or its [B] matched kMB.
def predict_bistab_run_both_sims (GP, NAB,NR,plus,kMA,kMB,n_cells,title,expect):
    (A,B,OA,OB) = ions ('A','B','OA','OB')
    real = char_01BX (GP, NAB, NR, plus, n_cells)

    result=""
    for i in range (n_cells):
        if (real[i] != expect[i]):
            edge = (sim.cc_cells[A,i]==kMA) or (sim.cc_cells[B,i]==kMB)
            result += f'#{i}{"e" if edge else ""}:{expect[i]}->{real[i]},'
    print (f'{title}, expect {expect}, {result}')

# What sim results do we expect for these params? We return a string with one
# char per cell, where the char is
#	0: OA=0, OB=100 regardless of initialization
#	1: OA=100, OB=0 regardless of initialization
#	X: both OA=0 and OB=0 regardless of initialization
#	B: both OA=100/OB=0 and OA=0/OB=100 are stable.
# So remember: "1" or "0" for a summary refers to the value of OA, which is
# usually the complement of the value of A.
def expects (kMA, kMB, n_cells):
    expect=""
    for i in range (n_cells):
        concA = i*100/n_cells
        A_off = concA<kMA
        B_off = 100-concA<kMB
        if (A_off):
            char = 'B' if B_off else '1'
        else:
            char = '0' if B_off else 'X'
        expect += char
    return (expect)

#######################################################
# This section of the file characterizes the small-signal gain -- the delta in
# OA for a small delta in A -- at [A]=50. It does so for various paremeter
# values.
# It's not part of the cancer narrative and is not described in 0readme_zombie;
# so it's really just dead code.
#######################################################

# The main entry point for this section. It loop through all of the "reasonable"
# parameter choices and, for each one, runs ssgain_run_both_sims() to
# characterize the gain. By "reasonable," we mean:
#	- NAB and NR each range, independently, in (2,3,5,10).
#	- kMR is always 50
#	- kMA and kMB are always identical at 50+plus, where 'plus' ranges
#	  in (0,5,10,15,20,25). This is meant to have a switching point at 50,
#	  with a variable bistability region around that.
def small_sig_gain():
    GP = sim.Params()
    GP.no_dumps = True

    n_cells=21
    kVMax=100

    for NAB in [2,3,5,10]:
        #print (f"NAB={NAB} => inverter gain at [A]=50 is {inv_gain (NAB):.3f}")
        for NR in [2,3,5,10]:
            for plus in [0,5,10,15,20,25]:
                # Build the network.
                setup_basic_network (GP, NR, 1,1,n_cells, NAB=NAB, plus=plus)

                # Do both sims and analyze the results.
                title = f'NAB/R={NAB}/{NR}, delta={plus}'
                ssgain_run_both_sims(kVMax, plus, n_cells, title)

# The function that does the work for this file section. It runs two sims: one
# with all cells initialized to OA=100, OB=0 and one with OA=0, OB=100.
# It then computes the small-signal gain at any [A] as delta_OA / delta_A (where
# the deltas are the [A] and [OA] at two nearby values of [A]).
# Why do we need two sims? For bistable cells, [OA] depends on the initial
# conditions. And so in the formula above, the delta-[OA] is maximized: we
# subtract the smaller value of [OA] between both sims from the larger value
# between both sims. So essentially, we are assuming that a bistable cell flips
# right around here, and using that to increase gain.
def ssgain_run_both_sims(kVMax, delta, n_cells, title):
    #print (f'{title}')
    (A,B,OA,OB) = ions ('A','B','OA','OB')
    seed_A_B ()	# Set up the complementary gradients on A and B
    startup_init_Vmem()

    # First sim: all cells initialized to OA=100, OB=0
    sim.cc_cells[OA] = kVMax; sim.cc_cells[OB] = 0
    end_time = 100
    t_shots, cc_shots = sim.sim (end_time)
    OA_concs1 = cc_shots[-1][OA]

    # Second sim: all cells initialized to OA=0, OB=100
    sim.cc_cells[OA] = kVMax; sim.cc_cells[OB] = kVMax
    t_shots, cc_shots = sim.sim (end_time)
    OA_concs2 = cc_shots[-1][OA]

    OA_max = np.maximum (OA_concs1, OA_concs2)
    OA_min = np.minimum (OA_concs1, OA_concs2)

    dA = sim.cc_cells[A,1] - sim.cc_cells[A,0]
    gains = (OA_max[:n_cells-2]-OA_min[2:])/dA
    gains = np.concatenate (([0.0], gains, [0.0]))

    # Print the gains as a one-line summary.
    gain_max = np.max (gains)
    gain_50 = gains[int((n_cells-1)/2)]
    print (f"{title}: gain_max={gain_max:.3f}, gain@50={gain_50:.3f}")
    return

    # Print the gains in detail, and also plot them.
    np.set_printoptions (formatter={'float': '{:.3f}'.format})
    print (gains)
    plt.plot (cc_shots[-1][A], OA_min, label='OA_min')
    plt.plot (cc_shots[-1][A], OA_max, label='OA_max')
    plt.plot (cc_shots[-1][A], gains, label='gain')
    plt.xlabel('[A]')
    plt.ylabel('Concentration')
    plt.legend(loc='best')
    plt.title (title)
    plt.show()
