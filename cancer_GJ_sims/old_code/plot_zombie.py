import re
import numpy as np
import matplotlib, matplotlib.pyplot as plt

#	NAB/R=2/2, delta=0: gain_max=2.867, gain@50=2.192
def main():
    NABs=[]; NRs=[]; deltas=[]; gains=[]
    infile = open ("zombie.out", "r")
    for line in infile:
        fields = line.split (" ")
        NAB_NR = re.match ("NAB/R=([0-9]+)/([0-9]+),", fields[0])
        (NAB, NR) = (int(NAB_NR.group(1)), int(NAB_NR.group(2)))
        delta = re.match ("delta=([0-9]+):", fields[1])
        delta = int (delta.group(1))
        gain_max = float (fields[2][9:-1])
        gain_50  = float (fields[3][8:-1])
        #print (f"NAB/R={NAB}/{NR}, delta={delta}: gain_max={gain_max}, gain@50={gain_50}");
        NABs.append(NAB)
        NRs.append(NR)
        deltas.append(delta)
        gains.append(gain_50)
    NABs = np.array (NABs)
    NRs  = np.array (NRs)
    deltas = np.array (deltas)
    gains  = np.array (gains)

    plot_it (x, x_choices, x_name,
             lines, lines_choices, lines_name,
             outer, outer_choices, outer_name):

    # Gain vs. delta for each NR at a single NAB
    for NAB in [2,3,5,10]:
        for NR in [2,3,5,10]:
            indices = np.flatnonzero (np.logical_and (NRs==NR,NABs==NAB))
            data = gains [indices]
            #import pdb; pdb.set_trace()
            plt.plot ([0,5,10,15,20,25], data, label=f'NR={NR}')
        plt.xlabel('delta')
        plt.ylabel('gain')
        plt.legend(loc='best')
        plt.title (f'Gain vs. delta for each NR, NAB={NAB}')
        plt.show()

    # Gain vs. NAB for each delta at a single NR
    for NR in [2,3,5,10]:	# The single NR for this plot.
        for delta in [0,5,10,15,20,25]:	# The multiple lines on this plot.
            indices = np.flatnonzero (np.logical_and (NRs==NR,deltas==delta))
            data = gains [indices]
            #import pdb; pdb.set_trace()
            plt.plot ([2,3,5,10], data, label=f'delta={delta}')
        plt.xlabel('NAB')
        plt.ylabel('gain')
        plt.legend(loc='best')
        plt.title (f'Gain vs. NAB for each delta, NR={NR}')
        plt.show()

    # Gain vs. NR for each delta at a single 

def plot_it (x, x_choices, x_name,
             lines, lines_choices, lines_name,
             outer, outer_choices, outer_name):
    for O in outer_choices:		# The single value for this graph
        for L in lines_choices:		# The multiple lines on this graph
            indices = np.flatnonzero (np.logical_and (outer==O,lines==L))
            data = gains [indices]
            #import pdb; pdb.set_trace()
            plt.plot (x_choices, data, label=f'{lines_name={L}')

        plt.xlabel(x_name)
        plt.ylabel('gain')
        plt.legend(loc='best')
        plt.title (f'Gain vs. {x_name} for each {lines_name}, {outer_name}={O}')
        plt.show()

def inv_gain (NAB):
    out_50 = kVMax / (1 + ((50.0/50.0)**NAB))
    out_49 = kVMax / (1 + ((49.9/50.0)**NAB))
    return ((out_49 - out_50) / .1)


#	NAB=2 => inverter gain at [A]=50 is -1.0009999979959616
main()
