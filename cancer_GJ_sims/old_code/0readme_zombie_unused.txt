
Here are basic characterization results for cross-coupled inverters A=!B and
B=!A. They came from zombie.py char_latch(). All inverters had kVMax=100; both
inverters of the pair had the same kM and the same N. Results are in
zombie_char_latch.txt.

These sims are not part of the cancer narrative.

For various combinations of kM, and N, we looked at
	- A and B at the stable points
	- meta: what the metastable point is (it's where A=B, and is also the
	  inverters' switching point)
	- deltaA: how much [A] increases when we add a generator of 1 mole/m3*s
	  to the cell.

Results:
kM=20, N=2: A=4.174, B=95.826, deltaA=1.228, meta=30.3
kM=35, N=2: A=14.293, B=85.707, deltaA=2.135, meta=41.5
kM=50, N=2: A=43.216, B=57.276, deltaA=flipped
kM=65, N=2: A=56.747, B=56.747, no convergence
kM=80, N=2: A=62.271, B=62.271, no convergence

kM=20, N=3: A=0.794, B=99.994, deltaA=1.002, meta=27.6
kM=35, N=3: A=4.131, B=99.836, deltaA=1.018, meta=40.0
kM=50, N=3: A=11.472, B=98.807, deltaA=1.117, meta=50
kM=65, N=3: A=24.210, B=95.087, deltaA=1.558, meta=58.2
kM=80, N=3: A=47.614, B=82.588, deltaA=flipped

kM=20, N=5: A=0.032, B=100.000, deltaA=1.000, meta=24.9
kM=35, N=5: A=0.522, B=100.000, deltaA=1.000, meta=38.4
kM=50, N=5: A=3.030, B=100.000, deltaA=1.000, meta=50
kM=65, N=5: A=10.401, B=99.990, deltaA=1.003, meta=60.0
kM=80, N=5: A=24.956, B=99.705, deltaA=1.064, meta=68.5

kM=20, N=10: A=0.000, B=100.000, deltaA=1.000, meta=22.6
kM=35, N=10: A=0.003, B=100.000, deltaA=1.000, meta=36.9
kM=50, N=10: A=0.098, B=100.000, deltaA=1.000, meta=50
kM=65, N=10: A=1.328, B=100.000, deltaA=1.000, meta=61.9
kM=80, N=10: A=9.696, B=100.000, deltaA=1.000, meta=72.6

Conclusions for A & B at the fixed points:
      - Increasing N gives us higher separation between the low & high points;
	it makes the low point lower and the high point higher. This is totally
	expected.
      -	Increasing kM raises [A] and lowers [B] at the fixed point, and thus
	does the reverse of increasing N; I'm not sure why.

Conclusions for the metastable point:
      - Increasing N brings the metastable point closer to kM. That makes sense;
	kM is the point where very little input change can produce almost any
	output at all. So the higher N is, the steeper the sigmoid slope at kM,
	and the more this statement is true, which means that the switching
	point *must* be at kM.
      - For the same reasons, increasing kM increases the metastable point.

Conclusions for the deltaA:
      -	The overly simple calculation just says that an extra gen=1, combined
	with our decay=1, should give us deltaA=1. But that's just a *minimum*.
      -	Adding a flux of A (via the generation) originally increases [A] by 1.
	But B=!A, and so that decreases [B]. But A=!B, and so that increases
	[A] by a bit more, and so forth. Eventually, this will either explode
	or will settle out, depending on the loop gain -- but it will *always*
	result in deltaA>1.
      -	When N gets larger, we have a steeper sigmode knee, but a
	correspondingly less steep curve elsewhere. Since the fixed points are
	not at the knee, then in fact increasing N puts us in a region of less
	and less gain, which is why deltaA gets closer to 1 as we raise N.
      - Increasing kM works the opposite way. As noted above, it brings the
	stable [A] and [B] both closer to the middle (though I still don't
	know why). This brings them both closer to the knee, and thus closer to
	a high-gain point -- which is why the same flux of A results in a
	higher final deltaA.

****************************************************************************

The next set of analyses is from the function predict_bistab(), and finds which
[A] force a single cell to be 1, which force it to 0 and which allow
bistability. The outputs are saved in zombie_bistab_21c.txt. We also have _20c
for the 20-cell version, which is the same except that it uses a field of 20
cells rather than 21 cells (which means that there will not be a cell where
[A]=[B]=50, and which thus will eventually have every row having an unambiguous
"correct" outcome in later experiments).

We test a simple GRN with OA=!A & !OB, OB=!B & !A0. The gates are Hill
functions, parameterized with kMA, kMB, kMR (for the repressors A0 and B0), and
similarly NA, NB and NR.

For each parameter combo, we analyze the GRN. We instantiate a straight chain
of 21 cells, but there are no GJs -- so the cells are separate. We then create
fixed gradients of A (running from 0 in cell 0 to 100 in cell 20) and B (from
100 in cell 0 to 0 in cell 20). We then check the steady-state response of each
cell. In fact, we run two sims -- first with all cells initialized to OA=0
and OB=100, and next with all cells initialized to OA=100 and OB=0. If both sims
wind up with OA>50 and OB<50, the cell is marked as a 1; both with OA<50 and
B0>50 => 0, both sims with OA<50 and B0<50 => X; and if both sims hold their
values for OA and OB => B.

There are a few big takeaways. First, we want to keep NR reasonably high;
setting NR=2 often precludes bistability, and bistability is really the whole
point of these sims.
Second, we also want to keep NA and NB reasonably high. We discriminate [A] and
[B] using expressions like kVMax / [1 + ([A]/kMA)**NA + ([B]/kMR)**NR].
When [A]<kMA, we want the ([A]/kMA)**NA term to approach 0 so that the
expression can simplify to cross-coupled inverters. But that can only happen if
NA is big enough.
Third, with hindsight there was no reason to ever have NA!=NB (and I've edited
the code to reflect this).
Finally, we want to keep kMR=50. After all, it's mostly sensing the output of a
gate that detects a threshold of [A] or [B], and by definition an input of kM
gives an output of .5 * max, or in our case 50.

So, on to the detailed data.
NAB/R=10/10, kMA/B/R=50/50/50, expect 111111111110000000000, #10e:1->X,
Position #10 is the final "1", at [A]=50.
The results of the first sim:
   [   0    5   10   15   20   25   30   35   40   45   50   55   60   65   70   75   80   85   90   95  100] A
   [ 100   95   90   85   80   75   70   65   60   55   50   45   40   35   30   25   20   15   10    5    0] B
   [ 100  100  100  100  100  100   99   97   90   74   49    2    0    0    0    0    0    0    0    0    0] OA1
   [ 100  100  100  100  100  100   99   97   90   74   35    2    0    0    0    0    0    0    0    0    0] OA2
   [   0    0    0    0    0    0    0    0    0    2   35   74   90   97   99  100  100  100  100  100  100] OB1
   [   0    0    0    0    0    0    0    0    0    2   49   74   90   97   99  100  100  100  100  100  100] OB2
   [   0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20] cell
So cell #10 is the perfect [A]=[B]=50 cell.
It gets [OA]=49,[OB]=35 for the first (OA=100) sim, and vice versa in the second sim.
So, it was predicted as 1 (because I used "on if >= kM" rather than just ">"),
but is actually bistable -- and the bistable values are just slightly under 50.
So this is really an edge effect. That's not surprising: the "#10e" means that cell #10
has a [A] or [B] that exactly matches its corresponding kMA or kMB.
-------------------
NAB/R=10/10, kMA/B/R=65/35/50, expect 111111111111110000000, #13e:1->X,
NAB/R=10/10, kMA/B/R=35/65/50, expect 111111110000000000000, #7e:1->X,
NAB/R=10/10, kMA/B/R=80/20/50, expect 111111111111111110000, #16e:1->X,
NAB/R=10/10, kMA/B/R=20/80/50, expect 111110000000000000000, #4e:1->X,
Exactly the same issue.
-------------------
NAB/R=10/10, kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #13e:B->0,
This is the final "B" of the sequence becoming a "0", and is another edge,
since cell #13 is when [A]=65.
-------------------
NAB/R=10/10, kMA/B/R=65/50/20, expect 11111111111BBB0000000, #9:1->1000,#10e:1->1000,#13e:B->0001,#14:0->0001,
This is the cells at the 1/B transition and the B/0 transition.
And it involves cell #10 ([A]=50), #13([A]=65), both being edge cells.
But cells #9 and #14 are not edge-effect cells. What happened with them?

   [   0    5   10   15   20   25   30   35   40   45   50   55   60   65   70   75   80   85   90   95  100] A
   [ 100   95   90   85   80   75   70   65   60   55   50   45   40   35   30   25   20   15   10    5    0] B

   [ 100  100  100  100  100  100  100  100   99   98   93   84   69   50   32    0    0    0    0    0    0] OA
   [ 100  100  100  100  100  100  100  100   99    4    0    0    0    0    0    0    0    0    0    0    0] OA

   [   0    0    0    0    0    0    0    0    0    0    0    0    0    0    1  100  100  100  100  100  100] OB
   [   0    0    0    0    0    0    0    0    0   28   50   74   90   97   99  100  100  100  100  100  100] OB

   [   0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20] cell
So cells [9,14] are bistable; we had predicted cells [11,13].
Cells 9 and 10 are bistable -- but [OB], which is "high", never rises above 50 and is hence misinterpreted.
Cells 13 and 14 are similar, but now it's [OA] that's "high" but never rises above 50.
Cell 90 was predicted as 1, since [A]=45<65 and [B]=55>50. So we figured that the OB gate is forced low,
and OA goes high. That's indeed what happened when we initialized OA=100. But when we initialized
OB=100, we got OA=4 and OB=28. So [B]=55>50 pushed OB somewhat low (28), but we had kMR=20 -- which didn't
call 28 low enough. So the bottom line is to always pick kMR=50.
In fact, the kMR=50 with N=10/10/10 has no errors other than edge effects!
-------------------
Let's do some more data analysis to see what worked well.
The entire data set has 8000 permutions: N=(2,3,5,10) and kM=(20,35,50,65,80)
for the A, B and R gates, for 20*20*20=8000.
The full data set has 21779 mistakes, of which 8828 edge cases and 12951 real,
or 1.6 non-edge mistakes/case.

There are 125 10/10/10 cases (the cross product of [20,35,50,65,80] for each of
kMA, kMB and kMR). They have 201 total mismatches; 145 of them are edge cases,
eaving 56 non-edge cases, or average .45 non-edge mismatches/case.

There are 125 5/5/5 cases. They have 282 total mismatches; 145 of them are
edge cases, leaving 137 non-edge cases, or average 1.1 non-edge mismatches/case.

There are 125 3/3/3 cases. They have 325 total mismatches; 135 of them are
edge cases, leaving 190 non-edge cases, or average 1.5 non-edge mismatches/case.

There are 125 2/2/2 cases. They have 439 total mismatches; 130 of them are
edge cases, leaving 309 non-edge cases, or average 2.5 non-edge mismatches/case.

So clearly high gain helps, and gain of 2 is a big step worse. But which matters
more, gain in kMA and kMB, or in kMR?

Let's look at 10/10/3 vs. 3/3/10.
There are 125 10/10/3 cases. They have 239 total mismatches; 135 of them are
edge cases, leaving 104 non-edge cases, or average .8 non-edge mismatches/case.

There are 125 3/3/10 cases. They have 404 total mismatches; 145 of them are
edge cases, leaving 259 non-edge cases, or average 2.1 non-edge mismatches/case.
So clearly, for our case, the ability to discriminate between nearby [A] and [B]
values is important.

Next, there's little reason to have the cross-repression inflection point kMR
set to anything other than 50. After all, it's mostly sensing the output of a
gate that detects a threshold of [A] or [B], and by definition an input of kM
gives an output of .5 * max, or in our case 50. So let's restrict ourself to
kMR=50. How does that affect our results?

Looking only at kMR=50, the 25 10/10/10 cases have only edge errors.
The 25 5/5/5 cases have 37 errors, of which 25 are edge cases and 12 are real,
so .48 non-edge mismatches/case (vs 1.1 before)
The 25 3/3/3 cases have 69 errors, of which 25 are edge cases and 44 are real,
so 1.8 non-edge mismatches/case (vs 1.5 before)
The 25 2/2/2 cases have 89 errors, of which 27 are edge cases and 62 are real,
so 2.4 non-edge mismatches/case (vs 2.5 before)
So somewhat mixed results for sticking to kMR=50.

Let's also say that the concept of having "X" cases (where we pick kMA and kMB
such that we have both [A]>kMA and [B]>kMB in some single cell) seems like a bad
idea; it means that the cell is forced into an illegal state. Let's remove all
permutations that have and "X" in their expected strings.
We now have 2899 cases; 14066 errors, of which 3698 are edges and 10368 are
real, or 3.6 non-edge mismatches/case (vs. the original 1.6). So this has
made things much worse!

What about the combination of kMR=50 and no "X" cases? Now we get 516 cases;
they have 1754 errors, of which 516 edge cases and 1238 real, or 2.4 non-edge
mismatches/case. So we're still worse off than the original full data set.
1.6) rather than better!

Let's look at some useful cases. 65/65/50 is a simple detector of [A]=[B]=50
with bistability from [A]=35 to [A]=65. It has the following results:
NAB/R=2/2,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#9:B->X,#10:B->X,#11:B->X,#12:B->0,#13e:B->0,
NAB/R=2/3,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#9:B->1,#10:B->X,#11:B->0,#12:B->0,#13e:B->0,
NAB/R=2/5,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#9:B->1,#11:B->0,#12:B->0,#13e:B->0,
NAB/R=2/10,   kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #13e:B->0,

NAB/R=3/2,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#9:B->1,#10:B->X,#11:B->0,#12:B->0,#13e:B->0,
NAB/R=3/3,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#9:B->1,#11:B->0,#12:B->0,#13e:B->0,
NAB/R=3/5,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#12:B->0,#13e:B->0,
NAB/R=3/10,   kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #13e:B->0,

NAB/R=5/2,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#9:B->1,#10:B->X,#11:B->0,#12:B->0,#13e:B->0,
NAB/R=5/3,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#9:B->1,#11:B->0,#12:B->0,#13e:B->0,
NAB/R=5/5,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#12:B->0,#13e:B->0,
NAB/R=5/10,   kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #13e:B->0,

NAB/R=10/2,  kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#9:B->1,#10:B->X,#11:B->0,#12:B->0,#13e:B->0,
NAB/R=10/3,  kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#12:B->0,#13e:B->0,
NAB/R=10/5,  kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #13e:B->0,
NAB/R=10/10, kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #13e:B->0,

It's amazing how much good a little bit of gain does!
In the 10/2 case, there's just not enough cross-coupled gain to be bistable,
and so all of the expected Bs go to 0 or 1. The same, of course, is true for
5/2, 3/2 or 2/2. Motto: NR=2 is bad :-(.

But we also need gain for NA and NB. We discriminate [A] and [B] using
expressions like kVMax / [1 + ([A]/kMA)**NA + ([B]/kMR)**NR]. When [A]<kMA, we
want the ([A]/kMA)**NA term to approach 0 so that the expression can simplify
to cross-coupled inverters. But that can only happen if NA is big enough.
Thus, we have
NAB/R=2/5,    kMA/B/R=65/65/50, expect 11111111BBBBBB0000000, #8:B->1,#9:B->1,#11:B->0,#12:B->0,#13e:B->0
In this case, only one of the expected B cells was actually bistable. And so the
other message: NA=2 or NB=2 are also bad.

So the bottom line is that 3/3/5 is reasonable, and 3/3/10 is even better.

***********************************************************************


The next set of analyses computes small-signal gain of a single cell.
We use the function char_GRN1() and plot the results with plot_zombie.py.
Results are in zombie.out22. Note that both char_GRN1() and zombie.out22 are
lost.

We only look at "reasonable" parameter sets:
	- NAB and NR each range, independently, in (2,3,5,10).
	- kMR is always 50
	- kMA and kMB are always identical at 50+plus, where 'plus' ranges
	  in (0,5,10,15,20,25). This is meant to have a switching point at 50,
	  with a variable bistability region around that.
For each parameter set, we compute the small-signal gain at [A]=50. Roughly,
this is (([OA] at [A]=49.9)-([OA] at [A]=50))/.1. However, there are two tricks:
- unsurprisingly, we always keep [B]=100-[A]
- cells can be bistable, with the final [OA] depending on initial conditions.
  We thus run *two* sims for each operating point (and so four sims altogether).
  One sim initializes all cells to OA=100, OB=0, and one to OA=0, OB=100.
  We maximize gain by redefining our gain formula as
	((maximum [OA] at [A]=49.9)-(minimum [OA] at [A]=50))/.1.
  So essentially, we are assuming that a bistable cell flips right around our
  operating point, and using that to increase gain.

The results:
 - For NR>2, gain rises with delta and with NAB.
 - For NR=2, life is a bit complex. First, the gain for NR=2 is always much
   less than with NR>2. Second, for NAB=2,3,5, gain with NR=2 is almost
   independent of delta. For NAB=10, though, gain with NR=2 actually falls with
   delta.

Why does increasing NAB increase gain? With delta=0, it's obvious: we have a
front end of an inverter with a switching point at 50; higher N gives higher
gain. With delta>0, it's a bit less obvious; what if NAB is high enough that
suddenly [A]=50 is now on the flat portion of the sigmoid curve? If so, then in
fact small changes in [A] make no change in [OA] or [OB]; in that sense, the
gain is zero. But again, we've *defined* delta-[OA] as including bistability.
And when NAB is large, it enables [A] to "get out of the way" in the term
([A]/kMA)**NAB, which lets bistability really create a large difference between
the two stable operating points.

Why does increasing NR increase gain? Because it makes the cell more bistable;
and specifically puts more space between OA and OB. And from our definition of
gain, again, more bistability increases gain.

Why does increase delta increase gain? Because, once again, it increases
bistability.

***********************************************************************

Zombie experiments

These experiments use zombie(), with results in zombie_1x1, zombie_2x2,
zombie_3x3 and zombie_4x4.
They build a 12x21 2D field and do lots of things with it.

At a high level, these experiments show that the diffusivity of OA and OB must
be tightly regulated. Too little and cells don’t rescue bad neighbors; too much
and boundaries are very rough. This makes it hard to get sharp boundaries
simply by reducing diffusivity. 

We don't use a Gillespie simulator. But these sims are the next-best thing;
probabilistically, the bistable cells tend to be correct, but are not
guaranteed so. And that's why we see the small wrong-value patterns that we're
simulating here.
 
These experiments also show the good-zombie effect; the cells that flip become
totally indistinguishable from their neighbors.

We see the expected results, individually in every single parameter combo:
  - with Kd too low, each cell is isolated.
  - with Kd just right, majority wins.
  - with Kd too large, cells collapse and cannot sustain a border.

In more detail, the sims show that
   - as we raise Kd, the [OA] and [OB] is first changing very little.
   - when we hit a critical value, the center cell full snaps and
     is indistinguishable from its neighbors. It's a true zombie! All of this is
     still happening with minimal changes in [OA] and [OB].
   - as we raise Kd still further, the collapse gets substantial in the bistable
     zone, but still is minimal at the ends.
   - when we hit a large enough Kd, the collapse spreads much further, and
     eventually the field is essentially uniform. At this point, the Thevenin
     resistance of the GJs is presumably much less than that of the GRN.

Interesting observation by manually inspecting the outputs: for ungated
parameter sets, Kd_min0 is always the same as Kd_min1. So it's exactly as easy
to fix a 1-spot on a zero background as the reverse.

Look at how Kd_min0 varies with spot size. Using anal_by_cat() on spot_size, we
see that all 84 groups have Kd_min0 increasing with spot size, by an average of
1.8x from 1x1 to 4x4. This is expected and important -- it says that larger
spots are better at surviving.

Let's look at Kd_rat, the ratio of Kd values over which the system works.
Using zombie_plot.py / anal_by_cat() on zombie_1x1, 2x2 and 3x3 merged:
	gated=([False, True]) => avg Kd_rat=(58, 38), std Kd_rat=(33, 32),
				 min Kd_rat=(19, 3.4), max Kd_rat=(160, 130)
	NR=([3, 5, 10]) => avg Kd_rat=(58, 45, 43), std Kd_rat=(42, 31, 26)
			   min Kd_rat=(3.6, 3.4, 6.2), max Kd_rat=(160,130,110)
	NAB=([3, 5, 10]) => avg Kd_rat=(43, 47, 53), std Kd_rat=(33, 33, 34)
			    min Kd_rat=(3.4, 3.6, 3.9), max Kd_rat=(150,160,160)
	delta=([5,10,15,20,25]) =>avg=(40,45,47,55,50),std=(36,35,30,34,33)
				  min=(3.4,3.6,9,11,7),max=(160,160,140,150,140)
    So gating hurts a bit (30%), higher NR hurts a bit (20%), higher NAB helps
    20%, and higher delta helps by 25% unless it's too high. But this is all
    dwarfed by the enormous variability across parameter sets. Basically, most
    parameter combos that increase Kd_min also increase Kd_max!

    Larger spot sizes (e.g., 4x4 vs. 1x1) produce uniformly lower Kd_rat values.
    The ratio is the ratio of Kd for border collapse divided
    by Kd for spot removal. Since Kd for border collapse is independent
    of spot size, and removing a spot needs a higher Kd for larger spots,
    this follows obviously.

Let's confirm the effect of gating on Kd_rat by using anal_groups() on all
three spot sizes merged. First look at Kd. For all 126 pairs of gated/ungated
params:
      -	Kd_min0 always increases when gating. The average ratio is 2.7x.
      - Kd_max increases in 99 cases and decreases in 27, by an average of 2.1x.
      - Comparing the above two facts, it's not surprising that Kd_rat increases
	when gating in 35 pairs and decreases in 91 pairs; average ratio is .71.

Next, look at border sharpness using anal_groups() to compare the border
sharpness using gated vs. ungated GJs. Specifically, for each parameter-combo
pair where one of the pair is ungated and the other is gated, we'll compare
ordOrgMin across the two. The hope is that gating will be a big win, but that's
not what we see:
      - bordOrgMin always increases, but only by 1.3x on average (and once stays
	the same).
      - bordOrgMid increases by 1.5x on average, but only in 125 of the cases.
      - bordOrgMax increases by 1.1x on average, but only in 93 cases (39 are
	equal or decreasing).
Note that this is a bit of an odd measure. For a *gated* parameter combo,
BordUnMin actually represents the sim that uses gating from the start and then
doesn't do a second sim. So we wouldn't expect gating to help much -- and it
doesn't. What happens, of course, is that gating increases both Kd_min and also
Kd_max.

But now look at border sharpness a different way, using anal_by_cat(). We set
it up to only use ungated parameter sets, and we look at bordMinRat (the extra
benefit we get by post-sim gating when Kd is at the minimum value). We have 126
ungated combos.
    For bordMinRat
	Over all 126 param sets: bordMinRat average=2, in [1,4.1], std=.95
	By NR=(3,5,10):  avg=(1.2, 1.7, 3.0), std=(0.15, 0.48, 0.87)
	By NAB=(3,5,10): avg=(1.6, 2.0, 2.3), std=(0.71, 0.94, 1.00)
	By delta=(5,10,15,20,25): avg=(1.5,1.9,2,2.2,2.3), std=(.56,.86,.95,1,1)
	So the best predictor is NR.
    For bordMidRat:
	Over all 126 param sets: bordMinRat average=5, in [1.2,16], std=3.6
	By NR=(3,5,10):   avg=(2.4, 3.9, 8.2), std=(0.8, 1.8, 4)
	By NAB=(3,5,10):  avg=(3.5, 5.0, 6.3), std=(2.6, 3.6, 3.9)
	By delta=(5..25): avg=(2.9,4.5,5.1,5.8,6.3), std=(2,3.3,3.6,3.8,4)
	So the gating is now much more effective, and again NR predicts best.
    For bordMaxRat:
	Over all 126 param sets: bordMinRat average=7.6, in [1.3,20], std=6.4
	By NR =(3,5,10):  avg=(3.3, 5.9, 13), std=(1.4, 3.3, 7.5)
	By NAB=(3,5,10):  avg=(5.0, 7.5, 10), std=(4.2, 6.3, 7.0)
	By delta=(5..25): avg=(3.9,6.6,7.8,9,9.9), std=(3.2,5.6,6.4,6.7,6.9)
	So the gating is even more effective, and again NR predicts best.
These results are *much* better!

Adding gated GJs has ambiguous results. Typically, the valid Kd range goes down
about 2x (from, say, 8x to 4x) and the border becomes slightly less sharp (e.g.,
from 2x to 1.9x). But there's lots of variation; sometimes gating improves
things instead. And a few parameters, combined with gating, result in a "cannot
fix" where, instead of the neighboring 0 cells fixing the bad 1 cell, the
reverse happens and row 10 becomes all 1s.

***********************************************************************
