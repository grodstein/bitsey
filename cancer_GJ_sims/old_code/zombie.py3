import numpy as np
import matplotlib, matplotlib.pyplot as plt
import sys; sys.path.append ("..")	# To get the usual Bitsey files
import GJ_lib
import sim, eplot as eplt, edebug as edb
#import pdb; pdb.set_trace()

kVMax=100

################################################################
# Conventions
#	0 means that OA<<OB; 1 means that OA>>OB; X means neither is true
#	Or..."0" means that OA<50 and OB>50, independent of initialization,
#	     "1" means that OA>50 and OB<50, independent of initialization.
#	     "B" means that both initial values are stable
#	     "X" means that OA<50 and OB<50, independent of initialization
#	     (because both NOR gates are forced on by [A] or [B])
################################################################

################################################################
# Random utilities
################################################################

# Given a list of ion names, return a list of ion positions
def ions(*ion_list):
    pos_list=[]
    for i in ion_list:
        pos_list.append (sim.ion_i[i])
    return (pos_list)

BAD_SEARCH = -99
def binSearch (calc_func, x0, x2, y0=None, y2=None, print_func=None, why=""):
    if (why != ""):
        print ("Starting binary search for", why)
    if (y0 == None):
        y0 = calc_func(x0)
    if (y2 == None):
        y2 = calc_func(x2)
    if (y0 == y2):
        print (f"Binsearch points are {y0} and {y2}")
        return (BAD_SEARCH,BAD_SEARCH)
    if (y0== BAD_SEARCH) or (y2==BAD_SEARCH):
        print ("Bad search returned by calculation function")
        return (BAD_SEARCH,BAD_SEARCH)

    while (x2 > 1.001 * x0):
        x1 = (x0+x2)/2
        y1 = calc_func(x1)
        if (print_func != None):
            print_func (x1, y1)
        if (y1 == y0):
            x0 = x1
        elif (y1==y2):
            x2 = x1
        else:
            print ("Bad search returned by calculation function")
            return (BAD_SEARCH,BAD_SEARCH)

    #print (f"Binary search returning ({x0:.4g},{x2:.4g})")
    return (x0,x2)

def seed_A_B ():
    n_cells = sim.cc_cells.shape[1]
    (A,B) = ions ('A','B')
    sim.cc_cells[A] = np.linspace (0, kVMax, n_cells)
    sim.cc_cells[B] = np.linspace (kVMax, 0, n_cells)

# Create a 3D array of integers, such that each element is
#     - 0 if cc3D[OA] < cc3D[OB] - 2
#     - 1 if cc3D[OA] > cc3D[OB] + 2
#     - 2 otherwise (i.e., X)
def array_to_01X (cc):
    (OA,OB) = ions ('OA','OB')
    n_ions = sim.cc_cells.shape[0]
    cc3D = cc.reshape ((n_ions,g_maxX,g_maxY,g_maxZ))
    is_1 = (cc3D[OA,:,:,:] > cc3D[OB,:,:,:] + 2)
    is_0 = (cc3D[OA,:,:,:] < cc3D[OB,:,:,:] - 2)
    is_X = np.logical_not(is_0) & np.logical_not(is_1)
    out = np.zeros_like (is_1, dtype=np.int32)
    out[is_1] = 1
    out[is_X] = 2
    return (out)

def char_01BX (p, NAB, NR, plus, n_cells):
    setup_basic_network (p, NAB, NR, plus, 1,1,n_cells)
    startup_init_Vmem()

    (A,B,OA,OB) = ions ('A','B','OA','OB')
    seed_A_B ()		# Set up the complementary gradients on A and B

    # First sim: all cells initialized to OA=100, OB=0
    sim.cc_cells[OA] = kVMax; sim.cc_cells[OB] = 0
    end_time = 100
    t_shots, cc_shots = sim.sim (end_time)
    out_1 = array_to_01X (cc_shots[-1])

    # Second sim: all cells initialized to OA=0, OB=100
    sim.cc_cells[OA] = 0; sim.cc_cells[OB] = kVMax
    t_shots, cc_shots = sim.sim (end_time)
    out_2 = array_to_01X (cc_shots[-1])

    is0 = ((out_1==0) & (out_2==0)) [0,0]
    is1 = ((out_1==1) & (out_2==1)) [0,0]
    isB = ((out_1==1) & (out_2==0)) [0,0]

    str = ""
    for i in range(n_cells):
        str += ("0" if is0[i] else "1" if is1[i] else "B" if isB[i] else "X")
    return (str)

# Setup the basic network.
def setup_basic_network (p, NAB, NR, plus, dimX,dimY,dimZ,
			 do_gate=False, do_drive_Vmem=False, Kd=0):
    coord_set_dimensions (dimX, dimY, dimZ)
    n_GJs = setup_GJ_3D ()
    n_cells = dimX * dimY * dimZ
    sim.init_big_arrays (n_cells=n_cells, n_GJs=n_GJs,p=p,
    			 extra_ions=['A','B','OA','OB'])
    (A,B,OA,OB) = ions ('A','B','OA','OB')

    # Set up cross-coupled-NOR GRN, identically in every cell.
    nor1=Hill_NOR2_gate (sim.Gate.GATE_GD,OA,A,OB,
			 kMM=50+plus,kMR=50, NM=NAB,NR=NR, kVMax=kVMax)
    nor2=Hill_NOR2_gate (sim.Gate.GATE_GD,OB,B,OA,
			 kMM=50+plus,kMR=50, NM=NAB,NR=NR, kVMax=kVMax)

    # Set up decay of OA and OB
    dOA = sim.GD_const_gate (sim.Gate.GATE_GD, OA, decay=1)
    dOB = sim.GD_const_gate (sim.Gate.GATE_GD, OB, decay=1)

    # Physical parameters of A, B, OA and OB
    sim.z_array[[A,B,OA,OB]] = 0	# neutral ions
    sim.GJ_diffusion[[A,B]] = 0

    # Create the [A]-drive-Vmem gatings by instantiating a gate. 
    if (do_drive_Vmem):
        set_Vmem_from_conc()

    # Full GJ connections along the field. All are simple & ungated for now.
    GJs_3D_give_to_sim ()

    # Set up voltage-gated GJs by, again, instantiating a gate.
    # No need to give it any connections; it applies equally well to all GJs.
    if (do_gate):
        GJ_lib.setup_GJ_model ('C45', 'C45', -.1, .1, .002)
        GJ_lib.GJ_gate()

# The gate class implementing a two-input NOR gating.
# It's the Hill function meant to be used as a "generate" term:
#	kVmax / (1 + ([M]/kMM)*NM + ([R]/kMR)*NR)
# where "M" is the morphogen input (either A or B) and "R" is the
# cross-repressor (either OA or OB).
# So we would instantiate one gate with output<=OA, M<=A, R<=OB and another
# with output<=OB, M<=B, R<=OA.
class Hill_NOR2_gate (sim.Gate):
  def __init__(self, gtype,out_ion,
 		in_ionM,in_ionR,kMM=1,kMR=1,NM=1,NR=1, kVMax=1):
    sim.Gate.__init__ (self, gtype, out_ion)
    self.in_ionM=in_ionM; self.in_ionR=in_ionR
    self.kMM=kMM; self.kMR=kMR
    self.NM=NM;   self.NR=NR
    self.kVMax=kVMax

  # The function that actually computes the gating vector at runtime.
  def func (self, cc, Vm_ignore, deltaV_GJ_ignore, t_ignore):
    # self.params[] is [5, # cells or GJs]. Assign each of its rows (i.e., a
    # vector of one parameter across all cells) to one variable.
    concM = cc[self.in_ionM]	# vector [N_CELLS]
    concR = cc[self.in_ionR]	# vector [N_CELLS]
    termM = (concM/self.kMM)**self.NM
    termR = (concR/self.kMR)**self.NR
    return (self.kVMax / (1 + termM + termR))

# This is an ion-channel gating function; we use it to set a cell's Vmem based
# on its concentrations of A and B:
#	If [A]>[B], set Dm_K=160e-18 => Vmem=-72mV
#	If [A<>[B], set Dm_K=  1e-18 => Vmem=-13mV
# The default is Dm_K=1e-18, so the gating function returns a number in [1,160].
class set_Vmem_from_conc (sim.Gate):
  def __init__ (self):
    sim.Gate.__init__ (self, dest=self.GATE_IC, out_ion=sim.ion_i['K'])
    (self.A,self.B) = ions ('A','B')

  def func (self, cc, Vm_ignore, deltaV_GJ_ignore, t_ignore):
    ratio = (cc[self.A]/(.001+cc[self.B]))**4
    factor = ratio / (1+ratio)
    out = 1 + factor*159	# interpolate between 1e-18 & 160e-18.
    return (out)

# Keep the same network, but reset sim.cc_cells to prepare for a new simulation.
def network_reset():
    (Na,K,Cl,P,A,B) = ions ('Na','K','Cl','P','A','B')
    cc_cells[Na,:] = 10
    cc_cells[K,:]  = 125
    cc_cells[Cl,:] = 55
    cc_cells[P,:]  = 80
    seed_A_B ()

################################################################
# A group of functions and globals to deal with GJs between nearest neighbors
# in a 3D field of cells. We use
# To find the index of a particular GJ, use
#	GJs_3D.index (GJs_3D_canonID (x,y,z,dx,dy,dz))
################################################################

# Globals and constants for our 3D GJ package.
GJs_3D = []		# One list element per GJ. The value of GJs_3D[i] is the
			# canonical ID, returned by GJs_3D_canonID(),for that GJ

# Store the size of the 3D field
g_maxX=0
g_maxY=0
g_maxZ=0

def coord_set_dimensions (x,y,z):
    global g_maxX, g_maxY, g_maxZ
    g_maxX=x; g_maxY=y; g_maxZ=z

def seed3D_A_B (kVMax):
    (A,B) = ions ('A','B')
    n_ions = sim.cc_cells.shape[0]
    cc3D = sim.cc_cells.reshape ((n_ions,g_maxX,g_maxY,g_maxZ))
    cc3D[A] = np.linspace (0, kVMax, g_maxZ)
    cc3D[B] = np.linspace (kVMax, 0, g_maxZ)

# Handle GJs in a 1D, 2D or 3D field of cells. The code always uses 3D; to
# setup, say, a 1D field, you just give dimX=dimY=1
def setup_GJ_3D ():
    global GJs_3D
    GJs_3D=[]
    for x in range (g_maxX):
        for y in range (g_maxY):
            for z in range (g_maxZ):
                if (x+1<g_maxX):
                    alloc_GJ (x,y,z,1,0,0)
                if (y+1<g_maxY):
                    alloc_GJ (x,y,z,0,1,0)
                if (z+1<g_maxZ):
                    alloc_GJ (x,y,z,0,0,1)
    return (len(GJs_3D))

# After someone has called init_big_arrays() and the official GJ arrays exists,
# we now set their 'from' and 'to' appropriately.
def GJs_3D_give_to_sim ():
    for idx in range (len(GJs_3D)):
        (x,y,z,dx,dy,dz) = coord_canonID2xyzd (GJs_3D[idx])
        sim.GJ_connects['from'][idx] = coord_xyz2idx (x,y,z)
        sim.GJ_connects['to'][idx]   = coord_xyz2idx (x+dx,y+dy,z+dz)
        #print (f"Set GJ[{idx}] = {x},{y},{z} -> {x+dx},{y+dy},{z+dz}")
        #print (f"Set GJ[{idx}] = {coord_xyz2idx (x,y,z)}->{coord_xyz2idx (x+dx,y+dy,z+dz)}")

# Add an entry in the list GJs_3D[] for a new GJ, from x,y,x to x+dx,y+dy,z+dz.
# Dx, dy and dz can be 0 or +1.
def alloc_GJ (x,y,z,dx,dy,dz):
    assert (x<g_maxX) and (y<g_maxY) and (z<g_maxZ)
    ID = coord_xyzd2canonID (x,y,z,dx,dy,dz)
    GJs_3D.append (ID)
    #print (f"x={x},y={y},z={z},dx={dx},dy={dy},dz={dz} => ID={ID}")

# Given a GJ from the cell at (x,y,z) to the cell at (x+dx,y+dy,z+dz): change
# that six-number identifier into a single unique integer suitable for lookup.
def coord_xyzd2canonID (x,y,z,dx,dy,dz):
    xyz = coord_xyz2idx (x,y,z)
    return ((xyz<<3) + (dx<<2) + (dy<<1) + dz)
    
# Given a field of dimensions (g_maxX, g_maxY, g_maxZ), transate a coordinate
# set (x,y,z) in to a linear index. Used only just above.
def coord_xyz2idx (x,y,z):
    assert (x<g_maxX) and (y<g_maxY) and (z<g_maxZ)
    return (x*g_maxY*g_maxZ + y*g_maxZ + z)

def coord_canonID2xyzd (canonID):
    (dx,dy,dz) = ((canonID&4)>>2, (canonID&2)>>1, canonID&1)
    idx = canonID>>3
    (x,y,z) = coord_idx2xyz (idx)
    return (x,y,z,dx,dy,dz)

def coord_idx2xyz (idx):	# used only just above
    z = idx % g_maxZ; idx = int ((idx-z) / g_maxZ)
    y = idx % g_maxY; idx = int ((idx-y) / g_maxY)
    x = idx
    return (x,y,z)

# Given the coordinates of a GJ, return its linear index.
def coord_xyzd2idx (x,y,z,dx,dy,dz):
    return (GJs_3D.index (coord_xyzd2canonID (x,y,z,dx,dy,dz)))

# Gien a GJ's linear index, return its coordinates.
def coord_idx2xyzd (idx):
    canonID = GJs_3D[idx]
    return (x,y,z,dx,dy,dz)

def dump_3D (cc,what_to_show="OA:OB",bool=True):
    (A,B,OA,OB) = ions ('A','B','OA','OB')
    field = 3 if bool else 6
    print ("z=... ", "".join ([f'{z:^{field}}' for z in range(g_maxZ)]))

    # We index cc3D as [ion,x,y,z]
    n_ions = cc.shape[0]
    cc3D = cc.reshape ((n_ions,g_maxX,g_maxY,g_maxZ))

    for x in range (g_maxX):
        for y in range (g_maxY):
            print (f"x={x},y={y} ",end="")
            for z in range (g_maxZ):
                if (what_to_show=="OA:OB"):
                  if (bool):
                    str = '1' if cc3D[OA,x,y,z]>cc3D[OB,x,y,z]+2 \
                    else  '0' if cc3D[OB,x,y,z]>cc3D[OA,x,y,z]+2 \
                    else  'X'
                  else:
                    str = f'{cc3D[OA,x,y,z]:.0f}:{cc3D[OB,x,y,z]:.0f} '
                elif (what_to_show=="A:B"):
                    str = f'{cc3D[A,x,y,z]:.0f}:{cc3D[B,x,y,z]:.0f} '
                print (f"{str:{field}}",end="")
            print ('')
        print ('')

    #idx = coord_xyz2idx (0,2,10)
    #print (f"Cell is index {idx}")
    #quit()


################################################################
# The first part of the file: characterizing simple cross-coupled inverters.
# Simply characterize a single cross-coupled-inverter latch (A=!B and B=!A).
# Hill functions for the two inverters and linear decay. Just one cell, no GJs.
# Do the characterization for a range of kM and N. What we characterize is
# - find the single stable point via a single sim of the cross-coupled invs.
# - find its resilience. I.e., disturb the latch by injecting a constant
#   generation rate of A, and see how much the stable values change. In some
#   cases, we may even flip the cell when we do this.
# - finally, find the metastable point (and re-find all stable points) by a
#   binary-search technique using an open-loop circuit.
################################################################

def char_latch():		# The top-level entry point.
    GP = sim.Params()
    GP.no_dumps = True

    for N in [2,3,5,10]:
        for kM in [20,35,50,65,80]:
            # Find the steady-state operating point, and measure resilience.
            # We set up two inverters A=!B and B=!A.
            closed_loop_sims (GP, N, kM)

            # Now find all stable & metastable points. We use the open-loop
            # circuit B=!A, C=!B.
            setup_latch_openloop (GP, N, kM)	# Build the network.
            startup_init_Vmem()
            (A,B,C) = ions ('A','B','C')

            # Find zero-crossing points and then use binary search to narrow in.
            sign_old = openloop_sim (Ainit=0)
            for Anew in range (1, kVMax+1):
                sign_new = openloop_sim (Ainit=Anew)
                if (sign_new != sign_old):
                    (Aloop,_) = binSearch (openloop_sim, Anew-1, Anew)
                    openloop_sim (Ainit=Aloop)
                    type = "metastable" if (sign_new==1) else "stable"
                    print (f"\tFixed {type} point with {sim.cc_cells[A,0]:.3f} "
                           f"-> {sim.cc_cells[B,0]:.3f} -> {sim.cc_cells[C,0]:.3f}")
                sign_old = sign_new


# Build the network for the "closed-loop" configuration, where A=!B and B=!A.
def setup_latch (p, N, kM, genA):
    sim.init_big_arrays (n_cells=1, n_GJs=0, p=p, extra_ions=['A', 'B'])
    (A,B) = ions ('A','B')

    # gtype, out_ion, in_ion, inv=None,kM=1,N=1,kVMax=1,offset=0.0):
    gAB=sim.Hill_gate (sim.Gate.GATE_GD,B,A, inv=True,kM=kM,N=N,kVMax=kVMax)
    #gAB=sim.Hill_gate (sim.Gate.GATE_GD,B,A, inv=True,kM=50,N=N,kVMax=kVMax)
    gBA=sim.Hill_gate (sim.Gate.GATE_GD,A,B, inv=True,kM=kM,N=N,kVMax=kVMax)
    dA = sim.GD_const_gate (sim.Gate.GATE_GD, A, gen=genA, decay=1)
    dB = sim.GD_const_gate (sim.Gate.GATE_GD, B, decay=1)

    # Set up initial concentrations
    sim.cc_cells[A,0] = 0
    sim.cc_cells[B,0] = kVMax

# Set up and run two simulations on the "closed-loop" configuration, where
# A=!B and B=!A.
# Simulation #1: just simulate it and note the final values for A and B.
# Simulation #2: add an additional source of A at 1 moles/(m3*s), resim, and
# note how much [A] actually increased at steady state.
# Print the results, but don't return any values
def closed_loop_sims (GP, N, kM):
    end_time=100
    setup_latch (GP, N, kM, genA=0)
    startup_init_Vmem()
    t_shots, cc_shots = sim.sim (end_time)
    (A,B) = ions ('A','B')
    final_A0 = cc_shots[-1][A][0]
    final_B0 = cc_shots[-1][B][0]

    setup_latch (GP, N, kM, genA=1)
    startup_init_Vmem()
    t_shots, cc_shots = sim.sim (end_time)
    final_A1 = cc_shots[-1][A][0]
    final_B1 = cc_shots[-1][B][0]
    deltaA = f'deltaA={(final_A1-final_A0):.3f}'
    if (final_A1 > final_B1):
        deltaA = "deltaA flipped the cell"
    if (final_A0+.05 > final_B0):
        deltaA = "no convergence"
    print (f'kM={kM}, N={N}: A={final_A0:.3f}, B={final_B0:.3f}, {deltaA}')

# Open-loop simulation: B=!A, C=!B.
# Each inverter is a Hill gate, with gen/decay on its output to provide decay.
# The input A has no gen/decay; someone initializes it in cc_cells[A,0] and it
# drives the simulation with that constant value.
# The purpose of this network (and the sims that follow) is simply to find
# Vsw of the inverters/
def setup_latch_openloop (p, N, kM):
    sim.init_big_arrays (n_cells=1, n_GJs=0, p=p, extra_ions=['A', 'B','C'])
    (A,B,C) = ions ('A','B','C')

    # First the two Hill gates for B=!A and C=!B.
    # gtype, out_ion, in_ion, inv=None,kM=1,N=1,kVMax=1,offset=0.0):
    gAB=sim.Hill_gate (sim.Gate.GATE_GD,B,A, inv=True,kM=kM,N=N,kVMax=kVMax)
    gBC=sim.Hill_gate (sim.Gate.GATE_GD,C,B, inv=True,kM=kM,N=N,kVMax=kVMax)

    # And the gen/decay gates (just to do decay, no generation).
    dB = sim.GD_const_gate (sim.Gate.GATE_GD, B, decay=1)
    dC = sim.GD_const_gate (sim.Gate.GATE_GD, C, decay=1)

    # Set up initial concentrations
    sim.cc_cells[A,0] = 0	# Someone will likely overwrite this.

# Initialize sim.cc_cells[A,0] = Ainit to drive our simulation.
# Then run a sim, look at the final output C and return the sign of
# (final_C - Ainit). This will be used to find zero-crossings. The idea is that
# when final_C==Ainit, we have a stable point (be it stable or metastable).
# This is the inverter Vsw.
def openloop_sim (Ainit):
    (A,C) = ions ('A','C')
    sim.cc_cells[A,:] = Ainit
    end_time=200
    t_shots, cc_shots = sim.sim (end_time)
    final_C = cc_shots[-1][C,0]
    return (-1 if (final_C<Ainit) else 1)

def post_latch (GP, t_shots, cc_shots, N, kM, genA):
    (A,B) = ions ('A','B')
    print (f'kM={kM}, N={N}, genA={genA}:'
	   f' final A={cc_shots[-1][A]:.3f}, B={cc_shots[-1][B]:.3f}')
    #eplt.plot_ion (t_shots, cc_shots, ['A','B'],
    #		    title=f'kM={kM}, N={N}, genA={genA}')

def startup_init_Vmem():
    Vm = sim.compute_Vm (sim.cc_cells)
    assert (np.abs(Vm)<.5).all(), \
            "Your initial voltages are too far from charge neutral"

#######################################################
# The next set of functions in this file is the "spreading" sims. They test
# our basic assumption that gated GJs can make a morphogen transition sharper.
# 
# We build a 1D chain of cells. Each cell is just cross-coupled inverters.
# We initialize the chain so that one or more cells on one end are high; the
# other cells are low. We then monitor how far the "1" pattern diffuses; i.e.,
# how sharp the transition of [A] is. We do this across various parameter
# choices for N, kM, Kd).
#
# The point of these sims is to note the sharpness of the transition in [A],
# and then add gating to the GJs to see if that improves the sharpness.
# Spoiler: it does :-).
# 
# In order to create the voltages, we need a class set_Vmem_from_conc that
# sets a cell's Vmem based on whether [A]>[B] or vice versa. Specifically,
# if [A]>[B] then we bump up Dm,K (the conductance of the K channel), making
# Vmem more negative (VNernst,K is about -72mV).
#######################################################

# The top-level function to loop through many parameter sets, call
# setup_spread() to buld a network with a single parameter set, simulate and
# print results.
def measure_spread():
    end_time=100
    GP = sim.Params()
    GP.no_dumps = True

    GJ_lib.setup_GJ_model ('C45', 'C45', -.1, .1, .002)

    # With GJ_diffusion=1e-13, the field is flat.
    # With GJ_diffusion=1e-15, there is no visible communication between cells

    n_cells=8; n_one=3
    for N in [2,3,5,10]:
        for kM in [20,35,50,65,80]:
            for Kd in [1e-14]:
                setup_spread (GP, N, kM, Kd, n_cells, n_one)
                startup_init_Vmem()
                t_shots, cc_shots = sim.sim (end_time)
                #edb.dump (t_shots[-1], cc_shots[-1], edb.Units.mol_per_m3s, long=True)
                #edb.dump_gating (t_shots[-1], cc_shots[-1])

                (A,B) = ions ('A','B')
                #eplt.plot_worm (0, cc_shots[-1], ['A','B'],
                #		title=f'kM={kM}, N={N}')
                #eplt.plot_worm (0, cc_shots[-1], 'V', title=f'kM={kM}, N={N}')
                base = cc_shots[-1][A,0]
                np.set_printoptions (formatter={'float': '{:.3f}'.format})
                print (f'N={N}, kM={kM}: A[0]={base:.4}, deltas={cc_shots[-1][A]-base}')

# The initialization function that creates our straight-line network of cells.
def setup_spread (p, N, kM, Kd, n_cells, n_one):
    n_GJs = n_cells-1
    sim.init_big_arrays (n_cells=n_cells, n_GJs=n_GJs,p=p, extra_ions=['A','B'])
    (A,B) = ions ('A','B')

    # gtype, out_ion, in_ion, inv=None,kM=1,N=1,kVMax=1,offset=0.0):
    # Set up cross-coupled inverters
    gAB=sim.Hill_gate (sim.Gate.GATE_GD,B,A, inv=True,kM=kM,N=N,kVMax=kVMax)
    gBA=sim.Hill_gate (sim.Gate.GATE_GD,A,B, inv=True,kM=kM,N=N,kVMax=kVMax)

    # Set up decay of A and B.
    dA = sim.GD_const_gate (sim.Gate.GATE_GD, A, decay=1)
    dB = sim.GD_const_gate (sim.Gate.GATE_GD, B, decay=1)

    # Create the [A]-drive-Vmem gatings by instantiating a gate. 
    set_Vmem_from_conc()

    # Straight-line GJ connections along the field. All are simple & ungated.
    sim.GJ_connects['from']  = range(n_GJs)
    sim.GJ_connects['to']    = sim.GJ_connects['from'] + 1

    # Set up voltage-gated GJs by, again, instantiating a gate.
    # No need to give it any connections; it applies equally well to all GJs.
    GJ_lib.GJ_gate()

    # Physical parameters of A and B.
    sim.z_array[[A,B]] = 0	# neutral ions
    sim.GJ_diffusion[[A,B]] = Kd

    # Set up initial concentrations
    n_zero = n_cells-n_one
    sim.cc_cells[A,0:n_zero] = 0
    sim.cc_cells[B,0:n_zero] = kVMax
    sim.cc_cells[A,n_zero:n_cells] = kVMax
    sim.cc_cells[B,n_zero:n_cells] = 0


#######################################################
# This set of functions tries to understand the behavior of a simple GRN.
# The GRN is cross-coupled NOR2 gates. The two non-cross-coupled inputs are
# morphogens A and B, which are complementary in that they come from opposing
# gradients.
# The big-picture goal is to find which parameters result in nice behavior:
#     -	When A is very high and B is very low (or vice versa), the GRN has only
#	one possible state.
#     - When A and B are both kind of midrange, the GRN is bistable.
#
# Our actual strategy is that, for numerous parameter sets,
#     -	build a straight-line network of cells that are not connected
#     -	set up the opposing gradients of A and B, so that each cell represents
#	the GRN's response to a particular [A]
#     - initialize all cross-coupled structures to 1:0 and sim
#     - initialize all cross-coupled structures to 0:1 and sim
#
# For each parameter set, the characterization produces/prints a string with
# one character per cell, where
#     -	"0" means that OA >> OB0, independent of initialization.
#     -	"1" means that OA << OB, independent of initialization.
#     - "B" means that both initial values are stable
#     - "X" means anything else.
# It also compares the string to a prediction via a simple algorithm.
#
# The NOR gate is implemented as
#	kVmax / (1 + (M/kM)^NM + (R/kR)^NR)
# where M is a morphogen (A or B), and R is a repressor (the cross-coupling).
#######################################################

# The top-level function to loop through many parameter sets, call
# setup_basic_network() to build a network with a single parameter set,
# simulate and print results.
def predict_bistab():
    GP = sim.Params()
    GP.no_dumps = True

    n_cells=20
    kVMax=100

    # Tons of parameter sets
    #for NAB in [2,3,5,10]:
    #   for NR in [2,3,5,10]:
    #       for kMA in [20,35,50,65,80]:
    #           for kMB in [20,35,50,65,80]:
    #              for kMR in [20,35,50,65,80]:

    # More "plus" values of the "reasonable" parameter sets.
    for plus in [0,5,10,15]:
        for NAB in [2,3,5,10]:
            for NR in [2,3,5,10]:
                     kMA = 50+plus
                     kMB = 50+plus
                     kMR = 50
                     # Build the network.
                     #(NAB,NR,kMA,kMB,kMR) = (10,10,10,65,50,20)
                     setup_basic_network (GP, NAB, NR, plus, 1,1,n_cells)

                     # Do both sims and analyze the results.
                     title = f'NAB/R={NAB}/{NR}, kMA/B/R={kMA}/{kMB}/{kMR}'
                     expect = expects (kMA, kMB, n_cells)
                     predict_bistab_run_both_sims(GP, NAB, NR, plus, kMA,kMB,
                                             n_cells, title, expect)
                     #quit()

# Given a single specific parameter combo, run both sims and interpret/print
# the results. We print lines like
#	NAB/R=10/10, kMA/B/R=80/80/65, expect 11111BBBBBBBBBBBB0000,
#			#5:B->1,#15:B->0,#16e:B->0
# where NAB and NR are the Hill exponents, kMA, kMB, kMR are the Michaelis-
# Menton kM values, "expect ..." is the results from our simple prediction
# algorithm, and "#5:B->1" means that cell #5 was predicted to have B but
# simulated to "1", the "#16e" means that cell #16 was an "edge;" its [A]
# matched kMA or its [B] matched kMB.
def predict_bistab_run_both_sims (GP, NAB,NR,plus,kMA,kMB,n_cells,title,expect):
    (A,B,OA,OB) = ions ('A','B','OA','OB')
    real = char_01BX (GP, NAB, NR, plus, n_cells)

    result=""
    for i in range (n_cells):
        if (real[i] != expect[i]):
            edge = (sim.cc_cells[A,i]==kMA) or (sim.cc_cells[B,i]==kMB)
            result += f'#{i}{"e" if edge else ""}:{expect[i]}->{real[i]},'
    print (f'{title}, expect {expect}, {result}')

# What sim results do we expect for these params? We return a string with one
# char per cell, where the char is
#	0: OA=0, OB=100 regardless of initialization
#	1: OA=100, OB=0 regardless of initialization
#	X: both OA=0 and OB=0 regardless of initialization
#	B: both OA=100/OB=0 and OA=0/OB=100 are stable.
# So remember: "1" or "0" for a summary refers to the value of OA, which is
# usually the complement of the value of A.
def expects (kMA, kMB, n_cells):
    expect=""
    for i in range (n_cells):
        concA = i*100/n_cells
        A_off = concA<kMA
        B_off = 100-concA<kMB
        if (A_off):
            char = 'B' if B_off else '1'
        else:
            char = '0' if B_off else 'X'
        expect += char
    return (expect)

#######################################################
# This section of the file characterizes the small-signal gain -- the delta in
# OA for a small delta in A -- at [A]=50. It does so for various paremeter
# values.
#######################################################

# The main entry point for this section. It loop through all of the "reasonable"
# parameter choices and, for each one, runs ssgain_run_both_sims() to
# characterize the gain. By "reasonable," we mean:
#	- NAB and NR each range, independently, in (2,3,5,10).
#	- kMR is always 50
#	- kMA and kMB are always identical at 50+plus, where 'plus' ranges
#	  in (0,5,10,15,20,25). This is meant to have a switching point at 50,
#	  with a variable bistability region around that.
def small_sig_gain():
    GP = sim.Params()
    GP.no_dumps = True

    n_cells=21
    kVMax=100

    for NAB in [2,3,5,10]:
        #print (f"NAB={NAB} => inverter gain at [A]=50 is {inv_gain (NAB):.3f}")
        for NR in [2,3,5,10]:
            for plus in [0,5,10,15,20,25]:
                # Build the network.
                setup_basic_network (GP, NAB, NR, plus, 1,1,n_cells)

                # Do both sims and analyze the results.
                title = f'NAB/R={NAB}/{NR}, delta={plus}'
                ssgain_run_both_sims(kVMax, plus, n_cells, title)

# The function that does the work for this file section. It runs two sims: one
# with all cells initialized to OA=100, OB=0 and one with OA=0, OB=100.
# It then computes the small-signal gain at any [A] as delta_OA / delta_A (where
# the deltas are the [A] and [OA] at two nearby values of [A]).
# Why do we need two sims? For bistable cells, [OA] depends on the initial
# conditions. And so in the formula above, the delta-[OA] is maximized: we
# subtract the smaller value of [OA] between both sims from the larger value
# between both sims. So essentially, we are assuming that a bistable cell flips
# right around here, and using that to increase gain.
def ssgain_run_both_sims(kVMax, delta, n_cells, title):
    #print (f'{title}')
    (A,B,OA,OB) = ions ('A','B','OA','OB')
    seed_A_B ()	# Set up the complementary gradients on A and B
    startup_init_Vmem()

    # First sim: all cells initialized to OA=100, OB=0
    sim.cc_cells[OA] = kVMax; sim.cc_cells[OB] = 0
    end_time = 100
    t_shots, cc_shots = sim.sim (end_time)
    OA_concs1 = cc_shots[-1][OA]

    # Second sim: all cells initialized to OA=0, OB=100
    sim.cc_cells[OA] = kVMax; sim.cc_cells[OB] = kVMax
    t_shots, cc_shots = sim.sim (end_time)
    OA_concs2 = cc_shots[-1][OA]

    OA_max = np.maximum (OA_concs1, OA_concs2)
    OA_min = np.minimum (OA_concs1, OA_concs2)

    dA = sim.cc_cells[A,1] - sim.cc_cells[A,0]
    gains = (OA_max[:n_cells-2]-OA_min[2:])/dA
    gains = np.concatenate (([0.0], gains, [0.0]))

    # Print the gains as a one-line summary.
    gain_max = np.max (gains)
    gain_50 = gains[int((n_cells-1)/2)]
    print (f"{title}: gain_max={gain_max:.3f}, gain@50={gain_50:.3f}")
    return

    # Print the gains in detail, and also plot them.
    np.set_printoptions (formatter={'float': '{:.3f}'.format})
    print (gains)
    plt.plot (cc_shots[-1][A], OA_min, label='OA_min')
    plt.plot (cc_shots[-1][A], OA_max, label='OA_max')
    plt.plot (cc_shots[-1][A], gains, label='gain')
    plt.xlabel('[A]')
    plt.ylabel('Concentration')
    plt.legend(loc='best')
    plt.title (title)
    plt.show()

#######################################################
# These analyses look at how hard it is for a cell to get flipped by a bunch of
# neighboring cells. For each reasonable parameter set, we start with one
# central cell initialized to OA=0, OB=100. We surround it with two cells, then
# three cells, etc., all initialized to OA=100, OB=0. For each number of cells
# around it, we use a binary search to find the minimum GJ size that will flip
# the inner cell.
# Obviously, the more surrounding cells, the easier it is to flip the central
# cell.
# We expect to see that the higher NR is, the harder a cell is to flip.
# A higher NAB will make a cell slightly easier to flip; since cells are in the
# bistable region where [A]<kMA and [B]<kMB, a higher NAB makes the inputs
# more completely "get out of the way."
#######################################################

def surround_flip ():
    GP = sim.Params()
    GP.no_dumps = True

    for NAB in [2,3,5,10]:
        for NR in [3,5,10]:
            for plus in [5,10,15,20,25]:
                if (NR==2) or (NAB==2 and NR==3):
                    continue
                if  (plus<=10 and NAB==3 and NR==3) \
                  or (plus==5 and ((NAB==2 and NR==5) or (NAB==5 and NR==3))):
                    continue
                surround_flip_sims(NAB, NR, plus)

# Do all of the simulation/analysis for a single parameter set.
def surround_flip_sims (NAB, NR, plus):
    GP = sim.Params()
    GP.no_dumps = True

    for n_surround in range(2,6):	# Number of surrounding cells.
        # Our GJ configuration is a star rather than a straight line. So we
        # start with the standard setup and then modify the GJ config. Note this
        # only works because we use the same *number* of GJs as usual, just in
        # a different configuration.
        setup_basic_network (GP, NAB, NR, plus, 1,1,n_surround+1)
        sim.GJ_connects['from']  = 0
        sim.GJ_connects['to']    = range(1,n_surround+1)

        # Use a binary search to find the minimum GJ size that's big enough for
        # the surrounding cells to flip the center cell.
        (GJ,_) = binSearch (surround_flip_sim, 0, 1e-13, print_func=None)
        title = f'Surround={n_surround}, NAB/NR={NAB}/{NR}, delta={plus}'
        print (f"{title}: final GJ_scale={GJ:.3g}")

def surround_flip_sim (GJ_scale, title=""):
    (A,B,OA,OB) = ions ('A','B','OA','OB')
    sim.GJ_diffusion[[OA,OB]] = GJ_scale

    # Initialize A, B, OA, OB
    sim.cc_cells[(A,B),:] = 50
    sim.cc_cells[OA,0 ]=100; sim.cc_cells[OB,0] =0
    sim.cc_cells[OA,1:]=  0; sim.cc_cells[OB,1:]=100

    startup_init_Vmem()
    #print (f"Simming {title}...")
    t_shots, cc_shots = sim.sim (100)
    results = surround_flip_summarize_results (cc_shots[-1])
    #print (f'{title},GJ_scale={GJ_scale} -> {results}')
    return (results)

    #eplt.plot_ion (t_shots, cc_shots, ['OA','OB'],title=title,cells=[0])
    #eplt.plot_ion (t_shots, cc_shots, ['OA','OB'],title=title,cells=[1])
    #eplt.plot_ion (t_shots, cc_shots, ['OA','OB'],title=title,cells=[2])

def surround_flip_summarize_results (cc):
    DELTA = 2
    (OA,OB) = ions ('OA','OB')
    cells_0 = (cc[OB] > cc[OA] + DELTA)
    cells_1 = (cc[OA] > cc[OB] + DELTA)

    is_init = cells_1[0] and np.all(cells_0[1:])
    is_final= np.all(cells_0)

    return ('I' if is_init else 'F')

#######################################################
# Zombie2
# This set of functions creates a 2D or 3D field of cells, spreads A and B
# as usual, does some initialization and then simulates.
########################################################

def zombie2():
    GP = sim.Params()
    GP.no_dumps = True

    (dimX, dimY, dimZ) = (1,5,21)
    Kd=1e-15
    coord_set_dimensions (dimX, dimY, dimZ)

    GP.time_step = .0001		# min time step for explicit integration

    for NAB in [2,3,5,10]:
        for NR in [3,5,10]:
            for plus in [5,10,15,20,25]:
                if (NR==2) or (NAB==2 and NR==3):
                    continue
                if  (plus<=10 and NAB==3 and NR==3) \
                  or (plus==5 and ((NAB==2 and NR==5) or (NAB==5 and NR==3))):
                    continue
                zombie2_sims(GP, dimX,dimY,dimZ, NAB, NR, plus, gated=False)
                #quit()

def zombie2_sims (p, dimX,dimY,dimZ, NAB, NR, plus, gated):
    gated = " (gated)" if gated else ""
    title=f'NAB/NR={NAB}/{NR}, delta={plus}, field={dimX}x{dimY}x{dimZ}{gated}'
    # Build the network for this parameterization.
    #print (title+": "+char_01BX (p, NAB, NR, plus, dimZ))
    n_cells = dimX*dimY*dimZ
    setup_basic_network (p, NAB, NR, plus, dimX,dimY,dimZ)

    # Set up initial concentrations
    seed3D_A_B (kVMax)

    (A,B,OA,OB) = ions ('A','B','OA','OB')
    print (title)
    startup_init_Vmem()

    print ("bumping up Kd until the error is fixed")
    Kd = 1e-15
    fixed = False
    while (not fixed):
        Kd *= 2
        fixed = zombie2_sim_fixed(Kd)
        #print (f"\tKd={Kd:.4g} => fixed if {fixed}")
        if (Kd > 1e-11):
            print (f'{title}: final result is "cannot fix"')
            return

    # Binary search to find the lowest Kd that fixes the error
    (Kd0,Kd2) = zombie2_binSearch (Kd/2, Kd, "lowest Kd that fixes the error")
    #(Kd0,Kd2) = binSearch (zombie2_sim_fixed, Kd/2, Kd,
    #			   why="lowest Kd that fixes the error")
    print (f"Binary search returning ({Kd0:.4g},{Kd2:.4g})")
    if (Kd0==0):
            print (f'{title}: final result is "weird first bin search"')
            return

    # How far before the pattern collapses?
    Kd_max = 1e-10	# biggest reasonable Kd
    #(Kdf,x) = binSearch(zombie2_sim_fixed, Kd2,Kd_max,
    #			why="highest Kd that doesn't collapse")
    (Kdf,x) = zombie2_binSearch(Kd2,Kd_max,"highest Kd that doesn't collapse")
    print (f"Binary search returning ({Kd0:.4g},{x:.4g})")
    if (Kd0==0):
            print (f'{title}: final result is "weird second bin search"')
            return

    print (f"{title}: final workable Kd range is {Kd2:.4g}-{Kdf:.4g}, or {Kdf/Kd2:.3g}x")
    #edb.dump(0, cc_shots[-1], edb.Units.mol_per_m3s, True, [OA,OB], idx)
    #eplt.plot_worm (cc_shots[-1], ['A','B','OA','OB'], title)
    # quit()

# Implement various initializations to see what happens.
def zombie2_init_OA_OB():
    (A,B,OA,OB) = ions ('A','B','OA','OB')
    n_ions = sim.cc_cells.shape[0]
    cc3D = sim.cc_cells.reshape ((n_ions,g_maxX,g_maxY,g_maxZ))

    cc3D[OA] = 0	# OA=0, OB=100 for the entire field.
    #cc3D[OA] = 100	# OA=100, OB=0 for the entire field.
    cc3D[OA,:,:,:10] = 100	# Bottom half has OA=100

    # Center row = 00100
    cc3D[OA,:,:,10]=0
    cc3D[OA,:,2,10]=100

    cc3D[OB,:,:,:] = kVMax - cc3D[OA,:,:,:]

def zombie2_sim_fixed (Kd, title=""):
    cc = zombie2_sim (Kd)
    result = zombie2_fixed (cc)
    print (f"\tKd={Kd:.4g} => fixed if {result}")
    return (result)

def zombie2_sim (Kd, title=""):
    (OA,OB) = ions ('OA','OB')
    sim.GJ_diffusion[[OA,OB]] = Kd
    #print (f'Kd={Kd:.4g}')

    # Initialize OA and OB; we have a few interesting cases.
    zombie2_init_OA_OB ()
    #dump_3D (sim.cc_cells,bool=False)	# Is the initialization correct?

    end_time = 100
    t_shots, cc_shots = sim.sim (end_time)

    #dump_3D (cc_shots[-1],bool=False,what_to_show="OA:OB")
    #idx = coord_xyz2idx (0,2,10)
    #eplt.plot_ion (t_shots, cc_shots, ['OA','OB'], cells=[52])
    #quit()
    return (cc_shots[-1])

# Are all the mistakes that we initialized fixed?
# We want cc3D[z=0:10] to be all 1, and [10:] to be all 0.
def zombie2_fixed(cc):
    (A,B,OA,OB) = ions ('A','B','OA','OB')
    n_ions = sim.cc_cells.shape[0]
    cc3D = cc.reshape ((n_ions,g_maxX,g_maxY,g_maxZ))

    is_1 = (cc3D[OA,:,:,:] > cc3D[OB,:,:,:] + 2)
    is_0 = (cc3D[OA,:,:,:] < cc3D[OB,:,:,:] - 2)
    bottom = is_1[:,:,0:10].all()
    top    = is_0[:,:,10:].all()
    return (bottom and top)

# Binary search to find a stable point of the open-loop circuit.
def zombie2_binSearch (Kd0, Kd2, reason):
    print ("Starting binary search for", reason)
    fixed0 = zombie2_sim_fixed(Kd0)
    fixed2 = zombie2_sim_fixed(Kd2)
    #dump_3D (sim.cc_cells,bool=False,what_to_show="OA:OB")
    if (fixed0 == fixed2):
        print (f"Binsearch points are {fixed0} and {fixed2}")
        return (0,0)

    while (Kd2 > 1.001 * Kd0):
        Kd1 = (Kd0+Kd2)/2
        fixed1 = zombie2_sim_fixed(Kd1)
        #print (f"\tKd={Kd1:.4g} => fixed if {fixed1}")
        if (fixed1 == fixed0):
            Kd0 = Kd1
        else:
            Kd2 = Kd1
    #print (f"Binary search returning ({Kd0:.4g},{Kd2:.4g})")
    return (Kd0,Kd2)

#char_latch()
#measure_spread()
#predict_bistab()
#small_sig_gain()
#surround_flip()
zombie2()

def zombie2_pat1_is_OK (bistab_map):
    return (bistab_map[10]=='B')
def zombie2_pat1_set (cc3D, OA, OB):
    # Center row = 00100
    cc3D[OA,:,:,10]=0
    cc3D[OA,:,2,10]=100

def zombie2_pat2_is_OK (bistab_map):
    return (bistab_map[10:12]=='BB')
def zombie2_pat2_set (cc3D, OA, OB):
    # Set the pattern 00110
    #                 00110
    cc3D[OA,:,:,10:12]=0
    cc3D[OA,:,2:4,10:12]=100
