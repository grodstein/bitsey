kM=20, N=2: A=4.174, B=95.826, deltaA=1.228
	Fixed stable point with 4.172 -> 95.830 -> 4.174
	Fixed metastable point with 30.312 -> 30.330 -> 30.306
	Fixed stable point with 95.812 -> 4.175 -> 95.824
kM=35, N=2: A=14.293, B=85.707, deltaA=2.135
	Fixed stable point with 14.289 -> 85.714 -> 14.291
	Fixed metastable point with 41.500 -> 41.564 -> 41.489
	Fixed stable point with 85.688 -> 14.298 -> 85.698
kM=50, N=2: A=43.216, B=57.276, deltaA flipped the cell
	Fixed stable point with 50.000 -> 50.000 -> 50.000
kM=65, N=2: A=56.747, B=56.747, no convergence
	Fixed stable point with 56.719 -> 56.772 -> 56.726
kM=80, N=2: A=62.271, B=62.271, no convergence
	Fixed stable point with 62.250 -> 62.287 -> 62.259
kM=20, N=3: A=0.794, B=99.994, deltaA=1.002
	Fixed stable point with 0.793 -> 99.994 -> 0.794
	Fixed metastable point with 27.578 -> 27.610 -> 27.540
	Fixed stable point with 99.938 -> 0.795 -> 99.994
kM=35, N=3: A=4.131, B=99.836, deltaA=1.018
	Fixed stable point with 4.129 -> 99.836 -> 4.131
	Fixed metastable point with 40.031 -> 40.061 -> 40.008
	Fixed stable point with 99.812 -> 4.133 -> 99.836
kM=50, N=3: A=11.472, B=98.807, deltaA=1.117
	Fixed stable point with 11.469 -> 98.808 -> 11.472
	Fixed metastable point with 49.969 -> 50.047 -> 49.930
	Fixed stable point with 98.750 -> 11.489 -> 98.801
kM=65, N=3: A=24.210, B=95.087, deltaA=1.558
	Fixed stable point with 24.203 -> 95.091 -> 24.208
	Fixed metastable point with 58.188 -> 58.228 -> 58.177
	Fixed stable point with 95.062 -> 24.224 -> 95.079
kM=80, N=3: A=47.614, B=82.588, deltaA flipped the cell
	Fixed stable point with 47.594 -> 82.606 -> 47.597
	Fixed metastable point with 65.000 -> 65.088 -> 64.996
	Fixed stable point with 82.562 -> 47.637 -> 82.567
kM=20, N=5: A=0.032, B=100.000, deltaA=1.000
	Fixed stable point with 0.032 -> 100.000 -> 0.032
	Fixed metastable point with 24.922 -> 24.973 -> 24.782
	Fixed stable point with 99.938 -> 0.032 -> 100.000
kM=35, N=5: A=0.522, B=100.000, deltaA=1.000
	Fixed stable point with 0.522 -> 100.000 -> 0.522
	Fixed metastable point with 38.438 -> 38.499 -> 38.310
	Fixed stable point with 99.938 -> 0.524 -> 100.000
kM=50, N=5: A=3.030, B=100.000, deltaA=1.000
	Fixed stable point with 3.029 -> 100.000 -> 3.030
	Fixed metastable point with 49.969 -> 50.078 -> 49.805
	Fixed stable point with 99.938 -> 3.040 -> 100.000
kM=65, N=5: A=10.401, B=99.990, deltaA=1.003
	Fixed stable point with 10.398 -> 99.990 -> 10.401
	Fixed metastable point with 59.938 -> 59.999 -> 59.876
	Fixed stable point with 99.938 -> 10.426 -> 99.989
kM=80, N=5: A=24.956, B=99.705, deltaA=1.064
	Fixed stable point with 24.953 -> 99.706 -> 24.956
	Fixed metastable point with 68.438 -> 68.579 -> 68.356
	Fixed stable point with 99.688 -> 24.973 -> 99.704
kM=20, N=10: A=0.000, B=100.000, deltaA=1.000
	Fixed stable point with 0.000 -> 100.000 -> 0.000
	Fixed metastable point with 22.609 -> 22.683 -> 22.121
kM=35, N=10: A=0.003, B=100.000, deltaA=1.000
	Fixed stable point with 0.003 -> 100.000 -> 0.003
	Fixed metastable point with 36.906 -> 37.044 -> 36.179
kM=50, N=10: A=0.098, B=100.000, deltaA=1.000
	Fixed stable point with 0.098 -> 100.000 -> 0.098
	Fixed metastable point with 49.969 -> 50.156 -> 49.220
kM=65, N=10: A=1.328, B=100.000, deltaA=1.000
	Fixed stable point with 1.328 -> 100.000 -> 1.328
	Fixed metastable point with 61.906 -> 61.956 -> 61.768
kM=80, N=10: A=9.696, B=100.000, deltaA=1.000
	Fixed stable point with 9.695 -> 100.000 -> 9.696
	Fixed metastable point with 72.562 -> 72.627 -> 72.446
	Fixed stable point with 99.938 -> 9.751 -> 100.000
