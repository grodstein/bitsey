###########################################################
# Plot the data from zombie_sims_metastasis() that show bigger tumors being
# more invasive.
###########################################################

import re
from operator import attrgetter
import numpy as np
import matplotlib, matplotlib.pyplot as plt
#import pdb; pdb.set_trace()

############################################################
# Basics.
# - Our object that holds one sim result
# - routines to parse files and build those objects.
############################################################

# A class object that holds the metastasis results from one parameter combo.
# So, given the tumor size, Kd_field, GJ_power, ... => did it grow?
# 'direction' is either 'grew', 'shrank' or 'constanted'.
class Result:
  def __init__(self,fname,sizeZ,Kd_field,GJ_power,GJ_NCMM,GJ_tau,strat,direction):
    (self.fname,self.size,self.Kd_field) = (fname,sizeZ,Kd_field)
    (self.GJ_power,self.GJ_NCMM,self.GJ_tau) = (GJ_power,GJ_NCMM,GJ_tau)
    (self.strat,self.direction) = (strat,direction)
  def __str__(self):
    return (f"[{self.strat} {self.size}x{self.size},Kd={self.Kd_field},"\
	   +f"power={self.GJ_power},NCMM={self.GJ_NCMM},tau={self.GJ_tau}"\
           +f"->{self.direction}]")

res_list = []


# Read in a bunch of files; for each final-result line, parse the line and
# build/save a Result object. At the end, we have res_list[] with them all.
def parse_files (filenames):
    for fname in filenames:
        infile = open (fname, "r")
        print (f"Reading {fname}...")
        for line in infile:
            if (not re.search (" final ", line)):
                continue
            #print (line)
            res = parse (line, fname)
            res_list.append (res)
        print (f"After reading {fname}, we have {len(res_list)} records.")

# Parse one line of the form
#	3. NR=10. 1x3x3 tumor final at Kd_field=6.00e-12, GJ_power=100,
#		GJ_NCMM=1, GJ_tau=10, hyperpol-tumor shrunk from 81
#		cells (13%) to 0 cells (0%).
# Return the individual fields.
def parse (line, fname):
    fields = line.split (" ")
    fields = fields[1:]	# Remove the initial "3. "
    NR = re.search ("NR=([0-9]+)\.", fields[0])
    NR = int(NR.group(1))
    size = re.match ("([0-9]+)x([0-9]+)x([0-9]+)", fields[1])
    (sizeX,sizeY,sizeZ) = [int(str) for str in size.groups()]
    Kd_field = re.match ("Kd_field=([0-9.e-]+)", fields[5]).group(1)
    Kd_field = float (Kd_field)
    GJ_power = re.match ("GJ_power=([0-9]+)", fields[6]).group(1)
    GJ_power = int(GJ_power)
    GJ_NCMM  = re.match ("GJ_NCMM=([0-9.]+)", fields[7]).group(1)
    GJ_NCMM  = float (GJ_NCMM)
    GJ_tau   = re.match ("GJ_tau=([0-9.]+)", fields[8]).group(1)
    GJ_tau   = float (GJ_tau)
    strat    = re.match ("(hyperpol-tumor|neighbor-depol|dumb)",
			 fields[9]).group(1)
    direction= re.match ("(shrunk|grew|constanted)", fields[10]).group(1)
    #print(f"{NR=}, {sizeX=},{sizeY=},{sizeZ=}, {Kd_field=}, {GJ_power=}," \
    #     +f"{GJ_NCMM=}, {GJ_tau=}, {strat=}, {direction=}")

    return(Result(fname,sizeZ,Kd_field,GJ_power,GJ_NCMM,GJ_tau,strat,direction))

############################################################
# Non-monotonicity. Answer questions like
# - Does starting with a bigger tumor ever discourage metastasis? No.
# - How about higher GJ_power? Higher GJ_NCMM?
############################################################

# Look for "non-monotonic" cases, where one tumor grows and then a larger tumor
# (but with all other parameters equal) doesn't grow.
# Results typically show (for 33K records):
#     -	zero cases of monotonicity for tumor size.
#     -	840 cases have power=100 fail but power=10 pass. Power=1 is never
#	better than power=10 or 100.
#     -	332 cases of NCMM=.5 worse than NCMM=0; 110 cases of 1 worse than .5.
def check_monotonic_vs_tumor_size(attr):
    sort_records (attr)
    total_non_mon = 0		# Total number of non-monotonic cases.
    prev_r = None
    print (f"Non-monotonic records for {attr}...")
    for r in res_list:
        grew = (r.direction == "grew")
        new_run = (prev_r==None) or (getattr(r,attr)<getattr(prev_r,attr))
        if (not new_run):
            if (prev_grew and not grew):
                total_non_mon += 1
                print (f"{total_non_mon}. Non-monotonic: {prev_r} => {r}")
        prev_grew = grew
        prev_r = r
    print (f"Total of {total_non_mon} cases of *{attr}* non-monotonicity")

def sort_records (fastest):
    global res_list

    # Within each call to .sort(), rightmost item varies fastest.
    # So we remove 'fastest' and put it on the right.
    order = ['fname','strat','Kd_field','size','GJ_tau','GJ_NCMM','GJ_power']
    order.remove (fastest)
    order.append(fastest)
    res_list.sort (key=attrgetter(*order))

    #for i in range(min (100, len(res_list))):
    #    print (res_list[i])

############################################################
# Print out a 2D table of success numbers with arbitrary X & Y axes.
############################################################

def bucket (dict, key):
    buck = dict.get (key, -1)
    n_buckets = len(dict)
    if (buck==-1):	# A new bucket
        dict[key] = n_buckets
        #print (f"new bucket {key} => {dict[key]}")
    return (dict[key])

# Print out a 2D table of successes.
# The X and Y axes are arbitrary (change them via code in this function).
# Each table entry is the amount of successful metastases for that parameter
# pair.
def print_success_vs_XY ():
    x_buckets = {}	# Keep track of x_bucket_name => idx for all x buckets.
    y_buckets = {}	# Ditto for the x buckets.

    # 2D array[n_y_buckets, n_tumor_sizes] of how many tumors grew.
    n_grew = np.zeros ((10,20), dtype=np.int32)

    np.set_printoptions (linewidth=120)
    np.set_printoptions (formatter={'int': '{:3d}'.format})

    for r in res_list:
        # What are we using for X bucketing?
        x = r.size
        x = r.GJ_power
        x = r.GJ_NCMM
        x_bucket = bucket (x_buckets, x)

        # What are we using for Y bucketing?
        y = r.GJ_power
        y = r.GJ_NCMM
        y = r.GJ_tau
        y = r.Kd_field
        y = r.strat
        y_bucket = bucket (y_buckets, y)

        # Now we have the x and y buckets, which are the keys of the data
        # from this record into the x_buckets{} and y_buckets{} dicts.
        if (r.direction=="grew"):
            n_grew[y_bucket,x_bucket] += 1

    # We've gone through all of the records and built n_grew[x,y]. Next, invert
    # the x&y dictionaries to get arrays x/y_list[], the row and column labels
    # for printing.
    x_list=[""] * len(x_buckets)
    for name,idx in x_buckets.items():
        x_list[idx] = name
    y_list=[""] * len(y_buckets)
    for name,idx in y_buckets.items():
        y_list[idx] = name

    # Print the X bucket names
    print ("     ",end="")
    for x in range(len(x_buckets)):
        print (f"{x_list[x]:>4n}",end="")
    print()
    # Print the full 2D array. Each line starts with its y-bucket name.
    for yb in range(len(y_buckets)):
        print (f"{y_list[yb]:>4} {n_grew[yb,:len(x_list)]}")

# Draw a plot. X axis is tumor size, and Y axis is the fraction of tumors that
# grew for that tumor size. Draw a different plot line for the data from each
# input file.
# Assume that parse_files() has been called and left the results in res_list.
def plot_success_vs_tumor_size ():
    fname = ""
    for idx,r in enumerate(res_list):
        if (r.fname != fname) or (idx==len(res_list)-1):   # Begin/end a file.
            # Finish off the existing file & plot it.
            if (fname != ""):
                print (f"{fname}: {n_grew=},\n\t{n_total=}")
                n_grew[1:] = n_grew[1:] / n_total[1:]	# Change to fractions.
                plt.plot (n_grew, label=fname)
                plt.ylabel(f'fraction that grew')

            # Set up for the records from the new file.
            fname = r.fname		# So we know when the *next* file starts
            n_grew  = np.zeros(20)	# How many tumors grew vs. tumor size
            n_total = np.zeros(20)	# How many total tumors per tumor size

        # For this Result object, just track/count whether it grew or not.
        if (r.direction=="grew"):
            n_grew[r.size] = n_grew[r.size] + 1
        n_total[r.size] = n_total[r.size] + 1

    # Now finish and display the entire plot.
    plt.gca().xaxis.set_major_locator(plt.MultipleLocator(2))
    plt.xlabel('Tumor size')
    plt.legend (loc="upper left")
    plt.title (f'Growth vs tumor size')
    plt.show()




Z2D_HYP5= "zombie_metas_2D_5_hypol_1018"
Z2D_DEP5= "zombie_metas_2D_5_depol_1018"
Z2D_DU5="zombie_metas_2D_5_dumb_1018"

parse_files ([Z2D_HYP5, Z2D_DEP5, Z2D_DU5])
#parse_files ([Z2D_D3])
#parse_files (["tumor_depol", "tumor_hyperpol"])
#sort_records('GJ_power')
print_success_vs_XY()
#check_monotonic_vs_tumor_size('GJ_power')
#check_monotonic_vs_tumor_size('size')
#check_monotonic_vs_tumor_size('GJ_NCMM')
#plot_success_vs_tumor_size()
