###############################################
# This file plots ion channels used by the Mafe/Cervera papers to be
# sure that I understand them.
###############################################

import numpy as np, math
import matplotlib.pyplot as plt

# The inward-rectifying channel allows positive ions to flow in quite well, and
# prevents the Vmem from going too negative -- in this case, from going below
# -60mV.
def gate_inRect (Vmem):	# Vmem is in mV
    Gin = 1
    Vt = 27; VthIn = -27
    z = 3
    den = 1 + np.exp((z*Vmem-VthIn)/Vt)
    return (Gin/den)

def gate_outRect (Vmem):	# Vmem is in mV
    Gin = 1
    Vt = 27; VthIn = -27
    z = 3
    Ein = -60
    num = Gin * (Vmem-Ein)
    den = 1 + np.exp((z*Vmem-VthIn)/Vt)
    return (num/den)

# From channelpedia
def gate_inRect_Kir21 (V):
    m_inf = 1 /(1+np.exp((V-(-96.48))/23.26))
    h_inf = 1 /(1+np.exp((V-(-168.28))/-44.13)) 
    #plt.plot (V, m_inf, label='m_inf')
    #plt.plot (V, h_inf, label='h_inf')
    # m_power=1, h_power=2
    return (m_inf * h_inf * h_inf)

def base_simple (Vmem):
    flux = -(Vmem - -60)	# likes to be at -60mV
    return (flux / flux[0])	# normalize it

def base_GHK (V):
    Vmem = V / 1000		# from mV to Volts
    size = Vmem.size
    cExt = np.array ([5])	# [n_ions]; K cExt [moles/m3]
    cIn  = np.zeros ((1,size))	# [n_ions,n_cells]; K cInt=88
    cIn[:,:] = 5 / .0994906	# So Vnernst = -60mV
    D = np.zeros ((1,size))	# [n_ions,n_cells]; Dmem for K
    D = 1e-18
    ion_z = np.array ([1])	# ion_z[n_ions]	valence of the K is +1

    #import pdb; pdb.set_trace()
    print (f"Vnernst={26 * math.log(cExt[0]/cIn[0,0])}")

    # parameters from class Params
    p_F = 96485  # Faraday constant [C/mol]
    p_R = 8.314  # Gas constant [J/K*mol]
    p_T = 310  # temperature in Kelvin
    p_tm = 7.5e-9  # thickness of cell membrane [nm]
    p_k26mV_inv = p_F / (p_R*p_T)

    FLOAT_DELTA = 1.0e-25
    i = ion_z.size; c=Vmem.size
    ZVrel = (np.reshape(ion_z,(i,1))*np.reshape(Vmem*p_k26mV_inv,(1,c))) \
          + FLOAT_DELTA

    exp_ZVrel = np.exp(-ZVrel)		# exp (-Z*Vrel)
    deno = -np.expm1(-ZVrel)		# 1 - exp(-Z*Vrel), the GHK denominator
    # tm = thickness of the cell membrane [meters]
    P = D / p_tm 			# permeability of a channel = D/L; [i,c]

    # -P*Z*Vrel * (cIn - cExt*exp(-Z*Vrel)) / (1 - exp(-Z*Vrel))
    cExt_exp = (cExt.T * exp_ZVrel.T).T
    flux = -P*ZVrel*((cIn - cExt_exp)/deno)

    return (flux[0,:] / flux[0,0])	# normalize it

def main():
    V = np.linspace (-100,100,200)
    #plt.plot (V, base_GHK(V),    label="base_GHK")
    #plt.plot (V, base_simple(V), label="base_simple")

    #plt.plot (V, gate_inRect (V), label='gate inRect')
    #plt.plot (V, gate_inRect_Kir21(V), label='gateKir2.1')

    plt.plot (V, base_simple(V)*gate_inRect(V),       label='I simple inRect')
    plt.plot (V, base_simple(V)*gate_inRect_Kir21(V), label='I simple Kir2.1')
    plt.plot (V, base_GHK(V)   *gate_inRect(V),       label='I GHK inRect')
    plt.plot (V, base_GHK(V)   *gate_inRect_Kir21(V), label='I GHK Kir2.1')

    plt.legend (loc="upper right")
    plt.show()

main()

