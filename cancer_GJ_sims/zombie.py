# Add new mode to simulate metastasis with the tumor depolarized and no
# changes in healthy-cell Vmem.
# 23. Add binary search for integer-valued variables (e.g., tumor size) and
#     metas_compare_algorithms().
# 24. Code cleanup; change "depol" -> "algo"

import numpy as np, math
import matplotlib, matplotlib.pyplot as plt
import sys; sys.path.append ("..")	# To get the usual Bitsey files
import GJ_lib
import sim, eplot as eplt, edebug as edb
#import pdb; pdb.set_trace()
kVMax=100

################################################################
# Conventions
#	0 means that OA<<OB; 1 means that OA>>OB; X means neither is true
#	Or..."0" means that OA<50 and OB>50, independent of initialization,
#	     "1" means that OA>50 and OB<50, independent of initialization.
#	     "B" means that both initial values are stable
#	     "X" means that OA<50 and OB<50, independent of initialization
#	     (because both NOR gates are forced on by [A] or [B])
################################################################

################################################################
# Random utilities
################################################################

# Call a given function for a bunch of different parameter combinations.
def exhaustive (func,dimX=1,dimY=1,dimZ=21,NRs=[3,5,10],range=[],diags=[False]):
    GP = sim.Params()
    GP.no_dumps = True
    GP.time_step = .0001		# min time step for explicit integration

    if (dimX+dimY+dimZ>0):		# we call surround_flip_sims() with 0,0,0!
        coord_set_dimensions (dimX, dimY, dimZ)
    print(f"Running sims in a {dimX}x{dimY}x{dimZ} field with {diags=},"\
	 +f" {NRs=}\n")

    for diag in diags:
        for NR in NRs:
            title=f"{'diag,' if diag else ''}{NR=}"
            try:
                func (GP, dimX,dimY,dimZ, NR, range, title, diag)
            except ValueError as err:
                print (err)
                print (f'{title}: final result is a stack dump')
            except RuntimeError as err:
                print (err)
                print (f"Skipping the rest of {title}")
            #quit()

# Given a list of ion names, return a list of ion positions
def ions(*ion_list):
    pos_list=[]
    for i in ion_list:
        pos_list.append (sim.ion_i[i])
    return (pos_list)

# Just count the number of "1" cells in the field, where "1" is defined as
# [OA] > [OB]+2. Used, e.g., to measure the size of a tumor.
def n_ones():
    global cc3D
    (OA,OB) = ions ('OA','OB')

    is_1 = (cc3D[OA,:,:,:] > cc3D[OB,:,:,:] + 2)	# [dimX,dimY,dimZ] bool
    n_1s = np.count_nonzero(is_1)			# scalar
    return (n_1s)

# Binary search. Given:
# - initial bounds (x0,x2)
# - a function func() that returns two values
# if func(x0) == func(x2) => return (BAD_SEARCH)
# else
# - keep subdividing the interval (x0,x2) in half so that func(x0)!=func(x2)
#   until the interval is really small
# - return the tiny interval.
# If 'why' is supplied, we print it once up front.
# "Integer"=True restricts the search to integers.
BAD_SEARCH = -99
def binSearch (func, x0, x2, why="", integer=False):
    if (why != ""):
        print ("Starting binary search for", why)
    y0 = func(x0)
    y2 = func(x2)
    if (y0 == y2):
        print (f"Binsearch points are {y0} and {y2}")
        return (BAD_SEARCH,BAD_SEARCH)

    while (x2>x0+1) if integer else (x2 > 1.001 * x0):
        x1 = (x0+x2)/2
        if (integer):
            x1 = int(x1)
        y1 = func(x1)
        if (y1 == y0):
            x0 = x1
        elif (y1==y2):
            x2 = x1
        else:
            print ("Bad search returned by calculation function")
            return (BAD_SEARCH,BAD_SEARCH)

    print (f"Binary search for {why} returning ({x0:.4g},{x2:.4g})")
    return (x0,x2)

# Seed A and B linearly across a 1D array of cells.
def seed_A_B ():
    n_cells = sim.cc_cells.shape[1]
    (A,B) = ions ('A','B')
    sim.cc_cells[A] = np.linspace (0, kVMax, n_cells)
    sim.cc_cells[B] = np.linspace (kVMax, 0, n_cells)

# Set up a straight chain of n_cells cells with complementary gradients of A/B.
# Find which cells (typically the ones near the two ends of the chain) are
# forced to 0 or 1 by the gradients, and which cells can be bistable.
def char_01BX (p, NAB, NR, plus, n_cells):
    setup_basic_network (p, NR, 1,1,n_cells, NAB=NAB, plus=plus)
    startup_init_Vmem()

    (A,B,OA,OB) = ions ('A','B','OA','OB')
    seed_A_B ()		# Set up the complementary gradients on A and B

    # First sim: all cells initialized to OA=100, OB=0
    sim.cc_cells[OA] = kVMax; sim.cc_cells[OB] = 0
    end_time = 100
    t_shots, cc_shots = sim.sim (end_time)
    out_1 = array_to_01X (cc_shots[-1])

    # Second sim: all cells initialized to OA=0, OB=100
    sim.cc_cells[OA] = 0; sim.cc_cells[OB] = kVMax
    t_shots, cc_shots = sim.sim (end_time)
    out_2 = array_to_01X (cc_shots[-1])

    is0 = ((out_1==0) & (out_2==0)) [0,0]
    is1 = ((out_1==1) & (out_2==1)) [0,0]
    isB = ((out_1==1) & (out_2==0)) [0,0]

    str = ""
    for i in range(n_cells):
        str += ("0" if is0[i] else "1" if is1[i] else "B" if isB[i] else "X")
    return (str)

# Create a 3D array of integers, such that each element is
#     - 0 if cc3D[OA] < cc3D[OB] - 2
#     - 1 if cc3D[OA] > cc3D[OB] + 2
#     - 2 otherwise (i.e., X)
# Used only by char_01BX().
def array_to_01X (cc):
    (OA,OB) = ions ('OA','OB')
    n_ions = sim.cc_cells.shape[0]
    cc3D = cc.reshape ((n_ions,g_maxX,g_maxY,g_maxZ))
    is_1 = (cc3D[OA,:,:,:] > cc3D[OB,:,:,:] + 2)
    is_0 = (cc3D[OA,:,:,:] < cc3D[OB,:,:,:] - 2)
    is_X = np.logical_not(is_0) & np.logical_not(is_1)
    out = np.zeros_like (is_1, dtype=np.int32)
    out[is_1] = 1
    out[is_X] = 2
    return (out)

# Assuming that somebody has created cc3D, set [A] to be a linear ramp from
# conc0 to conc1, and [B] to ramp from conc1 to conc0.
# Really, we always call seed3D_A_B(50,50), setting [A]=[B]=50 everywhere.
def seed3D_A_B (conc0, conc1):
    global cc3D
    (A,B) = ions ('A','B')

    cc3D[A,:] = np.linspace (conc0, conc1, g_maxZ)
    cc3D[B,:] = np.linspace (conc1, conc0, g_maxZ)

# Decide whether the most recent sim (which must already have set up cc3D)
# has fixed all tumor spots. That means that
# - rows[0:first_OA_0) are 1 (OA >> OB)
# - rows[first_OA_0:end] are 0 (OA << OB)
# - all rows are "snapped" -- i.e., [OA] is about the same across the row.
def spot_is_fixed ():
    global cc3D, first_OA_0
    (OA,OB) = ions ('OA','OB')
    is_1 = (cc3D[OA,:,:,:] > cc3D[OB,:,:,:] + 2)	# over all cells
    is_0 = (cc3D[OA,:,:,:] < cc3D[OB,:,:,:] - 2)	# over all cells
    bottom = is_1[:,:,:first_OA_0].all()		# rows[ 0:10] are 1
    top    = is_0[:,:,first_OA_0:].all()		# rows[10:20] are 0
    minrows = cc3D[OA].min(axis=(0,1))		# row-by-row min val of [OA]
    maxrows = cc3D[OA].max(axis=(0,1))		# row-by-row max val of [OA]
    snapped = (minrows+2 >= maxrows).all()	# all rows have about const [OA]
    dump_3D(sim.cc_cells,
	    title=f"checking for fixed (1s left of row[{first_OA_0}])")
    dump_GJ_3D(sim.cc_cells, title="checking for fixed, looking at GJs")
    return (bottom and top and snapped)

################################################################
# A group of functions and globals to deal with GJs between nearest neighbors
# in a 3D field of cells. Some useful globals and functions:
#     -	g_maxX,g_maxY,g_maxZ: the field size (set by coord_set_dimensions()).
#     -	coord_xyz2idx(), coord_idx2xyz(): transform between x,y,x and a single
#	integer that packs all three into one integer.
#     -	coord_xyzd2canonID(), coord_canonID2xyzd(): transform between x,y,z,dx,
#	dy,dz and a single integer that packs all six into one integer.
#     -	GJs_3D[i] holds the canonical single integer (which encodes the .from
#	and .to cells) for sim.GJ[i].
################################################################

# First, globals and constants for our 3D GJ package.

# Store the size of the 3D field
g_maxX=0
g_maxY=0
g_maxZ=0

# One list element per GJ, so it's a list of [N_GJs]. GJs_3D[i] contains a
# canonical integer that has the 'from' and 'to' cells for sim.GJ_connects[i]
# packed into it.
GJs_3D = []
			
def coord_set_dimensions (x,y,z):
    global g_maxX, g_maxY, g_maxZ
    g_maxX=x; g_maxY=y; g_maxZ=z
    print (f"Field={x}x{y}x{z}")

# Called by setup_basic_network().
# Figure out where the GJs are in a 1D, 2D or 3D field of cells. The code
# always uses 3D; to setup, say, a 1D field, you just give dimX=dimY=1.
# The main result of this function is that it uses alloc_GJ() to build GJs_3D[],
# the list of all GJs in this set of sims.
def setup_GJ_3D (allow_diag):
    global GJs_3D
    GJs_3D=[]
    for x in range (g_maxX):
        for y in range (g_maxY):
            for z in range (g_maxZ):
                for dx in [0,1]:
                  for dy in [0,1]:
                    for dz in [0,1]:
                        if (dx+dy+dz>0) and ((allow_diag) or (dx+dy+dz==1)) \
                          and (x+dx<g_maxX) and (y+dy<g_maxY) and (z+dz<g_maxZ):
                            alloc_GJ (x,y,z,dx,dy,dz)
    return (len(GJs_3D))

# Add an entry in the list GJs_3D[] for a new GJ, from x,y,x to x+dx,y+dy,z+dz.
# Dx, dy and dz can be 0 or +1. These six numbers are represented by a single
# canonical ID that contains all of them packed in one number.
def alloc_GJ (x,y,z,dx,dy,dz):
    assert (x<g_maxX) and (y<g_maxY) and (z<g_maxZ)
    ID = coord_xyzd2canonID (x,y,z,dx,dy,dz)
    GJs_3D.append (ID)
    #print (f"x={x},y={y},z={z},dx={dx},dy={dy},dz={dz} => ID={ID}")

# Given a field of dimensions (g_maxX, g_maxY, g_maxZ), transate a coordinate
# set (x,y,z) in to a linear index. 
# Coord_idx2xyz() and coord_xyz2idx() are inverses.
def coord_xyz2idx (x,y,z):
    assert (x<g_maxX) and (y<g_maxY) and (z<g_maxZ)
    return (x*g_maxY*g_maxZ + y*g_maxZ + z)

# Given a linear index of a cell, return its (x,y,z) tuple.
# Coord_idx2xyz() and coord_xyz2idx() are inverses.
def coord_idx2xyz (idx):
    z = idx % g_maxZ; idx = int ((idx-z) / g_maxZ)
    y = idx % g_maxY; idx = int ((idx-y) / g_maxY)
    x = idx
    return (x,y,z)

# Given a GJ from the cell at (x,y,z) to the cell at (x+dx,y+dy,z+dz): pack
# those six numbers into a single canonical integer.
# It's (the linear index for x,y,z)<<3 + (dx,dy,dz as a 3-bit int).
def coord_xyzd2canonID (x,y,z,dx,dy,dz):
    xyz = coord_xyz2idx (x,y,z)
    return ((xyz<<3) + (dx<<2) + (dy<<1) + dz)
    
# Unpack a canonical ID into (from-cell = bits[max..3]), (dx,dy,dz)=bits[2:0]
def coord_canonID2xyzd (canonID):
    (dx,dy,dz) = ((canonID&4)>>2, (canonID&2)>>1, canonID&1)
    idx = canonID>>3
    (x,y,z) = coord_idx2xyz (idx)
    return (x,y,z,dx,dy,dz)

# Called by setup_basic_network(), after it has called init_big_arrays() and
# sim.GJ_connects[] thus exists.
# We now set GJ_connects.from and .to appropriately, by just walking through
# GJs_3D[] and grabbing the from/to cells of each GJ[i] from GJs_3D[i].
def GJs_3D_give_to_sim ():
    for idx in range (len(GJs_3D)):
        (x,y,z,dx,dy,dz) = coord_canonID2xyzd (GJs_3D[idx])
        sim.GJ_connects['from'][idx] = coord_xyz2idx (x,y,z)
        sim.GJ_connects['to'][idx]   = coord_xyz2idx (x+dx,y+dy,z+dz)
        #print (f"Set GJ[{idx}] = {x},{y},{z} -> {x+dx},{y+dy},{z+dz}")
        #print (f"Set GJ[{idx}] = {coord_xyz2idx (x,y,z)}->{coord_xyz2idx (x+dx,y+dy,z+dz)}")

# Our main dumping function for 1D, 2D or 3D cell arrays. "What_to_show" can be
# "OA:OB" (the default), "dOA:OB", "m", "Vmem" "KionC" (the gating of the
# potassium ion channel by set_Vmem_from_conc), or "A:B".
def dump_3D (cc,what_to_show="OA:OB",bool=False, title=""):
    if (cc.shape[1] > 2000):	# Avoid huge printouts.
        return

    (A,B,OA,OB,m) = ions ('A','B','OA','OB','m')
    width = 3 if bool else 6
    if (title != ""):
        print (f"3D dump of {title}...")
    print ("z=... ", "".join ([f'{z:^{width}}' for z in range(g_maxZ)]))

    # We index cc3D as [ion,x,y,z]
    n_ions = cc.shape[0]
    cc3D = cc.reshape ((n_ions,g_maxX,g_maxY,g_maxZ))

    for x in range (g_maxX):
        for y in range (g_maxY):
            print (f"x={x},y={y:2d} ",end="")
            for z in range (g_maxZ):
                if (what_to_show=="OA:OB"):
                  if (bool):
                    str = '1' if cc3D[OA,x,y,z]>cc3D[OB,x,y,z]+2 \
                    else  '0' if cc3D[OB,x,y,z]>cc3D[OA,x,y,z]+2 \
                    else  'X'
                  else:
                    str = f'{cc3D[OA,x,y,z]:.0f}:{cc3D[OB,x,y,z]:.0f} '
                elif (what_to_show=="dOA:OB"):
                    str = f'{cc3D[OA,x,y,z]-cc3D[OB,x,y,z]:.0f}'
                elif (what_to_show=="A:B"):
                    str = f'{cc3D[A,x,y,z]:.0f}:{cc3D[B,x,y,z]:.0f} '
                elif (what_to_show=="m"):
                    str = f'{cc3D[m,x,y,z]:.2f}'
                elif (what_to_show=="Vmem"):
                    Vm = sim.compute_Vm (cc)
                    Vm3D = Vm.reshape ((g_maxX,g_maxY,g_maxZ))
                    str = f'{Vm3D[x,y,z]*1000:.1f}'
                elif (what_to_show=="KionC"):
                    if (len(sim.IC_gates)!=1):
                        return
                    IC_gate = sim.IC_gates[0]
                    gates = IC_gate.func (cc, None, None, None)
                    gates = gates.reshape ((g_maxX,g_maxY,g_maxZ))
                    str = f"{gates[x,y,z]:.1f}"
                print (f"{str:{width}}",end="")
            print ('')
        if (x < g_maxX-1):
            print ('')

# Dumps an output like
#	3D dump of GJs after part 3...
#	Most common gatings are [0.509, 1.0]
#	GJ from (0,0,0)=[97:13] to (0,0,1)=[96:13], gatings=[0.509, 0.5]
#	GJ from (0,0,1)=[96:13] to (0,0,2)=[94:15], gatings=[0.509, 0.5]
# So, for each GJ that's not the most common value, it dumps that GJ, with the
# OA:OB for the GJ's from and to nodes, and its gatings. In this example, all
# gatings are a list of two values, since there are two GJ gatings in place.
def dump_GJ_3D (cc, title=""):
    if (cc.shape[1] > 2000):	# Avoid huge printouts.
        return

    (OA,OB) = ions ('OA','OB')
    if (title != ""):
        print (f"3D GJ dump of {title}...")

    # Prepare to call the GJ gating functions from sim.py
    Vm = sim.compute_Vm(cc)
    deltaV_GJ = (Vm[sim.GJ_connects['from']]-Vm[sim.GJ_connects['to']])

    # Build gatings[] and MCS[]; each is [number of gating funcs]
    # Gatings[i] is the vector[n_GJs] returned by the i'th gating func
    # MCS[i] is the most common scale factor in gatings[i].
    gatings = []		# Will collect gating-func *return* values
    MCS = []			# MCS[i] is the most common value in gatings[i].
    for g in sim.GJ_gates:
        gat = g.func (cc, Vm, deltaV_GJ, 0)	# vector[n_GJs]
        gat = [round3D(x,3) for x in gat]# round to avoid FP roundoff in the max
        mc = max (gat, key=gat.count)	# most common element in 'gat'
        gatings.append (gat)
        MCS.append (mc)
    if (len (sim.GJ_gates)==0):
        print ("No GJ gatings exist now")
    else:
        print (f"\tMost common gatings are {MCS}")

    # Now print all of the GJs in order of our creating them, with their
    # from-cell and to-cell [OA].
    n_ions = cc.shape[0]
    cc3D = cc.reshape ((n_ions,g_maxX,g_maxY,g_maxZ))
    for idx in range (len(GJs_3D)):
        GJ = GJs_3D[idx]			# The packed ID
        gats = [g[idx] for g in gatings]	# [My gating val in each gating]
        close = [math.isclose(MCS[i],gats[i]) for i in range(len(MCS))]
        #print (close)
        if (all ([math.isclose(MCS[i],gats[i]) for i in range(len(MCS))])):
            continue
        (x,y,z,dx,dy,dz) = coord_canonID2xyzd (GJ)
        print (f"GJ from ({x},{y},{z})=[{cc3D[OA,x,y,z]:.0f}"\
              +f":{cc3D[OB,x,y,z]:.0f}] "\
	      +f"to ({x+dx},{y+dy},{z+dz})=[{cc3D[OA,x+dx,y+dy,z+dz]:.0f}"\
	      +f":{cc3D[OB,x+dx,y+dy,z+dz]:.0f}]"\
              +f", gatings={[gating[idx] for gating in gatings]}")

# Small function to round to 3 significant digits. The usual Python round()
# rounds to the third digit *after the decimal point*, which isn't what we want
# (since we may have GJ gating function up to 100).
def round3D (number, n_dig):
    # With n_dig=3: 1234 => -1, 12.345=>1, 1.2345=>2, .1234=>
    n_round = n_dig - 1 - int (math.floor (math.log10(number)))
    return (round (number, n_round))

################################################################
# A few network pieces:
# - A routine to build the basic network,
# - A class for the NOR2 gate that we use to build our cross-coupled state
# - The class that we use to set Vmem as a function of [OA]
# - GJ gating classes used by the tumors.
####################################################################

# Setup the basic network.
def setup_basic_network (p, NR, dimX,dimY,dimZ, allow_diag=False,
			 do_gate=False, do_drive_Vmem=False, NAB=5,plus=15,
                         decay_rate=1):
    coord_set_dimensions (dimX, dimY, dimZ)
    print (f"Routing GJs {'diagonally' if allow_diag else 'rectangularly'}")
    n_GJs = setup_GJ_3D (allow_diag)
    n_cells = dimX * dimY * dimZ
    sim.init_big_arrays (n_cells=n_cells, n_GJs=n_GJs,p=p,
    			 extra_ions=['A','B','OA','OB','m'])
    (A,B,OA,OB,m) = ions ('A','B','OA','OB','m')

    # Set up a 3D view of sim.cc_cells.
    global cc3D
    n_ions = sim.cc_cells.shape[0]
    cc3D = sim.cc_cells.reshape ((n_ions,g_maxX,g_maxY,g_maxZ))

    # Set up cross-coupled-NOR GRN, identically in every cell.
    # Note that this includes both generation and decay.
    # See the Google Docs NOR2_model for details on what's happening here.
    alpha = 1 + (50/(50+plus))**NAB
    beta = alpha ** (1/NR)
    nor1=Hill_NOR2_gate (sim.Gate.GATE_GD,OA,A,OB, decay=decay_rate,
    			 kMM=50+plus,kMR=50/beta, NM=NAB,NR=NR, kVMax=100*alpha)
    nor2=Hill_NOR2_gate (sim.Gate.GATE_GD,OB,B,OA, decay=decay_rate,
    			 kMM=50+plus,kMR=50/beta, NM=NAB,NR=NR, kVMax=100*alpha)

    # Physical parameters of A, B, OA, OB and m
    sim.z_array[[A,B,OA,OB,m]] = 0	# neutral ions
    sim.GJ_diffusion[[A,B,m]] = 0

    # Create the [OA]-drive-Vmem gatings by instantiating a gate. 
    if (do_drive_Vmem):
        set_Vmem_from_conc("OA","OB")

    # Full GJ connections along the field. All are simple & ungated for now.
    GJs_3D_give_to_sim ()

    # Set up voltage-gating of the GJs by, again, instantiating a gate.
    # No need to give it any connections; it applies equally well to all GJs.
    if (do_gate):
        GJ_lib.GJ_gate()

# The gate class implementing a two-input NOR gating.
# It's the Hill function meant to be used as a "generate" term:
#	kVmax / (1 + ([M]/kMM)*NM + ([R]/kMR)*NR)
# where "M" is the morphogen input (either A or B) and "R" is the
# cross-repressor (either OA or OB).
# So we would instantiate one gate with output=OA, M=A, R=OB and another
# with output=OB, M=B, R=OA.
class Hill_NOR2_gate (sim.Gate):
  def __init__(self, gtype,out_ion,
 		in_ionM,in_ionR,kMM=1,kMR=1,NM=1,NR=1, kVMax=1, decay=1):
    sim.Gate.__init__ (self, gtype, out_ion)
    self.in_ionM=in_ionM; self.in_ionR=in_ionR
    self.kMM=kMM; self.kMR=kMR
    self.NM=NM;   self.NR=NR
    self.kVMax = kVMax*decay
    self.decay_rate = decay

  # The function that actually computes the gating vector at runtime.
  def func (self, cc, Vm_ignore, deltaV_GJ_ignore, t_ignore):
    # self.params[] is [n_params, # cells or GJs]. Assign each of its rows
    # (i.e., a vector of one parameter across all cells) to one variable.
    concM = cc[self.in_ionM]	# vector [N_CELLS]
    concR = cc[self.in_ionR]	# vector [N_CELLS]
    termM = (concM/self.kMM)**self.NM
    termR = (concR/self.kMR)**self.NR
    gen = self.kVMax / (1 + termM + termR)
    decay = cc[self.out_ion] * self.decay_rate
    return (gen - decay)

# This is an ion-channel gating function; we use it to set a cell's Vmem based
# on its concentrations of ions "H" and "D" (for hyperpolarize and depolarize):
#	If [H]>[D], set Dm_K=31e-18 => Vmem=-68mV
#	If [H]<[D], set Dm_K= 1e-18 => Vmem=-15mV
# The default is Dm_K=1e-18, so the gating function returns a number in [1,31].
# Note that we typically instantiate set_Vmem_from_conc ("OA","OB"), but
# sometimes we use ("B","A").
# If neighbor=True, then allow a tumor cell, when its [M] says it's truly a
# tumor, to depolarize its higher-numbered neighbors. So the ratio [OA]/[OB] for, e.g., cells[2,5,8]
# is the max of [OA]/[OB] for cells [2,5,8], [1,5,8], [2,4,8] and [2,5,7]. This
# only works because tumors always start in the corner at [0,0,0].
# We set
#	ratio = (H/D)**10
#	if requested, push to the neighbors based on [M]
#	return 1 + 30*ratio/(1+ratio), which is in [1..31].
class set_Vmem_from_conc (sim.Gate):
  def __init__ (self, depIon, hypIon, neighbor=False):
    sim.Gate.__init__ (self, dest=self.GATE_IC, out_ion=sim.ion_i['K'])
    (self.depIon,self.hypIon,self.m) = ions (depIon,hypIon,"m")
    self.neighbor = neighbor

  def func (self, cc, Vm_ignore, deltaV_GJ_ignore, t_ignore):
    # ratio is [x,y,z], and each element is in [0,inf]
    ratio = (cc[self.hypIon]/(.001+cc[self.depIon]))**10	# in [0,inf]
    factor = ratio / (1+ratio)					# in [0,1]
    if (self.neighbor):
        global g_maxX, g_maxY, g_maxZ
        M3D = cc[self.m].reshape ((g_maxX,g_maxY,g_maxZ))
        ratio = (cc3D[self.hypIon]/(.001+cc3D[self.depIon]))**10
        neighb3D = 1-M3D	# So 1=healthy, 0=tumor based on [M]
        f3D = factor.reshape ((g_maxX,g_maxY,g_maxZ))	# 
        if (ratio.shape[0]>1):
            f3D[1:,:,:] = np.minimum (f3D[1:,:,:], neighb3D[:-1,:,:])
        if (ratio.shape[1]>1):
            f3D[:,1:,:] = np.minimum (f3D[:,1:,:], neighb3D[:,:-1,:])
        if (ratio.shape[2]>1):
            f3D[:,:,1:] = np.minimum (f3D[:,:,1:], neighb3D[:,:,:-1])
        factor = f3D.ravel()	# Convert back to the usual 1D array[n_cells].
    out = 1 + factor*30	# interpolate between 1e-18 and 31e-18.
    return (out)

# Set up a GJ gating that shrinks Kd on any cell with OA>>OB. It assumes
# GJ_NCMM=0 (neighbor-channel min/max), so any GJ conducts less if *either* of
# its two cells is a tumor.
class tumor_isolate(sim.Gate):
  def __init__ (self, shrink_amt):
    sim.Gate.__init__ (self, dest=self.GATE_GJ, out_ion=None)
    (self.OA,self.OB) = ions ("OA", "OB")
    self.shrink_amt = shrink_amt

  def func (self, cc, Vm_ignore, deltaV_GJ_ignore, t_ignore):
    ratio = (cc[self.OA]/(.001+cc[self.OB]))**10
    # Build the gating amount (still per-cell). It now maps ratio in [0,inf] to
    # factor in [1,shrink_amt]. So if shrink_amt=.5, then OA<<OB => factor=1,
    # and OA>>OB => factor=.5. This number says how many connexins that cell
    # will create. Then any GJ *between* two cells will have x-sec area
    # proportional to the min of the connexins that the two cells create.
    factor = self.shrink_amt + (1-self.shrink_amt)/(1+ratio)
    from_factor = factor[sim.GJ_connects['from']] # now [n_GJs]
    to_factor   = factor[sim.GJ_connects['to']] # now [n_GJs]
    out = np.minimum (from_factor, to_factor)
    return (out)

# Class M_gate_tumorness tracks how long cells have been in a tumor state, using
# a dedicated species 'm' (which is a full-fledged species in BITSEY, but is
# not physically real, and is named after the Hodgkin-Huxley gating fraction).
# M exists in all cells and doesn't diffuse at all.
# 'M' is essentially a slow-changing (with a time constant of tau) marker of
# tumor-ness. So we set
#	ratio = (OA/OB)**10
#	m_inf = 1 + (ratio / (1+ratio))... so, mostly 0 or 1
#	exponential ramp to m_inf with the desired tau.
class M_gate_tumorness (sim.Gate):
  def __init__ (self, tau):
    self.tau = tau
    (self.OA,self.OB,self.m) = ions ("OA", "OB", "m")
    sim.Gate.__init__ (self, dest=self.GATE_GD, out_ion=self.m)

  def func (self, cc, Vm, deltaV_GJ_ignore, t_ignore):
    # ratio is a vector[n_cells]; each element of it is in [0,inf]
    ratio = (cc[self.OA]/(.001+cc[self.OB]))**10
    # M_inf is a number in [0,1] as desired. This is what 'm' will
    # move towards... but it will be a slow approach.
    m_inf = ratio/(1+ratio)

    m = cc[self.m,:]
    m_tau = np.ones_like (Vm)*self.tau		# All tau, but size of [n_cells]
    #print ("m_inf={}, tau={:.6g}".format(m_inf, m_tau[0]))
    return ((m_inf - m) / self.tau)		# Gen-decay amount in each cell

# This class ups the GJ gating from 1 up to "power" when [OA]>[OB]. It does so
# slowly -- based on [M] from class M_gate_tumorness.
class tumor_power_GJ_gate (sim.Gate):
  def __init__ (self, power, GJ_NCMM):	# neighbor-channel min/max
    sim.Gate.__init__ (self, dest=self.GATE_GJ, out_ion=None)
    (self.m) = ions ("m")
    (self.power, self.GJ_NCMM) = (power, GJ_NCMM)

  def func (self, cc, Vm_ignore, deltaV_GJ_ignore, t_ignore):
    # ratio is a vector[n_cells]; each element of it is in [0,inf]
    from_m = cc[self.m,sim.GJ_connects['from']]	# now [n_GJs]
    to_m   = cc[self.m,sim.GJ_connects['to']]	# now [n_GJs]
    min_m = np.minimum (from_m, to_m)
    max_m = np.maximum (from_m, to_m)
    my_m = min_m + self.GJ_NCMM * (max_m-min_m)
    return (1 + my_m*(self.power-1))

################################################################
# Build and test the Thevenin equivalent network model
################################################################

# Compute and print our Thevenin equivalent model for GJs and decay.
def print_Thev_model (GJ_diff, power, K_decay, title=""):
    (_, R_decay, cap_cell) = Thev_cell (0, K_decay)
    R_GJ = Thev_GJ (GJ_diff)

    tumor = f", or {R_GJ/power:.2g} in a tumor" if power>1 else ""
    if (title==""):
        title=f"{K_decay=:.2g}, {GJ_diff=:.2g}, {power=}"
    print (f"{title} => C_cell={cap_cell:.2g} m3; {R_decay=:.2g} s/m3;"\
          +f" {R_GJ=:.2g}s/m3{tumor}.")

# Compute the Thevenin equivalent circuit for a cell with gen & decay.
# And as a bonus, return cap_cell = vol_cell.
# - K_decay => G_norton => R_norton
# - gen => I_norton
# - and thus return V_thev, R_thev
# Note that a bit of algebra shows that V_thev = gen / K_decay
def Thev_cell (gen, K_decay):
    #GP = sim.Params()
    vol_cell = (4/3) * 3.1416 * (sim.GP.cell_r**3)	# m3
    I_norton = gen * vol_cell			# mol/m3s * m3 = mol/s
    G_norton = K_decay * vol_cell		# 1/s * m3 = m3/s
    R_thev   = 1 / G_norton			# s/m3
    V_thev   = I_norton * R_thev		# mol/s * s/m3 = mol/m3	
    cap_cell = vol_cell				# m3
    return (V_thev, R_thev, cap_cell)

# Computes and returns GJ resistance (s/m3)
def Thev_GJ (GJ_diff):
    #GP = sim.Params()
    G_GJ = GJ_diff * sim.GP.cell_sa / sim.GP.GJ_len	# m3/s
    return (1/G_GJ) if G_GJ!=0 else float("inf")

# Assume two cells (each with gen & decay) connected by a GJ.
# Use Thev_cell() to get an equivalent circuit for each cell, and Thev_GJ() for
# the GJ. Then analyze the network to find the resultant Vmem for each cell.
def two_cell_Thev (gen0, dec0, gen1, dec1, GJ_diff):
    (Vth0, R_dec0, cap_cell) = Thev_cell (gen0, dec0)
    (Vth1, R_dec1, _)        = Thev_cell (gen1, dec1)
    R_GJ = Thev_GJ (GJ_diff)

    vol_cell = cap_cell
    deltaV = Vth0 - Vth1	# mol/m3
    totalR = R_dec0 + R_GJ + R_dec1	# s/m3 
    I = deltaV / totalR			# (mol/m3) / (s/m3) = mol/s
    V0 = Vth0 - I*R_dec0		# mol/m3
    V1 = Vth1 + I*R_dec1		# mol/m3
    return (V0, V1, Vth0, Vth1, R_dec0, R_dec1, R_GJ)

# Top-level code to test the model. Build two cells, each with simple gen/decay,
# and connect them with a GJ. Sim it, and check that the sim matches the model
# predictions.
def test_Thev ():
    # Parameters of the test.
    (gen0, gen1) = (10,30)	# moles/m3s
    decay = 1			# 1/s
    GJ_diff = 1e-13		# m2/s

    # Build a simple network with two cells connected by a GJ.
    GP = sim.Params()
    GP.no_dumps = True
    sim.init_big_arrays (n_cells=2, n_GJs=1,p=GP, extra_ions=['OA'])

    # OA setup
    (OA,) = ions ('OA')
    sim.z_array[OA] = 0		# neutral ion
    sim.Dm_array[OA] = 0	# No ion-channel diffusion

    # GJ setup
    sim.GJ_diffusion[OA] = GJ_diff
    sim.GJ_connects['from'][0] = 0
    sim.GJ_connects['to'][0] = 1
    
    # Set up gen/decay.
    GD = sim.GD_const_gate(sim.Gate.GATE_GD, OA, gen=gen0, decay=decay)
    GD.change_GD_const_gate (cell=1, gen=gen1, decay=decay)

    # Sim and dump.
    t_shots, cc_shots = sim.sim (100)

    # Model
    (V0,V1,Vth0,Vth1,R_dec0,R_dec1,R_GJ) \
	= two_cell_Thev(gen0,decay,gen1,decay,GJ_diff)
    import math
    assert math.isclose (R_dec0,R_dec1)
    print (f"{Vth0=}, Rth={R_dec0:.3e}; {R_GJ=:.3e}; {Vth1=}")
    print (f"{V0=:.4g}mol/m3, {V1=:.4g}mol/m3")
    cc_OA = cc_shots[-1][OA]
    print (f"Actual cc_cells[OA]=[{cc_OA[0]:.4f},{cc_OA[1]:.4f}]")

    #eplt.plot_ion (t_shots, cc_shots, ['OA'])

################################################################
# Characterize our cross-coupled NOR gates.
# We used this code to first understand the deficiencies of the "old" NOR2
# mode, and then design the "new" NOR2 gate Hill_NOR2_gate.
################################################################

def char_NOR2 (p, dimX,dimY,dimZ, NR, diag, title):
    global g_NAB, g_NR, g_plus	# For communication with NOR2_func().
    (g_NAB, g_NR, g_plus) = (NAB, NR, plus)
    # Only NAB, NR and plus are relevant (and with the new NOR2 model, only NR
    # is relevant). Ignore the rest.
    title = f"{NAB=}, {NR=}, {plus=}"

    # First, find the metastable point.
    (meta, _) = binSearch (NOR2_meta, 0, 100, why=f"{title} metastable search")

    # Next find a stable point, by starting wiht OA=OB=0 and simming until
    # convergence.
    OA=0; OB=0
    for i in range(10):
        OB = NOR2_func(OA)
        #print (f"{i}. {OA:.3f}:{OB:.3f}")
        OA = NOR2_func(OB)
    # Print the stable and metastable points.
    print(f"{title}. Stable={OA:.0f}:{OB:.0f}, {meta=:.0f}="\
	 +f"mid{meta-(OA+OB)/2:+.0f}\n")

# This is the basic NOR2 function; it returns the output of the NOR.
# We assume linear decay with k_decay=1, so the generation output is identically
# the NOR2 output.
# Note that while we originally used the "classic" NOR2, it now uses the
# "new" NOR2, which is independent of g_NAB and g_plus.
def NOR2_func (OA):
    global g_NAB, g_NR, g_plus
    alpha = 1 + (50/(50+g_plus))**g_NAB
    beta = alpha ** (1/g_NR)
    out = alpha*100 / (1 + (50/(50+g_plus))**g_NAB + (OA/(50/beta))**g_NR)
    #print (f"NOR2_func({OA:.1f}) => {out:.1f}")
    return(out)

# We give this function to binSearch() to find the metastable point of
# cross-coupled NOR2 gates.
# This zero of this function (well, the border between it returning True and
# False) is when NOR2_func(OA)=OA, which is the metastable point.
def NOR2_meta (OA):
    return (NOR2_func(OA) > OA)

################################################################
# Help pick parameters for our Vmem = f([OA],[OB]) model; i.e., for the
# set_Vmem_from_conc() class.
################################################################

# Test 1: vary the Na and K ion-channel permeabilities and see how that affects
# Vmem.
def set_Vmem_class1 ():
    GP = sim.Params()
    num_cells=17
    n_GJs = 0
    sim.init_big_arrays (num_cells, n_GJs, GP)
    GP.no_dumps = True

    # By default, we have D_Na = D_K = 1e-18
    (Na,K) = ions ('Na','K')
    #sim.Dm_array[K,:]  = np.linspace (1, 160, num_cells) * 1e-18
    sim.Dm_array[K,:]  = np.linspace (1, 30, num_cells) * 1e-18
    #sim.Dm_array[Na,:] = np.linspace (160, 1, num_cells) * 1e-18

    Vm = sim.compute_Vm (sim.cc_cells)
    t_shots, cc_shots = sim.sim (1000)
    edb.dump (t_shots[-1], cc_shots[-1], edb.Units.mol_per_m3s, False)

    eplt.plot_Vmem (t_shots, cc_shots)
    eplt.plot_ion (t_shots, cc_shots, 'Na')
    np.set_printoptions (formatter={'float': '{:3.0f}'.format})
    print (sim.compute_Vm (sim.cc_cells)*1000)
    print (np.linspace (1,31,11))

# Instantiate 11 disconnected individual cells that have 'set_Vmem_from_conc'
# ion-channel gates in them. Set OA and OB directly to vary linearly cell by
# cell, and see how Vmem responds. This led to our final version of class
# set_Vmem_from_conc.
def set_Vmem_class2 ():
    GP = sim.Params()
    GP.no_dumps = True
    sim.init_big_arrays (n_cells=11, n_GJs=0,p=GP, extra_ions=['OA','OB'])
    (OA,OB) = ions ('OA','OB')
    sim.z_array[[OA,OB]] = 0	# neutral ions
    set_Vmem_from_conc("OA","OB")
    sim.cc_cells[OA,:] = np.linspace (0,100,11)
    sim.cc_cells[OB,:] = 100 - sim.cc_cells[OA,:]
    t_shots, cc_shots = sim.sim (1000)
    np.set_printoptions (formatter={'float': '{:3.0f}'.format})
    print (sim.compute_Vm (sim.cc_cells)*1000)
    print (np.linspace (0,10,11))
    eplt.plot_worm (cc_shots[-1], 'V', title='Vmem')
    eplt.plot_worm (cc_shots[-1], ['OA','OB'], title='[OA] and [OB]')

#######################################################
# These analyses look at how hard it is for a cell to get flipped by a bunch of
# neighboring cells. For each reasonable parameter set, we start with one
# central cell initialized to OA=0, OB=100. We surround it with two cells, then
# three cells, etc., all initialized to OA=100, OB=0. For each number of cells
# around it, we use a binary search to find the minimum GJ size that will flip
# the inner cell.
# Obviously, the more surrounding cells, the easier it is to flip the central
# cell.
# We expect to see that the higher NR is, the harder a cell is to flip.
# A higher NAB will make a cell slightly easier to flip; since cells are in the
# bistable region where [A]<kMA and [B]<kMB, a higher NAB makes the inputs
# more completely "get out of the way."
#######################################################

# Do all of the simulation/analysis for a single parameter set.
def surround_flip_sims (GP, _dimX,_dimY,_dimZ, NR, _diag,_title):
    GP.no_dumps = True
    GP.time_step = .005		# restore the default value.
    global OA_disc, OB_disc	# Results of a disconnected sim.

    central_node = False	# arrange the cells as one central node.
    for n_surround in range(2,6):	# Number of surrounding cells.
        # Our GJ configuration is a star rather than a straight line. So we
        # start with the standard setup and then modify the GJ config. Note this
        # only works because we use the same *number* of GJs as usual, just in
        # a different configuration.
        setup_basic_network (GP, NR, 1,1,n_surround+1)
        if (central_node):
            sim.GJ_connects['from']  = 0
            sim.GJ_connects['to']    = range(1,n_surround+1)
        else:	# Straight chain with the tumor still at #0
            sim.GJ_connects['from']  = range(n_surround)
            sim.GJ_connects['to']    = sim.GJ_connects['from'] + 1

        # Initialize [A]=[B]=50 to prepare for both sims.
        (A,B,OA,OB) = ions ('A','B','OA','OB')
        sim.cc_cells[(A,B),:] = 50
        print (f"Disconnected sim with {n_surround} surround cells...")

        # Set [OA,OB]=100/0 in cell 0 and 0/100 elsewhere.
        # Then do a sim *with all cells disconnected* to get more reasonable
        # values for [OA] and [OB]. We'll then use this as the init conditions
        # for each sim of our upcoming binary search.
        sim.cc_cells[OA,0 ]=100; sim.cc_cells[OB,0] =0
        sim.cc_cells[OA,1:]=  0; sim.cc_cells[OB,1:]=100
        sim.GJ_diffusion[[OA,OB]] = 0
        t_shots, cc_shots = sim.sim (100)
        #eplt.plot_ion (t_shots, cc_shots, ['OA','OB'],cells=[0])
        dump_3D (cc_shots[-1], title="disconnected sim at GJ_scale=0")
        OA_disc = cc_shots[-1][OA,-1]	# Disconnected [OA] and [OB] in any
        OB_disc = cc_shots[-1][OB,-1]	# outer cell, for later use.

        # Use a binary search to find the minimum GJ size that's big enough for
        # the surrounding cells to flip the center cell.
        title = f"{NR=}, surround={n_surround}"
        (_,Kd) = binSearch (surround_flip_sim, 0, 1e-12, why=title)
        if (Kd==BAD_SEARCH):
            print (f"{title} final: BAD_SEARCH")
            return
        print (f"{title}: final GJ_scale={Kd:.3g}")
        print_Thev_model (Kd, power=1, K_decay=1, title=title)

        (V0, V1, Vth0, Vth1, R_dec0,R_dec1, R_GJ) \
            = two_cell_Thev (gen0=OB_disc,dec0=1,
                             gen1=OA_disc*n_surround, dec1=n_surround,
                             GJ_diff=Kd*n_surround)
        print (f"{title}: final {V0=:.3f}, {V1=:.3f}, {Vth0=:.3f}, {Vth1=:.3f}"\
              +f", {R_dec0=:.3g}, {R_dec1=:.3g}, {R_GJ=:.3g}")

# Does this GJ_scale allow the outer cells to zombie-flip the central cell?
def surround_flip_sim (GJ_scale):
    global OA_disc, OB_disc	# Results of a disconnected sim.
    (A,B,OA,OB) = ions ('A','B','OA','OB')

    # Set up this sim. Binsearch() tells us GJ_diffusion; we initialize [OA],
    # [OB] from our original disconnected sim.
    sim.GJ_diffusion[[OA,OB]] = GJ_scale
    sim.cc_cells[OA,0 ]=OB_disc; sim.cc_cells[OB,0] =OA_disc
    sim.cc_cells[OA,1:]=OA_disc; sim.cc_cells[OB,1:]=OB_disc

    # Do the sim.
    t_shots, cc_shots = sim.sim (100)
    cc = cc_shots[-1]
    dump_3D (cc_shots[-1], title=f"connected sim at {GJ_scale=:.5g}")

    # Analyze the simulation output to see if we zombie-flipped the cell --
    # i.e., if *all* cells, including the center cell, now have roughly the
    # same values that the outer cells settled to in the disconnected sim.
    OA_good = np.abs(cc[OA]-OA_disc) < 2
    OB_good = np.abs(cc[OB]-OB_disc) < 2
    all_good = OA_good.all() and OB_good.all()
    print(f"{GJ_scale=:.2g} -> zombied={all_good}\n")
    return (all_good)

#######################################################
# The next set of functions in this file are the "spreading" sims. They test
# our basic assumption that gated GJs can make a morphogen transition sharper.
# 
# For the usual three NR choices, and a few pre-picked Kd values, we...
# - build an ungated 1D chain of cells using setup_basic_network()
# - initialize the chain so that one or more cells on one end are high, with
#   the other cells low.
# - sim, and monitor how sharp the transition of [OA] is.
# - add GJ gating, continue the sim, and see how the sharpness changes.
#
# The point of these sims is to see if adding gating to the GJs improves the
# border sharpness. Spoiler: it does :-).
# 
# Note that we ask setup_basic_network() to set Vmem based on whether [OA]>[OB]
# or vice versa. This happens in both sims; but it only matters once we add
# voltage gating to the GJs in the second sim.
#######################################################

# The top-level function to loop through Kd_GJ (which exhaustive() doesn't do),
# call spread() to build a network with a single parameter set, simulate and
# print results.
def measure_spread (p, dimX,dimY,dimZ, NR, _range, title, _diag):
    n_cells=12; n_one=6
    for Kd_GJ in [5e-15, 1e-14,1e-13]:
        title2 = f"{title}, Kd_GJ={Kd_GJ}"
        spread (p, NR, Kd_GJ, n_cells, n_one, title2)

# - Instantiate n_cells straight-line-connected individual cells with
#   setup_basic_network().
# - Set [A],[B] = 50 everywhere
# - GJ_diffusion[[OA,OB]] = whatever we're told.
# - Seed [OA]:[OB] to 100:0 on the left, 0:100 on the right.
def spread (GP, NR, Kd_GJ, n_cells, n_one, title):
    GP.no_dumps = True
    # print (f"Starting {title}...")
    setup_basic_network (GP, NR, 1,1,n_cells, do_drive_Vmem=True)
    (A,B,OA,OB) = ions ('A','B','OA','OB')
    sim.cc_cells[[A,B],:] = 50		# All cells have [A]=[B]=50
    sim.cc_cells[OA,:]    = 0		# Seed [OA], [OB]
    sim.cc_cells[OA,0:n_one] = kVMax
    sim.cc_cells[OB,:]    = kVMax - sim.cc_cells[OA,:]

    # Sim #1 has ungated GJs. Save the results as OA_sim1 and gain1.
    sim.GJ_diffusion[[OA,OB]] = Kd_GJ
    t_shots, cc_shots1 = sim.sim (1000)
    gain1 = cc_shots1[-1][OA,n_one-1] / cc_shots1[-1][OA,n_one]

    # Sim #1 had ungated GJs. Now add GJ gating and continue the simulation,
    # saving the results as OA_sim2 and gain2.
    # print ("Adding gating to GJs...")
    GJgate = GJ_lib.GJ_gate()		# Add gating to the GJs
    t_shots, cc_shots2 = sim.sim (100)	# Sim, starting from the original endpt
    gain2 = cc_shots2[-1][OA,n_one-1] / cc_shots2[-1][OA,n_one]

    # Print and/or plot the results.
    print (f"{title}: border sharpness is {gain1:.4g} ungated",
           f"and {gain2:.4g} gated")
    if (gain1 > 200):
        return

    np.set_printoptions (formatter={'float': '{:3.0f}'.format}, linewidth=100)
    if (True):
        print (cc_shots1[-1][OA], 'OA ungated')
        print (cc_shots2[-1][OA], 'OA gated')
        print (cc_shots1[-1][OB], 'OB ungated')
        print (cc_shots2[-1][OB], 'OB gated')
        print (sim.compute_Vm (cc_shots1[-1])*1000, 'Vmem ungated')
        print (sim.compute_Vm (cc_shots2[-1])*1000, 'Vmem gated')
        print (np.linspace (0,n_cells-1,n_cells), 'cell #')
    return
    plt.plot (range(n_cells), cc_shots1[-1][OA], label='ungated', marker='.')
    plt.plot (range(n_cells), cc_shots2[-1][OA], label='gated', marker='.')
    plt.title ('[OA] without & with gating')
    plt.legend (loc="upper right")
    plt.show()

    #eplt.plot_worm (cc_shots[-1], 'V', title='Vmem')
    #eplt.plot_worm (cc_shots[-1], ['OA','OB'], title='[OA] and [OB]')

#######################################################
# Zombie border-sharpness sims. They are similar to the simpler spread() sims,
# except that:
# - while spread() works at a fixed set of Kd points, we now adaptively pick
#   three values of the minimum Kd to fix a tumor, the max Kd before border
#   collapse, and the geometric mean of those two.
# - while spread() always uses 1D sims, we now can do 2D or even 3D. This is
#   important because tumors behave very differently in 2D than 1D.
#
# Big picture steps:
# 1. Set up an ungated network, with [A]=[B]=50 always. This is because we're
#    modeling the border region between [A]>[B] and [B]>[A] (where there are
#    numerous cells with [A]=[B]=50) to see how sharp the OA/OB boundaries are.
# 2. Build a border of OA & OB; find the largest Kd that doesn't collapse it
# 3. OA=0 everywhere but a spot; find the smallest Kd to fix it
# 4. Find similar Kd values for the gated case.
# 5. Border sharpness at Kd_min, Kd_max and midway for ungated, gated and
#    post-gated.
#######################################################

def zombie_sims_border_sharpness(p,dimX,dimY,dimZ,NR,_range, title, _diag):
    # Three-entry arrays, holding Kd min for fixing, Kd max for collapse, and
    # the geometric mean of the two. Two separate arrays for ungated and gated.
    Kd_ug=[0,0,0]; Kd_g=[0,0,0]
    # The corresponding arrays holding border gains. We have a post-gated gain
    # array, which uses the ungated Kd values.
    gain_ug=[0,0,0]; gain_g=[0,0,0]; gain_pg=[0,0,0]
    print (title)

    # Step 1 above: build the network for this parameterization.
    setup_basic_network(p,NR,dimX,dimY,dimZ, do_gate=False,do_drive_Vmem=True)

    # Set up A=B=50. This is common for all sims we do here. And since A and B
    # don't diffuse, this setup need only be done once.
    seed3D_A_B (50,50)

    # Step 2 above. Build a tumor-free border of OA & OB, then find the largest
    # Kd that doesn't kill it in an ungated network.
    Kd_ug[2] = find_Kd_border_collapse (dimZ)

    # Step 3 above: OA=0 background + 1-tumor; find the smallest Kd to fix it.
    Kd_ug[0] = find_min_Kd_tumorFix (Kd_ug[2], 1, dimZ, title)

    # Our third Kd is always the geometric mean of the low & high values.
    Kd_ug[1] = math.sqrt (Kd_ug[0] * Kd_ug[2])

    # Step 4: fill in Kd_g[], using gated GJs.
    GJgate = GJ_lib.GJ_gate()			# Add gating to the GJs
    Kd_g[2] = find_Kd_border_collapse (dimZ)
    Kd_g[0] = find_min_Kd_tumorFix (Kd_g[2], 1, dimZ, title)
    Kd_g[1] = math.sqrt (Kd_g[0] * Kd_g[2])
    sim.GJ_gates.remove (GJgate)

    # The rest of the sims are just border sims, with no tumor. We save/restore
    # the tumor size because we'll need it again at the next NR value.
    global first_OA_0, spot_dX, spot_dY, spot_dZ, g_maxZ
    first_OA_0 = int(g_maxZ/2)
    (saveX,saveY,saveZ)=(spot_dX,spot_dY,spot_dZ)  # Save tumor size before...
    (spot_dX,spot_dY,spot_dZ) = (0,0,0)	# No tumor in this sim.

    # Part 5 above. Border sharpness at Kd_min, Kd_collapse and midway.
    # If this is an ungated sim, then how much more border sharpness will GJ
    # gating give us?
    for i in range(3):
        gain_ug[i] = border_gain_at_Kd (Kd_ug[i])
        GJgate = GJ_lib.GJ_gate()			# Add gating to the GJs
        gain_pg[i] = border_gain_at_Kd (Kd_ug[i], continuation=True)
        gain_g[i] = border_gain_at_Kd (Kd_g[i])
        sim.GJ_gates.remove (GJgate)
    (spot_dX,spot_dY,spot_dZ) = (saveX,saveY,saveZ)	# Restore for next NR

    print (f"{title}. Final useful Kd range is" \
	 + f" {Kd_ug[0]:.3g}/{Kd_ug[2]:.3g}"\
	 + f"={Kd_ug[2]/Kd_ug[0]:.3f}x ug,"\
	 + f" {Kd_g[0]:.3g}/{Kd_g[2]:.3g}"\
	 + f"={Kd_g[2]/Kd_g[0]:.3f}x g"\
	 + f", bGain min={gain_ug[0]:.3g}/{gain_g[0]:.3g}/{gain_pg[0]:.3g}"\
	 + f", bGain mid={gain_ug[1]:.3g}/{gain_g[1]:.3g}/{gain_pg[1]:.3g}"\
	 + f", bGain max={gain_ug[2]:.3g}/{gain_g[2]:.3g}/{gain_pg[2]:.3g} ug/g/pg")

# Find the minimum Kd that causes an existing border to collapse.
# We use binSearch(), which in turn uses sim2_is_fixed(), which in turn uses
# setup_OA_OB_Kd() and spot_is_fixed() to do the heavy lifting.
# Note that we don't use a tumor -- which means we must set the globals
# spot_dX/dY/dZ to zero, which means we must save/restore the existing values.
# Also note that this is currently used only by zombie_sims_border_sharpness().
def find_Kd_border_collapse (dimZ):
    # Set globals to put in the OA and OB border halfway across the field. These
    # globals will be accessed by setup_OA_OB_Kd() to initialize each sim.
    global first_OA_0, spot_dX,spot_dY,spot_dZ
    first_OA_0 = int(dimZ/2)

    (saveX,saveY,saveZ)=(spot_dX,spot_dY,spot_dZ)  # Save tumor size before...
    (spot_dX,spot_dY,spot_dZ) = (0,0,0)		   # ...no tumor in this sim.

    # Find the smallest value of Kd that makes the border collapse.
    Kd_max = 1e-10	# biggest reasonable Kd
    (Kd_collapse,_) = binSearch (sim2_is_fixed, 0, Kd_max,
				 why="highest Kd that doesn't collapse")

    if (Kd_collapse==BAD_SEARCH):
        raise RuntimeError ("binSearch couldn't find min Kd for border")
    (spot_dX,spot_dY,spot_dZ) = (saveX,saveY,saveZ)
    return (Kd_collapse)

# Compute the "border gain" -- i.e., (cc3D[OA,:,:,-]/cc3D[OA,:,:,+]).mean()
# (where "-" and "+" are the Z coordinates of the cells before and after the
# border). Do it ungated, gated and post-gated.
# First do a sim at Kd to get results. If do_gate, then follow up by adding GJ
# gating to see how much sharper that makes the results.
# Return a tuple of (gain_ungated, gain_gated, gain_post-gated)
# Note that we set the globals first_OA_0, spot_dX, spot_dY, spot_dZ
# to not have a spot, which means that we obliterate them. So the caller
# should do a save/restore if needed.
def border_gain_at_Kd (Kd, continuation=False):
    print (f"Checking border gain at Kd={Kd:.3g}")

    (OA,OB) = ions ('OA','OB')
    fixed = sim2_is_fixed (Kd, do_sim1=not continuation)
    if (not fixed):
        raise RuntimeError (f"Ungated border-gain sim at Kd={Kd:.3g} broke")
    gain = (cc3D[OA,:,:,first_OA_0-1]/cc3D[OA,:,:,first_OA_0]).mean()
    return (gain)

#######################################################
# Tumor isolation sims.
#
# This is a top-level function, called by exhaustive(). It tries two strategies
# to make a tumor more resistant to being fixed:
# - baseline (not a strategy): the tumor has no GJ gating or Vmem changes.
# - strategy #1: simple voltage-gated GJs -- since the tumor is depolarized,
#   these isolate the tumor.
# - strategy #2: underexpress GJs in the tumor.
# It also tries both strategy #1 and #2 at once. For all four cases, none, #1,
# #2, both), we find the minimum Kd that will fix a 1-spot tumor on a 0
# background).
#
# The hope is that these two strategies will make a tumor more resistant to
# being overwhelmed by an increased D_GJ (both inside and outside the tumor).
# Notes:
# - the user must set the globals spot_dX,spot_dY,spot_dZ, g_spot_at_middle
#   at the bottom of the file. These eventually get used by sim2_is_fixed().
# - strategy #2 underexpresses GJs by using the gating tumor_isolate(), which
#   decreases D_GJ by a factor of 2 on any GJ connected to a tumor cell (i.e.,
#   *either* of the GJ's two cells has [OA]>[OB]). Note that we're really
#   talking about *growing* fewer GJs rather than gating them (we implement it
#   as a ligand-gated GJ gate for simplicity).
#
# Note that these sims all have GJ_power=1 and GJ_NCMM=0.
#######################################################
def zombie_tumor_isolate (p,dimX,dimY,dimZ,NR,_range,title,diag):
    global spot_dX,spot_dY,spot_dZ

    # Build the network for this parameterization.
    setup_basic_network(p, NR, dimX,dimY,dimZ, allow_diag=diag,
                        do_gate=False,do_drive_Vmem=False)

    # Set up A=B=50. This is common for all sims we do here. And since A and B
    # don't diffuse, this setup need only be done once.
    seed3D_A_B (50,50)

    # Baseline non-strategy: find the smallest Kd to fix a tumor with no tricks.
    print (f"Fixing a {spot_dX}x{spot_dY}x{spot_dZ} 1-tumor with no tricks")
    title1 = title + " simple ungated"
    Kd_minfix_1 = find_min_Kd_tumorFix (1e-10, 1, dimZ, title1)

    # Part 2. Add set_Vmem_from_conc gating so that the tumor (specifically,
    # any cell with [OA]>>[OB]) is depolarized, and add normal GJ voltage-gating
    # to take advantage of it.
    # Then retake Kd_minfix_1s.
    print (f"Fixing the tumor, isolated only by Vmem-gated GJs")
    set_Vmem_from_conc("OA","OB")	# [OA]>>[OB] -> depolarized Vmem
    Vgate = GJ_lib.GJ_gate()		# Normal voltage-gated GJs
    title2 = title + "  Vmem gating"
    Kd_minfix_2 = find_min_Kd_tumorFix (Kd_minfix_1*20, 1, dimZ, title2)

    # Part 3. Instead, add tumor_isolate gating to shrink any GJ connected
    # to a tumor cell (based on [OA]/[OB]). Then retake Kd_minfix_1s.
    print (f"Fixing the tumor, isolated only by GJ underexp")
    sim.GJ_gates.remove (Vgate)		# remove the Vmem-GJ gate we just added
    GJgate = tumor_isolate(.5)		# Add the underexpression gate
    title3 = title + " GJ underexp"
    Kd_minfix_3 = find_min_Kd_tumorFix (Kd_minfix_2*20, 1, dimZ, title3)

    # Part 4: both of the above.
    print (f"Fixing the tumor, isolated both by GJ underexp and Vmem-gated GJs")
    title4 = title + " Vmem gating and GJ underexp"
    Vgate = GJ_lib.GJ_gate()		# Normal voltage-gated GJs
    Kd_minfix_4 = find_min_Kd_tumorFix (Kd_minfix_2*20, 1, dimZ, title4)

    # Print & compare 1 vs. 2 vs. 3 vs. 4.
    print (f"Final {title}: no tricks/Vmem-gating/underexp/both:"\
	  +f" {Kd_minfix_1:.3g}/{Kd_minfix_2:.3g}/{Kd_minfix_3:.3g}/{Kd_minfix_4:.3g}")

    # Multi-size testing on a 2D tumor.
    if (spot_dY==1) or (spot_dX>1):	# Skip for 1D or 3D sims.
        return
    Kd_minfix_5 = Kd_minfix_4				# Seed the bin. search
    for tsize in range (1,9):				# The tumor size...
        (spot_dX,spot_dY,spot_dZ) = (1,tsize,tsize)
        print (f"Fixing a {tsize}x{tsize} tumor")
        title5 = title + f" {tsize}x{tsize}"
        Kd_minfix_5 = find_min_Kd_tumorFix (Kd_minfix_5*3, 1, dimZ, title5)
        print (f"{title5} final Kd={Kd_minfix_5:.3g}")

# Find the min Kd to fix a tumor.
# We use binSearch, which uses sim2_is_fixed() -- so as usual, the globals
# first_OA_0, spot_dX,spot_dY,spot_dZ determine sim2_is_fixed's behavior.
def find_min_Kd_tumorFix (Kd_collapse, tumor_val, dimZ, title):
    global first_OA_0, spot_dX,spot_dY,spot_dZ, g_spot_at_middle
    first_OA_0 = (0 if tumor_val==1 else dimZ)
    print (f"Using a {spot_dX}x{spot_dY}x{spot_dZ}"\
	  +f" {tumor_val}-spot on a {1-tumor_val} background"\
	  +f" {'in the middle' if g_spot_at_middle else 'at the end'}.")

    # The binary search is bounded by
    # - at Kd=0, the spot is obviously static and thus not fixed.
    # - at Kd_collapse, everything collapses and the tumor is fixed.
    (_,Kd_minfix_1s) = binSearch (sim2_is_fixed, 0, Kd_collapse,
				  why="lowest Kd that fixes a 1-spot")
    if (Kd_minfix_1s==BAD_SEARCH):
        raise RuntimeError ("binSearch couldn't find min Kd to fix a tumor")
    return (Kd_minfix_1s)

#######################################################
# Metastasis sims.
#######################################################

# This is a top-level function called by exhaustive(). It mostly just builds
# the network, iterates through lots of metastasis parameters, and calls
# tumor_GJ_metast() to do the work for each parameter set.
def zombie_sims_metastasis (p,dimX,dimY,dimZ,NR,range,title,diag):
    print (f"Running metastasis sims for {title}") # "Title" is diag, NR vals

    # Build the network. Tumor_GJ_metast() wants us to always instantiate
    # voltage-gated GJs; but decide itself on all other gatings.
    setup_basic_network(p, NR, dimX,dimY,dimZ, allow_diag=diag,
                        do_gate=True,do_drive_Vmem=False)

    # Set up A=B=50. This is common for all sims we do here. And since A and B
    # don't diffuse, this setup need only be done once.
    seed3D_A_B (50,50)

    # Tumor GJ invasion sims.
    # Kd_tumor must be at least 2e-14 to have R_GJ almost= R_decay.
    # The "depol" choices are False (to hyperpolarize the tumor), True (to
    # depolarize the tumor and also its nearby healthy neighbors), or "dumb"
    # (to depolarize the tumor but leave its healthy neighbors hyperpolarized,
    # and hence the tumor mostly disconnected).
    n=0
    # (3 algo)*(8 Kd)*(3 power)*(3 NCMM)*(4 tau)*(19 sizes)= 3*5472
    for algo in [False]:	# "hypol", "depol" or "dumb"
      for Kd_field in np.geomspace (1e-16,6e-12,8):	# healthy-tissue Kd
        for power in (1,10,100):	# Kd_tumor = Kd_field * power
          for GJ_NCMM in [0,.5,1]:	# How much big GJs attach to neighbors
            for GJ_tau in [.01,.1,1,10]:# Tau(s) for powering up a tumor GJ
              for tumor_size in range(1,20):
                if (n >= range[0]) and (n<range[1]):   #For debug, multiple runs
                  try:
                   tumor_GJ_metast (algo, Kd_field, power, GJ_NCMM, GJ_tau,
				    tumor_size, title, n)
                  except ValueError as err:
                   print (err)
                   print (f'{title}: final result is a stack dump')
                   #quit()
                n += 1

# Set the appropriate parameters, sim, and see if the tumor grows or shrinks.
# - The network must already exist, and must already have voltage-gated GJs
#   (but no other gatings, including not setting Vmem from concs).
# - We're responsible for creating any other gatings (based on "algo"), and we
#   remove them when done (so we can easily be called again and reuse the same
#   network).
# - We use double_sim for the simulation. It sets all values in cc_cells,
#   including the tumor. This means that we must (and do) set the globals
#   (spot_dX,spot_dY,spot_dZ) and g_spot_at_middle (and set_Vmem_from_conc()
#   only works if g_spot_at_middle=False).
def tumor_GJ_metast (algo,Kd_field,GJ_power,GJ_NCMM,GJ_tau,tumor_size,title,n):
    # Set the tumor size, location. These globals will get used by double_sim().
    global first_OA_0, spot_dX,spot_dY,spot_dZ
    global g_maxX, g_maxY, g_maxZ
    first_OA_0 = 0	# To create a 0 background.
    # Create the tumor in the appropriate number of dimensions.
    spot_dX = tumor_size if g_maxX>tumor_size else 1
    spot_dY = tumor_size if g_maxY>tumor_size else 1
    spot_dZ = tumor_size if g_maxZ>tumor_size else 1

    algo_str = "dumb" if algo=="dumb" else ("neighbor-depol" if (algo=="depol") else "hyperpol-tumor")
    title2 = f"{Kd_field=:.2e}, {GJ_power=}, {GJ_NCMM=}, {GJ_tau=}, {algo_str}"
    title_tumor = f"{spot_dX}x{spot_dY}x{spot_dZ} tumor"
    print (f"{n}. Seeing if a {title} {title_tumor} spreads with {title2}")
    (_,R_decay,_) = Thev_cell (gen=0,K_decay=1)
    R_GJ_field = Thev_GJ (Kd_field)
    print (f"These parameters give {R_decay=:.2e}; {R_GJ_field=:.2e},"\
	 + f" R_GJ_tumor={R_GJ_field/GJ_power:.2e}")

    # We already instantiated voltage-gated GJs in zombie_sims_metastasis().

    # Tumors overexpress GJs. We use GJ_power to implement the overexpression.
    # Type-1-power GJs simply get denser when [OA]>>[OB] in a cell; type-2 still
    # do so, but slowly (working with a species 'm' and GJ_tau)
    GJgate = tumor_power_GJ_gate (GJ_power, GJ_NCMM)

    # The "M gate" tracks how long a cell has been a tumor (i.e., how long it
    # has had [OA]>[OB]. The M gate maintains [M] between 0 and 1; it sets M_inf
    # to roughly 1 when [OA]>[OB] and roughly 0 elsewise, and moves to M_inf
    # with the time constant GJ_M_tau.
    if (GJ_tau > 0):
        GJ_Mgate  = M_gate_tumorness (GJ_tau)

    # A tumor has two possible voltage strategies. It can hyperpolarize itself
    # so that its cells have the same Vmem as the healthy tissue, with no
    # voltage gradients anywhere. This is the default, and needs no code to set
    # up.
    # The other strategy is to keep the tumor depolarized, but allow it to
    # instruct nearby healthy cells to depolarize also. To implement this, we
    # first instantiate set_Vmem_from_conc() to set Vmem in each cell.
    # The "neighbor=True" is what allows a tumor cell to depolarize its neighbor
    # cells (but only the higher-numbered neighbors).
    if ((algo=="depol") or (algo=="dumb")):
        Vm_gate = set_Vmem_from_conc("OA","OB", neighbor=(algo=="depol"))

    # Create the tumor, simulate once with Kd=0 to get reasonable concentrations
    # (not just 100 or 0), and again to get results. The tumor size was set
    # above when we set the globals spot_dX, and will be built by double_sim().
    # Sim #1 of double_sim() will also set cells to their steady-state [m].
    (t_shots, cc_shots) = double_sim (Kd_field)
    (OA,OB,m) = ions ('OA','OB','m')

    # All done... now do a bit of debug printing/plotting.
    #eplt.plot_worm_times ('m', t_shots, cc_shots, 10)
    #eplt.plot_worm_times ('OA', t_shots, cc_shots, 10)
    #eplt.plot_worm (cc_shots[-1], ['OA','OB'])
    #eplt.plot_worm (cc_shots[-1], 'V')
    #for idx in range(len(t_shots)):
    for idx in [len(t_shots)-1]:
        t = f"t={t_shots[idx]:.4f}"
        #print (t)
        #dump_3D (cc_shots[idx],"dOA:OB",title=f"{t}: metastasis sim OA-OB")
        #dump_3D (cc_shots[idx],"m", title=f"{t}: metastasis sim [m]")
        #dump_3D (cc_shots[idx],"Vmem",  title=f"{t}: metastasis sim Vmem")
        #dump_3D (cc_shots[idx],"KionC", title=f"{t}: metastasis sim KionC")
        np.set_printoptions (formatter={'float':'{:5.1f}'.format},linewidth=170)
        #dump_GJ_3D(cc_shots[idx], title=f"{t}: metastasis sim")
        #print()

    # Decide whether the tumor grew or shrank, and print a nice summary of it.
    end_size = n_ones()
    spot_size = spot_dX * spot_dY * spot_dZ
    field_size = g_maxX * g_maxY * g_maxZ
    direc = "grew" if (end_size>spot_size) else \
            "shrunk" if (end_size<spot_size) else "constanted"
    print(f"{n}. {title}. {title_tumor} final at {title2} {direc} from"\
         +f" {spot_size} cells ({spot_size/field_size:.0%}) to"\
         +f" {end_size} cells ({end_size/field_size:.0%}).\n")

    # Cleanup by removing the GJ-gating function we just added.
    sim.GJ_gates.remove (GJgate)	# The tumor_power gate.
    if (GJ_tau > 0):
        sim.GD_gates.remove (GJ_Mgate)	# The gen/decay gate that makes M.
    if ((algo=="depol") or (algo=="dumb")):
        sim.IC_gates.remove (Vm_gate)	# The gate that sets Vmem.
    assert len(sim.IC_gates)==0		# We removed the ligand-gated IC gate
    assert len(sim.GJ_gates)==1		# Always keep the Vmem-gated GJs
    return (direc=="grew")		# Metas_compare_algorithms() uses status

#######################################################
# Metastasis sims to quantify how well depol and hypol work vs. dumb.
# The "best" algorithm is the one that needs the smallest value of GJ_power to
# succeed at metastasis. So:
#     for each combination of Kd_field, GJ_tau, GJ_NCMM, ...
#	  1. Do a tumor-size search. For each of the algorithms dumb, depol &
#	     hypol, it finds the range of "acceptable" tumor sizes where power=1
#	     fails to metastasize, but power=100 succeeds. The goal of this is
#	     to find a common set of conditions for...
#	  2. Based on the results of the tumor-size search, try to find a single
#	     tumor size where, for all three algorithms, power=1 doesn't
#	     metastasize but power=100 does. Then do a power search:
#		for each algorithm dumb, depol and hypol
#		    do a binary search to find the borderline power that
#		    separates failure from success
#		print the borderline power value for each algorithm.
#######################################################

def metas_compare_algorithms (p,dimX,dimY,dimZ,NR,range,title,diag):
    print (f"Running metastasis-algorithm-comparison sims for {NR=}")

    # Build the network. Tumor_GJ_metast() wants us to always instantiate
    # voltage-gated GJs; but decide itself on all other gatings.
    setup_basic_network(p, NR, dimX,dimY,dimZ, allow_diag=diag,
                        do_gate=True,do_drive_Vmem=False)

    # Set up A=B=50. This is common for all sims we do here. And since A and B
    # don't diffuse, this setup need only be done once.
    seed3D_A_B (50,50)

    n=0
    for Kd_field in np.geomspace (1e-16,6e-12,8):	# healthy-tissue Kd
     if (Kd_field==6e-12):
      sim.GP.time_step = .00005		# min time step for explicit integration
     for GJ_tau in [.01,.1,1,10]:# Tau(s) for powering up a tumor GJ
      for GJ_NCMM in [.5,1]:	# How much big GJs attach to neighbors
          if (n>=range[0]) and (n<range[1]):
              (P100_minP,P1_maxF)=search_tsize(n,Kd_field,GJ_tau,GJ_NCMM,title)
              search_power (n,Kd_field,GJ_tau,GJ_NCMM,title,P1_maxF,P100_minP)
          n += 1

# Tumor-size search. For each of the algorithms dumb, depol & hypol, it finds
# the range of "acceptable" tumor sizes where power=1 fails to metastasize, but
# power=100 succeeds.
# Specifically, given the current Kd_field, GJ_tau and GJ_NCMM:
#    for each of the algorithms "dumb", "depol" and "hypol"...
#	 P1_maxF[algo]   = the maximum tumor size where power=1 fails
#	 P100_minP[algo] = the minimum tumor size where power=100 passes
#    Return the two dictionaries P1_maxF{} and P100_minP{}.
# For any of the three algorithms, the potential range of tsize where the
# caller can do a power search is then [P1_maxF[algo], P100_minP[algo]].
# The binary search is implemented by using binS_tsize(), which is just a
# wrapper around tumor_GJ_metast().
def search_tsize (n, Kd_field, GJ_tau, GJ_NCMM, title):
    # wrap tumor_GJ_metast() as a single-param function for binSearch().
    def binS_tsize (tsize):
       grow=tumor_GJ_metast(algo,Kd_field,GJ_power,GJ_NCMM,GJ_tau,tsize,title,n)
       return (grow)

    P1_maxF={}; P100_minP={}
    title2 = f"{title} {Kd_field=:.2e}, {GJ_tau=}, {GJ_NCMM=}"
    for algo in ["dumb", "depol", "hypol"]:
        print (f"{n}. Allowable-tsize analysis for {algo} {title2}")

        # max tumor size where power=1 fails
        print (f"Find the max tSize where {algo} power=1 fails")
        GJ_power=1; maxF=0
        if (not binS_tsize(1)):	# if it fails at tSize=1...
            print ("Tsize=1 fails, so find the max shrinking-tsize @power=1")
            (maxF,_) = binSearch(binS_tsize,1,20,f"{algo} tsize @power=1",
				 integer=True)
            # BAD_SEARCH means we failed at both tSize=0 and 20. Good!
            maxF = 20 if (maxF==BAD_SEARCH) else maxF
        P1_maxF[algo] = maxF

        # min tumor size where power=100 passes
        print (f"Find the min tSize where {algo} power=100 passes")
        GJ_power=100; minP=100
        if (binS_tsize(20)):	# passed at tSize=20
            print(f"{algo} Tsize=20 grows: find min tsize to grow @pow=100")
            (_,minP)=binSearch(binS_tsize,1,20,f"{algo} tsize @power=100",
                               integer=True)
            # BAD_SEARCH means we grew at both tSize=1 and 20. That's good!
            minP = 0 if (minP==BAD_SEARCH) else minP
        P100_minP[algo] = minP
        print (f"{algo} => [pass100,fail1] range is [{minP},{maxF}]")
    return (P100_minP, P1_maxF)

# Power-size search. We're given dictionaries 
#	 P1_maxF[algo]   = the maximum tumor size where power=1 fails
#	 P100_minP[algo] = the minimum tumor size where power=100 passes
# We then do
#   if (there a single tumor size where, for all three algorithms, power=1
#		fails to metastasize but power=100 succeeds)
#	for each algorithm dumb, depol and hypol
#	    do a binary search to find the borderline power that separates
#		failure from success
#	    print the borderline power value
# If there is no single tumor size that works for all three algorithms, then
# try to find one that works for two of the three and do the same
# search/compare/print.
# The binary searches are implemented by using binS_power(), which is just a
# wrapper around tumor_GJ_metast().
def search_power (n,Kd_field,GJ_tau,GJ_NCMM,title,P1_maxF,P100_minP):
    def binS_power (power):
      grow=tumor_GJ_metast(algo,Kd_field,power,GJ_NCMM,GJ_tau,avg_tsize,title,n)
      return (grow)

    # What range of tumor sizes work for pair/triplets of dumb/depol/hypol?
    # First, the max tsize where all algos fail for power=1.
    # Then,  the min tsize where all algos pass for power=100.
    P1_dumb_dep   = min (P1_maxF["dumb"], P1_maxF["depol"])	# All P1s fail
    P1_dumb_hyp   = min (P1_maxF["dumb"], P1_maxF["hypol"])	# All P1s fail
    P1_triple     = min (P1_dumb_hyp, P1_dumb_dep)		# All P1s fail
    P100_dumb_dep = max (P100_minP["dumb"],P100_minP["depol"])#All p100 pass
    P100_dumb_hyp = max (P100_minP["dumb"],P100_minP["hypol"])#All p100 pass
    P100_triple   = max (P100_dumb_hyp, P100_dumb_dep)	  #All p100 pass
    title2 = f"{title} {Kd_field=:.2e}, {GJ_tau=}, {GJ_NCMM=}"

    # Is there a tumor size where all algos fail at P1 and pass at P100?
    if (P100_triple <= P1_triple):
        avg_tsize = int ((P100_triple + P1_triple)/2)
        print (f"Using {avg_tsize=} for all three algos")
        # binary search on power for each algorithm, all at avg_tsize
        algo="dumb"
        (_,Pdumb)=binSearch (binS_power, 1,100, why=f"dumb power @{avg_tsize=}")
        algo="depol"
        (_,Pdepol)=binSearch (binS_power,1,100,why=f"depol power @{avg_tsize=}")
        algo="hypol"
        (_,Phypol)=binSearch (binS_power,1,100,why=f"hypol power @{avg_tsize=}")
        print (f"{n}. Final comparison: {Pdumb=:.4},{Pdepol=:.4},{Phypol=:.4}",
               f"at {title2}")
    elif (P100_dumb_dep <= P1_dumb_dep):
        avg_tsize = int ((P100_dumb_dep + P1_dumb_dep)/2)
        print (f"Using {avg_tsize=} for dumb and depol")
        # binary search for each algorithm, all at avg_tsize
        algo="dumb"
        (_,Pdumb)=binSearch (binS_power, 1,100, why=f"dumb power @{avg_tsize=}")
        algo="depol"
        (_,Pdepol)=binSearch (binS_power,1,100,why=f"depol power @{avg_tsize=}")
        print (f"{n}. Final comparison: {Pdumb=:.4},{Pdepol=:.4} at {title2}")
    elif (P100_dumb_hyp <= P1_dumb_hyp):
        avg_tsize = int ((P100_dumb_hyp + P1_dumb_hyp)/2)
        print (f"Using {avg_tsize=} for dumb and hypol")
        # binary search for each algorithm, all at avg_tsize
        algo="dumb"
        (_,Pdumb)=binSearch (binS_power, 1,100, why=f"dumb power @{avg_tsize=}")
        algo="hypol"
        (_,Phypol)=binSearch (binS_power,1,100,why=f"hypol power @{avg_tsize=}")
        print (f"{n}. Final comparison: {Pdumb=:.4},{Phypol=:.4} at {title2}")
    else:
        print (f"{n}. Final: no useful tumor-size choices at {title2}\n")

#######################################################
# Higher-level utility functions.
#######################################################

# Simulate the existing network with our new Kd. Return True iff the network
# is "fixed" -- roughly, rows[0:first_OA_0) are 1 and [first_OA_0:end] are 0.
# We reset sim.cc_cells[] to the same state to start every sim, for which we
# use setup_OA_OB_Kd(), which depends on the globals first_OA_0,
# spot_dX,spot_dY,spot_dZ and g_spot_at_middle.
# But we don't instantiate or remove any gatings.
def sim2_is_fixed (Kd, do_sim1=True, do_sim2=True):
    t_shots, cc_shots = double_sim (Kd, do_sim1, do_sim2)
    #dump_3D   (cc_shots[-1], what_to_show="OA:OB", title="pre-sim OA,OB")
    #dump_GJ_3D(cc_shots[-1], title="pre-sim GJs")

    # Are all the mistakes that we initialized fixed?
    fixed = spot_is_fixed()
    print (f"\tKd={Kd:.4g} => fixed if {fixed}\n")
    return (fixed)

# Initialize all relevant ions to the usual pre-sim values, including a tumor
# spot (for which we use setup_OA_OB_Kd()).
# Then sim twice:
# - once with Kd=0 to get reasonable initial values for OA and OB.
# - again with the real Kd, and return the results.
def double_sim (Kd, do_sim1=True, do_sim2=True):
    (OA,OB,Na,K,Cl) = ions ('OA','OB','Na','K','Cl')

    # Insert the spot, and isolate all cells for the initial sim.
    setup_OA_OB_Kd (0)	# The parameter is Kd, so we're setting Kd=0 here.
    
    # Re-initialize the various charged ions that can migrate in the last sim.
    if (do_sim1):
        sim.GP.sim_dump_interval=5000	# e.g., every 10 time steps
        sim.GP.sim_long_dump_interval=5000	# e.g., every 10 time steps

        sim.cc_cells[Na,:]=10
        sim.cc_cells[K,:] =125
        sim.cc_cells[Cl,:]=55
        t_shots, cc_shots = sim.sim (100)
        #dump_3D (cc_shots[-1], what_to_show="OA:OB", title="disconnected")
        #dump_GJ_3D(cc_shots[-1], title="disconnected GJs")

    # Now connect the cells and finish the sim.
    if (do_sim2):
        sim.GJ_diffusion[[OA,OB]] = Kd
        t_shots, cc_shots = sim.sim (1000)

    return (t_shots, cc_shots)

# - Set the background of [OA] and [OB]. OA is 100 in cells with Z in
#   [0,first_OA_0), and 0 in cells with Z >= first_OA_0.
# - Insert a spot of size (spot_dX,spot_dY,spot_dZ) either at the left edge, or
#   in the middle, of the field (according to g_spot_at_middle). If
#   spot_dX/dY/dZ are all 0, then we don't add a spot.
# - We depend on the global first_OA_0 to know how to build the background, and
#   the globals spot_dX,spot_dY,spot_dZ,g_spot_at_middle to build the spot.
def setup_OA_OB_Kd (Kd):
    global first_OA_0, spot_dX,spot_dY,spot_dZ,g_spot_at_middle, cc3d
    assert first_OA_0>=0, "Illegal negative first_OA_0"
    (OA,OB) = ions ('OA','OB')
    # First set the background. 
    cc3D[OA,:,:,:first_OA_0] = kVMax
    cc3D[OA,:,:,first_OA_0:] = 0

    # Put in the spot. It can go at the origin or in the middle of the field.
    (Xmin, Ymin, Zmin) = (0,0,0)
    if (g_spot_at_middle):
        Xmin = int ((g_maxX-spot_dX)/2)
        Ymin = int ((g_maxY-spot_dY)/2)
        Zmin = int ((g_maxZ-spot_dZ)/2)
    assert (Xmin>=0) and (Ymin>=0) and (Zmin>=0)
    assert (Xmin+spot_dX<=g_maxX) and (Ymin+spot_dY<=g_maxY) \
				  and (Zmin+spot_dZ<=g_maxZ)
    spot_color = kVMax if (cc3D[OA,Xmin,Ymin,Zmin]==0) else 0
    cc3D[OA,Xmin:Xmin+spot_dX,Ymin:Ymin+spot_dY,Zmin:Zmin+spot_dZ] = spot_color
    cc3D[OB,:,:,:] = kVMax - cc3D[OA,:,:,:]
    sim.GJ_diffusion[[OA,OB]] = Kd

#######################################################

GJ_lib.setup_GJ_model ('C45', 'C45', "mafe_cervera", -.1, .1, .002)

g_spot_at_middle = False			# ... and location.
(spot_dX,spot_dY,spot_dZ) = (6,6,6)		# The tumor size...

#set_Vmem_class1()
#char_latch()
#predict_bistab()
#small_sig_gain()
# python zombie.py measure_spread 1 25 25 3,5,10
# python zombie.py zombie_sims_border_sharpness 1 25 25 3,5,10
# python zombie.py surround_flip_sims 0 0 0
# python zombie.py zombie_tumor_isolate 25 25 25 3
# python zombie.py zombie_sims_metastasis 1 40 40 5
# python zombie.py metas_compare_algorithms 1 40 40 5
# python zombie.py char_NOR2 0 0 0 3,5,10

#test_Thev()

# Just grab command-line args and call exhaustive().
# The command-line format is
#	python zombie.py test_name field_x,y,z NR-vals [min max]
# Exhaustive() will then sequence through the given NR values and call the
# given function.
# Min and max are optional, and they are just passed through exhaustive() to go
# to the given function, which typically runs lots of numbered tests and now
# only runs those numbered in [min,max).
def main ():
    import sys
    if (len (sys.argv) <= 1):
       print("Format: python zombie.py test_name field_x,y,z NR-vals [min max]")
       quit()
    
    (func_name,field_x,y,z,NR_vals) = sys.argv[1:6]
    range=f"[{sys.argv[6]},{sys.argv[7]}]" if(len(sys.argv)>6) else "[0,100000]"
    cmd = f"exhaustive ({func_name}, {field_x}, {y}, {z}, [{NR_vals}], {range})"
    print (f"*{cmd}*")
    eval (cmd)
main()
