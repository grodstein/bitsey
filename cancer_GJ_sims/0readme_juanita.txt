These first sims all use QSS_test() in main.py.

Experiment #1 is our first set of sims, saved in sim_juanita1_*.png. It looks at
how far Vmem bounces back after we swing the ion channels to hyperpolarize a
depolarized tumor cell.

    # Initialize cells so that they reach SS almost immediately. These values
    # are for the default values of 1e-18 for all channels, nicely depolarized
    # at about -13mV.
    sim.cc_cells[Na,:] = 5.88984
    sim.cc_cells[K,:]  = 160.6111
    sim.cc_cells[Cl,:] = 86.50495

    # Set up 5 cells; they start out with the conditions above, and then they
    # multiply the K leak channel by the given amount. Only cells[2,3,4] were
    # interesting.
    QSS_check (K, [1,5,10,100,200])
    Remember that the transition starts at t=300.
    Na rose, peaked and fell; K and Cl fell smoothly.
	Cell		2		3		4
	G_K multiplier	10		100		200
	Peak Vm,time	-53.3@t=780	-83.5@t=405	-87.6@t=300.5
	Final Vm	-50.0		-70.1		-72.1
	Vm recovery	 3.3		 13.4		 14.5
	Peak [Na],time	7.5@t=1127	8.7@t=910	8.9@890
	Final [Na]	7.38		8.21		8.29
	Final [K]	95.8		82.4		81.6
	Final [Cl]	23.2		10.7		 9.9
	

Experiment #2: we'll look at what happens when we change the pump strength.
We're still using QSS_check(), but commenting out various lines of code.
    # The default is cATP=1.5 mol/m3. All channels at 1e-18 => Vmem=-13mV
    # The Vnernst are from Cenv: [Na]=145, [K]=5, [Cl]=140
    # cATP	Vmem	Na	K	Cl	Vn,Na	Vn,K	Vn,Cl
    # 0		-6.5mV	184	  6	110	-6	- 5	- 6
    # .01	-8mV	143	 41	104	.4	-55	- 8
    # .02	-9.3mV	103	 76	99	9	-71	- 9
    # .05	-12.5mV	19	149	88	53	-89	-12
    # .1	-12.7mV	11	156	87	68	-90	-12
    # 1.5	-12.9mV	5.9	161	86.5	84	-91	-13
    # 3		-12.8mV	5.6	161	86.5	85	-91	-13
    # 30	-12.8mV	5.3	161	86.4	87	-91	-13

    So as expected, when the pumps are off we reach a true equilibrium with
    Na, K and Cl all at the same VNernst. Clearly turning the pumps off
    completely won't reproduce Juanita's behavior -- the only way the cells
    could have been hyperpolarized at all is if one of the VNernst values were
    still highly negative. What if we pick [ATP]=.02? That still leaves us at
    Vmem=-12 mV, but gives us some room to hyperpolarize.

Experiment #3: just like experiment #1, but with pATP=.02. This gives us some
very nice results! They're in sim_juanita3_*.
    
    QSS_check (K, [1,5,10,100,200])
    Remember that the transition starts at t=300.
    Na rose, peaked and fell; K and Cl fell smoothly.
	Cell		2		3		4
	K multiplier	10		100		200
	Peak Vm,time	-34@t=303	-63@t=302	-68@t=302
	Final Vm	-15.5		-16.1		-16.2
	Vm recovery	 18.5		 47		 52
	Final [Na]	141		147		147
	Final [K]	17		9.9		 9.6
	Final [Cl]	78		77		 76
	Min [Cl],time	76@t=9580	72@t=4510	72@4160
