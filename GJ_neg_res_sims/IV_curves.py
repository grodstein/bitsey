import numpy as np, matplotlib.pyplot as plt
import GJ_lib
import sys; sys.path.append ("..")	# To get the usual Bitsey files
import sim, eplot as eplt, edebug as edb
#import pdb; pdb.set_trace()

# Basic setup (setting integration parameters, dumping intervals,
# calling init_big_arrays() and making GJs, etc).
# We keep the default ion-channel diffusion constants, but overrule
# sim.GJ_diffusion.
# Setup_common() is used by all the sims in this file.
# In this file, we always have n_cells=2 and extra_ions=[].
def setup_common (p, time_step, adaptive, sim_dump_interval,
		  sim_long_dump_interval, extra_ions, n_cells):
    # Parameters for numerical integration and printout.
    p.time_step = time_step
    p.adaptive_timestep = adaptive	# So that the params above get used.
    p.use_implicit = False		# Use explicit integration
    # Change from seconds to number of time_steps
    p.sim_dump_interval = int(sim_dump_interval/time_step)
    p.sim_long_dump_interval = int(sim_long_dump_interval/time_step)

    n_GJs = n_cells-1
    #n_GJs = 0
    sim.init_big_arrays (n_cells, n_GJs, p, extra_ions)

    # Straight-line GJ connections along the 1D field. All are simple & ungated.
    sim.GJ_connects['to']   = range(n_GJs)
    sim.GJ_connects['from'] = sim.GJ_connects['to'] + 1
    p.GJ_len = 15e-9	# Change the gap-junction length from 100nm to 15nm.
    GJ_scale = .05
    sim.GJ_diffusion *= GJ_scale	# Default is 1e-18 for Na,K,Cl.

    # Straight-line GJ connections along the 1D field. All are simple & ungated.
    #sim.GJ_connects['to']  = range(n_GJs)
    #sim.GJ_connects['from']    = sim.GJ_connects['from'] + 1

    p.GJ_len = 15e-9	# Change the gap-junction length from 100nm to 15nm.

# Given a desired Vmem, return the Na, K and Cl ion-channel multipliers that
# produce it in SS. It's used only by set_G_IC().
V_ss  = np.array ((-.059,-.050,-.039,-.031,-.014,-.002, .004, .012, .021, .032, .042))
Na_ss = np.array ((  1,  1,  1,  1,  1, 2, 4,  5, 10, 30, 30))
K_ss  = np.array (( 20, 10,  5,  3,  1, 1, 1,  1,  1,  1, .5))
Cl_ss = np.array ((  1,  1,  1,  1,  1, 1, 1,  1,  1,  1, .5))
def channels (Vmem):
    Na = np.interp (Vmem, V_ss, Na_ss)
    K  = np.interp (Vmem, V_ss,  K_ss)
    Cl = np.interp (Vmem, V_ss, Cl_ss)
    return (Na, K, Cl)

# Given a deltaV (in Volts) measured from cell 1 to cell 0, return the (Na,K,Cl)
# IC conductance multipliers for cells 0 and 1. We decide how to set Vmem for
# each of the two cells to create the desired deltaV, then use channels() to
# get the ion-channel conductances for both cells.
def set_G_IC (deltaV):
    dV = abs (deltaV)
    assert dV<.101, "Cannot build a Vmem > 101mV"
    if (dV < .059):	# the most negative Vmem we can set.
        mid = -.059/2	# both cells negative, use our -59mV negative range
    else:
        mid = (.042-.059)/2	# must have a positive cell.
    V1 = (mid+dV)/2
    V0 = (mid-dV)/2
    print (f"Breaking {dV} into {V1} and {V0}")
    if (deltaV<0):
        V1,V0 = V0,V1
    return (channels(V0) + channels(V1))	# return a 6-item tuple.

# The first set of sims to measure an IV curve.
# Prep with a sim to SS at default conditions. Then, lots of sims with both
# going to SS (so really, the initial sim is mostly irrelevant).
# and 200K seconds for SS. 
def IV_curves_SS():
    p = sim.Params()
    setup_common (p, .005, True, 5000,20000, [], n_cells=2)

    # One sim to get consistent initial conditions for the remaining sims.
    t_shots, cc_shots = sim.sim (200000)

    IV_curves (200000, cc_shots[-1])

# Next sim: just like the first, but now the mass of sims are only to QSS and
# not SS.
def IV_curves_QSS():
    p = sim.Params()
    setup_common (p, .005, True, 5000,20000, [], n_cells=2)

    # One sim to get consistent initial conditions for the remaining sims.
    t_shots, cc_shots = sim.sim (200000)

    IV_curves (10, cc_shots[-1])

# This QSS sim uses different initial conditions than the one above -- it starts
# with SS sims that have each of the two cells at a very different SS voltage.
# Then the main course is sweeps in QSS.
def IV_curves_QSS2(deltaV):
    p = sim.Params()
    setup_common (p, .005, True, 5000,20000, [], n_cells=2)
    Na=sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']
    (mNa0,mK0,mCl0,mNa1,mK1,mCl1) = set_G_IC (deltaV)
    sim.Dm_array [Na,0] *= mNa0
    sim.Dm_array [K, 0] *= mK0
    sim.Dm_array [Cl,0] *= mCl0

    sim.Dm_array [Na,1] *= mNa1
    sim.Dm_array [K, 1] *= mK1
    sim.Dm_array [Cl,1] *= mCl1

    t_shots, cc_shots = sim.sim (200000)

    IV_curves (10, cc_shots[-1])

# Like the above, but now we use unequal GJ conductances.
# So 100mV deltaV, Na and Cl at normal G_GJ but no K conductance.
def IV_curves_QSS3(deltaV):
    p = sim.Params()
    setup_common (p, .005, True, 5000,20000, [], n_cells=2)
    Na=sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']
    (mNa0,mK0,mCl0,mNa1,mK1,mCl1) = set_G_IC (deltaV)
    sim.Dm_array [Na,0] *= mNa0
    sim.Dm_array [K, 0] *= mK0
    sim.Dm_array [Cl,0] *= mCl0

    sim.Dm_array [Na,1] *= mNa1
    sim.Dm_array [K, 1] *= mK1
    sim.Dm_array [Cl,1] *= mCl1
    sim.GJ_diffusion.T[:,0:3] = (1e-18,0,1e-18)
    t_shots, cc_shots = sim.sim (200000)
    IV_curves (10, cc_shots[-1], (1e-18,0,1e-18))

# Just like QSS3, but zero out the Cl rather than K GJ flux.
def IV_curves_QSS4(deltaV):
    p = sim.Params()
    setup_common (p, .005, True, 5000,20000, [], n_cells=2)
    Na=sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']
    (mNa0,mK0,mCl0,mNa1,mK1,mCl1) = set_G_IC (deltaV)
    sim.Dm_array [Na,0] *= mNa0
    sim.Dm_array [K, 0] *= mK0
    sim.Dm_array [Cl,0] *= mCl0

    sim.Dm_array [Na,1] *= mNa1
    sim.Dm_array [K, 1] *= mK1
    sim.Dm_array [Cl,1] *= mCl1
    sim.GJ_diffusion.T[:,0:3] = (1e-18,1e-18,0)
    t_shots, cc_shots = sim.sim (200000)
    IV_curves (10, cc_shots[-1], (1e-18,1e-18,0))

# This function is used by IV_curves_SS(), IV_curves_QSS(), etc.
# It goes through a wide range of deltaV values. For each...
#	setup a new two-cell network with our desired GJ size
#	set the IC conductances to hopefully create that deltaV
#	sim & see what we get.
# The caller tailors our sims with parameters:
#	end_time is the simulation end time (so the caller can get us to do
#		either QSS or SS)
#	cc[] lets us initialize all of our sims
#	GJ_diff can either be None, or a 3-tuple of values that we use to scale
#		our Na, K and Cl GJ conductances respectively.
def IV_curves (end_time, cc, GJ_diff=None):
    #for deltaV in np.arange (.005,.006,.005):
    for deltaV in np.arange (-.1,.101,.005):
        p = sim.Params()
        setup_common (p, .005, True, 5000,20000, [], n_cells=2)
        if (GJ_diff!=None):
            sim.GJ_diffusion.T[:,0:3] = GJ_diff
        GJ_lib.GJ_gate()	# Instantiate a gate for the GJs
        Na=sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']

        # set Na and K ion-channel conductances in both cells
        (mNa0,mK0,mCl0,mNa1,mK1,mCl1) = set_G_IC (deltaV)
        sim.Dm_array [Na,0] *= mNa0
        sim.Dm_array [K, 0] *= mK0
        sim.Dm_array [Cl,0] *= mCl0

        sim.Dm_array [Na,1] *= mNa1
        sim.Dm_array [K, 1] *= mK1
        sim.Dm_array [Cl,1] *= mCl1

        sim.cc_cells = cc
        t_shots, cc_shots = sim.sim (end_time)
        post_IV_curves (p, t_shots, cc_shots, deltaV, end_time)

def post_IV_curves (p, t_shots, cc_shots, deltaV, end_time):
    edb.dump(end_time, sim.cc_cells, edb.Units.mol_per_m2s, True)
    np.set_printoptions (formatter={'float': '{:.0f}'.format})
    Vm = sim.compute_Vm (sim.cc_cells)
    print (f"Goal deltaV={1000*deltaV:.1f}mV -> final Vmem={1000*Vm}mV, delta={1000*Vm[1]-1000*Vm[0]:.0f}mV")
    #eplt.plot_Vmem (t_shots, sim.cc_cells, title=f"Vmem for deltaV={deltaV}")

    # We're printing:
    #	GJ drift currents (pre-gating, mol/m2s, with Cl reversed for simplicity)
    #	GJ diffusion currents (pre-gating) in mol/m2s.
    #	gating value
    # Get the gap-junction Norton-equivalent circuits for all ions at once.
    deltaV_GJ = (Vm[sim.GJ_connects['from']]-Vm[sim.GJ_connects['to']]) #[n_GJs]
    (GJ_Ith, GJ_Gth) = sim.GJ_norton(deltaV_GJ)	# Both are [n_ions,n_GJs].
    np.set_printoptions (formatter={'float': '{:.2g}'.format})
    GJ_drift = GJ_Gth * deltaV_GJ
    #import pdb; pdb.set_trace()
    f = sim.GJ_gates[0].func (sim.cc_cells, Vm, deltaV_GJ, 100000)
    I_total = (GJ_drift + GJ_Ith) * f
    I_total = I_total[0,0] + I_total[1,0] - I_total[2,0]
    print (f'Final pre-gate GJ diff={GJ_Ith[0:3].T}, drift={GJ_drift[0:3].T}, GJ_gate={f}, net={I_total:.3g}')
    #edb.dump_gating(100000, sim.cc_cells[-1])

# A temporary test used to see how ion concentrations change with Vmem.
def test1 ():
    p = sim.Params()
    p.time_step = .005
    p.adaptive_timestep = True	# So that the params above get used.
    p.use_implicit = False	# Use explicit integration
    # Change from seconds to number of time_steps
    p.sim_dump_interval = int(10000/p.time_step)
    p.sim_long_dump_interval = int(10000/p.time_step)

    num_cells=4
    n_GJs = 0
    sim.init_big_arrays (num_cells, n_GJs, p)
    p.use_implicit = False	# Use the implicit integrator

    t_shots, cc_shots = sim.sim (100000)
    eplt.plot_Vmem (t_shots, cc_shots)

    # By default, we have D_Na = D_K = 1e-18
    Na = sim.ion_i['Na']; K = sim.ion_i['K']
    sim.Dm_array[Na,0]= 20.0e-18	# +28 mV (reversal potential of Na+)
    sim.Dm_array[Na,1]= 10.0e-18	# +21 mV
    sim.Dm_array[K,2] =  10.0e-18	#
    sim.Dm_array[K,3] =  20e-18	# -28mV

    t_shots, cc_shots = sim.sim (100000)
    eplt.plot_Vmem (t_shots, cc_shots)
    eplt.plot_ion  (t_shots, cc_shots, 'Na')
    eplt.plot_ion  (t_shots, cc_shots, 'K')
    eplt.plot_ion  (t_shots, cc_shots, 'Cl')


GJ_lib.setup_GJ_model ('C45', 'C40', -.1, .1, .002)
IV_curves_SS()
#IV_curves_QSS()
#IV_curves_QSS2(.1)
#IV_curves_QSS3(.1)
#IV_curves_QSS4(.1)
#test1 ()
