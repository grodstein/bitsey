The simulations described in this file use IV_curves.py, with results in
IV_curves_results/. They try to build a GJ I-V curve that shows negative
resistance.

DELTA_V SIMULATIONS
The first set of sims uses an older (i.e., no longer in GJ_sys.py) version of
IV_curves() to just give us good values for Vmem in QSS for various values of
ion-channel conductance.  The sims run for just a few seconds, just long enough
to settle to a QSS value. Note that with the Bitsey default ion-channel values
(1e-18 everywhere) taken to SS, VnNa=72mV, K=-86mV and Cl=-25mV. The numbers
here are the multipliers for the ion-channel conductances (relative to the
default 1e-18).
	0,.1,.1 -> bad idea
	0,.5,.5 -> -162
	0,.8,.8 -> -112
	0,1,1 -> -96
	0,2,1  -> -95
	0,5,1 -> -92
	.5,0,0 -> -66		@t=6
	.6,0,0 -> -53		@t=6
	1,0,0 -> -20		@t=4
	1.5,0,0 -> ?
	2,0,0 ->  12		@t=2
	5,0,0 -> 42
	10,0,0 -> 55
	20,0,0 -> 63

      A few themes in these numbers:
      - Obviously, making Na bigger gives more positive Vmem, and K or Cl
	bigger makes Vmem more negative.
      - To get Vmem below -86mV, you set Na=0 and also set K and Cl low (so as
	to allow the pumps to have maximum relative effect).

Next, the same sims but in SS (to t=100K seconds)
	1,20,1 -> -59mV
	1,10,1 -> -50mV
	1,5,1 -> -39mV
	1,4,1 -> -36mV
	1,3,1 -> -31mV
	1,2,1 -> -24mV
	1,1,1 -> -14mV
	2,1,1 -> -2mV
	2.2,1,1 -> -.5mV
	2.3,1,1 -> .2mV
	3,1,1 -> -2mV
	4,1,1 ->  4mV
	5,1,1 ->  12mV
	10,1,1 -> 21mV
	20,1,1 -> 28mV
	30,1,1 -> 32mV
	30,.8,.8 -> 35mV
	30,.7,.7 -> 37mV
	30,.5,.5 -> 42mV

We used those numbers to write the function channels(Vmem); given a Vmem, it
comes up with the Na, K and Cl relative conductances to achieve that Vmem in SS.

Next, set_G_IC(deltaV) takes a deltaV between two cells, and somewhat
arbitrarily comes up with a Vmem for each cell that gives the requested deltaV.
It calls channels() twice to give the pair of Na,K,Cl relative conductances
producing deltaV. I then ran 41 sims with set_deltaV() (every 5mV between -100
and +100mV) to show that it works pretty well; the actual simulation deltaV is
pretty close to the requested deltaV, as well as being monotonic and reasonably
linear (test_deltaV_ss.txt).

IV_CURVE SIMULATIONS FOR C31/C31
The next set of experiments tries to actually build a negative-resistance
IV curve. The code is in the functions IV_curves_*(); the result data files are
in results/, and the plotting code is in results/IV_curves_plot.py.
All of them use C31/C31.
1. IV_curves_31_31_SS.txt uses that model in SS to get simple IV data.
   It first shows that the gating is working correctly: a plot of the actual
   gate vs. deltaV shows the usual gating shape. But, as both expected and
   feared, it also shows that the gating is producing only very minor areas of
   negative resistance (from about 88:95mV and -88:-95mV). As long as we wait
   for SS, the changes in applied deltaV produce changes in intracellular ion
   concentrations. And the bigger the deltaV, the bigger the delta in
   concentrations -- which means bigger drift and diffusion currents.
   The data also shows that drift flux is *far* bigger than diffusion flux, as
   expected. So the wider negative-R area from diffusion flux doesn't
   materialize.
   And, drift flux is *not* linear in Vmem; rather, it gets nonlinearly larger
   (i.e., G gets smaller) as |Vmem| gets larger. This means that the net flux
   winds up pretty much being linear in V, with little negative resistance.
   Finally, most of the drift current is from K rather than Cl; this is just
   because the direction of ion flow is always from an area of low [Cl] to an
   area of high [Cl], with the reverse being true for K.
2. IV_curves_31_31_QSS.txt first goes to SS with all IC conductances at the
   default 1e-18. It then does lots of small sims in QSS (i.e., the sims only
   last for 10 seconds from the SS endpoint. Now our negative-resistance area is
   a bit  more pronounced, from 65:98mV and -61:-99mV.
   Since concentrations don't really change, the more-ions-at-more-positive-Vmem
   effect doesn't happen; GJ drift current is quite linear in deltaV. That's
   what helps us.
   As expected, the diffusion current is still very small -- both cells start
   out at exactly the same concentrations, and a short 10s sim doesn't allow
   those concentrations to change much.
3. IV_curves_31_31_QSS2.txt tries to get a larger negative-resistance region by
   creating more diffusion current. Bottom line -- it fails at that (though it
   does still have a region of 77:99mV and -99:-134mV, which is similar to the
   original QSS experiment just above).
   Our plan was to start with both cells in SS -- but in *different* SS
   voltages, with deltaV=100mV. Then run the IV curves in QSS. So the idea is
   that while the cells can swing their voltages very quickly in QSS, their
   macro-scale ion concentations won't change much, and we will have massive
   diffusion currents.
   Good idea -- but it doesn't work. We've already argued that if all GJ
   diffusion constants are equal, then diffusion fluxes must sum to 0; this
   sim shows that to be exactly true. There are large diffusion fluxes for K and
   for Cl, but they almost exactly cancel, leaving drift flux once more far
   outweighing diffusion flux. And since ion concentations once more increase
   with more positive Vmem, then as usual we have drift flux not really showing
   much negative resistance.
4. IV_curves_31_31_QSS3.txt starts like IV_curves_31_31_QSS2 (i.e., the two
   cells at very different SS Vmem values), but now makes the GJ diffusion rates
   for different ions unequal. Specifically, it cuts off K GJ fluxes. Indeed, we
   now have substantial net diffusion fluxes. The result is weird: the deltaV>0
   negative-R area vanishes, but we still have -65:-99mV.
   Another oddity: with diffusion now being nonzero, the IV curve now no longer
   intersects the origin: deltaV=0 does not imply I=0. In fact, the net flux is
   *always* negative, even for deltaV>>0! The diffusion flux is large, is near-
   constant across all deltaV (as expected), and is larger in magnitude than the
   drift flux at all deltaV > -24mV. A bit more detail: the Na flux is near
   zero, the K flux is of course zero, and the Cl flux is the constant negative.
   This is why the deltaV>0 negative-R area vanished: the constant negative
   flux, combined with the gating function approaching zero as deltaV>50mV,
   resulted in flux that got closer to zero *and hence more positive*, which
   hurt rather than helped. But it did help in the deltaV<0 area :-).
5. IV_curves_31_31_QSS4.txt is similar, but zeroes the Cl rather than K GJ flux.
   No surprise: we now get a nice negative-R region from 65:88mV, but none in
   the deltaV<0 region. Everything is essentially flipped from QSS3.

IV_CURVE SIMULATIONS FOR C45/C40
1. IV_curves_45_40_SS.txt is the usual SS sim. The IV curve is fairly flat; the
   gating function is monotonically decreasing, but as usual the ion
   concentrations are mostly monotonically increasing, and they cancel out.
   Essentially this is zero conductance (an open circuit)
   There are some areas of minor negative-R; 45-56mV and 62-75mV.
   And as usual, there's almost no diffusion flux, and the drift is mostly K.
2. IV_curves_45_40_QSS.txt is the usual QSS sim (SS with all conductances at
   1e-18, then short QSS sims). It's much the same as the SS sim, as expected.
   But the negative-R region is wider: from 37-82mV.
3. IV_curves_45_40_QSS2.txt, again, has each cell in a different SS region
   before doing QSS sims. Now the negative-R region is 39-81mV, but otherwise
   pretty similar to QSS.txt.
4. IV_curves_45_40_QSS3.txt cuts off K fluxes. Essentially no negative-R region.
5. IV_curves_45_40_QSS4.txt cuts off Cl fluxes. Now we have a *very* significant
   negative-R region covering most of the entire deltaV>0 space. Even the
   -56:0mV region is slightly negative-R.
