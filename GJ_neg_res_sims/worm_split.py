import numpy as np

import sys; sys.path.append ("..")	# To get the usual Bitsey files
import sim, worm, GJ_lib
import eplot as eplt, edebug as edb
#import matplotlib.pyplot as plt
#import pdb; pdb.set_trace()

############################################################
# C
############################################################

# Start with a worm and do the usual startup sims to SS; it should go to TH.
# Then shut off one GJ in the middle of the worm and re-sim; it should go to
# THTH. Finally, restore that middle GJ and, since it effectively stays off due
# to the high deltaV across it, the worm stays at THTH.
def GJ_split_basic (W, idx):
    print (f"Worm #{idx} . Worm={W}")
    sim.GP.use_implicit = True
    OK = worm.sim_start (W, 2000000, only_TH=True, resim=False)
    if (not OK):
        return

    cc_list, min_cells, max_cells = push_both_ways (W, idx)
    print (f"Min-max range={min_cells}-{max_cells}; next is re-sim of {max_cells}-cell worm")
    assert len(cc_list)==max_cells-min_cells+1

    # Sim the final worm again.
    W.PD['num_cells'] = max_cells
    W.setup_Bitsey_network()
    sim.GP.use_implicit = True
    sim.cc_cells = cc_list[-1].copy()

    OK = W.sim (200000) and W.shape()=="TH"
    if (not OK):
        print ("Huh? Resim failed.")
        return
    print (f"Sanity resim of {max_cells}-cell worm is still TH")

    M=sim.ion_i['M']
    cc = W.cc_shots[-1].copy()
    cc_list[-1] = cc.copy()
    print (f"After resim, worm {W} M={cc[M]}")

    # GJ #g has g+1 cells on its left and N-(g+1) on its right.
    for n_cells in range (2*min_cells, max_cells+1):
        for left in range (min_cells,n_cells-min_cells):
            if (cc[M,left-1] < cc[M,0]):   # Make sure we have a chance of the
                continue		   # left half becoming TH and not HT.

            print (f"Trying split {max_cells}->{left}+{n_cells-left}")
            try_worm_split (W,idx, cc_list[n_cells-min_cells],left,n_cells-left)

def try_worm_split (W, idx, cc, n_cells_left, n_cells_rite, seed=False):
    # If we're running with a different-sized worm than before, then rebuild.
    # The big arrays in sim would all be the wrong size otherwise.

    if (n_cells_left + n_cells_rite != W.PD['num_cells']):
        print ("Building new worm with correct number of cells")
        W.PD['num_cells'] = n_cells_left + n_cells_rite
        W.setup_Bitsey_network()
        sim.GP.use_implicit = True

    # Cut off the desired GJ.
    assert (n_cells_left+n_cells_rite == W.PD['num_cells'])
    g = n_cells_left-1	# which GJ to cut
    me = f"Worm #{idx}/{n_cells_left}*{n_cells_rite}"
    print (f"\nKilling GJ[{g}] to make {me}")
    save_GJ = sim.GJ_diffusion [:,g].copy()	# So we can restore it later.
    sim.GJ_diffusion [:,g] = 0			# And, yes, kill it.

    # Sim in our killed-GJ state so we hopefully xform into two small worms.
    sim.cc_cells = cc.copy()
    M=sim.ion_i['M']
    print (f"{me}: simming without GJ[{g}]...before sim M=",
           cc[M,0:g+1], "*", cc[M,g+1:])
    OK = W.sim (200000)

    # Restore the missing GJ.
    sim.GJ_diffusion [:,g] = save_GJ

    if (OK == None):
        print(f"{me} fail3: no-GJ[{g}] sim wanted THTH but did not finish")
        return		# Try the next GJ location

    # And did we in fact become two small worms?
    print (f"Without GJ[{g}]: M=", sim.cc_cells[M,0:g+1], "**",
           sim.cc_cells[M,g+1:])
    #eplt.plot_worm_times ('M', W.t_shots, W.cc_shots, 6,0,.5)
    Vm = sim.compute_Vm(W.cc_shots[-1])
    deltaV_mV = (Vm[g+1]-Vm[g])*1000
    print (f"Without GJ#{g}, deltaVm across it is {Vm[g]*1000:.2f}->"
           f"{Vm[g+1]*1000:.2f}={deltaV_mV:.2f}mV")
    shape = W.shape(OK)
    if (shape != "THTH"):
        print(f"{me} fail3: no-GJ[{g}] sim wanted THTH but got {shape}")
        return		# Try the next GJ location
    #eplt.plot_worm_times ('M', W.t_shots, W.cc_shots, 6,0,.5)

    # We restored the missing GJ. Now sim again; will the THTH stick?
    print (f"{me}: simming with GJ[{g}] restored...")
    OK = W.sim (200000)
    Vm = sim.compute_Vm(W.cc_shots[-1])
    deltaV_new_mV = (Vm[g+1]-Vm[g])*1000
    shape = W.shape(OK)
    if (not OK) or (shape != "THTH"):
        print(f"{me} fail4: restored GJ[{g}] & THTH went to {shape}."
              f" DeltaV went from {deltaV_mV:.2f}"
              f" (G={GJ_lib.eval_GJ(deltaV_mV/1000):.3f})"
              f" to {deltaV_new_mV:.2f}mV"
	      f" (G={GJ_lib.eval_GJ(deltaV_new_mV/1000):.3f})")
        #eplt.plot_worm_times ('M', W.t_shots, W.cc_shots, 6,0,.1)
        #import pdb; pdb.set_trace()
    else:
        print (f"{me} killing GJ[{g}] success!")
        print (f"{me} successful final shape {sim.cc_cells[M,0:g+1]}**"
	       f"{sim.cc_cells[M,g+1:]}."
               f" DeltaV went from {deltaV_mV:.2g} to {deltaV_new_mV:.2f}mV")

    Vm = sim.compute_Vm(W.cc_shots[-1])
    print (f"Final deltaVm across GJ#{g} is {Vm[g]*1000:.2f}->"
           f"{Vm[g+1]*1000:.2f}={(Vm[g+1]-Vm[g])*1000:.2f}mV")
    #edb.dump (0, W.cc_shots[-1], units=edb.Units.mol_per_m3s, long=True)
    #eplt.plot_worm_times ('M', W.t_shots, W.cc_shots, 6,0,.1)



# For a given worm: push its length shorter and longer and see how far
# it can go.
def check_ncells_range (W, idx):
    print (f"Worm #{idx} . Worm={W}")
    sim.GP.use_implicit = True
    OK = worm.sim_start (W, 2000000, only_TH=True, resim=False)
    cc_save = W.cc_shots[-1].copy()
    #eplt.plot_worm_times ('M', W.t_shots, W.cc_shots, 6,0,.1)
    if (not OK):
        return

    cc_list, min_cells, max_cells = push_both_ways (W, idx)
    print (f"Push range: worm #{idx} was TH from {min_cells}-{max_cells} cells,"
           f" or {max_cells/min_cells:.2f}x for GJ_scale={W.PD['GJ_scale']}")
    print (f"Push range={max_cells/min_cells:.2f} for #{idx} {W}")

# Given a worm, see how far we can increase and decrease its size (always
# keeping the same parameter set). Stop when we're no longer TH.
# Return the cc_list of results of all successful worm sizes.
def push_both_ways (W, idx):
    # Increase n_cells as much as we can & save the final worm.
    n_cells_save = W.PD['num_cells']
    cc_save = W.cc_shots[-1].copy()
    max_cc_list = push_n_cells (W, idx, n_cells_save, cc_save, 1)
    min_cc_list = push_n_cells (W, idx, n_cells_save, cc_save, -1)
    cc_list = min_cc_list[::-1] + [cc_save] + max_cc_list
    min_cells = cc_list[0].shape[1]
    max_cells = cc_list[-1].shape[1]
    return (cc_list, min_cells, max_cells)

# We have already done a successful sim with 'n_cells' cells that resulted in
# 'cc'. Try to extend it by repetitively scaling n_cells by 'factor'.
# Every time we extend the worm size by one and remain at TH, we save the
# successful results and eventually return cc_list (the list of all successful
# results).
# We get called twice: once to extend 20 cells upwards and once downwards.
def push_n_cells (W, idx, n_cells, cc, direction):
    print (f"Worm #{idx}: starting to increment n_cells by {direction}...")
    cc_list=[]	# Accumulate & return final results for each good worm size.
    for iter in range(500):
        n_cells += direction
        me = f"worm {idx}/{n_cells}c/i{iter}"
        W.PD['num_cells'] = n_cells
        W.setup_Bitsey_network()
        sim.GP.use_implicit = True

        # spread from the results of our prev iteration using classic spread_M.
        print (f"Push {me} trying classic spread_M()")
        worm.cc_init = cc.copy()	  # This is what worm.spread_M will use.
        worm.Vm_init = sim.compute_Vm(cc) # Note cc is from the last iteration.
        worm.spread_M ("TH")	# From whichever profile we chose last iteration
        print ("After classic spread_M():", sim.cc_cells)
        print (" giving Vm=", sim.compute_Vm(sim.cc_cells))
        OK1 = W.sim (200000) and W.shape()=="TH"
        cc1 = W.cc_shots[-1].copy()
        goodness1 = f_good (W, OK1, me, "classic spread_M()")

        # spread with classic spread_M(), but now from the largest monotonic
        # piece of whatever we chose last iteration.
        (start,len) = worm.biggest_monotonic_seg (cc)
        worm.cc_init = cc[:,start:start+len].copy()
        worm.Vm_init = sim.compute_Vm(worm.cc_init)
        worm.spread_M ("TH")
        OK2 = W.sim (200000) and W.shape()=="TH"
        cc2 = W.cc_shots[-1].copy()
        goodness2 = f_good (W, OK2, me, "largest-monotonic")

        # "spread" from by simple interpolation of all ions.
        old_n_cells = cc.shape[1]
        for ion in range(cc.shape[0]):	# for each ion...
            sim.cc_cells[ion]=np.interp(np.arange(n_cells)*old_n_cells/n_cells,
	 				np.arange(old_n_cells), cc[ion])
        OK3 = W.sim (200000) and W.shape()=="TH"
        cc3 = W.cc_shots[-1].copy()
        goodness3 = f_good (W, OK3, me, "interp-all-ions")

        if (not OK1) and (not OK2) and (not OK3):
            print (f'Push {me} all failed, so done with {"grow" if direction==1 else "shrink"}ing worm')
            return (cc_list)

        if (max (goodness1,goodness2,goodness3) \
		> min(goodness1,goodness2,goodness3)+.01):
            print (f"Push {me} goodnesses differ: {goodness1},{goodness2},{goodness3}")
        if ((goodness1>=goodness2) and (goodness1>=goodness3)):
            print (f"Push {me} using standard spread")
            cc = cc1
        elif ((goodness2>=goodness1) and (goodness2>=goodness3)):
            print (f"Push {me} using biggest-mono spread")
            cc = cc2
        elif ((goodness3>=goodness1) and (goodness3>=goodness2)):
            print (f"Push {me} using all-ions prop spread")
            cc = cc3
        else:
            assert False, "all bad options!"
        cc_list.append (cc.copy())	# Save/return whichever good sim we used
        print (f"Push {me} succeeded!")

# How good did a simulation attempt do?
#	Failed sim (gradient<30mV or crash) -> -100
#	30mV < gradient < 50mV -> gradient in mV
#	gradient > 50mV -> 50 + length of the longest TH piece of the sim output
def f_good (W, OK, me, spread_str):
    gradient = W.gradient()
    (start,len) = worm.biggest_monotonic_seg (W.cc_shots[-1])
    if (not OK):
        goodness = -100
    elif (gradient < .050):
        goodness = gradient*1000
    else:
        goodness = 50+len
    print (f"Push {me} spread with {spread_str} => OK={OK},"
           f" gradient={gradient} {W.shape()}, mono_len={len} =>"
           f" goodness={goodness}")
    return (goodness)

def plot_pushing (cc_list):
    import matplotlib.pyplot as plt
    plt.figure()
    n_worms = len(cc_list)
    M=sim.ion_i['M']
    if (0):
      for w in range(n_worms):
        plt.plot (cc_list[w][M], label=f"W #{w}")
      plt.title ("[M] profile")
      plt.legend (loc="upper right")
      plt.show()

      plt.figure()
      for w in range(n_worms):
        plt.plot (cc_list[w][M,1:]-cc_list[w][M,:-1], label=f"W #{w}")
      plt.title ("delta-[M] profile")
      plt.legend (loc="upper right")
      plt.show()

    for w in range(n_worms):
        Vm = sim.compute_Vm (cc_list[w])
        plt.plot (Vm, label=f"{len(Vm)} cells")
    plt.title ("Vm profile")
    plt.legend (loc="upper right")
    plt.show()

    plt.figure()
    for w in range(n_worms):
        Vm = sim.compute_Vm (cc_list[w])
        plt.plot (Vm[1:]-Vm[:-1],
		  label=f"{len(Vm)} cells")
    plt.title ("delta-Vm profile")
    plt.legend (loc="upper right")
    plt.show()


# If a worm, after splitting, cannot create a gradient, then try again with:
#	- slightly smaller GJ_scale
#	- slightly slower GD_tau
# If a worm, after splitting, becomes HTHT, HTH or THT rather than THTH, try
#	- slightly slower GJ_diffusion of M
#	- slightly slower GD_tau
def GJ_split_munge_HTHT (W, idx):
    print (f"Munging HTHT worm #{idx}: {W}")
    # This gives us 9*5*5=225 versions of each of the 28 worms, so 6300 worms!
    #worm.munge_exhaustive (W, GJ_split_basic, [['GD_tau',1,9,9],
    #					       ['GJ_scale',.6,1,5],
    #					       ['diff_GJ_M',.6,1,5]])
    worm.exhaustive (GJ_split_basic, range(10000), W, 
		GD_tau=W.PD['GD_tau']*np.linspace(1,9,9),
		GJ_scale=W.PD['GJ_scale']*np.linspace(.6,1,5),
		diff_GJ_M=W.PD['diff_GJ_M']*np.linspace(.6,1,5))


func = GJ_split_basic; rang=range(2500)
#func = GJ_split_munge_HTHT; rang = [355,370,371,499,515,547,563,599,603,615,619,647,663,707,711,747,751,763,767,791,807,811,934,951,1039,1055,1098,1099]
gate_GJs=[['C45','C45']]
gate_GJs=[['C37y','C37y']]
#gate_GJs=[None]
#func = check_ncells_range; rang=range(5000)

worm.exhaustive (func, rang, num_cells=[20],
		 GJ_scale=[1,3,5,6,7,10,15], kM=[.3,.5,.8], N=[2,5,8],
		 M_valence = [-1,-2,-3,-4], GD_tau = [10,100,1000,2000,3000,4000,6000,8000],
		 gate_GJs=gate_GJs) 
