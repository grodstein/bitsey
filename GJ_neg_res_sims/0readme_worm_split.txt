The worm_split experiments try to:
	- start with a simple TH worm.
	- completely block a GJ in the middle of the worm, separating it into
	  two smaller worms. These start out (and hopefully remain) both as TH;
	  but one has a very low [M] and the other has a high [M].
	- after a bit of time, hopefully (for some worm parameter set) they both
	  remodel to have normal [M] ranges.
	- when you reinstate the central GJ, then nothing changes -- the GJ has
	  a large deltaV across it and is thus essentially off anyway.
We also did some bonus experiments:
	- play with worm.sanitize() to see if we could improve it.
        - test the hypothesis that the GJ IV curve makes TH stick over a wider
	  range of worm sizes than an ungated GJ. The idea is that when you
	  increase the number of worm cells, each has a smaller deltaV across
	  it. A gated GJ will thus increase the GJ conductance, which
          effectively makes the worm behave as if it were smaller. It didn't
          work -- the deltaV values are too small to have any practical effect.

First two worm-split experiments with simple linear GJs
	The first try for a split used an earlier version of GJ_split_basic().
        Specifically, worm.exhaustive() calls GJ_split_basic() for 1152
        different sets of 20-cell worm params:
		(GJ_scale=[.4,.6,.8,1,1.5,2,3,4])*(kM=.3,.5,.8)*(N=2,5,8)
				* (valenceM=-1,-2,-3,-4)*(GD_tau=1,10,100,1000)
	We used ungated GJs, and so we cannot expect it to maintain the split
	worms after we reconnect the central GJ. But hopefully it can at least
	form two small worms. The results are in results1.txt.
	None of the worms work! We got 734 worms that couldn't do an initial
	sim, 259 that could but couldn't hold an initial TH spread, and 157 that
	could hold an initial TH. Of these 157, none of them split to THTH:
	* 129 split to no gradient. These mostly (though not all) have GJ_scale
	  quite high (3 or 4), and the two smaller worms cannot maintain a
	  gradient.
	* 12 to HTHT
	* 14 to HTH and 2 to THT

	The next try takes the 28 worms that split to HTHT, HTH or THT. For each
	of these, we try to "munge" the parameters just a bit, by tweaking the
	9*5*5=225 combinations
		(GD_tau = 1x,2x,...9x)*(GJ_scale=.6x,.7x,...1x)
		*(diff_GJ_M=.6x,.7x,...1x)
	For these tweaks, eventually found two worms that held THTH after
	splitting. Of course, they still couldn't hold this after restoring the
	middle GJ, since they don't use nonlinear GJs.
	The results are in results2.txt. We implemented this by having
	worm.exhaustive() call GJ_split_munge_HTHT(), which in turn calls
	worm.munge_exhaustive().

Detour: try to improve sanitize().
	I don't trust worm.sanitize(); I've been seeing worms that initially
	sim fine, and then get sanitized, and then won't converge any more. Is
	there a better way to set up initial spreads before simulation? Perhaps
	we can predict what the post-sim profiles of the various ions will be,
	and then spread them to that profile to the subsequent sim is easy.
	I collected data in char_curves_data.txt on all worms that initially
	simmed to a TH. Then some characterization, by plotting them with
	parse.py:
	* I looked at [M] vs cell number for 111 worms. It was all over the map:
	  first, in terms of the min & max [M] and second, in terms of where the
	  graph reached half its final height.
	* I tried segregating the plots by GJ_scale; so each GJ_scale value had
	  its M-vs-cell plot colored a different color. It didn't help much.
	  I looked at the gradient of [M] and GJ_scale didn't predict it well.
	  Then I scaled all the plots so that they all went from [M]=0 to 1;
	  GJ_scale didn't predict where the 50% cell was.
	* Segregating by GD_tau: higher tau gave generally larger gradients, and
	  to the knee being closer to the head (higher cell number). These were
	  done with plot(), using either color_GJ() or color_GD_tau().
	* Segregate by GJ_scale, and then plot groups of worms that are all TH,
	  and all have all the same parameters except for GJ_scale. As
	  expected, *almost* always the worms with larger GJ_scale have a
	  smaller gradient. But not always! (Perhaps some sims didn't quite
	  finish?). Then do it again with GD_tau; now *every* worm with larger
	  GD_tau has more gradient. These were done with plot_pieces().

Baby step, looking for parameter sets that support both small and large worms:
      - Part of the reason our first efforts had so little success is that it's
	not so easy to find parameter sets that support a wide range of worm
        sizes as TH. When we split a worm into two, not only must the parent
        N-cell worm hold TH, but both N/2-cell worms also most. So only a worm
        that can support a 2-to-1 range of worm sizes are feasible.
      - Can we find parameter sets that create worms that sim to TH over a wide
	of worm size (i.e., # of cells)? The first try is with the "C26"
        experiments.
      -	This uses worm_split.py check_ncells_range(). It tests the full gamut of
	1152 worms just as above (and, in later sims, many more than that). For
	any that sim to TH, it then sees how much higher and lower we can push
	num_cells (from 20) until it's no longer a good TH. The goals: first, if
	we can find a param set that works over at least a 2x range of cell
	counts, then perhaps we can build the worm splitting into two mini-worms
	as desired. Second, hopefully we can see that GJs allow worms to work
	over a wider range of cell sizes.
      - These sims have one more trick: as we bump the cell count up or down, we
	try each sim at the new num_cells with three different ways to do the
	initial TH spread, to see which works best. The results of that, in
	general, are that method 1 (the traditional spread) and method 3 (a
	simple interpolation of all ions) work pretty much equally well. But
	method 2 (modifying sanitize() to find the longest stretch of an
	existing run where all ions concentrations were monotonic) didn't work
	well.
      - For C26-C26 sims, I have push3_C26(a,b,c).txt. We got up to range=4.15,
	which means that a single parameter combo worked for C26c worm #482 all
	the way from 14 to 58 cells (and 58/14=4.15). Some general observations:
	* Higher (i.e., slower) GD_tau pretty much always improved the range
	  ratio. It slows down generation, and generation leads to worm
	  collapse. And since our experimental method doesn't try to regenerate
	  from fragments, there's actually almost no need for generation or
	  decay at all. Specifically, higher GD_tau allows worms to grow to
	  longer lengths, since those longer lengths are where the normal
	  feedback process takes longest to establish a gradient.
	* Higher GJ_scale also helped. As long as it doesn't collapse the
	  gradient, it prevents wild swings that generate multi-head patterns.
      - Now look at worm #482 in more detail. Use plot_pushing() to get sequence
	of Vm plots for all the worms from 14 to 58 cells (plot_C26_482.png),
	and also to plot delta-Vm (plot_C26_482d.png). This shows that as the
	worm gets longer, it always has a relatively flat stage near at the
	tail, then a steep body slope, and a low-slope rolloff at the head; the
	flat tail-end part gets much longer as the worm does, and the steep part
	gets slightly longer (but keeps its slope). And the maximum deltaV
	ranges from 4mV for the 14-cell worm to 7mV for the 58-cell worm.
      - Unfortunately -- C26 has a very sharp knee at about 95mV, so it's
	effectively acting as ungated in the region we care about! And even
	C45 has only a very minor change in gating from 4mV-7mV. So I think
	the goal of getting a wider cell range for gated GJs won't work.

Splitting a worm into two pieces via GJs -- step 1
      - Our first will be using "perfect" GJs. For any parameter set, we'll
	start by seeing how long and short we can make the worm and still have
        it hold TH. Next, we'll try every reasonable place to split the worm;
        knock out one GJ and see if we now get two smaller TH worms. We won't
        try restoring the GJ yet.
      - Success! Results are in push3_C45.txt and push3_C26.txt.
	Let's take a few cases from push3_C45.txt. Some cases with the largest
	range are
	    #1127: 13-56 cells(4.31x), GJ_scale=6, kM=.8, N=8, qM=-1, GD_tau=8K
	    #1415: 14-60 cells(4.29x), GJ_scale=7, kM=.8, N=8, qM=-1, GD_tau=8K
	However, all of these cases have N=8, which won't be conducive to
	regrowing a half-worm fragment. The best N=2 cases are
	    #319: 10-30 cells(3x), GJ_scale=3, kM=0.3, N=2, qM=-4, GD_tau=8K
	    #607: 13-39 cells(3x), GJ_scale=5, kM=0.3, N=2, qM=-4, GD_tau=8K
	    #1175:16-48 cells(3x), GJ_scale=7, kM=0.3, N=2, qM=-3, GD_tau=8K

The full monty
      - Now we'll try to add on the final step: restoring the killed GJ, and
        hope that it has so little conductivity (due to the high deltaV across
        it) that the THTH pattern remains.
      - The first attempts used all of our normal GJs, and none worked. The
        results are in worm_split_basic_C45.txt.
        Basically, even with a GJ such as C45 or C37, which has a large ratio
        of G_on/G_off, it wasn't enough -- restoring the GJ, even with a large
        deltaV across it, collapsed the THTH pattern back to just TH.
      - I got it to work by making a new near-ideal GJ, with parameters
		Gmin=1e-6, Gmax=1, V0=.022, A=300
        The results are in worm_split_basic_C37y1.txt; there are 27 successes.
      - Then, 21 successes in worm_split_basic_C37y2.txt with
		Gmin=1e-5, Gmax=1, V0=.022, A=300
      - Then, 18 successes in worm_split_basic_C37y3.txt with
		Gmin=1e-4, Gmax=1, V0=.022, A=300
      - Then, 2 successes in worm_split_basic_C37y4.txt with
		Gmin=1e-3, Gmax=1, V0=.022, A=300
      - Then changed to only trying to split the longest worm we had found to
	trying smaller worms, too. In worm_split_basic_C37y5.txt, that made no
        difference.
