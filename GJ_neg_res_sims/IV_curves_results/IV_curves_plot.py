import re
import matplotlib.pyplot as plt
import numpy as np
#import pdb; pdb.set_trace()

# The diffusion fields (diff_Na, _K and _Cl) are in moles/m2s and don't include
#   gating. They *completely* ignore ion valence.
# The drift fields (drift_Na, _K, _Cl) are also in moles/m2s and don't include
#   gating. They include ion valence in the sense that valence influences
#   ion-flow drift direction, and in that higher valence means faster ion drift
#   velocity.
# But both diffusion and drift are still in units of moles/m2s rather than
# Coulombs/m2s; i.e., to get a charge flux we still have to multiply them by the
# appropriate ion charge.
# Both diffusion and drift are in the direction GJ.from->GJ.to, which for us is
# cell #1 to #0.
goal_deltaV=[]		# What deltaV we want to set (in mV)
actual_deltaV=[]	# What we actually get (in mV)
drift_Na=[];drift_K=[];drift_Cl=[]	# drift fluxes (mole/m2s) without gating
diff_Na=[]; diff_K=[]; diff_Cl=[]	# diff fluxes (mole/m2s) without gating
gate=[]			# GJ gating value (in [0,1]).
net=[]			# Net charge flux (mole/m2s plus per-ion charge scaling)

def read_file(fname):
    print ('Reading',fname)
    file = open (fname)
    #print ('goal,actual,drift0,drift1,drift2,diff0,diff1,diff2,gate,net')
    for line in file:
        line = line.strip()
        if (re.search ('Goal', line)):
            goal_line = line
        if (re.search ('Final', line)):
            process (goal_line, line)

# Goal deltaV=-100 -> final Vmem=[67 -58]mV, delta=-125mV
# Final pre-gate GJ diff=[[1.4e-10 0 3.8e-09]], drift=[[2.4e-09 0 -8.6e-09]], GJ_gate=[0.53], net=3.9ee-09

def process (line1, line2):
    global goal_deltaV, actual_deltaV, drift_Na,drift_K,drift_Cl, \
           diff_Na,diff_K,diff_Cl, gate,net

    match = re.search ('Goal deltaV=([0-9-+.e]+)',line1)
    assert match
    goal = match.group(1)
    goal_deltaV.append (float(goal))

    # Get the actual deltaV across the GJ
    match = re.search ('delta=([0-9-+]+)mV$',line1)
    assert match
    actual = match.group(1)
    actual_deltaV.append (float(actual))

    # Get the GJ diffusion current for each ion.
    match = re.search ('GJ diff=\[\[([0-9-+.e]+) ([0-9-+.e]+) ([0-9-+.e]+)\]\]',
                       line2)
    assert match
    diff_Na.append (float(match.group(1)))
    diff_K.append  (float(match.group(2)))
    diff_Cl.append (float(match.group(3)))

    # Get the GJ drift current for each ion.
    match = re.search ('drift=\[\[([0-9-+.e]+) ([0-9-+.e]+) ([0-9-+.e]+)\]\]',
                       line2)
    assert match
    drift_Na.append (float(match.group(1)))
    drift_K.append  (float(match.group(2)))
    drift_Cl.append (float(match.group(3)))

    # Get the GJ gating function.
    match = re.search ('GJ_gate=\[([0-9-+.e]+)\]', line2)
    assert match
    gate.append (float(match.group(1)))

    # Net current (diffusion + drift) across all ions summed.
    match = re.search ('net=([0-9-+.e]+)$', line2)
    assert match
    net.append (float(match.group(1)))

# Turn global lists into arrays
def make_arrays():
    global goal_deltaV, actual_deltaV, drift_Na,drift_K,drift_Cl, \
           diff_Na,diff_K,diff_Cl, gate,net

    goal_deltaV = np.array (goal_deltaV)
    actual_deltaV = np.array (actual_deltaV)
    drift_Na = np.array (drift_Na)
    drift_K = np.array (drift_K)
    drift_Cl = np.array (drift_Cl)
    diff_Na = np.array (diff_Na)
    diff_K = np.array (diff_K)
    diff_Cl = np.array (diff_Cl)
    gate = np.array (gate)
    net = np.array (net)

# Finish up a plot with a few matplotlib calls.
def finish ():
    #plt.title (title)
    plt.show()

def plotN (y_list, label_list):
    for i in range (len(y_list)):
        plt.plot (actual_deltaV, y_list[i], label=label_list[i])
    plt.xlabel ('actual deltaV(mV)')
    plt.legend (loc="upper right")
    # Add axes
    x_min,x_max = plt.gca().get_xlim()
    y_min,y_max = plt.gca().get_ylim()
    plt.plot ([0,0], [y_min,y_max], color='black', linewidth=2)
    plt.plot ([x_min,x_max], [0,0], color='black', linewidth=2)

    finish ()

def plot (y, ylabel):
    plt.plot (actual_deltaV, y)
    plt.xlabel ('actual deltaV(mV)')
    plt.ylabel (ylabel)
    finish ()
    

def do_plots():
    print ("Plotting...")
    #import pdb; pdb.set_trace()
    ungate=net/gate; gate_scale=gate*(ungate.max()-ungate.min())+ungate.min()
    plotN ((net, ungate,gate_scale),
           ('net gated flux','net ungated flux','scaled gate'))
    plotN ((diff_Na+diff_K-diff_Cl, drift_Na+drift_K-drift_Cl),
           ('net diffusion flux','net drift flux'))
    plotN ((diff_Na,diff_K,-diff_Cl),('diff Na','diff K','diff Cl'))
    plotN ((drift_Na,drift_K,-drift_Cl),('drift Na','drift K','drift Cl'))
    plotN ((diff_Na+drift_Na,diff_K+drift_K,-diff_Cl-drift_Cl),
           ('Na_net','K_net','Cl_net'))
    plotN ((diff_K,-diff_Cl,diff_K-diff_Cl),('diff_K','diff_Cl','sum'))
    quit()
    plot (goal_deltaV, 'goal_deltaV(mV)')
    plot (gate, 'gating value')

read_file ('IV_curves_45_40_QSS4.txt')
make_arrays()
do_plots()
