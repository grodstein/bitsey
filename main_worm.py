#!/usr/bin/env python3

# Copyright 2018 Alexis Pietak and Joel Grodstein
# See "LICENSE" for further details.

#    import pdb; pdb.set_trace()
import numpy as np, random, copy
import sim, eplot as eplt, edebug as edb, worm

############################################################
# Routines to support evolutionary algorithms.
############################################################
class EvWorm (worm.Worm):
  # Set some/all parameters randomly. For each parameter, either leave it
  # unchanged, or (with likelihood 'mut_frac') set it randomly. If we set it
  # randomly, then use self.rand() to do so.
  def set_params_random(self, mut_frac=1):
    # see __init__() above for more comments about what each param means
    self.rand ('num_cells', 5, 50, mut_frac, to_int=True)	# the obvious
    self.rand ('GJ_scale', .001, 10, mut_frac)	# how strong the GJs are
    self.rand ('GJ_len', 3e-9, 50e-9, mut_frac)	# GJ length
    self.rand ('kM', .1, 1, mut_frac)
    self.rand ('N', 1, 2.99, mut_frac, to_int=True) # for ion-channel Hill model
    self.rand ('diff_IC_K', 1e-18, 1e-17, mut_frac) # IC diff const for K
    self.rand ('diff_GJ_Na', 1e-18, 3e-17, mut_frac)	# GJ diff consts
    self.rand ('diff_GJ_K', 1e-19, 3e-17, mut_frac)
    self.rand ('diff_GJ_M', 1e-17, 1e-13, mut_frac)
    self.rand ('env_Na', 100, 200, mut_frac)	# environmental [Na], [K] and
    self.rand ('env_K', 3, 10, mut_frac)	# [P]; then choose [Cl] for
    self.rand ('env_P', 5, 15, mut_frac)	# charge neutrality.
    self.rand ('int_Na', 5, 15, mut_frac)	# ditto, but cell-internal.
    self.rand ('int_K', 100, 150, mut_frac)
    self.rand ('int_P', 80, 150, mut_frac)
    self.rand ('M_valence',-3.99,-1,mut_frac,to_int=True)     # pick -3,-2 or -1
    self.rand ('GD_tau', 1, 1000, mut_frac)	# time constant for M gen/decay
    self.rand ('stronger_at_end', 1, 10, mut_frac)
    self.rand ('scale_pumps', 0, 1.99, mut_frac, to_int=True) # pick 0 or 1

  # used only by set_params_random() just above.
  # Randomly decide (with likelihood 'mutation_frac') whether to change the
  # given parameter (vs. just leaving it untouched). If we change it, then set
  # it randomly within the range ['min','max']
  def rand (self, param, min, max, mutation_frac, to_int=False):
    if (random.uniform (0,1) < mutation_frac):
        self.PD[param] = random.uniform (min, max)
        if (to_int):
            self.PD[param] = int (self.PD[param])

  # Mate with a spouse; return a child. The child is a new object; both
  # parents are untouched.
  def mate (self, spouse):
      child = copy.deepcopy (self)	# Deep, so the dictionary is copied.
      for name in sorted (self.PD):
        if (name!='gate_GJs'): 	#JJJJ!!!!
          if (random.uniform (0,1) > 0.5):
              child.PD[name] = spouse.PD[name]
      return (child)

  # Mutate myself -- don't return anything, but I *do* get altered.
  def mutate (self):
      self.set_params_random (MUTATION_FRAC)

# There are 17 parameters that can mutate. So some odds:
# 0 mutations: 1 - .85^17 = 6%
# 1 mutation:  17 * .85^16 * .15 = 19%
# 2 mutations: 17*16/2 * .85^15 * .15^2 = 26%
# 3 mutations: 17*16*15/6 * .85^14 * .15^3 = 23%
MUTATION_FRAC = .4 #.15

############################################################
# EVOLUTION
############################################################
# What parameters should evolution play with and which should it not touch?
# We probably must play with these eight:
#	GJ_scale, kM, N, M_valence, GD_tau, stronger_at_end, diff_GJ_M, diff_IC_K
# These are probably pretty irrelevant
#	scale_pumps, int_Na, int_K, int_P
# num_cells should be a top-level parameter that only changes run to run.
# These are physical constants and cannot change (for GJs, unlike ICs, they do
#	*not* include the GJ cross-sectional area):
#	diff_GJ_Na, diff_GJ_K
# These seem unlikely to change env_Na, env_K, env_P
############################################################

# run_generation(pop, n_matings, n_mutations) runs one generation of a genetic
# algorithm. It:
#   - starts with the given population 'pop' (a Numpy array of Worm objects).
#   - makes n_matings calls to the function parent1.mate(parent2); each time
#     picking a random pair of parents from population to mate.
#   - calls parent.copy().mutate() n_mutations times; each time picking a
#     parent randomly from the population.
#   - after adding the new children from mate() and mutate() to the population,
#     prunes the population down to the original number of individuals, mostly
#     by preserving the fittest ones (but with some randomness).
#   - returns the resultant population, ordered by fitness (i.e., population[0]
#     is the most fit).
def run_generation (pop, n_matings, n_mutations):
    pop_size = pop.size # Remember the size, even as we add & cull new children
    print ("\n*** Running a generation with",pop_size,"individuals ***")
    print ("Incoming fitnesses: [",
           ",".join(["{:.2f}".format(p.fitness()) for p in pop]),"]")

    # Create an array of new_children. First, fill it in with the matings.
    # Each mating is two randomly-chosen parents.
    new_children = np.empty (n_matings+n_mutations, dtype=object)
    for i in range (n_matings):
        p1 = pop [random.randrange (0,pop_size)]
        p2 = pop [random.randrange (0,pop_size)]
        new_children[i] = p1.mate(p2)
        new_children[i].setup_Bitsey_network()
        sim.GP.use_implicit = True
        new_children[i].fitness()
        print ("Built mating worm",new_children[i],"\n")

    # Next, add the mutations (again, mutating random parents).
    for i in range (n_mutations):
        numb = random.randrange (0,pop_size)
        p = pop [random.randrange (0,pop_size)]
        child = copy.deepcopy (p)	# Deep, so the dictionary is copied.
        child.mutate()
        child.PD['num_cells']=5
        child.setup_Bitsey_network()
        sim.GP.use_implicit = True
        child.fitness()
        print ("Mutated #",numb,'=',p,'\n...to build mutation worm',child,"\n")
        print ("Built mutation worm",child,"\n")

        new_children[n_matings+i] = child

    # Add the new children to the population.
    print ("We created",len(new_children), "new children [",
           ",".join(["{:.3f}".format(c.fitness()) for c in new_children]),"]")
    pop = np.append (pop, new_children)
    print ("The entire population now has",pop.size,"individuals")

    # Sort the entire merged population by fitness. Remember that the new ones
    # had fitness evaluated as they were created.
    fit = np.array ([pop[i].fitness() for i in range(pop.size)])
    indices = np.argsort (fit)
    indices = indices[-1::-1]	# Reverse it, so the fittest is now first.
    pop = pop[indices]
    fit = fit[indices]
    fit -= fit[-1]  # offset everyone so the least fit has fitness=0
    print("Sorted fitnesses: [",
          ",".join(["{:.2f}".format(p.fitness()) for p in pop]),"]")
    fit = fit + .01	# Avoid divide-by-zero error if all have equal fitness.

    # Always keep the best 10 individuals
    new_pop = np.empty (pop_size, dtype=object)
    new_pop[0:10] = pop[0:10]

    # Pick 3 more completely randomly
    new_pop[10:13] = np.random.choice (pop[10:], 3, replace=False)

    # Pick the rest randomly, but weighted by fitness.
    sum = np.sum(fit[10:])
    probs = fit[10:] / sum
    new_pop[13:] = np.random.choice (pop[10:], pop_size-13,
                                     replace=False, p=probs)

    print("Outgoing fitnesses: [",
          ",".join(["{:.3f}".format(p.fitness()) for p in new_pop]),"]")
    return (new_pop)

# The top-level call for evolution.
def evolve (n_gen, pop_size, n_matings, n_mutations, seed):
    # Seed the random-number generator so that we get the same "random"
    # numbers every time :-)
    random.seed(seed)
    np.random.seed(seed)
    worm.set_default_spread_M()

    # First create a population of random individuals.
    pop = np.empty ((pop_size), dtype=object)
    for i in range (pop_size):
        pop[i] = EvWorm()
        pop[i].set_params_random ()
        pop[i].PD['full_body']= True;  pop[i].PD['do_gen_decay']= True
        pop[i].PD['VNChead_pres']=0;   pop[i].PD['VNCtail_pres']=0
        pop[i].PD['num_cells']=5

        pop[i].setup_Bitsey_network()	# Set up the network and, with the
        sim.GP.use_implicit = True
        pop[i].fitness ()		# network built, evaluate fitness.
        print (f"Built worm #{i} = {pop[i]}\n")

    print ("Built random population of",pop_size,"individuals")

    # Then run evolution.
    for i in range(n_gen):
        pop = run_generation (pop, n_matings, n_mutations)
        print ('After generation #',i,'/', n_gen, ', best=',pop[0].fitness())
        if (i%20>=0):
            print ("Generation=",i,", best=", pop[0])
            for W in pop:
              str= f"[Fitness={W.fitness():.2f}: shape={W.shape()}"
              for name in sorted (W.PD):
                  if (W.PD[name]!=None):
                      str = str + f"{name}={W.PD[name]:.3g}; "
              print(str+"]")

    print ("All done. Best=", pop[0])
    print ("The entire population:")
    for W in pop:
        print (W)

############################################################
# A few random functions taken from our hybrid-model paper for regression.
############################################################

# Run the functions
#	crypa
#	Joh19b
#	crypc
#	crypd
#	Fal19b
# Print all of the results.
def cryptic_etc (W, idx):
    sim.GP.use_implicit = True
    random.seed(0); np.random.seed(0)

    (crypa_good,crypa_msg) = crypa(W,idx)
    print ("Worm #{} crypa {}. Worm={}".format (idx, crypa_msg,W))
    #Joh19b_good = Joh19b(W, idx); quit()
    if (crypa_good):
        fit = W.fitness()
        Joh19b_good = Joh19b(W, idx);
        crypc_good = crypc(W, idx)
        crypd_good = crypd(W, idx)
        Fal19b_good = Fal19b(W, idx)
        print ("Worm #{}, {}: crypa={}, Joh19b={}, crypc={}, crypd={}\n".format(idx,W, crypa_good, Joh19b_good, crypc_good, crypd_good, Fal19b_good))

SIM_TIME = 20000000

# Building cryptics
# Verify an experiment in Fallons original cryptic-worm paper.
# Do sims to show that (with the current parameters) HT and HTH are stable, but
# that THT and HTHT are not. This shows that when we start with a GJ blocker
# (to get a complex pattern) and then regenerate in water, we wind up with only
# HT or HTH.
# Update this to use sim_start() instead!!!
def crypa(W, idx):
    import re
    print ("\nChecking crypa: we can build a cryptic and never get THT")
    worm.set_VNC_WT (W)				# Cryptics have a normal WT VNC

    # See if TH is stable. If not, then return -- nothing else is worth checking
    worm.spread_M ("TH")
    sim_results = W.sim (SIM_TIME)
    shape_TH = W.shape(sim_results)
    if (sim_results == None):
        return (False, "bad initial sim did not complete")
    if (sim_results == False):
        return (False, "bad initial sim NoGrd")

    # We converged. Save our stable cc_cells[] to help initialize future worms.
    worm.sanitize (sim.cc_cells)
    #return (True, "temporary shortcut")

    # If we turned into something like THT or HTH, then maybe the problem was
    # bad initialization. Try again with our nice new profile.
    if (len(shape_TH)>2):
        worm.spread_M ("TH")
        sim_results = W.sim (SIM_TIME)
        shape_TH = W.shape(sim_results)

    if (not sim_results or ((shape_TH != "TH") and (shape_TH != "HT"))):
        return (False, "bad TH->{}".format(idx, shape_TH))

    # See if HTH is stable (even with a WT VNC).
    worm.spread_M ("HTH")
    sim_results = W.sim (SIM_TIME)
    shape_HTH = W.shape(sim_results)

    # Hopefully show that THT is *not* stable.
    worm.spread_M ("THT")
    sim_results = W.sim (SIM_TIME)
    shape_THT = W.shape(sim_results)

    # And THTH is not stable either.
    worm.spread_M ("THTH")
    sim_results = W.sim (SIM_TIME)
    shape_THTH = W.shape(sim_results)

    msg = "results: TH->{}, HTH->{}, THT->{}, THTH->{}".format (shape_TH,
		shape_HTH, shape_THT, shape_THTH)

    # We're good if HTH is stable but neither of THT or THTH are.
    if (("HTH" == shape_HTH) and
        re.match ("(HT|TH|HTH|NoGrd)$", shape_THT) and
        re.match ("(HT|TH|HTH|NoGrd)$", shape_THTH)):
        return (True, "good " + msg)

    return (False, "bad " + msg)

def Joh19b(W, idx):
    print ("\nChecking Joh19b: Johanna's collision zone moves the Vmem tail")

    # Shortcut if there is no VNC tail pressure. In this case, we *should*
    # eventually discover that Joh19b doesn't work. But the shortcut saves some
    # time. And some cases are actually oscillatory (!), in which case the three
    # sims below might come out to different values and inadvertently pass.
    if (W.PD['VNCtail_pres']==0):
        print ("Joh19b bad by default, since VNCtail_pres=0")
        return (False)

    # Initialize a normal DH Vmem profile
    print ("Setting HTH Vmem profile in cc_cells[]")
    worm.spread_M ("HTH")

    N = W.PD['num_cells']
    step = max (round (N/20), 1)
    cz=round(N/2)
    OKs=[]; CZs=[]; tails=[]
    while (cz>=1):
        (OK, tail) = Joh19b_piece (W, cz)
        OKs.append (OK); CZs.append(cz); tails.append(tail)
        cz -= step

    print (OKs)
    print (CZs)
    print (tails)
    #print ("Joh19b results #{}: {}. {}".format(idx, "good" if good else "bad",
    #	   summary))
    return (True)

# Set the collision zone by applying VNC pressure at various places.
# Cz is the cell number of the VNC collision zone.
# Note that we do not call spread_M() at each new collision-zone location;
# instead we just restart the new sim with cc_cells[] from the last sim.
# This mirrors how the biology works, and probably enhances the odds of
# convergence after the first sim.
def Joh19b_piece (W, cz):
    print ("Checking Joh19b with collision zone at cell #{}".format(cz))
    worm.set_VNC_HTH (W, cz)		# Setup two-headed VNC pressure onto Vmem.

    if (not W.sim (SIM_TIME)):	# Failed sim or not enough gradient
        print ("Joh19b bad: unsuccessful sim for Joh19b simulation")
        return (False, 0)

    tail = M_min()        
    if (not (W.shape() == "HTH")):
        print ("Joh19b bad: worm became", W.shape(), "instead of HTH")
        return (False, 0)
    return (True, tail)

# Start with cryptic (i.e., HTH Vmem and WT VNC) middle segment.
# Hyperpolarize it and regrow -> all WT
# In fact, we run 5 sims with progressively less hyperpolarization and see
# what happens in each case.
# Current status: return True if at least 4/5 of the sims get WT, since we
# don't know what an in-the-field hyperpolarization really is.
def crypc(W, idx):
    print ("\nChecking crypc: hyperpolarize a cryptic -> always becomes WT")

    # And simulate the cryptic worm.
    worm.set_VNC_WT (W)			# Cryptics have a normal WT VNC
    worm.spread_M ("HTH")
    if (not W.sim (SIM_TIME)):
        print ("crypc: original cryptic sim was unsuccessful")
        return (False)
    state = sim.cc_cells.copy()
    Na=sim.ion_i['Na']
    original_Na = sim.Dm_array[Na,:].copy()

    # Hyperpolarize by (mostly) turning off Na channels. This works since
    # both Cl and K have negative Vnernst. On the first iteration, scale=0
    # and we completely kill all Na channels. Next we leave some partly on.
    # Specifically, we will set D_Na *= (random in [0,1) * scale), and use a
    # different random number for each cell.
    scales = [0,.05,.1,.1,.1]	# Scale=0 means D_Na=0.

    results = ""; n_good=0
    for i,scale in enumerate(scales):		# a few iterations...
        print ("{}; random scales in [0,{}]".format(i, scale))
        rand = np.random.random ((W.PD['num_cells'])) * scale	# in [0,.1]
        sim.Dm_array[Na,:] = sim.Dm_array[Na,:] * rand

        # Sim to get nicely hyperpolarized. If it fails to complete, then this
        # iteration was useless. However, it may or may not wind up with a
        # substantial gradient; either way is OK. Some of the hyperpolarization
        # sims wind up with a gradient -- but are already TH (even before
        # removing the hyperpolarizer)!
        sim.cc_cells = state.copy()
        res = W.sim (SIM_TIME)

        # Wash out the hyperpolarizer; to prepare for the next sim, or to
        # restore the original network if this sim has failed.
        sim.Dm_array[Na,:] = original_Na.copy()

        if (res==None):				# Sim did not finish...
            results = results + " DNF1 "	#   so mark that down, and don't
            continue				#   even try the washing-out sim

        # We've washed out the hyperpolarizer -- now sim some more.
        res = W.sim (SIM_TIME)
        if (res==None):				# Washed sim did not finish...
            results = results + " DNF2 "
        elif (res == False):			# Finished, but no gradient
            results = results + " noGrad "
        else:
            # Update count of how many sims result as WT.
            shape = W.shape()
            n_good = n_good + ((shape=="HT") or (shape=="TH"))
            results = results + " " + W.shape() + " "

    print ("Results crypc #{} (want WT) = {}".format(idx, results))
    return (n_good >= 4)

# Hyperpolarize the middle segment from a true HTH worm (not a cryptic).
# Desired result: 1/3 become WT, with the rest staying HTH.
# Show that only some of our randomly-initialized sims become WT,
# while the rest remain as HTH
# In fact, we do 5 sims (each with progressively less hyperpolarization);
# we just want some of them to be TH.
def crypd(W, idx):
    print ("\nChecking crypd: hyperpolarize HTH -> some become WT")
    # Set up a true HTH worm.
    # A two-headed VNC, with the VNC tail at midway.
    worm.set_VNC_HTH (W, round(W.PD['num_cells']/2))

    worm.spread_M ("HTH")
    if (not W.sim (SIM_TIME)):
        print ("crypd: original HTH sim was unsuccessful")
        return (False)
    HTH_state = sim.cc_cells.copy()

    # Try various schemes hyperpolarization for tail, mid and head.
    good = False
    good = crypd_scheme (W, idx, 1, .5, 0, HTH_state) or good
    good = crypd_scheme (W, idx, 1, .9, .8, HTH_state) or good
    good = crypd_scheme (W, idx, 1, .8, .6, HTH_state) or good
    return (good)

# amount of hyperpolarization for tail, mid and head
def crypd_scheme (W, idx, tail, mid, head, HTH_state):
    # Which cells 
    num_cells = sim.cc_cells.shape[1]
    cellsT = range (0, round(num_cells/3))
    cellsM = range (round(num_cells/3), round(num_cells*2/3))
    cellsH = range (round(num_cells*2/3), num_cells)
    
    scales = [.25,.5,.75,1]  # how far to scale D_Na (i.e., to hyperpolarize)

    Na=sim.ion_i['Na']
    original_Na = sim.Dm_array[Na,:].copy()

    results = ""; good = False
    for scale in scales:		# a few iterations...
        # Hyperpolarize by (mostly) turning off Na channels. This works since
        # both Cl and K have negative Vnernst.

        sim.Dm_array[Na,cellsT] = sim.Dm_array[Na,cellsT] * (1 - scale*tail)
        sim.Dm_array[Na,cellsM] = sim.Dm_array[Na,cellsM] * (1 - scale*mid)
        sim.Dm_array[Na,cellsH] = sim.Dm_array[Na,cellsH] * (1 - scale*head)

        # Sim to get nicely hyperpolarized. If it fails to complete, then this
        # iteration was useless. However, it may or may not wind up with a
        # substantial gradient; either way is OK. Some of the hyperpolarization
        # sims wind up with a gradient -- but are already TH (even before
        # removing the hyperpolarizer)!
        sim.cc_cells = HTH_state.copy()
        print ("Running hyperpolarizing sim with scale={} -> D_Na={}*1e-18".format (scale, sim.Dm_array[Na]*1e18))
        res = W.sim (SIM_TIME)

        # Always wash out the hyperpolarizer immediately; to prepare for the
        # next sim, or to restore the original network if this sim has failed.
        sim.Dm_array[Na,:] = original_Na.copy()

        if (res==None):				# Sim did not complete...
            results = results + " DNF1 "
            continue				#     so ignore this iteration.

        # We've washed out the hyperpolarizer -- now sim some more.
        print ("Running post-hyperpolarizing sim")
        res = W.sim (SIM_TIME)
        if (res==None):				# Sim did not finish...
            results = results + " DNF2 "
            continue				#     so ignore this iteration.
        if (res != True):
            results = results + " noGrad "
            continue

        # Show that at least one sim results as WT.
        shape = W.shape()
        results = results + " " + W.shape() + " "
        good = good or (shape=="HT") or (shape=="TH")

    print ("Results crypd #{} profile ({},{},{}) (want some WT) = {}" \
	.format (idx, tail, mid, head, results))
    return (good)

# Fallon's 2019 paper "The role of early bioelectric signals in ..." used an
# ionophore to depolarize a worm; then washed it away and 13% of worms become
# HTH. We hypothesize that since this only happened 13% of the time, it required
# some coincidence -- e.g., that the middle segment was depolarized less then
# the ends. So:
# - depolarize WT end (and sort of middle) segments
# - in fact: do 5 sims, starting all cells fully depolarized, and each successive
#   sim depolarizing the center of the worm a bit less (so as to make each
#   successive worm start out more like HTH).
# - call it success if any sim becomes HTH.
# This was written for the hybrid-worm paper.
def Fal19b(W, idx):
    print ("\nChecking Fal19b: depolarize WT middle -> some become HTH")
    worm.set_VNC_WT (W)			# Set up a normal WT VNC

    # Simulate to get our normal WT starting point, and save it for the
    # multiple successive sims.
    worm.spread_M ("TH")
    if (not W.sim (SIM_TIME)):
        print ("Fal19b: original WT sim was unsuccessful")
        return (False)
    state = sim.cc_cells.copy()

    # Depolarize by (mostly) turning off K channels. This works since
    # K has negative Vnernst. We will run multiple sims, as follows...
    # - First try: sim to get all cells uniformly depolarized (by turning off
    #   all K channels). Then restore the K channels, resim and hope we get HTH.
    # - On subsequent tries, while we still fully depolarize the cells at both
    #   ends of the worm, we depolarize the center cells less and less. Hence,
    #   the results of the first depolarizing sim will start to look more and
    #   more like HTH. Thus, each successive sim becomes more and more likely
    #   to actually wind up HTH.
    K=sim.ion_i['K']
    original_K = sim.Dm_array[K,:].copy()	# To recover from depolarization
    results = ""; good=False
    for i in range (5):
        n = W.PD['num_cells']//3
        sim.Dm_array[K,:n]  = 0		# Depolarize the left side
        sim.Dm_array[K,-n:] = 0		# Depolarize the right side
        sim.Dm_array[K,n:-n] = original_K[n:-n]*i/4  #... the middle, somewhat

        # Do the depolarizing sim. It of course must complete; it may or may not
        # make a gradient (since each try does less & less central depolarizing)
        print ("Fal19b: depolarizing sim #{}/{}".format(i,4))
        res = W.sim (SIM_TIME)

        # Restore the original Dk (i.e., wash out the depolarizer)
        sim.Dm_array[K,:] = original_K.copy()

        if (res == None):
            print ("Depolarizing simulation did not complete")
            results = results + " DNF1 "
            continue

        # The depolarizer is washed out; re-sim and see what we get.
        sim.Dm_array[K,:] = original_K.copy()	# can remove this code!
        res = W.sim (SIM_TIME)
        if (res==None):				# Sim did not finish...
            print ("Fal19b: post-depolarized simulation did not complete")
            results = results + " DNF2 "
            continue

        shape = W.shape()
        results = results + " " + W.shape() + " "
        good = good or (shape=="HTH")

    print ("Results Fal19b #{} (want some HTH) = {}".format(idx,results))
    return (good)

############################################################
# START OF WORM REGRESSION TESTS
############################################################

def regress_command_line ():
    import sys
    test_name = sys.argv[1]

    W = worm.Worm()

    # Run whatever test got chosen on the command line.
    eval ('setup_'+test_name+' (W)')

# Run the cryptic-1 (crypa) test on worm #3500, and then check fitness.
def setup_worm_exhaustive(W):
    worm.exhaustive (cryptic_etc, [3968],
	num_cells=[5,30,100],
	GJ_scale=[.001,.01,.1,1], kM = [.3, .5, .8], N = [2, 5, 8],
	gate_GJs=[None],
	M_valence = [-1, -2, -3, -4], GD_tau = [1, 10, 100, 1000],
	stronger_at_end = [100, 10, 3, 1],
	scale_pumps = [0,1],
	VNChead_pres = [0, .1, .2, .3],
	VNCtail_pres = [0, .1, .2, .3])
        

def setup_worm_evolve(W):
    evolve (n_gen=1, pop_size=13, n_matings=5, n_mutations=5, seed=0)

# This particular parameter set is a two-tailed worm for a while (and then it
# flips back to one head & one tail!). The trick is that kM=.2 is low enough
# that both ends have [M]>kM for a while.
def setup_worm_2tailed (W):
    W.PD['full_body']=False
    W.PD['do_gen_decay']=False
    W.setup_Bitsey_network()
    regress_end(W, 0, 2)

def setup_worm_lab (W):
    W.PD['full_body']=False
    W.PD['do_gen_decay']=False

    # N and kM are for the Hill model, where [M] controls the K channels.
    # 'GJ_scale' controls how strong the gap junctions are
    #kM=1; N=10; GJ_scale=.1	# Lab sim #1
    #kM=1; N=2;  GJ_scale=.1	# Lab sim #2
    #kM=.2;N=5;  GJ_scale=.1	# Lab sim #3
    W.PD['kM']=.2; W.PD['N']=5; W.PD['GJ_scale']=.02	# benchmark
    W.setup_Bitsey_network()
    regress_end(W, 0, 2)

# Observe how gated ion channels produce deltaV in a worm.
# Given the inputs to our ion-channel Hill-model gating in the head & tail:
# - head & tail [M]
# - gating-K-channel Hill-model parameters kM and N
# Then:
# - manually evaluate the Hill model for the given [M], kM and N and compute
#   the resulting gated Dm_array[K, head & tail]. Note that actual K channels in
#   the sim will not be gates, but will be hard coded to the values we compute.
# - simulate
# Result: observe the resultant head and tail Vmem. Note that M is mostly
# irrelevant in the sim itself; it mostly just affects the just-described
# computation of Dm_array[K, head & tail].
# For that matter, so is GJ_scale -- since we're looking at Vmem
# after just a few seconds
def setup_worm_gating (W):
    M_head=1.64; M_tail=.492; num_cells=5
    kM=1; N=10; GJ_scale=.1	# Lab sim #1, and benchmark
    # kM=1; N =2; GJ_scale=.1	# Lab sim #2

    # The ligand-gated K channel has diff = 1.7 e-17 m2/s when fully on.
    head_K = 1.7e-17 / (1. + (M_head/kM)**N)
    tail_K = 1.7e-17 / (1. + (M_tail/kM)**N)
    print ("Scale at head={:.2g}, tail={:.2g}".format (1/(1.+(M_head/kM)**N),
                                               1/(1.+(M_tail/kM)**N)))
    # gen_decay = False.
    W.PD['full_body']=False
    W.PD['do_gen_decay']=False

    # N and kM are for the Hill model, where [M] controls the K channels.
    # 'GJ_scale' controls how strong the gap junctions are
    W.PD['GJ_scale']=GJ_scale; W.PD['num_cells']=num_cells

    W.setup_Bitsey_network()

    # setup_one_worm() has created K-feedback ion channels at the head & tail
    # cells. Remove the feedback, and set Dm_array[K] to what we want.
    K=sim.ion_i['K']
    sim.Dm_array[K,-1] = head_K
    sim.Dm_array[K,0]  = tail_K
    sim.IC_gates = []	# Turn off all IC gating, specially for this test.
    regress_end(W, M_tail, M_head)

# Function called right after the per-benchmark simulation setup_*() function.
# It prints a few pre-sim values, runs the actual simulation, and prints post-
# sim values.
def regress_end(W, M_min, M_max):
    Na=sim.ion_i['Na']; K=sim.ion_i['K']; M=sim.ion_i['M']
    spread = np.linspace (M_min, M_max, W.PD['num_cells'])
    sim.cc_cells[M, :]   = spread
    sim.cc_cells[Na, :] -= spread * W.PD['M_valence']

    print (f"Head D_K={sim.Dm_array[K,-1]:.2e}, tail D_K={sim.Dm_array[K,0]:.2e}, GJ_scale={W.PD['GJ_scale']}")
    if (len(sim.IC_gates)>0):
        print (f'Head kM={sim.IC_gates[0].params[0,-1]}')
        print (f'Tail kM={sim.IC_gates[0].params[0,0]}')

    np.set_printoptions (formatter={'float': '{: 6.3g}'.format}, linewidth=90)
    Vm = sim.compute_Vm (sim.cc_cells)
    print (f'Initial Vm   ={1000*Vm}mV\n')

    print ('Starting main simulation loop')
    GP = sim.GP
    GP.adaptive_timestep = False; GP.use_implicit = False
    GP.sim_dump_interval = 1;     GP.sim_long_dump_interval = 10
    W.sim (5.0)
    print ('Simulation is finished.')
    edb.dump (5.0, W.cc_shots[-1], edb.Units.mol_per_m3s, True)

# Used for running regression tests.
if (__name__=="__main__"):
    regress_command_line(); quit()
