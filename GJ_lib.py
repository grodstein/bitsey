'''
This file contains library routines to implement a single type of GJ -- e.g.,
C26/C30. It also supports multiple GJ models (right now I'm using the
Mafe-Cervera model, with numbers taken from Barrio2007).
You use it by:
      - call setup_GJ_model (GJ1, GJ2, Vmin=-.1, Vmax=.1, granularity=.002),
	where each of GJ1 and GJ2 is a connexin type (e.g., C26). It uses 
	set_GJ_params() to look up the parameters of the two connexins and save
	them away.
      - setup_GJ_model() then creates g_V_vals[] and g_G_vals[]. No matter
	what model it uses, it still creates these same arrays.
      - instantiate objects of class GJ_gate. This allows GJs to be used as
	Bitsey gating objects. Bitsey will then call GJ_gate.func() at the
	appropriate times during simulation, which uses g_V_vals and g_G_vals
	for interpolation.
      -	Note that GJs use the same gating fraction to scale both drift and
	diffusion currents (since a GJ doesn't know what force is moving any
	given molecule).

Polarity:
      - for a homotypic GJ (i.e., where type1==type2), then the G-vs-deltaV
	function is symmetric and the sign of deltaV is irrelevant when
	computing G_GJ.
      - for a heterotypic GJ (where type1!=type2), we compute G_GJ by applying
	deltaV to the type2 side of the GJ and holding the type1 side at ground.
      - when you create a GJ_gate object, sim.py computes the deltaV values by
	subtracting Vmem_to - Vmem_from. So Vmem_to is the type2 side, and
	Vmem_from is the type1 side.

Results
	A1<0, A2>0: G_GJ is monotonically falling vs deltaV
	A1>0, A2<0: G_GJ is monotonically rising  vs deltaV
	else: the usual hump in the middle (i.e., G highest near deltaV=0).
Looking more closely at the A1,A2 both positive case: when the two half-channels
are identical, the graph is symmetric with a peak at V=0. Otherwise it's not --
the peak can move from V=0 and the rolloff can be different on each side.
With enough asymmetry, we can make the entire usable range (e.g, from
[-50mV,+50mV]) either monotonically rising or monotonically falling.

From Mike: our experts on this are Nicolas Palacios-Prado (whom Mike knows
well) and Jose Ek Vitorin.

Here's some intuition on the conductance of a half-channel, whose basic formula
from the literature is
	G_one_side = G_min + (G_max-G_min)/(1+exp(A*(V-V0))))
This describes a sigmoid with a knee at V=V0, where A describes the sharpness
of the knee. Why? Consider G_min=0, G_max=1 for simplicity. Then 
	G_one_side = 1/(1+math.exp(A*(V-V0))))
For A positive, it is monotonically decreasing:
	starts at G_one_side=1 at V=-infinity, 
	is always G_one_side=.5 at V=V0
	reaches G_one_side=0 at V=infinity.
For A negative, we just flip the graph around the vertical line V=V0, so the
behavior at -infinity and +infinity are swapped.
As A gets very big, the knee of course gets very steep and we look very sigmoid.
So it's a decreasing(increasing) sigmoid when A is positive(negative).
'''
import numpy as np, matplotlib.pyplot as plt, math
import sys; sys.path.append ("..")	# To get the usual Bitsey files
import sim
FLOAT_NONCE = 1.0e-10
#import pdb; pdb.set_trace()

################################################################
# The externally-useful routines.
# This file has routines to take a GJ deltaV and return a gating fraction. But
# that's an iterative (and hence slow) calculation. The routines here make it
# fast -- they do the full, slow calculations for a range of deltaV values,
# save away the results, and then do a simple (and fast) interpolation later.
#
# The routines:
#     -	setup_GJ_model () is the externally entry point to call first. It takes
#	the two connexins for GJs and the range of legal deltaV values. It calls
#	set_GJ_params() to save away the GJ types, and then calls one of the
#	GJ models to do lots of computation and set up our interpolation tables.
#     -	class GJ_gate() can then be instantiated. Its .func() then gets called
#	by sim.py, and knows how to run a quick interpolation.
################################################################

# Globals that hold numpy arrays used for interpolation.
g_V_vals=None		# Will be array of deltaV values
g_G_vals=None		# Will be array of G values (corresp. to each deltaV)
g_granularity=None	# The granularity of the g_G_vals array in volts.

# The external routine that must be called first. Given two connexin types, it
# builds the global arrays g_V_vals and g_G_vals, and g_granularity. Then
# eval_GJ() can use those arrays.
def setup_GJ_model (GJ1, GJ2, model="joel_old",
		    Vmin=-.1, Vmax=.1, granularity=.002):
    # Only joel_old() can handle heterotypic GJs.
    assert (GJ1==GJ2) or (model=="joel_old")

    global g_V_vals, g_G_vals, g_granularity
    set_GJ_params (GJ1, GJ2)	# Saves the 4 params for each GJ as globals.
    g_V_vals = np.arange (Vmin, Vmax+FLOAT_NONCE, granularity)
    model = eval (model)
    g_G_vals = np.array ([model(V) for V in g_V_vals])
    g_granularity = granularity
    print (f"Set G_GJ1 to Gmin={G_min1}, Gmax={G_max1}, V0={V0_1}, A={A1}")
    print (f"Set G_GJ2 to Gmin={G_min2}, Gmax={G_max2}, V0={V0_2}, A={A2}")

# The main way to use gated GJs (called by class GJ_gate.func()).
# It does linear interpolation using the function whose points are given by
# g_V_vals[] (the x coordinates of the points) and g_G_vals[] (the corresponding
# y coordinates).
# Note that deltaV[] is an array. But the magic of advanced indexing means we
# can write the code here as if deltaV were a scalar.
def eval_GJ (deltaV):
    # Clip to avoid out-of-bounds array accesses.
    deltaV = np.clip (deltaV, g_V_vals[0], g_V_vals[-1]-.0000001)
    idx_flt = (deltaV-g_V_vals[0])/g_granularity
    idx_floor = np.clip(np.floor(idx_flt), None,g_G_vals.size-2)
    frac = idx_flt - idx_floor
    idx_floor = idx_floor.astype(int)
    G0 = g_G_vals[idx_floor]
    G1 = g_G_vals[idx_floor+1]
    G = frac*G1 + (1-frac)*G0
    return(G)

# The gating class, working with sim.Gate. As always, its .func() gets called
# during simulation with parameters for the current sim state; all we care
# about is the GJ delta-V values.
class GJ_gate (sim.Gate):
  def __init__(self):
    super().__init__ (self.GATE_GJ, None)
  # The function that actually computes the gating vector at runtime.
  def func (self, cc_ignore, Vm_ignore, deltaV_GJ,t_ignore):
    out = eval_GJ (deltaV_GJ)
    return (out)

################################################################
# This part of the file maintains a few global variables to hold the basic
# parameters of the connexins for type1 and type2.
################################################################

# The internal function that GJ_gate.__init__ and setup_GJ_model() call to
# save away (as globals) the four parameters of each the two GJs connexin types.
# We take them as strings (e.g., "C26" and "C30"), look up the
# characteristics of those two types to save them as globals.
# Then G_GJ1() and G_GJ2() use the globals.
def set_GJ_params (type1, type2):
    global A1, G_min1, G_max1, V0_1, A2, G_min2, G_max2, V0_2
    G_min1, G_max1, V0_1, A1, sign1 = GJ_params[type1]
    G_min2, G_max2, V0_2, A2, sign2 = GJ_params[type2]
    assert (sign1==-1 or sign1==+1) and (sign2==-1 or sign2==+1)
    A1 *= sign1
    A2 *= sign2
    V0_1 *= sign1
    V0_2 *= sign2

# Here is my dictionary of GJ parameters (G_min, G_max, V0, A, polarity) taken
# from Gonzalez/Barrio 2007.
# * G_min is the "residual conductance" when the GJ is fully off.
# * G_max is the maximum-ever conductance when the GJ is fully on.
# * V0 is the voltage at which G = (G_min + G_max)/2, in mV
# * A tells us how sharp the falloff is on either side of V0; sort of like the
#   Hill exponent. And its sign tells us whether the hemichannel opens with
#   increasing or decreasing Vj. Its units are 1/mV. Equivalently, if the
#   "gating charge" is z, then A=z/26mV
GJ_params = {
	'C26':(.19, .97, .095, 130, +1),
	'C30':(.17, 1,   .052, 180, +1),
	'C31':(.22, 1.1, .044,  70, -1),
	'C32':(.26, 1.02,.061,  70, -1),
	'C37':(.027,1,   .028,  80, +1), # unknown if it's +1 or -1.
	'C37x':(.027,1,  .028, 160, +1), # unknown if it's +1 or -1.
	'C37y':(1e-3,1,  .022, 300, +1), # unknown if it's +1 or -1.
	'C40':(.19, 1,   .035, 320, +1), # Note this is mouse, not human.
	'C43':(.29, 1,   .061,  60, -1),
	#'C45':(.08, 1.18,.022,  80, -1)}
	'C45':(.08, 1.18,.022,  80, +1)} # Really, it's -1

################################################################
# My original algorithm, which analyzes the series connection of the two gates.
# Call it as joel_old().
# It finds a voltage for the "center" node of the GJ, where the two connexins
# meet, that supports equal currents flowing into and out of the center node.
# What makes this hard is finding that voltage! Without knowing that voltage,
# we don't know the deltaV across either of the two hemichannels, and so we
# don't know the hemichannel conductances.
# We solve by assuming that the GJ very quickly reaches a "steady state" where
# the current flowing in from one side all flows out the other side. Find (via
# a binary search) the center voltage where this occurs.
################################################################

# Given a deltaV applied to the GJ, return the GJ’s effective conductance
# Specifically, the deltaV is applied to the GJ2 side, with the GJ1 side held
# at ground.
# Algorithm: use a binary search to find the intermediate V where both half-GJs
# conduct the same current.
# Return the GJ conductance at this operating point.
def joel_old (V):
    # Part 1: find the intermediate V.
    if (V==0):			# Stop binary search from crashing when lower
        V = .0000001		# and upper limits are identical.
    lower = 0	# running lower bound for the intermediate V
    upper = V	#    "    upper
    for i in range(100):			# binary search
        assert sign(lower,V) != sign(upper,V)
        mid = (lower+upper)/2
        #print(f'Mid={mid}V, low={lower}V,top={upper}V')
        if (sign(mid,V) == sign(upper,V)):
            upper = mid
        else:
            lower = mid

    # Part 2: find conductance given the intermediate V.
    g2 = G_GJ2(V-mid)		# GJ2: positive end at V, neg at Vmid.
    g1 = G_GJ1(-mid)		# GJ1: positive end at Vmid, neg at 0.
    g_vers1 = 1 / (1/g1 + 1/g2)	# Series resistance

    # Now some sanity checks.
    i2 = (V-mid) * g2		# I from V to the mid
    i1 = (0-mid) * g1		# I from 0 to the mid
    #print (f"V_GJ={V*1000:.0f}mV => mid={mid*1000:.1f}mV, " \
    # + f"g1({-mid*1000:.2f}mV)={g1:.3f}, g2({(V-mid)*1000:.2f}mV)={g2:.3f}, " \
    #  + f"g_total={g_vers1:.3f}, ")
    #  + f"i1={i1:.3f}, i2={i2:.3f}, r1={1/g1:.3f}, r2={1/g2:.3f}")
    #print (f"r1+r2={1/g1+1/g2:.3f}, V/i1={V/i1:.3f}")
    assert abs(i1+i2)<.01	# They are equal and opposite.
    g_vers2 = i2/V		# Compute series conductance two ways.
    #print (f'g_vers2={g_vers2}, g_vers1={g_vers1}')
    assert math.isclose(g_vers2,g_vers1,rel_tol=.00001, abs_tol=.0001)
    #print (f"V={V:.3f}V -> Vmid={mid:.3f}V, G={i2/V:.3f}")

    return (g_vers1)

# Helper function used by the binary search. Thinking of the current in each
# hemichannel as being positive if inwards towards the center, return the sign
# of the total current into the center from both sides. We want it to eventually
# go close to zero.
# Given: a GJ with left-side (GJ1) voltage of 0V, right-side (GJ) voltage of
# V and junction voltage x. Compute the current through each hemichannel, sum
# them to get the current into the center, and return its sign (1 if positive, 0
# if negative).
def sign(x,V):
    i2 = (V-x) * G_GJ2(V-x)	# current into the junction
    i1 = (0-x) * G_GJ1(-x)	# ditto
    net = i1 +i2
    #print(f'At x={x}/{V}: G2={G_GJ2(V-x):.5f}, G1={G_GJ1(-x):.5f}, i2={i2:.5f}, i1={i1:.5f}, net_in={i1+i2:.5f}')
    return (1 if net>0 else 0)

# A helper used by G_GJ1 and G_GJ2 below to compute the gating fraction of a
# single hemichannel.
# As noted above: if A>0, then G goes from Gmax to Vmin as V goes -inf to +inf.
def G_GJ (V, G_min, G_max, V0, A):
    return (G_min + (G_max-G_min)/(1+math.exp(A*(V-V0))))

# The gating fraction of our two hemichannels.
def G_GJ1(V):
    return (G_GJ (V, G_min1,G_max1, V0_1, A1))
def G_GJ2(V):
    return (G_GJ (V, G_min2,G_max2, V0_2, A2))

################################################################
# A few more -- much simpler -- models, and some code to compare them.
# All of the models use the simple API model_name(V), where V can be either a
# scalar voltage or a Numpy array of voltage (in Volts).
# Note that none of these models support heterotypic GJs.
################################################################

# The Mafe-Cervera model, but using parameters straight from Barrio2007.
def mafe_cervera (V):
    denom = 1 + np.exp(A1*(V-V0_1)) + np.exp(A1*(-V-V0_1))
    return (G_min1 + (G_max1-G_min1)/denom)

# The Barrio model, but not symmetric.
def barrio (V):
    return (G_min1 + (G_max1-G_min1)/(1+np.exp(A1*(V-V0_1))))

# The Barrio model, made symmetric by just replacing V with abs(V).
def barrio_with_abs (V):
    return barrio (abs(V))

# The Barrio model, made symmetric by putting two reversed hemichannels
# in series.
def barrio_series (V):
    gate1 = barrio (V)
    gate2 = barrio (-V)
    return (1 / ((1/gate1) + (1/gate2)))

# A utility to plot and compare the various models we have.
def plot_many_models(GJ):
    # First, my old model.
    set_GJ_params (GJ, GJ)
    mod="barrio_with_abs"
    setup_GJ_model (GJ, GJ, Vmin=-.2, Vmax=.2, granularity=.002, model=mod)
    G_my_old = eval_GJ (g_V_vals)

    # Now, some more models.
    G_mafe_cervera = mafe_cervera (g_V_vals)
    G_barrio_with_abs = barrio_with_abs (g_V_vals)
    G_barrio_series = barrio_series (g_V_vals)

    analyze_G (g_V_vals, G_my_old, "My old model")
    analyze_G (g_V_vals, G_mafe_cervera, "Mafe-Cervera")
    analyze_G (g_V_vals, G_barrio_with_abs, "Barrio_abs")
    analyze_G (g_V_vals, G_barrio_series, "Barrio_series")

    plt.plot (g_V_vals*1000, G_my_old, label="G my_old")
    plt.plot (g_V_vals*1000, G_mafe_cervera, label="G mafe_cervera")
    plt.plot (g_V_vals*1000, G_barrio_with_abs, label="G barrio with abs")
    plt.plot (g_V_vals*1000, G_barrio_series, label="G barrio series")
    plt.xlabel ("mV")
    plt.legend(loc='best')

    # Draw the X and Y axes
    plt.plot ([g_V_vals[0]*1000,g_V_vals[-1]*1000],[0,0],color='black')
    #y_min,y_max = plt.gca().get_ylim()
    #plt.plot ([0,0],[y_min,y_max],color='black')
    plt.title (GJ + "/" + GJ)
    plt.show()

def analyze_G (V_vals, G_vals, name):
    G_min_idx = np.argmin (G_vals)
    G_max_idx = np.argmax (G_vals)
    G_min = G_vals[G_min_idx]
    G_max = G_vals[G_max_idx]
    thresh = G_min + .5 * (G_max-G_min)
    #import pdb; pdb.set_trace()
    for i in range (V_vals.size-1,0,-1):
        if (G_vals[i] > thresh):
            print (f"{name}: Gmin={G_min:.3f}@{V_vals[G_min_idx]:.3f}V, "\
                 + f"Gmax={G_max:.3f}@{V_vals[G_max_idx]:.3f}V, "\
                 + f"50% is {G_vals[i]:.3f}@{V_vals[i]:.3f}V")
            return


################################################################
# A few plotting routines to plot gating fraction vs. deltaV.
################################################################

# Small routine to plot one hemichannel's G vs. V without even building an
# entire GJ.
def plot_one_side():
    V_vals = np.linspace (-.5,.5,100)
    G = np.array ([G_GJ(V, G_min1, G_max1, V0_1, A1) for V in V_vals])
    I = V_vals*G
    G_max = G.max()
    I_max = np.abs(I).max()
    I *= G_max/I_max

    plt.plot (V_vals*1000, G, label="G (mhos)")
    plt.plot (V_vals*1000, I, label="I (scaled mA)")
    plt.xlabel ("mV")
    plt.legend(loc='best')
    plt.show()

# For various values of V_GJ...
#    Plot the current flowing into and out of the junction node JN vs. V_JN.
# The intersection shows the correct value of V_JN.
def plot_junction_current():
    for V_GJ in np.linspace (.015,.20,10):
        I1=[]; I2=[]; Vx_range = np.linspace (0,V_GJ,50)
        for Vx in Vx_range:
            i2 = (V_GJ-Vx) * G_GJ2(V_GJ-Vx)	# current into the junction
            i1 = (0-Vx)    * G_GJ1(-Vx)		# ditto
            I1.append(-i1)			# current out of junction
            I2.append(i2)			# current into junction
        plt.plot (Vx_range*1000, I1, label=f"I_GJ1 out")
        plt.plot (Vx_range*1000, I2, label=f"I_GJ2 in")
        plt.xlabel ("V_junct(mV)")
        plt.ylabel ("I_GJ")
        plt.title (f"V_GJ={V_GJ*1000:.1f}mV")
        plt.legend(loc='best')
        plt.show()

def main(GJ1, GJ2):
    set_GJ_params (GJ1, GJ2)
    setup_GJ_model (GJ, GJ, Vmin=-.2, Vmax=.2, granularity=.002)
    G = eval_GJ (g_V_vals)
    I = g_V_vals*G
    #G_old = np.array ([eff_cond(V) for V in g_V_vals])
    #assert np.allclose (G,G_old)

    # Make the plot nicer by scaling I to have the same max value as G.
    G_max = G.max()
    I_max = np.abs(I).max()
    I *= G_max/I_max

    plt.plot (g_V_vals*1000, G, label="G (open fraction)")
    plt.plot (g_V_vals*1000, I, label="I (scaled)")
    plt.xlabel ("mV")
    plt.legend(loc='best')

    # Draw the X and Y axes
    plt.plot ([g_V_vals[0]*1000,g_V_vals[-1]*1000],[0,0],color='black')
    y_min,y_max = plt.gca().get_ylim()
    plt.plot ([0,0],[y_min,y_max],color='black')
    plt.title (GJ1 + "/" + GJ2)
    plt.show()

# Some code mostly for testing.
if (__name__=="__main__"):
    #main('C26', 'C26')
    #main('C31', 'C31')
    #main('C30', 'C30')
    #main('C40', 'C40')
    #main('C37y', 'C37y')
    #main('C45', 'C45')
    #main('C45', 'C37')
    #set_GJ_params ('C40','C40')
    #plot_one_side()
    #plot_junction_current()
    plot_many_models('C45')
