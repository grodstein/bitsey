#!/usr/bin/env python3

# Copyright 2018 Alexis Pietak and Joel Grodstein
# See "LICENSE" for further details.

# This file contains routines for a "Worm" class. Each Worm object contains a
# particular parameter set that makes that Worm unique. E.g., a number of cells,
# and GJ density, and ion-channel strengths and feedback characteristics, etc.
#
# Why is it useful to have an object with its parameters as instance variables?
# Mainly, it gives you the ability to have multiple Worm instances existing in
# the same program. This is invaluable for, e.g., evolutionary algorithms, where
# you have an entire population of Worms with different parameters.
# Note, however, that even though multiple Worm objects can exist at the same
# time, only one single *simulation* can be live at a time; the simulation data
# structures are all globals, not Worm instance parameters. You can kind of come
# close, though -- after you sim a Worm object, the results (i.e., everything
# you would need to do simulation plots) do get saved with the object.
#
# The other nice thing about having a Worm class is that it gives us a set of
# nice functions to use without reinventing the wheel. E.g.:
#	mating & mutation functions for evolutionary and other optimation algs
#	routines to build a Bitsey network for a worm, and to simulate it
#	routines to spread a morphogen through the worm for initialization
#	routines to analyze the shape of a worm (e.g., HT vs. HTH).
#	and more...

#    import pdb; pdb.set_trace()
import numpy as np
import sim, eplot as eplt, edebug as edb, GJ_lib
import re, itertools, math

SIM_TIME = 20000000
#SIM_TIME = 30000

NUM_PIECES = 20		# Number of children for measuring fitness.
VALUE_ERROR = -100
GRADIENT = .030		# How much delta-Vmem counts as growing a good gradient.
GD_FVAL=1		# Generation/decay -> [M] eventually becomes 1.

# What makes worms different? It's all in Worm.PD, the parameter dictionary:
# - num_cells, GJ_scale, ...: these affect how the network is built
# - kM, N, ...: these affect how ion-channel gating arrays are populated.
# - diff_IC_K, ...: these affect global arrays (Dm_array, GJ_connects, ...)
# - GJ_len: affects the global parameter object GP.
class Worm:
  def __init__(self):
    self._fitness = -1	 # Save the fitness value to avoid recomputes
    self._n_good_children = -1 # Part of fitness; how many child pieces lived.

    # Now set the default parameters (which match the Worm regression tests).
    # Any instance typically overrules some/most of these.
    self.PD = {}	  # Dictionary of the parameters for this Worm object.
    self.PD['num_cells'] = 5	# the obvious
    self.PD['GJ_scale'] = .02	# controls how strong the GJs are
    self.PD['GJ_len'] = 15e-9	# GJ length
    self.PD['kM'] = .2
    self.PD['N'] = 5		# for the ion-channel Hill model cooperativity
    self.PD['gate_GJs'] = None	# use nonlinear voltage-gated GJs

    self.PD['diff_IC_K'] =1.7e-17	# ion-channel diff const for K
                                        #    (i.e., sim.Dm_array[K,:])
    self.PD['diff_GJ_Na'] =1.33e-17	# GJ diff const
    self.PD['diff_GJ_K'] = 1.96e-17	#    (i.e., sim.GJ_diffusion[])
    self.PD['diff_GJ_M'] = 1e-14	#    for Na, K and M
    self.PD['env_Na'] = 145		# Environmental [Na], [K] and [P]; then
    self.PD['env_K'] = 5		# choose [Cl] automatically for charge
    self.PD['env_P'] = 10		# neutrality.
    self.PD['int_Na'] = 12		# Ditto, but cell-internal. Note these
    self.PD['int_K'] = 139		#    are just initial values, and may
    self.PD['int_P'] = 135		#    change over time.
    self.PD['M_valence'] =-1		# valence of the mystery ion M
    self.PD['GD_tau'] = 100		# time constant for gen/decay of M
    self.PD['do_gen_decay'] = True	# Do we have gen/decay at all?
    self.PD['full_body'] = True		# Ion channels & feedback everywhere.
    self.PD['stronger_at_end'] =1	# E.g., 2=>ICs weaker in the body by 2x
    self.PD['scale_pumps'] = 0		# ion pumps commensurately weaker also
    self.PD['VNChead_pres'] = 0		# Extra D_Na at VNC head
    self.PD['VNCtail_pres'] = 0		# Extra D_Na at VNC tail

  # Use my set of parameters (i.e., self.PD[...]) and create a Bitsey
  # simulation network.
  def setup_Bitsey_network(self):
    self._fitness = -1
    p=sim.Params()
    p.sim_dump_interval = 20000	# Not much debug printout during the sim
    num_cells = self.PD['num_cells']
    n_GJs = num_cells-1

    sim.init_big_arrays (num_cells, n_GJs, p, ['M']) # sim.GP gets set here
    Na=sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']
    P=sim.ion_i['P']; M=sim.ion_i['M']

    # Alexis uses feedback on every cell; usually I only do it at the head/tail.
    # Cells without feedback will not have any ion channels at all.
    fb_cells = range(num_cells) if self.PD['full_body'] else [0,num_cells-1]

    # We let evolution (and anyone else, for that matter) change the area of
    # K ion channels, but not of Na or Cl ion channels. This doesn't necessarily
    # make sense biologically, but it was done since we've typically built
    # positive feedback with K-ion-channel gates and not Na ones.
    # Default ion-channel diffusion coef: Na=K=M = 1e-18 m2/s, and P=0.

    # Set the K ion-channel diffusion rate (really, the cross-sec area).
    # If we're not in full-body mode, then kill *all* body-interior ICs (not
    # just K).
    sim.Dm_array[K, fb_cells] = self.PD['diff_IC_K']	# Set the K channels
    if (not self.PD['full_body']):			# Kill all interior
        sim.Dm_array[:,1:-1]= 0				# ICs if not full-body.

    # The "end" cells may have stronger ion channels. If so, then define "end"
    # as the 10% of cells on either end (but at least one cell if num_cells=5!).
    end = max (round(num_cells/10), 1)
    SAE = self.PD['stronger_at_end']
    sim.Dm_array[:,end:-end] = sim.Dm_array[:,end:-end] / SAE
    if self.PD['scale_pumps']:
        sim.pump_scaling[end:-end] = 1.0/SAE

    # Ligand-gated K channels at feedback cells (head, tail and maybe body).
    # First initialize to all off, then turn on the feedback cells.
    # Big picture (remember that K has Vnernst<0): the head is positive, which
    # attracts M, which turns off the K gate, which makes the head still more
    # positive. And the tail is negative, which repels M, which turns on the K
    # gate and makes the tail more negative.
    g=sim.Hill_gate (sim.Gate.GATE_IC, out_ion=K, in_ion=M)
    for c in fb_cells:
        g.change_Hill_gate (c, inv=True, kM=self.PD['kM'], N=self.PD['N'])

    # Dummy ligand-gated Na Hill channels everywhere, that really just serve to
    # allow an offset for VNC head pressure (tail pressure is through the K
    # channels just above).
    sim.Hill_gate (sim.Gate.GATE_IC, out_ion=Na, in_ion=Na)

    # Straight-line GJ connections along the worm. All are simple & ungated.
    sim.GJ_connects['from']  = range(n_GJs)
    sim.GJ_connects['to']    = sim.GJ_connects['from'] + 1

    # gate_GJs can be None for ungated GJs; otherwise, e.g., ("C32","C34")
    gate_GJs = self.PD['gate_GJs']
    if (gate_GJs != None):
        GJ_lib.setup_GJ_model (gate_GJs[0], gate_GJs[1]) # the two connexins
        GJ_lib.GJ_gate()		# Instantiate a gate for the GJs

    # Physical parameters of the 'mystery' ion.
    sim.z_array[M] = self.PD['M_valence']	# fixed ion valence

    # Initial concentrations: external {Na=145 mM, K=5mM, P=10mM}
    sim.cc_env[Na] = self.PD['env_Na']; sim.cc_env[K]=self.PD['env_K']
    sim.cc_env[P]=self.PD['env_P']
    # Adjust [Cl]ext to reach charge neutrality
    sim.cc_env[Cl] = self.PD['env_Na'] + self.PD['env_K'] - self.PD['env_P']

    # Initial concentrations: internal {Na=12, K=139, Cl=16, P=135}
    sim.cc_cells[Na]=self.PD['int_Na']; sim.cc_cells[K]=self.PD['int_K']
    sim.cc_cells[P] =self.PD['int_P']
    # Adjust [Cl]int to reach charge neutrality
    sim.cc_cells[Cl]=self.PD['int_Na'] + self.PD['int_K'] - self.PD['int_P']

    # Ligand creation/decay/gating: creation at .1uM/s, and decaying at
    # (.1/s)*(its concentration)). This yields concentration=1uM. However, we'll
    # spread it from head to tail to seed the gradient. Note that 1uM is 1
    # umole/liter, or 1 mmole/m3. We're actually doing 1000 times that big!
    if (self.PD['do_gen_decay']):
        gen_rate = GD_FVAL/self.PD['GD_tau']	# moles/m3 per s
        sim.GD_const_gate (sim.Gate.GATE_GD, M, gen_rate, 1/self.PD['GD_tau'])

    # Change the gap-junction length from 100nm to 15nm.
    p.GJ_len = self.PD['GJ_len']

    # Assign the GJ diffusion constants however this worm has been set up.
    sim.GJ_diffusion[Na,:] = self.PD['diff_GJ_Na']
    sim.GJ_diffusion[K,:] = self.PD['diff_GJ_K']
    sim.GJ_diffusion[M,:] = self.PD['diff_GJ_M']
    sim.GJ_diffusion[P,:] = 0
    sim.GJ_diffusion *= self.PD['GJ_scale']

    #Na/K-ATPase pump with a maximum rate of 1.0x10-7 mol/(m^2 s)
    #print ("Worm setup is done.")

  # The official Python print() calls this to print objects.
  # Simulate and return any of three statuses:
  # True  => successful simulation and built a gradient
  # False => successful simulation but no gradient
  # None  => simulation did not complete.
  # Note that both False and None act as False in an if() statement.
  # The big side effect is that the t_shots and cc_shots arrays get filled in;
  # we will check them to find our fitness and our body shape.
  def sim (self, end_time):
      #sim.GP.use_implicit = True
      #sim.GP.adaptive_timestep = False
      try:
          self.t_shots, self.cc_shots = sim.sim (end_time)
      except (ValueError, IndexError):
          print ('Simulation: valueError encountered!!!')
          return (None)

      #np.set_printoptions (formatter={'float': "{:.3f}".format})
      plots=False
      #import pdb; pdb.set_trace()
      if (plots):
          eplt.plot_worm_times ('M', self.t_shots, self.cc_shots, 10,0,1)
          eplt.plot_Vmem (self.t_shots, self.cc_shots)
          #eplt.plot_ion (self.t_shots, self.cc_shots, 'Na')
          #eplt.plot_ion (self.t_shots, self.cc_shots, 'K')
          #eplt.plot_ion (self.t_shots, self.cc_shots, 'Cl')
          eplt.plot_ion (self.t_shots, self.cc_shots, 'M')
      mV = sim.compute_Vm(sim.cc_cells)*1000
      print (f'    Vm={mV}mV\n    [M]={self.cc_shots[-1][4]} => {self.shape()}')
      grad = self.gradient()
      good = "good" if (grad>=GRADIENT) else "no"
      print (f'Simulation: gradient={grad*1000:.1f}mV => {good} gradient')
      return (grad >= GRADIENT)

  # Return the Vmem gradient -- i.e., (max Vmem) minus (min Vmem).
  # Assume somebody has already run a simulation.
  def gradient(self, only_TH=False):
      Vm = sim.compute_Vm (sim.cc_cells)
      if (only_TH):
          return (max (Vm[-1] - Vm[0], 0.0))
      return (Vm.max() - Vm.min())

  # If the simulation doesn't finish, fitness= 0. If it does...
  # A no-gradient worm gets 0-40 points for how long it takes to collapse.
  # A worm with enough gradient to be deemed alive can still get another 0-10
  # points for having still more gradient.
  # Finally, chop the worm into pieces, and give another 0-50 points for how
  # much gradient each of the child pieces can regrow to.
  #
  # Only_TH says only TH worms count as passing (not HTH, THT, etc). Currently
  # we ignore it completely in the first 0-50 points. Then don't even try the
  # children if we regrew ourself wrong; and give 0 points for any child that
  # regrew itself wrong.
  #
  # Note that we assume somebody has already set up spread_M() to work well;
  # e.g., by calling sim_start().
  def fitness (self, only_TH=True):
      if (self._fitness >= 0):		# As a worm moves from one generation to
          return (self._fitness)	# the next, only compute fitness once.

      # Seed the worm with [Na], [M], etc.
      print ("Initial fitness sim...")
      spread_M ("TH")

      self._fitness = 0			# Our reward just for starting :-)
      status = self.sim (SIM_TIME)
      if (status == None):	# Unsuccessful simulation => fitness = 0
          return (self._fitness)

      if (status == False):	# Sim finished, but not enough gradient
          # The full worm doesn't hold its spread. Return a score in [0,40]
          # based on how long it took for the original Δ[M] shrink away. We
          # don't need a new sim; we have the old data at numerous timepoints. 
          print ("Worm died")
          M=sim.ion_i['M']
          for idx,cc in enumerate(self.cc_shots):
              ratio = cc[M][-1] / cc[M][0]
              if (ratio < 1.6):
                  # The fraction of the way from t_old to t_new where we hit 1.6
                  alpha = (ratio_old-1.6)/(ratio_old-ratio)
                  # Interpolate the same fraction from t_old to t_new
                  t1 = self.t_shots[idx]; t0 = self.t_shots[idx-1]
                  t = t0 + alpha*(t1-t0)
                  # Map from t in [0,10000] to [0,40] points.
                  self._fitness = min (40.0, t*40/10000)
                  print (f"t0={t0}, t1={t1}, t={t}, fit={self._fitness}")
                  return (self._fitness)
              ratio_old = ratio
          self._fitness = 40.0		# gradient never shrunk much,
          print ("worm had ratio>1.6 until the end")
          return (self._fitness)	# so we get the full 40 points.

      # The original sim finished and had a good gradient (i.e., > GRADIENT).
      # 40 points for that, and another 0-10 points for getting an even bigger
      # gradient than the legal minimum.
      g = self.gradient()/GRADIENT - 1 # unitless gradient.
      # Map, so that g=0 => 40 points; g=1 => 46.7; g=2 => 48, g=inf => 50.
      # The "2*g" rather than just "g" helps get higher points for g=2 or so.
      self._fitness = 40 + (10 * 2*g/(2*g+1))

      print (f"Survival + gradient = {self._fitness:.2f} points")
      shape = self.shape()
      if (only_TH and (shape != "TH")):
          return (self._fitness)	# Don't even try any children.

      # From how many locations can you regrow a small piece? Each
      # location will start with a narrow spread of [M]. Instead of [M]=0-2,
      # we'll divide the worm into (e.g.) 5 pieces of [0-.4], [.4-.8], [.8-1.2],
      # [1.2-1.6] and [1.6-2].
      child_points=0; self._n_good_children=0
      for piece in range(NUM_PIECES):
          print ("\nSim for piece #",piece)
          spread_M ("TH", piece, NUM_PIECES)
          status = self.sim (SIM_TIME)
          shape = self.shape()
          if (status!=None) and not (only_TH and (shape!="TH")):  #sim finished
              self._n_good_children += 1	# for debug only
              g = self.gradient(only_TH) / GRADIENT	# unitless gradient
              print (f"Piece #{piece}-> unitless gradient={g:.2f}")
              # Map g=0 => 0 points, g=1->.66, g=inf=1 and then scale.
              child_points += (50/NUM_PIECES) * 2*g/(2*g+1)
      print ("Child regrowth: {}/{} and {:.2f}/50 points".format(
		self._n_good_children, NUM_PIECES, child_points))
      self._fitness += child_points
      print (f"Total fitness={self._fitness:.2f}")
      return (self._fitness)

  # Look at the profile of [M] and deduce where the worm's heads and tails are.
  # Return a string such as "TH" (hopefully), or "HTH" (for a two-headed worm).
  # Note that Vmem is irrelevant; we only look at [M].
  # The general idea is we walk the cells from left to right; we end a rising
  # segment when we see a new [M] that is at least DELTA smaller than the
  # largest value on the segment (which is usually but not always the [M] at
  # the segment's start). A falling segment is similar.
  def shape(self, sim_results=True):
    if (sim_results==None):
        return ("DNF")		# Sim did not finish
    if (sim_results==False):
        return ("NoGrd")	# No gradient of Vmem

    DELTA=.02	# Ignore any changes in [M] that are less then DELTA.
    shape = ''	# Start with an empty string and add to it as we go.
    mode = 'X'	# Is the current segment falling(F), rising(R) or not set(X)?
    M = sim.ion_i['M']
    mMin = sim.cc_cells[M,0]	# Running min & max [M] on the current segment.
    mMax = sim.cc_cells[M,0]
    n_cells=sim.cc_cells.shape[1]
    for cell in range (1,n_cells):	# Walk from left to right, and
        mNew = sim.cc_cells[M,cell]	# analyze the current cell's [M]
        # If we're walking a rising segment, then we've already outputted the
        # "H" corresponding to the end of the segment that we've not reached
        # yet, but that has to come at some point. (Startup is a special
        # case... see below).

        # print (f'M[{cell}]={mNew}...')
        # Rising segment ends because we see a fall in [M]. This means that the
        # segment's H (which we've already outputted) is the cell immediately
        # to our left. We output a T, for the tail at the end of the segment we
        # now start (we'll find the actual tail cell eventually).
        if (mode=='R') and (mNew < mMax-DELTA):
            mode='F'; shape=shape+'T'
            mMin=mNew; mMax=mNew
            # print ('R->F, output T')

        if (mode=='F') and (mNew > mMin+DELTA):	# Ditto, for a falling segment
            mode='R'; shape=shape+'H'
            mMin=mNew; mMax=mNew
            # print ('F->R, output H')

        # Two special cases for the start of a worm. When we see the initial
        # rise in [M], we output both the T (for cell #0), and also the H (for
        # the eventual head that we haven't seen yet).
        # Note that we don't decide whether this initial segment is rising or
        # falling until we see >DELTA of [M] change... which may take a few
        # cells. So [M]= [.999 .987 1.012  0.999 1.001] will be TH.
        if (mode=='X') and (mNew>mMin+DELTA):
            mode='R'; shape=shape+'TH'
            mMin=mNew; mMax=mNew
            # print ('Initial TH, mode=R')

        if (mode=='X') and (mNew<mMax-DELTA):	# Ditto for an initial
            mode='F'; shape=shape+'HT'		# falling segment.
            mMin=mNew; mMax=mNew
            # print ('Initial HT, mode=F')

        mMin = min (mMin, mNew)	# Update the running min & max [M]
        mMax = max (mMax, mNew)	# along the current segment.

    return (shape)

  def __str__ (self):
      str = f"{self.PD['num_cells']} cells" \
	  + ("(F" if self.PD['full_body'] else "(E") \
	  + ("G), " if self.PD['do_gen_decay'] else "), ") \
	  + (f"shape={self.shape()}, " if self.shape()!="" else "") \
	  + (f"Fitness={self._fitness:.2f}, " if (self._fitness>-1) else "") \
	  + (f" ({self._n_good_children}/20), " if (self._n_good_children>=0) else "") \
	  + f"GJ_scale={self.PD['GJ_scale']:.3f}, " \
	  + f"kM={self.PD['kM']:.2f}, N={self.PD['N']}, qM={self.PD['M_valence']}, " \
	  + f"GD_tau={self.PD['GD_tau']:.1f}, "
      SAE = f"SAE={self.PD['stronger_at_end']:.2f}"
      pres = f"{self.PD['VNChead_pres']}/{self.PD['VNCtail_pres']}"
      str += (SAE if (SAE!="SAE=1.00") else "") \
	  +  ("head/tail pres={pres}" if (pres!="0/0") else "")
      return (str)

############################################################
# Basic code for spreading a morphogen M around the worm.
############################################################

# Sanitize() produces versions of cc_init and Vm_init for spread_M(). An initial
# simulation may come up with a 2H Vmem, or have a normal Vmem but a 2H [M]
# profile (really! See worm_M_weird.txt for an example). Sanitize() changes
# that sim result into a nice monotonic cc_init and Vm_init.
# How? Simple. Start with the existing sim.cc_cells. Our own cc_init and Vm_init
# will always take their shape/size from sim.cc_cells. If sim.cc_cells shows
# monotonic TH profiles of Vm, M and Na, then we're done -- simply copy
# sim.cc_cells to our own cc_init and Vm_init, well and good. Any other profile,
# and we invoke the simple-but-stupid rules:
# - Note the min and max values of Vmem, [M] and [Na] across the entire worm
#   from sim.cc_cells and sim.Vm.
# - Stuff the min(max) value of Vmem into Vmem_init[0](-1).
#   Linearly interpolate Vmem in the other cells. Stuff/interpolate [M] and
#   [Na] similarly from sim.cc_cells into our own cc_init.
#
# This simple strategy leaves a lot to be desired; real profiles are rarely
# linear. Even in cases like an original HT profile (where we could easily just
# reverse the columns) we don't try.
#
# Why do we only try to set Vmem, [M] and [Na]? Because of how spread() works.
# - The only information we really provide to spread_M() is the Na and M rows;
#   spread_M() sets the Cl and K rows itself, as follows.
# - Always let [Cl] be dictated by Vmem (i.e., the Nernst voltage). Physics
#   demands this.
# - Then pick [K] in each cell to be whatever it needs to be to come up with
#   the desired Vmem. Typically, cell-internal [Na] and [M] are much smaller
#   than [K], and so any sanitizing we do to [Na] and [M], while making
#   relatively large changes in *those* concentrations, will really not affect
#   [K] very much. I.e., whatever we set [K] to is going to be pretty much what
#   it started as anyway.
# - Set [P] to whatever the sim said -- which had better also be the value in
#   self.PD['int_P'].
def sanitize (cc_cells):
    np.set_printoptions (formatter={'float': "{:.3f}".format})
    print ("Sanitizing...")
    global cc_init, Vm_init
    cc_init = cc_cells.copy()
    Vm_init = sim.compute_Vm (cc_cells).copy()

    Na=sim.ion_i['Na']; M=sim.ion_i['M']

    # If we're already nice and monotonic, then we're all done.
    if (np.all ((np.diff(Vm_init)>=0) & (np.diff(cc_cells[M])>=0) \
				      & (np.diff(cc_cells[Na])<=0))):
        return

    # Otherwise, just do a linear spread
    n_cells = Vm_init.size
    Vm_init    = np.linspace (Vm_init.min(), Vm_init.max(), n_cells)
    cc_init[M] = np.linspace (cc_cells[M].min(),  cc_cells[M].max(),  n_cells)
    cc_init[Na]= np.linspace (cc_cells[Na].max(), cc_cells[Na].min(), n_cells)
    #print ("Sanitized => ", cc_init)
    assert (np.all(np.diff(cc_init[Na])<=0) and np.all(np.diff(cc_init[M])>=0))

def biggest_monotonic_seg (cc_cells):
    np.set_printoptions (formatter={'float': "{:.3f}".format})
    print ("Getting biggest monotonic segment...")
    global cc_init, Vm_init
    Na=sim.ion_i['Na']; M=sim.ion_i['M']
    cc_init = cc_cells.copy()
    if (cc_init[M,0] > cc_init[M,-1]):
        cc_init = cc_init[:,::-1]	# flip the cells head-vs-tail
    Vm_init = sim.compute_Vm (cc_init).copy()

    Vm_good = (np.diff(Vm_init)>=0)
    M_good  = (np.diff(cc_cells[M])>=0)
    Na_good = (np.diff(cc_cells[Na])<=0)
    all_good = Vm_good & M_good & Na_good

    curr_start=-1; curr_len=0
    best_start=-1; best_len=0
    for i in range(Vm_good.size):
        if all_good[i]:
            curr_len += 1
            if curr_start < 0:
                curr_start=i
            #print (f"All_good[{i}]=True => curr_start={curr_start}, len={curr_len}")
            if (curr_len > best_len):
                best_start = curr_start
                best_len = curr_len
                #print ("	It's a new record!")
        else:
            curr_start=-1
            curr_len=0
    print (f"Best start={best_start}, length={best_len}")
    return (best_start,best_len)

# These help spread() initialize cc_cells. They're just the end results from
# the test1 benchmark; once our own sim passes someone usually calls sanitize()
# to use those numbers instead.
def set_default_spread_M():
    global cc_init, Vm_init
    cc_init = np.array ([[8.1, 7.2, 5.3, 4.5, 4.2], [0,0,0,0,0], [0,0,0,0,0],
        		 [0,0,0,0,0], [.17, .25, .7, 1.4, 2.4]])
    Vm_init = np.array ([-64, 9, 28, 34, 36])/1000

# Spread [M], [Na], [K], [Cl] according to the pattern string 'shape'. Roughly:
# - "TH"  would set cc_cells[M,0:5] to cc_init[M,0:5]
# - "THT" would set cc_cells[M,0:5] to cc_init[M,[0,2,4,2,0]]
# I.e., we're assuming that the [M]-vs-cell_number profile in cc_init[] is
# reasonable (which is sort-of-correct if it came from sanitize()).
# Specifically, for each worm cell w, we:
# - use 'shape' to map worm cell[w] into a corresponding cc_init index i.
# - set cc_cells[M,w]  = cc_init[M,i] (where i might be a fraction)
# - set cc_cells[Na,w] = cc_init[Na,i]
# - set cc_cells[Cl,w] from Vm_init[i] (since [Cl] is at equilibrium)
# - set cc_cells[K,w] to the correct charge so that the cell is at Vm_init[i]
# See the comment above sanitize() for our global strategy.
# The parameters piece,n_pieces are mostly used by fitness(); if, e.g., piece=1
# and n_pieces=10 then you will build an entire worm that has the very narrow
# range of [M] that would typically be found near the tail (i.e., near cell 0).
def spread_M (shape, piece=0, n_pieces=1):
    global cc_init, Vm_init

    # Note num_cellsI (for cc_init) vs. num_cellsW (for the worm). We can use
    # a 5-cell cc_init to initialize a 30-cell worm! (Only for the first such
    # sim; after which we push the 30-cell sim results into cc_init).
    num_cellsI = cc_init.shape[1]
    Na=sim.ion_i['Na']; M=sim.ion_i['M']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']

    # Usually, a "T" will mean cell #0 and "H"=cell #4. But if we're doing,
    # e.g., piece #1/5 then we'll only spread from T=1 to H=2. Worm.fitness()
    # uses this capability to create fragments.
    tail = (num_cellsI-1) * (piece/n_pieces)
    head = tail + (num_cellsI-1)/n_pieces

    # Indices[w] is the (potentially fractional) cc_init index for worm[w]
    # So worm_cell[w] will take its [M] from cc_init[M, indices[w]] (and
    # similar for Na and Vmem).
    num_cellsW = sim.cc_cells.shape[1]
    indices = np.empty(num_cellsW)

    # Fill in indices[]. For, e.g., THTH, we take each of the three segments
    # TH, HT and TH from it, and fill in the corresponding part of indices[].
    for pos in range (len(shape)-1):	# One letter at a time...
        # This single segment of the pattern will go into worm cells[w0,w1].
        w0 = round (pos    *(num_cellsW-1)/(len(shape)-1))
        w1 = round ((pos+1)*(num_cellsW-1)/(len(shape)-1))
        # Are we putting a TH pattern into cells[w0,w1), or a HT?
        seg = shape[pos:pos+2]
        assert ((seg=="TH") or (seg=="HT"))
        i0 = tail if (seg=="TH") else head	# cc_init idx for worm cell[w0]
        i1 = head if (seg=="TH") else tail	# cc_init idx for worm cell[w1]
        spread = np.linspace (i0, i1, w1-w0+1)	# Spreads this entire segment.
        indices[w0:w1+1] = spread

    #print ("For shape {shape} piece {piece}/{n_pieces}, spreading to indices {indices}")

    # Now set sim.cc_cells accordingly.
    sim.cc_cells[Na] = np.interp (indices, range(num_cellsI), cc_init[Na])
    sim.cc_cells[M]  = np.interp (indices, range(num_cellsI), cc_init[M])
    Vm_target = np.interp (indices, range(num_cellsI), Vm_init)
    #print ("Spread: desired final Vmem=", Vm_target)

    # Compute [Cl]_int. That's easy -- just make Vnernst match Vmem.
    # Vn = 26mV * ln(Cint / Cext), so Cint = Cext * exp(Vn / 26mv)
    k26mV = sim.GP.R * sim.GP.T / sim.GP.F	# a.k.a. kT/q
    sim.cc_cells[Cl] = sim.cc_env[Cl] * np.exp (Vm_target / k26mV)

    # Pick [K] so that the desired Vmem is correct. We could do the
    # simple physics. But interpolation is even easier.
    sim.cc_cells[K]=0
    K0 = sim.compute_Vm(sim.cc_cells)
    sim.cc_cells[K]=10
    K10 = sim.compute_Vm(sim.cc_cells)
    # Simple interpolation: v = k0 + (k10-k0)*k/10, or k = (v-k0)*10/(k10-k0)
    sim.cc_cells[K] = (Vm_target-K0)*10/(K10-K0)

    np.set_printoptions (formatter={'float': "{:.3f}".format})
    mV = sim.compute_Vm(sim.cc_cells)*1000
    print (f"Spread {shape} set Vm={mV}mV\n\tand M = {sim.cc_cells[M]}")
		

############################################################
# Basic code to help simulate worms.
############################################################

# This is typically the first simulation we do for any parameter set.
# Assume that somebody has already called 
# We do a simple TH simulation from a default [M] spread. If the sim completes
# and builds a gradient, then great -- use the results to call sanitize() and
# tailor spread_M() for *this* parameter set. Then return True.
# But if the sim dies or does not create a gradient, then just return False.
def sim_start(W, sim_time=SIM_TIME, only_TH=False, resim=False):
    print ("Starting simulation for a new worm")
    set_VNC_WT (W)				# Assume a normal WT VNC

    # See if TH is stable. If not, then return -- nothing else is worth checking
    set_default_spread_M()
    spread_M ("TH")
    sim_results = W.sim (sim_time)

    messages (W, sim_results, only_TH and not resim)
    if (not sim_results) or (only_TH and not resim and (W.shape()!="TH")):
        return (False)

    # We converged. Save our final cc_cells[] to help init future worms
    print ("Initial simulation converged!")
    sanitize (sim.cc_cells)
    if (resim):
        print ("Resimming with corrected initial spread")
        spread_M ("TH")
        sim_results = W.sim (sim_time)
        messages (W, sim_results, only_TH)
        if (not sim_results) or (only_TH and (W.shape()!="TH")):
            return (False)
    return (True)

def messages (W, sim_results, only_TH):
    if (sim_results==None):
        print ("Initial simulation did not complete")
    if (sim_results==False):
        print ("Initial simulation did not produce enough gradient")
    if (only_TH and (W.shape() != "TH")):
        print (f"Successful sim resulted in {W.shape()} instead of TH")

# Return the index of the cell with the lowest [M]. I.e., the Vmem "tail" It's
# currently used only for the hybrid paper, but seems general enough to go here.
def M_min ():
    M=sim.ion_i['M']
    min_idx = np.argmin (sim.cc_cells[M,:])
    num_cells = sim.cc_cells.shape[1]
    if ((min_idx==0) or (min_idx== num_cells-1)):
        return (min_idx)
    # Take three y values; y0, y1 and y2.
    y0 = sim.cc_cells[M,min_idx-1]
    y1 = sim.cc_cells[M,min_idx]
    y2 = sim.cc_cells[M,min_idx+1]
    # Fit to the polynomial y=a2*x^2 + a1*x + a0.
    a0=y1
    a1=(y2-y0)/2
    a2=(y2-2*y1+y0)/2
    # The min is where dy/dx=0, which is at x=-a1/(2*a2)
    min2 = -a1/(2*a2)
    print ("Min [M] at cell #", round(min_idx+min2,2), '\n')
    return (min_idx+min2)

# Implement the VNC pressure on Vmem. The VNC's "head"("tail") tries to
# de(hyper)polarize Vmem. The parameters:
#	'head' is True(False) if we're working with the VNC's head(tail).
#	'cell' is which cell to put pressure on.
# For a normal WT worm, we would be called as (False, 0) and also as
# (True, num_cells-1). However, e.g., a HTH worm would call us as (True, 0),
# (False, num_cells/2) and (True, num_cells-1).
def set_VNC_pres (W, amount, head, cell):
    # What range of cells should we touch? In principle, 10% of the worm's
    # length, centered on 'cell'.
    n_cells = max (round(W.PD['num_cells']/10), 1)	# num of cells to touch
    n_cells = max (round(W.PD['num_cells']/5), 1)	# num of cells to touch
    if (cell < 0):			# in case we're given
        cell = cell + W.PD['num_cells']	# cell=-1 to mean the head.
    c0 = max (round(cell-n_cells/2), 0, cell+1-n_cells)	# [c0,c1) is the range
    c1 = min (c0 + n_cells, W.PD['num_cells'])		# of cells to touch.

    # VNC pushes the head(tail) more positive(negative), so we implement
    # head(tail) pressure by increasing Na(K), whose Vnernst is
    # positive(negative).
    # Assume that setup_Bitsey_network() built K and Na ion-channel gates as the
    # first two ion-channel gates. We'll tweek their offset fields.
    Na=sim.ion_i['Na']; K=sim.ion_i['K']
    OFFSET=4
    if (head):
        print (f"\tSet Na IC_gate[{c0}:{c1}) offset to head pres={amount}")
        gate = sim.IC_gates[1]	# Assume the Na gate was declared second
    else:
        print (f"\tSet K  IC_gate[{c0}:{c1}) offset to tail pres={amount}")
        gate = sim.IC_gates[0]	# Assume the K gate was declared first
    gate.params[OFFSET,c0:c1] = amount

def clear_VNC_pres (W):
    OFFSET=4
    K_gate = sim.IC_gates[0]
    K_gate.params[OFFSET,:]=0		# Turn off the K offset
    Na_gate = sim.IC_gates[1]
    Na_gate.params[OFFSET,:] = 0	# And ditto for Na

def set_VNC_WT (W):
    print ("Setting WT VNC pressure")
    clear_VNC_pres (W)			# Make sure the previous shape is gone!
    set_VNC_pres (W, W.PD['VNChead_pres'], head=True,  cell=-1)
    set_VNC_pres (W, W.PD['VNCtail_pres'], head=False, cell=0)

def set_VNC_HTH (W, tail):
    print (f"Setting HTH VNC pressure with tail at cell #{tail}")
    clear_VNC_pres (W)			# Make sure the previous shape is gone!
    set_VNC_pres (W, W.PD['VNChead_pres'],   head=True,  cell=0)
    set_VNC_pres (W, W.PD['VNChead_pres'],   head=True,  cell=-1)
    # Double the tail pressure, since we have two VNCs joining at the tail.
    set_VNC_pres (W, 2*W.PD['VNCtail_pres'], head=False, cell=tail)

############################################################
# START OF EXHAUSTIVE
############################################################

# Inputs: one function and several tuples of parameters.
# Run the function lots of times -- once for every member of the cross product
# of the tuples. We use this to automatically explore a wide parameter space.
# We just run the function and don't check any return values; typically, the
# function will print out its go/no-go results and we'll grep through a big
# text file.
#
# With the default arguments, there are
# 	4(GJs)*3(kM)*3(N)*4(Mv)*4(GDt)*4(sae)*2(sp)*4(VNChead)*4(VNCtail)
#	  = 73728 combinations per worm size, or 18432 per worm/GJ size, or
#	    6144 per worm/GJ size/kM value	
def exhaustive (func, range_to_try=[0,1], do_gen_decay=True, do_full_body=True,
	num_cells=[5,30,100],
	GJ_scale=[.001,.01,.1,1], kM = [.3, .5, .8], N = [2, 5, 8],
	gate_GJs=[None],
	M_valence = [-1, -2, -3, -4], GD_tau = [1, 10, 100, 1000],
	stronger_at_end = [100, 10, 3, 1],	# stronger ion chans at the ends
	scale_pumps = [0,1],			# reduce pump strength with SAE
	VNChead_pres = [0, .1, .2, .3],		# Extra D_Na at VNC head
	VNCtail_pres = [0, .1, .2, .3]): 	# Extra D_K at VNC tail

    print (f"Num_cells={num_cells}, GJ_scale={GJ_scale}, kM={kM}, N={N}, M_val={M_valence}, GD_tau={GD_tau}, SAE={stronger_at_end}, VNC pres={VNChead_pres}/{VNCtail_pres}")

    # Create the huge cross product. 'Worms' will be a list, where each list
    # item is one member of the cross product of parameter tuples -- i.e., one
    # worm to check out.
    from itertools import product
    from time import strftime
    prod = product (num_cells, GJ_scale, gate_GJs, kM, N,
                              M_valence, GD_tau, stronger_at_end, scale_pumps,
                              VNChead_pres, VNCtail_pres)
    worms = list ([p for p in prod])

    for idx in range_to_try:
        W_param = worms[idx]
        (num_cells, GJ_scale,gate_GJs, kM,N, M_valence, GD_tau, stronger_at_end,
         scale_pumps, VNChead_pres, VNCtail_pres) = W_param
        W = Worm()
        print (f"Starting worm #{idx} at {strftime('%b %d %H:%M')}")
        set_default_spread_M()	# needed for repeatability between worms

        W.PD['do_gen_decay'] = do_gen_decay; W.PD['full_body'] = do_full_body
        W.PD['num_cells']=num_cells; W.PD['GJ_scale']=GJ_scale;
        W.PD['gate_GJs']=gate_GJs; W.PD['kM']=kM
        W.PD['N']=N; W.PD['M_valence']=M_valence; W.PD['GD_tau']=GD_tau
        W.PD['stronger_at_end']=stronger_at_end; W.PD['scale_pumps']=scale_pumps
        W.PD['VNChead_pres']=VNChead_pres; W.PD['VNCtail_pres']=VNCtail_pres

        # We have a good worm object; now build a Bitsey network.
        W.setup_Bitsey_network ()

        # Finally, do whatever sims we want.
        print ("Built worm",W)
        func (W, idx)

def exhaustive (func, range_to_run, worm=None, **change_list):
    print (f"Exhaustive change list is {change_list}")
    keys = list (change_list.keys())
    choices = [change_list[k] for k in keys]

    # Create the huge cross product. 'Worms' will be a list, where each list
    # item is one member of the cross product of parameter tuples -- i.e., one
    # worm to check out.
    from itertools import product; from time import strftime
    prod = product (*choices)
    worms = list ([p for p in prod])

    for idx in range_to_run:
        W_param = worms[idx]
        W = (Worm() if worm==None else worm.copy())
        print (f"Starting worm #{idx} at {strftime('%b %d %H:%M')}")
        set_default_spread_M()	# needed for repeatability between worms

        for i in range(len(keys)):
            W.PD[keys[i]] = W_param[i]

        # We have a good worm object; now build a Bitsey network.
        W.setup_Bitsey_network ()

        # Finally, do whatever sims we want.
        print ("Built worm",W)
        func (W, idx)

# Usages:
#	python3 worm.py func-to-run type n_cells start_idx end_idx
#	python3 worm.py func-to-run EG 30 17 23
# 	"E" -> ion channels only at ends, vs. "F" = full (at all cells).
#	"G" = use gen/decay.
# Runs exhaustive(), taking the arguments to exhaustive() from the command line.
def run_exhaustive():
    import sys
    (func, type, n_cells, start, end) = sys.argv[1:]
    print (type, n_cells, start, end)
    assert ((type.upper()=='E') or (type.upper()=='EG')
	or (type.upper()=='F') or (type.upper()=='FG'))
    full = (type[0].upper()=='F')
    gen =  (len(type)==2)
    exhaustive (eval(func), range(int(start),int(end)), do_full_body=full,
		do_gen_decay=gen, num_cells=[int(n_cells)], N=[2,5,8],
		GJ_scale=[.2, .8, 3, 12, 50],
		VNCtail_pres=[.025, .05, .1, .15, .2])
    print ("All worms complete!")
    #GJ_scale=[.001, .01, .05, .1, .3, 1, 1.5],

# Used for running regression tests.
#if (__name__=="__main__"):
#    regress_command_line(); quit()

#exhaustive (set_VNC_HTH, range(3), num_cells=(3,4), GJ_scale=(5,6))
