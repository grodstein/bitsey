#!/usr/bin/env python3

# Remove test_ED example; add QSS_test.
# Copyright 2018 Alexis Pietak and Joel Grodstein
# See "LICENSE" for further details.

#    import pdb; pdb.set_trace()
import re, numpy as np
import sim, eplot as eplt, edebug as edb

#####################################################
# QSS exploration
# A new example that helps understand the time between QSS and SS, mostly done
# to try to replicate Juanita's observation that she could hyperpolarize tumor
# cells but that they would return to depolarized in 10 minutes or so.
#####################################################

# A time-gate that returns a gating of 1 until t=300, and then 'scale'. We use
# it to alter the permeability of an ion channel and thus move Vmem at t=300.
# We usually instantiate it with scale being a vector of scales, one per cell,
# so that each cell moves to a different Vmem at t=300.
class QSS_check (sim.Gate):
  def __init__ (self, ion, scale):
    sim.Gate.__init__ (self, dest=self.GATE_IC, out_ion=ion)
    self.scale = np.ones_like(sim.cc_cells[0]) * scale

  def func (self, cc_ignore, Vm, deltaV_GJ_ignore, t):
    return (np.ones_like(Vm) if (t<300) else self.scale)

def setup_QSS_test(p):
    num_cells=5
    sim.init_big_arrays (num_cells, 0, p)
    p.no_dumps = True
    p.use_implicit = False		# No implicit integrator
    p.adaptive_timestep = True		# Yes to adaptive time_step
    p.sim_integ_max_delt_Vm = .0001	# Volts/step
    p.sim_integ_max_delt_cc = .00005
    p.time_step = .00002		# min time step for explicit integration
    Na=sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']

    # Experiment 1.
    # Initialize cells so that they reach SS almost immediately. These values
    # are for the default values of 1e-18 for all channels, nicely depolarized
    # at about -13mV. They use the default cATP=1.5
    '''sim.cc_cells[Na,:] = 5.88984
    sim.cc_cells[K,:]  = 160.6111
    sim.cc_cells[Cl,:] = 86.50495'''
    
    # Ditto, but for cATP=.02 (experiment #3).
    p.cATP=.02
    sim.cc_cells[Na,:] = 102.594
    sim.cc_cells[K,:]  =  75.985
    sim.cc_cells[Cl,:] =  98.582

    QSS_check (K, [1,5,10,100,200])


    # Experiment #2, no ATP. These concentrations are chosen so the the system
    # immediately goes into the equilibrium state. The sim parameters are to
    # ensure that we actually reach the equilibrium values, rather than
    # oscillating around them.
    '''p.cATP=0
    p.use_implicit=False; p.no_dumps=False; p.adaptive_timestep=False
    sim.cc_cells[Na,:] = 183.987560
    sim.cc_cells[K,:]  = 6.344035
    sim.cc_cells[Cl,:] = 110.333573

    # The default is cATP=1.5 mol/m3. All channels at 1e-18 => Vmem=-13mV
    # The Vnernst are from Cenv: [Na]=145, [K]=5, [Cl]=140
    # As we approach cATP=0, we approach Vn,Na = Vn,K = Vn,Cl = Vmem.
    # pATP	Vmem	Na	K	Cl	Vn,Na	Vn,K	Vn,Cl
    # 0		-6.5mV	184	  6	110	-6	- 5	- 6
    # .01	-8mV	143	 41	104	.4	-55	- 8
    # .02	-9.3mV	103	 76	99	9	-71	- 9
    # .05	-12.5mV	19	149	88	53	-89	-12
    # .1	-12.7mV	11	156	87	68	-90	-12
    # 1.5	-12.9mV	5.9	161	86.5	84	-91	-13
    # 3		-12.8mV	5.6	161	86.5	85	-91	-13
    # 30	-12.8mV	5.3	161	86.4	87	-91	-13'''

def post_QSS_test (GP, t_shots, cc_shots):
    print (cc_shots[-1]*1000)
    #return
    eplt.plot_Vmem (t_shots, cc_shots)
    eplt.plot_ion (t_shots, cc_shots, 'Na')
    eplt.plot_ion (t_shots, cc_shots, 'K')
    eplt.plot_ion (t_shots, cc_shots, 'Cl')
    print ("Done plotting")


######################################################
# Hodgkin-Huxley gating
######################################################

# The big picture:
# We gate the Na and K ICs with the usual HH model; Na via m^3*h and K via n^4.
# This is the functions HH_Na_gating() and HH_K_gating().
# But where do m, h and n come from?

# We'll have three ligands m, h and n. They are not "real" ligands; they exist
# only to calculate how to gate the Na and K ion channels. Thus, they all have
# Z=0 and they don't diffuse through ICs or GJs (so they stay in whatever cell
# they started in).
# Each cell creates its m, h and n via gen/decay. Like any gen/decay gate, we
# thus have gating functions -- HH_M_gen_decay(), HH_H_gen_decay() and
# HH_N_gen_decay() -- that return the net generation of their species at any
# point in time. They do this calculation by the classic method of first
# calculating (e.g., for M) M_tau and M_inf and then returning (M_inf-M)/M_tau.
# However, instead of taking the published equations for M_inf and M_tau, I
# tweaked my own (I couldn't get the standard ones to work).

# Finally, we implement a little Na-generation gate that kicks off our one and
# only AP at about t=200.

# The HH gating functions that gate the Na and K ion channels.
# The Na IC is gated by m^3 * h; the K IC by n^4. Since we have m, h and n
# simply being ligands, then Na and K are gated with simple ligand-gated ion
# channels. Here are the gating functions (the ones that are actually called
# at runtime).
# We return an array[num_cells] of numbers in [0,1] -- the gating numbers.
class HH_Na_gating (sim.Gate):
  def __init__ (self):
    sim.Gate.__init__ (self, dest=self.GATE_IC, out_ion=sim.ion_i['Na'])

  def func (self, cc, Vm_ignore, deltaV_GJ_ignore, t_ignore):
    m=sim.ion_i['m']; h=sim.ion_i['h']
    gate = np.power(cc[m,:], 3.0) * cc[h,:]	# m^3 * h
    # In principle, m and h are always in [0,1]. In practice, if m_tau is very
    # small then the numerical integrator can set [m] slightly <0. But since
    # m^3*h scales the GHK fluxes, then a negative [m] can *reverse* the flux
    # direction of Na, and suddenly our nice Na flux that GHK uses as a
    # stabilizing force instead makes the system run away :-(.
    return (np.clip (gate, 0, 1))

class HH_K_gating (sim.Gate):
  def __init__ (self):
    sim.Gate.__init__ (self, dest=self.GATE_IC, out_ion=sim.ion_i['K'])

  def func (self, cc, Vm_ignore, deltaV_GJ_ignore, t_ignore):
    n=sim.ion_i['n']
    return (np.power (cc[n,:],4.0))		# n^4

# Next, the HH gen/decay gating functions that control the concentrations of
# m, h and n. There are two steps for each:
# 1. Create (e.g., for m) m_inf and m_tau as functions of Vmem. They are
#    typically given by mathematical functions. Instead, we build them here with
#    simple linear interpolation.
# 2. dm/dt = (m_inf-m)/tau_m. And dm/dt is the net-generation rate that our
#    gen/decay gate must return.
class HH_M_gen_decay (sim.Gate):
  def __init__ (self):
    sim.Gate.__init__ (self, dest=self.GATE_GD, out_ion=sim.ion_i['m'])

  def func (self, cc, Vm, deltaV_GJ_ignore, t_ignore):
    M = cc[sim.ion_i['m'],:]
    M_inf = np.interp (Vm, [-1,-.050, .010, 1], [.1,.1,1,1])
    M_tau = np.ones_like (Vm)*1
    #print ("M_inf={}, tau={:.6g}".format(M_inf, M_tau[0]))
    return ((M_inf - M) / M_tau)

class HH_H_gen_decay (sim.Gate):
  def __init__ (self):
    sim.Gate.__init__ (self, dest=self.GATE_GD, out_ion=sim.ion_i['h'])

  def func (self, cc, Vm, deltaV_GJ_ignore, t_ignore):
    H = cc[sim.ion_i['h'],:]
    H_inf = np.interp (Vm, [-1,-.050, -.045, 1], [1,1,.001,.001])
    H_tau = np.ones_like (Vm)*10
    return ((H_inf - H) / H_tau)

class HH_N_gen_decay (sim.Gate):
  def __init__ (self):
    sim.Gate.__init__ (self, dest=self.GATE_GD, out_ion=sim.ion_i['n'])

  def func (self, cc, Vm, deltaV_GJ_ignore, t_ignore):
    N = cc[sim.ion_i['n'],:]
    N_inf = np.interp (Vm, [-1,-.050, .010, 1], [.1,.1,1,1])
    N_tau = np.ones_like (Vm)*10
    return ((N_inf - N) / N_tau)

# The gating function for Na gen/decay. We use it to slowly ramp up Vmem so as
# to trigger an AP at about t=200.
class HH_delayed_gen (sim.Gate):
  def __init__ (self):
    sim.Gate.__init__ (self, dest=self.GATE_GD, out_ion=sim.ion_i['Na'])

  def func (self, cc_ignore, Vm, deltaV_GJ_ignore, t):
    arr = np.zeros_like(Vm)
    if ((t > 200) and (t<205)):
        arr[0] = .03
    return (arr)

# This one is no longer used. While it does make Vmem increase in a nice linear
# manner, it also makes [Na] increase exponentially!
def HH_delayed_gen2 (g, cc, Vm, t):
    if (t > 2000):
        return (np.ones_like(Vm)*(t-2000)*.0001)
    return (np.zeros_like(Vm))

# The HH entry point.
def setup_HH (p):
    num_cells=1
    n_GJs = 0
    p.use_implicit = True	# Use the implicit integrator
    # Max_step ensures that the implicit integrator doesn't completely miss the
    # skinny input pulse of [Na] that kicks off the AP. Rel_tol and abs_tol get
    # tightened because otherwise we get nonphysical results such as Vmem
    # dropping down to -250mV even when the lowest Vnernst is -70mV.
    # Also note that the AP is quite skinny -- so if you run the sim for too
    # long, the plotting granularity becomes too wide to catch the AP!
    p.max_step=2; p.rel_tol=1e-5; p.abs_tol=1e-8

    # Each cell gets 3 new 'species' m, h and n. They are neutral and stay in
    # their original cells.
    sim.init_big_arrays (num_cells, n_GJs, p, ['m', 'h', 'n'])
    m=sim.ion_i['m']; h=sim.ion_i['h']; n=sim.ion_i['n']
    Na=sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']; P=sim.ion_i['P']
    sim.z_array[[m,h,n]] = 0
    sim.Dm_array[[m,h,n],:]= 0		# m, n, & h just stay in their cells.
    #sim.GJ_diffusion[[m,h,n]]=0	# kind of moot, since there are no GJs.

    # This test case was built around D_Na = 1e-18 and D_K = 20e-18 (which
    # makes our initial cell concentrations pretty stable). But that was before
    # we added our HH gating into the Na and K ICs.
    # At steady-state baseline (i.e., waiting for an AP), we have m^3h=1e-3
    # and n^4=1e-4. So change D_Na and D_K to make them right at baseline.
    sim.Dm_array[Na,:] = 1.0e-15
    sim.Dm_array[K,:] = 20.0e-14

    # Initial concentrations. Our sim will settle to whatever is stable. We
    # speed that up by seeding the final settled values from a previous sim.
    # This seeding means that we can kick off the AP nice and early, without
    # needing to wait a long time for things to stabilize first. Perhaps more
    # importantly, it means that we don't start off the sim at a Vmem that
    # would kick off an AP.
    sim.cc_cells[K, :] = 87.572
    sim.cc_cells[Cl,:] = 15.669
    sim.cc_cells[P,:] = 79.652

    # Our desired Vmem is -58.5mV. Pick Na by first setting it for initial
    # charge neutrality...
    sim.cc_cells[Na,:]  = sim.cc_cells[Cl,:]+sim.cc_cells[P,:]-sim.cc_cells[K,:]
    assert ((sim.cc_cells[Na,:]>0).all())

    # ... then offset [Na] a bit to get to -58.5mV initially.
    scale = (p.cell_sa*p.cm) / (p.F * p.cell_vol)
    sim.cc_cells[Na,:] = sim.cc_cells[Na,:] -.0585*scale

    # Initial baseline values of m,h,n. Again, these make sure that we start
    # the sim at baseline rather than in the middle of an AP.
    sim.cc_cells[m,:] = .1
    sim.cc_cells[h,:] = 1
    sim.cc_cells[n,:] = .1

    # Instantiate the gen/decay gates that generate [m,h,n] with a rate of
    # (e.g., for m) dm/dt=(m_inf-m)/m_tau.
    HH_M_gen_decay()
    HH_H_gen_decay()
    HH_N_gen_decay()

    # The IC gates: gate D_Na by m^3*h and D_K by n^4.
    HH_Na_gating()
    HH_K_gating()

    # Create a trickle of Na+ into the cell via generation to kick off the AP
    HH_delayed_gen()

def post_HH (GP, t_shots, cc_shots):
    #eplt.plot_Vmem (t_shots, cc_shots)
    # Dumps the ion concentrations at the beginning of the sim (the end-of-sim
    # values are always printed by default).
    #edb.dump (t_shots[0], cc_shots[0], edb.Units.mol_per_m3s, True)
    # Get end-of-sim Vnernst values as a sanity check.
    #edb.analyze_equiv_network (cc_shots[-1],GP)

    # In principle, the APs should be so fast the [Na], [K] and [Cl] don't
    # change much. Actually, they do change a bit.
    # eplt.plot_onecell (t_shots, cc_shots, 0, 'Na','K', 'Cl')

    # Look at m, h and n for debugging.
    #eplt.plot_onecell (t_shots, cc_shots, 0, ['m','h','n'])

    # Look at m^3*h and n^4, the final gates on the Na & K ICs.
    Na=sim.ion_i['Na']; K=sim.ion_i['K']
    m=sim.ion_i['m']; n=sim.ion_i['n']; h=sim.ion_i['h']
    Gna=[cc[m]**3 * cc[h]* sim.Dm_array[Na] for cc in cc_shots]
    Gk =[cc[n]**4 *sim.Dm_array[K, :] for cc in cc_shots]
    eplt.plot_onecell (t_shots, cc_shots, 0, [(Gna,'NaGate'), (Gk,'KGate')])

# Multiple HH cells, interconnected by GJs
def setup_cardiac_GJ (p):
    n_series=4; n_par=2
    G_GJ_ser=50; G_GJ_par=10

    num_cells = n_series+n_par
    n_GJs = num_cells-1
    p.use_implicit = True	# Use the implicit integrator

    # Max_step ensures that the implicit integrator doesn't completely miss the
    # skinny input pulse of [Na] that kicks off the AP. Rel_tol and abs_tol get
    # tightened because otherwise we get nonphysical results such as Vmem
    # dropping down to -250mV even when the lowest Vnernst is -70mV.
    p.max_step=2; p.rel_tol=1e-5; p.abs_tol=1e-8

    # Each cell gets 3 new 'species' m, h and n. They are neutral and stay in
    # their original cells.
    sim.init_big_arrays (num_cells, n_GJs, p, ['m', 'h', 'n'])
    m=sim.ion_i['m']; h=sim.ion_i['h']; n=sim.ion_i['n']
    Na=sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']; P=sim.ion_i['P']
    sim.z_array[[m,h,n]] = 0
    sim.Dm_array[[m,h,n],:]= 0		# m, n, & h just stay in their cells.
    sim.GJ_diffusion[[m,h,n]]=0

    # Add our series and parallel GJ connectivity.
    sim.GJ_connects['from'] = list(range(n_series-1)) + [n_series-1]*n_par
    sim.GJ_connects['to']   = range(1,num_cells)

    # Set GJ conductivity. P doesn't travel; most ions do, with conductivity
    # set by G_GJ_ser and G_GJ_par.
    sim.GJ_diffusion[:,0:n_series-1] *= G_GJ_ser	# The series chain
    sim.GJ_diffusion[:,n_series-1:]  *= G_GJ_par	# The branches

    # This test case was built around D_Na = 1e-18 and D_K = 20e-18 (which
    # makes our initial cell concentrations pretty stable). But that was before
    # we added our HH gating into the Na and K ICs.
    # At steady-state baseline (i.e., waiting for an AP), we have m^3h=1e-3
    # and n^4=1e-4. So change D_Na and D_K to make them correct at baseline.
    sim.Dm_array[Na,:] = 1.0e-15
    sim.Dm_array[K,:] = 20.0e-14

    # Initial concentrations. Our sim will eventually settle to whatever is
    # stable. We speed that up by seeding the final settled values from a
    # previous sim. This seeding means that we can kick off the AP nice and
    # early, without needing to wait a long time for things to stabilize first.
    # Perhaps more importantly, it means that we don't randomly start off the
    # sim at a Vmem that would kick off an AP.
    sim.cc_cells[K, :] = 87.572
    sim.cc_cells[Cl,:] = 15.669
    sim.cc_cells[P,:] = 79.652

    # Our desired initial Vmem is -58.5mV. We'll do that by tweaking Na. First
    # pick Na for initial charge neutrality...
    sim.cc_cells[Na,:]  = sim.cc_cells[Cl,:]+sim.cc_cells[P,:]-sim.cc_cells[K,:]
    assert ((sim.cc_cells[Na,:]>0).all())

    # ... then offset [Na] just a bit to get to -58.5mV initially.
    conc_per_volt = (p.cell_sa*p.cm) / (p.F * p.cell_vol)
    sim.cc_cells[Na,:] -= .0585*conc_per_volt

    # Initial baseline values of m,h,n. Again, these make sure that we start
    # the sim at baseline rather than in the middle of an AP.
    sim.cc_cells[m,:] = .1
    sim.cc_cells[h,:] = 1
    sim.cc_cells[n,:] = .1

    # Instantiate the gen/decay gates that generate [m,h,n] with a rate of
    # (e.g., for m) dm/dt=(m_inf-m)/m_tau.
    HH_M_gen_decay()
    HH_H_gen_decay()
    HH_N_gen_decay()

    # The IC gates: gate D_Na by m^3*h and D_K by n^4.
    HH_Na_gating()
    HH_K_gating()

    # Create a trickle of Na+ into the cell via generation to kick off the AP
    HH_delayed_gen()

def post_cardiac_GJ (GP, t_shots, cc_shots):
    edb.analyze_equiv_network (cc_shots[-1],GP)
    #eplt.plot_Vmem (t_shots, cc_shots)

######################################################
# End of Hodgkin Huxley tests.
######################################################

# 4 independent cells.
# Cell #0-3: generate A at rates from [10,20,50,100] moles/m3s respectively.
# All have A decay at 2/s, giving final [A] =  [5 10 25 50] (moles/m3)
# Then B is invert(A)
def setup_gen_decay (p):
    num_cells=4
    n_GJs = 0
    sim.init_big_arrays (num_cells, n_GJs, p, ['A', 'B'])

    # By default, new ions have D_ic=0; D_gj=1e-18; z=0, c_in=c_out=0.
    # [A] will reach 5,10,25,50 in cells 0,1,2,3 respectively.
    A=sim.ion_i['A']; B=sim.ion_i['B'];
    # Generation-rate units are moles/m3 per s; decay rates are 1/s.
    g0 = sim.GD_const_gate(sim.Gate.GATE_GD, A, gen=[10,20,50,100], decay=2)

    # B is an inv(A), with kVmax=50 moles/(m3*s) and kM=25 mole/m3
    g1 = sim.Hill_gate (sim.Gate.GATE_GD, B, A, inv=True,kM=25,N=2,kVMax=50)
    g2 = sim.GD_const_gate (sim.Gate.GATE_GD, B, decay=1)

def post_gen_decay (GP, t_shots, cc_shots):
    edb.dump (t_shots[-1], cc_shots[-1], edb.Units.mol_per_m3s, True)
    #eplt.plot_ion (t_shots, cc_shots, 'A')
    #eplt.plot_ion (t_shots, cc_shots, 'B')

# TEST #1:
# - No gap junctions (to isolate cells)
# - set membrane permeabilities differently for each cell.
#   Cells[0:4] get progressively lower P_Na; cells[4:11] get progressively
#   higher P_K.
# - watch the resultant Vmem become more negative for each cell.
#   They range from +50 mv for cell 0 (which is the Na Vnernst)
#   of Na+ to about -75 mV for cell 11 (which is the K Vnernst).
# The final cc_cells (after about 100K seconds) is
#	Na  [ 15  12   9   8   6   6   7   7   8   8   8   8]
#	K   [468 371 286 221 161 123 105  94  88  84  82  81]
#	Cl  [403 303 216 148  86  49  32  22  16  12  11  10]
#	Pr  [ 80  80  80  80  80  80  80  80  80  80  80  80]
#	Vmem[ 28  21  12   2 -13 -28 -39 -50 -59 -65 -69 -72]
#       GNa [ 20  10   5  2.5  1   1   1   1   1   1   1   1]*1e18
#       GK  [  1   1   1    1  1 2.5   5  10  20  40  80 160]*1e18
#	(ECF [Na]=145, [K]=5, [Cl]=140).
def setup_test1 (p):
    num_cells=12
    n_GJs = 0
    sim.init_big_arrays (num_cells, n_GJs, p)
    p.use_implicit = False	# Don't use the implicit integrator

    # Settings to help explore the final Vmem, but that fail regressions.
    # p.no_dumps = True
    # p.adaptive_timestep = True		# Yes to adaptive time_step
    # p.time_step = .0001

    # By default, we have D_Na = D_K = 1e-18
    Na = sim.ion_i['Na']; K = sim.ion_i['K']
    sim.Dm_array[Na,0]= 20.0e-18	# +28 mV (reversal potential of Na+)
    sim.Dm_array[Na,1]= 10.0e-18	# +21 mV
    sim.Dm_array[Na,2]= 5.0e-18		# +12mV
    sim.Dm_array[Na,3]= 2.5e-18		#  +2mV
    sim.Dm_array[Na,4]= 1.0e-18		# -13mV
    sim.Dm_array[K,4] =  1.0e-18	#
    sim.Dm_array[K,5] =  2.5e-18	# -28mV
    sim.Dm_array[K,6] =  5.0e-18	# -39mV
    sim.Dm_array[K,7] = 10.0e-18	# -50mV
    sim.Dm_array[K,8] = 20.0e-18	# -59mV
    sim.Dm_array[K,9] = 40.0e-18	# -65mV
    sim.Dm_array[K,10]= 80.0e-18 	# -69mV
    sim.Dm_array[K,11]= 160.0e-18	# -72mV (reversal potential of K+)
    #sim.Dm_array[Na,4:]= 1.8e-18		# -13mV ### for QSS exploration.

def post_test1 (GP, t_shots, cc_shots):
    eplt.plot_Vmem (t_shots, cc_shots)
    eplt.plot_ion (t_shots, cc_shots, 'Na')
    eplt.plot_ion (t_shots, cc_shots, 'K')
    eplt.plot_ion (t_shots, cc_shots, 'Cl')
    np.set_printoptions (formatter={'float': '{:4.1f}'.format})
    print (sim.cc_cells)
    print (sim.compute_Vm (sim.cc_cells))

# TEST #2:
# Network arrangement:
#	- Arrange the cells in a rectangle: 3 layers of 4 cells/layer.
#	- Connect the layers with GJs in straight lines; so cell0 in layer0 has
#	  a GJ to cell0 in layer1, which has a GJ to cell0 in layer2.
#	- All layers other than the input layer start out depolarized (high Dm
#	  for K, and so Vmem about +20mV).
#	- The input-layer (i.e., column #0) cells start out hyper-polarized
#	  low Dm for K and high Dm for Na, so Vmem about -67mV)
#	  Remember that the cells in the input layer are disconnected from
#	  each other (though they are connected to layer1).
# The big idea:
#	- The amount of conductance through GJs can be set to any of a few
#	  different values. It controls how much the cells talk to each other.
#	  With low enough conductance, the cells are all independent; high
#	  enough and the connected cells converge towards a common voltage.
#	  The mechanism for this is that Na, K and Cl diffuse through the GJs
#	  and, with enough connectivity, start to be the same everywhere (which
#	  results in the net charge starting to be the same in each cell).
def setup_test2 (p):
    num_layers=3; cells_per_layer=4
    num_cells = num_layers*cells_per_layer # total number of cells in network
    eplt.set_network_shape ((num_layers,cells_per_layer))	#For later pretty_plot()
    p.sim_dump_interval = 5000	# Not much debug printout during the sim

    # Straight-line connections: from each layer straight to the next, for test2
    n_GJs = cells_per_layer * (num_layers-1)
    sim.init_big_arrays (num_cells, n_GJs, p)
    P=sim.ion_i['P']

    sim.GJ_connects['from'] = range(n_GJs)
    sim.GJ_connects['to']   = sim.GJ_connects['from'] + cells_per_layer
    sim.GJ_diffusion[P] = 1e-18			# For historical reasons.

    # Set the inputs cells positive.
    # By default, we have D_Na=D_K=1e=18; we'll set D_Na 10x bigger.
    # With blocked GJs, this gives Vm for inputs = +20.7mV; 
    sim.Dm_array[sim.ion_i['Na'],0:cells_per_layer] = 10.0e-18

    # Set the remaining cells quite negative (by increasing D_K).
    # With blocked GJs, this gives Vm for inputs = -66.5mV
    sim.Dm_array[sim.ion_i['K'],cells_per_layer:] = 50.0e-18

    # Try the test with different values of GJ effective-diffusion
    # coefficient for ions, including 0.0 (blocked gap junctions) so that
    # you can prove to yourself that the depolarization of the first
    # layer of cells is being effectively shared with the next layers.
    #sim.GJ_diffusion *= 0	# Vin,mid,end=[21, -66, -66]
    sim.GJ_diffusion *= 10	# Vin,mid,end=[2.7,-39, -46]

def post_test2 (GP, t_shots, cc_shots):
    eplt.plot_Vmem (t_shots, cc_shots)
    eplt.plot_ion (t_shots, cc_shots, 'Na')
    eplt.plot_ion (t_shots, cc_shots, 'K')
    eplt.plot_ion (t_shots, cc_shots, 'Cl')
    eplt.plot_ion (t_shots, cc_shots, 'P')

# Start to build an inverter where Vmem = ~[A]
# 9 cells, all disconnected (no GJs).
# Each cell has a different [A], ranging from 0 to 100 mM. It is merely set by
# the initial conditions (with no generation or decay of A).
#
# The sim uses ligand-sensitive ion channels to set each cell's Vmem from the
# cells [A]. Specifically, each cell has default Dm for Na and K of 20e-18,
# which gives Vmem= -3mV. But the Na and K channels are ligand-sensitive to [A],
# thus making Vmem depend on [A]. Vmem ranges from 52mV at [A]=0 to -8mV at
# [A]=50, and -70mV at [A]=100.
# The simulation works in QSS.
def setup_test3 (p):
    num_cells=9
    n_GJs = 0
    # Add a new neutral ion "A".
    sim.init_big_arrays (num_cells, n_GJs, p, ['A'])

    # By default, we have D_Na = D_K = 1e-18
    Na = sim.ion_i['Na']; K=sim.ion_i['K']; A=sim.ion_i['A']
    sim.Dm_array[Na,:] = 20e-18		# This combination would
    sim.Dm_array[K, :] = 20e-18		# give Vmem=-3mV without gating

    # Set up our "input" -- i.e., [A]. It is neutral.
    sim.cc_cells[A, 0:12] = np.linspace (0, 100, num_cells)

    # Build an inverter in each cell; Vmem = ![A]
    # If [A] is high(low), then we reduce Kd for Na(K), and we make Vmem
    # look more like the V_Nernst for K(Na), which is negative(positive).
    # The Na(K) gating is then type=hill_inv(buf), kM=50, N=2, in_ion=A.
    # Note that each Hill_gate() instance will put gates into *every* cell.
    sim.Hill_gate (sim.Gate.GATE_IC,out_ion=Na,in_ion=A,inv=True, kM=50,N=2)
    sim.Hill_gate (sim.Gate.GATE_IC,out_ion=K, in_ion=A,inv=False,kM=50,N=2)

# This HW creates a single cell that is bistable (at -20mV and +20mV).
# We set up a range of such cells, all identical except for their initial Vmem,
# and watch them all transition to one of the stable points.
# This setup function does little except instantiate the cells and set them to
# a range of initial Vmem.
# All of the hard work is done in bistab_user_func(), which must be called from
# inside of sim_slopes().
# Interesting observations: 
# - The graph is quite lovely. It takes about 0.5 sec for everything to
#   converge.
# - The sim that starts at Vmem=+5mV stalls a while, and then drops to -20mV.
#   My guess is that this is because of a small "bug" -- the computation of
#   the "actual" flux in bistab_user_func() does not include the pump flux.
#   If we modified bistab_user_func() to include that, I think it would "work."
def setup_bistable_cell (p):
    num_cells=12
    n_GJs = 0
    sim.init_big_arrays (num_cells, n_GJs, p)

    # Next, change the initial charge on various cells.
    # They will range from -30mV for cell #0 to +25 for cell #11.
    Na = sim.ion_i['Na']
    scale = p.F * p.cell_vol / (p.cell_sa*p.cm)
    for i in range(num_cells):
        sim.cc_cells[Na,i] += .005*(i-6) / scale

# Requirement: tweak sim_slopes() so that it calls bistab_user_func() rather
# than GHK for the K ion-channel flux. So, in sim.py:
#    f_GHK = stb.GHK (cc_env, cc_cells, Dm_array, z_array, Vm, GP)
#    f_GHK[1] = main.bistab_user_func (Vm)
def bistab_user_func(Vm):
    # Build a PWL that implements a simple bistable flux. The "/1000" converts
    # Volts to mV. 'x' is Vmem, and 'y' the resultant flux. Flux=0 when Vmem
    # is -20mV or +20mV (and metastable at Vmem=0).
    x = np.array ([-30, -20.0, -10, 0, 10, 20, 30]) / 1000
    y = np.array ([  5,   0.0,  -5, 0,  5,  0, -5])
    desired_flux = np.interp (Vm, x, y)

    # Compute what the current Na and Cl GHK fluxes are.
    Na = sim.ion_i['Na']; Cl=sim.ion_i['Cl']
    num_cells = sim.cc_cells.shape[1]

    import sim_toolbox as stb
    f_GHK = stb.GHK (sim.cc_env, sim.cc_cells, sim.Dm_array, sim.z_array, Vm, sim.GP)

    # We know what we want the net flux to be, and also what we are getting
    # from Na and Cl (which is negative). The rest must come from K, which we
    # supply here. The 1e-8 is to scale the flux down to a reasonable amount,
    # so we don't oscillate wildly.
    net = desired_flux - (f_GHK[Na]+f_GHK[Cl])
    return (net * 1e-8)

def post_bistable_cell (GP, t_shots, cc_shots):
    eplt.plot_Vmem (t_shots, cc_shots)

# This example builds a neural net out of GRN buffers. It does not use
# bioelectricity at all! There's lots of explanation in the Google Docs document
# "non-neural network with just GRNs."
# Cells 0 and 1 are the inputs X and Y. We control their [A] by setting their
#	gen/decay of A.
# Cells 2 and 3 are input_bar. They have their [A] set by gen/decay of A -- but
#	via the non-physical gen=Hill([A]) in cells 0 and 1 respectively.
# Cell 4 is X & !Y; so the AND of cells 0 and 3 (by averaging [A] in cells 0
#	and 3). It then uses gen/decay to build an in-cell gain buffer [B]=[A]
#	(a.k.a. an excitation function).
# Cell 5 is Y & !X; so the AND of cells 1 and 2 (with another internal B=A
#	gain buffer).
# Cell 6 is 4 | 5; i.e., X^Y. It then has an in-cell gain buffer [A]=[B].
def setup_NN_GRN (p):
    p.use_implicit = True
    num_cells=7
    n_GJs=6
    sim.init_big_arrays (num_cells, n_GJs, p, ['A', 'B'])
    A=sim.ion_i['A']; B=sim.ion_i['B']

    # Default is 'D_GJ':1e-18 for all ions; GJ_scale scales that up. Remember
    # that Vmem is irrelevant for this simulation; we're just scaling A and B.
    GJ_scale = 200
    sim.GJ_diffusion *= GJ_scale

    # Use generation to set [A] in cells #0 and #1. We use a generation rate of
    # 1 for logic 1, and 0 for logic 0 (units of moles/m3 per s).
    # We also set the decay rates here; decay of A and B are both .01/s.
    sim.GD_const_gate(sim.Gate.GATE_GD, A, gen=[1,1,0,0,0,0,0], decay=.01)
    sim.GD_const_gate(sim.Gate.GATE_GD, B, decay=.01)

    # Create cell 2 = !(cell 0), and cell 3 = !(cell 1).
    # Do it by non-physical ligand-gated generation; cells 2,3 generate A based
    # on [A] in cells 0,1 respectively; "based" means a Hill function that can
    # be either a buffer or an inverter on a cell-by-cell basis. Note that our
    # kVMax allows generation only in cells #2,3.
    kVMax = [0,0,1,1,0,0,0]	# Only use cells[2,3]. Units are moles/m3 per s.
    remap = [0,0,0,1,0,0,0]	# And read from cells[0,1]
    g23=sim.Hill_gate (sim.Gate.GATE_GD,A,A, inv=True,kM=50,N=8,kVMax=kVMax)
    g23.Hill_gate_unphysical_inputs (remap)

    # Cell 4 = (cell 0) & !(cell 1) = (cell 0) & (cell 3)
    # Specifically, use type-0 GJs to set [A] in cell 4 to the average of that
    # in cells 0 and 3.
    # And then B=inv(A), which generates B
    # So cells 4 (and 5) use normal generation/decay of B; 
    sim.GJ_connects[0] = (0, 4)
    sim.GJ_connects[1] = (3, 4)
    # GJ type 0 does not conduct B; type 1 does not conduct A
    sim.GJ_diffusion[B,[0,1]]=0
    sim.Hill_gate (sim.Gate.GATE_GD,out_ion=B, in_ion=A, inv=False,kM=12,N=8,
                         kVMax=[0,0,0,0,1,1,0])

    # Ditto for cell 5 = !(cell 0) & (cell 1) = (cell 2) & (cell 1)
    # We already built cell 5's B=inv(A) buffer just above.
    sim.GJ_connects[2] = (1, 5)
    sim.GJ_connects[3] = (2, 5)
    # GJ type 0 does not conduct B; type 1 does not conduct A
    sim.GJ_diffusion[B,[2,3]]=0

    # Cell 6 = (cell 4) | (cell 5). This GJ OR conducts B;
    # Thus, we drive with GJ type 1 (which only conducts B and not A).
    # We then build an A=B buffer in cell 6; its kM=5 makes the entire thing
    # implement an OR rather than AND.
    sim.GJ_connects[4] = (4, 6)
    sim.GJ_connects[5] = (5, 6)
    sim.Hill_gate (sim.Gate.GATE_GD,out_ion=A, in_ion=B, inv=False, kM=5,N=8,
                         kVMax=[0,0,0,0,0,0,1])
    sim.Hill_gate (sim.Gate.GATE_GD,A,B, inv=False, kM=5,N=8,
			 kVMax=[0,0,0,0,0,0,1])
    # GJ type 0 does not conduct B; type 1 does not conduct A
    sim.GJ_diffusion[A,[4,5]]=0

def post_NN_GRN (GP, t_shots, cc_shots):
    edb.dump (t_shots[-1], cc_shots[-1], edb.Units.mol_per_m3s, False)
    #eplt.plot_ion (t_shots, cc_shots, 'A')
    #eplt.plot_ion (t_shots, cc_shots, 'B')

# This is our working QSS NN. It builds a weighted sum in QSS, then uses a
# layer-2A transfering an ion A to implement an activation function and restore
# a low-impedance vMem for the next layer. 
# Cells 0 and 1 are the input layer, each with a different Vmem.
# Cell 2 is a weighted sum of cells 0+1, and also generates an neutral ion A.
# A voltage-gated GJ then either does or doesn't transfer A to cell #3 (which
# thus implements the activation function). In fact, we'll use a clever little
# trick: connect cell 2 to cell 3 with *two* gated GJs -- one turns on when
# cell 2 has high Vmem and one when cell 2 has low Vmem, and so the only time
# cell 3 is undriven (and hence has low [A]) is when cell 2 has an intermediate
# Vmem. This gives us a very simple XOR.
# Cell #3 senses the presence of A, and uses ligand-gated Na and K channels to
# set its Vmem accordingly.
# Result: an XOR gate, implemented as a single-layer NN.
# Note that we can only implement a single layer (see
# in_progress/neural_net_qs.txt for details).
# A simulation takes 70K seconds of virtual time.
def setup_NN_QSS(p):
    p.adaptive_timestep = True	# The sims can be long...
    p.use_implicit = True
    num_cells=4
    n_GJs = 4
    sim.init_big_arrays (num_cells, n_GJs, p, ['A'])
    Na = sim.ion_i['Na']; K=sim.ion_i['K']; A=sim.ion_i['A']; P=sim.ion_i['P']
    sim.GJ_diffusion[P] = 1e-18			# For historical reasons.
    eplt.set_network_shape ([2, 3]) # For future pretty plotting.

    # For the weighted-sum layer. Scale=1 => 43,-64.
    # Scale=10=> 36, -53mV.
    GJ_scale_ws = 1
    GJ_scale_moveA = .05	# Moves 'A' from the 2 to the 2A layer
    genA = 1e-2			# Generation rate of 'A'

    # Cells 0 and 1 are the input layer, each with a different Vmem.
    # Remember that the default is Dm=1e-18.
    sim.Dm_array[Na, 0]= 20e-18 		# Vm =  43mV
    #sim.Dm_array[K , 0]= 20e-18 		# Vm = -64mV
    #sim.Dm_array[Na, 1]= 20e-18 		# Vm =  43mV
    sim.Dm_array[K,  1]= 20e-18 		# Vm = -64mV

    # Cell 2 = mix (0, 1) using GJ #0, #1.
    sim.GJ_connects[0] = (0, 2)
    sim.GJ_connects[1] = (1, 2)
    sim.Dm_array[:,2]= 0	# No ion channels of its own, so it senses well
    sim.GJ_diffusion[A,[0,1]]=0	# The weighted-sum GJs don't conduct A
    sim.GJ_diffusion[:,[0,1]] *= GJ_scale_ws

    # Cell 2 generates a neutral ion A
    # By default, new ions have D_ic=0; D_gj=1e-18; z=0, c_in=c_out=0.
    # genA is moles/m3/s.
    sim.GD_const_gate (sim.Gate.GATE_GD, A, gen=[0,0,genA,0], decay=genA/100)

    # GJ #2: gated GJ from cell 2->3, so that [A] in 3 depends on Vmem,2
    # It has kM=+19mV and negative N, so it's only on for Vmem>19mV (inputs=11).
    # (Remember that the Vmem gating is scale = 1 / 1+exp(N*(vMem-kM)). So
    # positive(negative) N is an inverter(buffer)).
    sim.GJ_connects[2] = (2, 3)
    sim.GJ_connects[3] = (2, 3)
    # type 1 GJ: GJ_diffusion for A = 1e-15
    sim.GJ_diffusion[A,[2,3]]=1e-15
    sim.GJ_diffusion[:,[2,3]] *= GJ_scale_moveA

    # GJ #3 has kM=-.030 and positive N, so it's on for vMem<-30mV (inputs=00).
    g=sim.Vmem_gate (sim.Gate.GATE_GJ, out_ion=None, kM=[100,100,.019,-.03],
			   N=[1,1,-400,400])
    # GJ gates always need a remap from num_cells to n_GJs; this one says our
    # four GJs are gated by Vmem in cells #0, 0, 2 and 2 respectively.
    g.Vmem_gate_unphysical_inputs ([0,0,2,2])
    
    # Cell #3 inverts [A] into Vmem (i.e., it's layer 2A), making an XOR.
    sim.Dm_array[Na, 3]= 20e-18 		# Vm =  44mV
    sim.Dm_array[K,  3]= 20e-18 		# Vm = -66mV
    gNa=sim.Hill_gate (sim.Gate.GATE_IC,out_ion=Na,in_ion=A)
    gK= sim.Hill_gate (sim.Gate.GATE_IC,out_ion=K, in_ion=A)
    gNa.change_Hill_gate (3, inv=True,  kM=23, N=4)
    gK.change_Hill_gate  (3, inv=False, kM=23, N=4)

def post_NN_QSS (GP, t_shots, cc_shots):
    sim.cc_cells = cc_shots[-1]
    #edb.dump (t_shots[-1], cc_shots[-1], edb.Units.mol_per_m3s, True)
    edb.dump_gating ()
    eplt.plot_Vmem (t_shots, cc_shots)
    eplt.plot_ion (t_shots, cc_shots, 'A')

# Cells 0, 1 are inputs; we use them for their Vmem (#0 is +V, #1 is -V).
# Then cells #2-5 are: 2=buf(0); 3=inv(0); 4=buf(1); 5=buf(1).
# We build them with voltage-controlled ion channels, look at the Vmem in
# their input cell and changing the destination cell's D_Na and D_K.
# I.e., they use voltage-controlled ion channels that gate one cell's channels
# by looking at a *different* cell's Vmem.
def setup_magic_invs (p):
    num_cells=6
    n_GJs = 0
    sim.init_big_arrays (num_cells, n_GJs, p)
    Na=sim.ion_i['Na']; K=sim.ion_i['K']

    # By default, we have D_Na = D_K = 1e-18.
    # Cell 0 & 1 are the inputs. Make cell #0 +Vm, cell #1 -Vm.
    sim.Dm_array[sim.ion_i['Na'],0]= 20e-18
    sim.Dm_array[sim.ion_i['K'], 1]= 20e-18

    # Cells #2-5 start with high Na, K Dm, so we can scale down one,
    # and the other one will dominate.
    sim.Dm_array[sim.ion_i['Na'],2:6]= 20e-18
    sim.Dm_array[sim.ion_i['K'], 2:6]= 20e-18

    gNa= sim.Vmem_gate (sim.Gate.GATE_IC, out_ion=Na, kM=100, N=1)
    gK = sim.Vmem_gate (sim.Gate.GATE_IC, out_ion=K, kM=100, N=1)

    # Cells 0,1 don't look at any Vmem
    # Cells 2,3 look at the Vmem from cell 0
    # Cells 4,5 look at the Vmem from cell 1
    in_cells = [0, 0, 0, 0, 1, 1]
    gNa.Vmem_gate_unphysical_inputs (in_cells)
    gK.Vmem_gate_unphysical_inputs  (in_cells)

    # Remember the Na has Vnernst>0, and K has Vnernst<0.
    # So to build a buffer, we want to scale D_K=0 when Vin>0 and D_Na=0 when
    # Vin<0. For an inverter, we do the reverse.

    # Cell2=buf(cell0)
    gNa.change_Vmem_gate (2, kM=0, N=-20, kVMax=1)	# Scale=0 when Vmem<0
    gK.change_Vmem_gate (2, kM=0, N= 20, kVMax=1)	# Scale=0 when Vmem>0

    # Cell3=inv(cell0)
    gNa.change_Vmem_gate (3, kM=0, N= 20, kVMax=1)	# Scale=0 when Vmem>0
    gK.change_Vmem_gate  (3, kM=0, N=-20, kVMax=1)	# Scale=0 when Vmem<0

    # cell4=buf(cell1)
    gNa.change_Vmem_gate (4, kM=0, N=-20, kVMax=1)	# Scale=0 when Vmem<0
    gK.change_Vmem_gate (4, kM=0, N= 20, kVMax=1)	# Scale=0 when Vmem>0

    # cell5=inv(cell1)
    gNa.change_Vmem_gate (5, kM=0, N= 20, kVMax=1)	# Scale=0 when Vmem>0
    gK.change_Vmem_gate  (5, kM=0, N=-20, kVMax=1)	# Scale=0 when Vmem<0

# This implements an XOR gate. The basic idea:
#	
# "1" cells are about +40mV and "0" cells are about -80mV. So when we use a GJ
# to average together two cells, and then compare the average to 0, we are
# building an AND cell. We then do AND(x,y) AND(~x,~y)
# This test is simple because it uses lots of non-physical gating -- an ion
# channel in one cell depends on Vmem in a different cell!
#
# A few notes:
# - Everything works in quasi-steady state. With the constants below, the sim
#   takes about 10-15 sec of simulated time.
# - Choosing the gap-junction permeabilities is a tradeoff. If they are too
#   small, then the simulation takes a long time, and at some point the "merge"
#   cells don't settle for several hundred seconds (which is long enough to
#   start to leave the QSS domain). If, on the other hand, they are large, then
#   they start to place a substantial load on their upstream cells. E.g., cell
#   #4 mixes together cells #0 and #1 by GJ connections from each of them to
#   cell #4 -- if the GJs are too big, then cells #0 and #1 could "share and
#   merge" their Vm's.
# - With GJ_connects[*].D=1-e20, "merge" nodes take about 150 sec to reach half
#   of their final value. At D=1e-19, it takes about 15 sec; at D=1e-18, about
#   5 seconds. Interestingly, even at D=1e-18, the upstream Vmem's were not
#   affected in any noticeable amount vs. D=1e-20. I never tried setting GJ
#   D higher than 1e-18.
# - All cells start out with the same internal concentrations of Na, K and Cl.
#   Thus, all of the ion-channel Vnernst start out identical. The cells with
#   ion channels actually do have their [] change by 5-10%, even during QSS.
#   The merge cells have essentially no change.
# - With concentrations being so similar, all of the GJ Vthev are essentially
#   zero.
def setup_magic_xor (p):
    num_cells=10
    n_GJs = 6
    sim.init_big_arrays (num_cells, n_GJs, p)

    # The plan:
    # Cells #0 and #1 are the inputs.
    # Cells #2 and #3 are inverted inputs (their ICs sense Vmem[0,1])
    # Cell #4 = mix(#0, #1) using GJ #0, #1 = AB
    # Cell #5 = mix(#2, #3) using GJ #2, #3 = ~A~B
    # Cell #6 = gain-inv (#4) = ~(AB)
    # Cell #7 = gain-inv (#5) = ~(~A~B) = A+B
    # Cell #8 = mix(#6, #7) using GJ #4, #5 = ~(AB) & (A+B) = A^B
    # Cell #9 = gain-buf (#8)

    Na = sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']; P=sim.ion_i['P']
    sim.GJ_diffusion[P] = 1e-18			# For historical reasons.
    gNa= sim.Vmem_gate (sim.Gate.GATE_IC, out_ion=Na, kM=100, N=1, kVMax=1)
    gK = sim.Vmem_gate (sim.Gate.GATE_IC, out_ion=K,  kM=100, N=1, kVMax=1)

    # Cells 0,1,4,5 don't look at any Vmem
    # Cells 2,3 look at the Vmem from cell 0,1
    # Cells 6,7 look at the Vmem from cell 4,5
    # Cell  9 looks at the Vmem from cell 8
    in_cells = [0, 0, 0, 1, 0, 0, 4, 5, 0, 8]
    gNa.Vmem_gate_unphysical_inputs (in_cells)
    gK.Vmem_gate_unphysical_inputs  (in_cells)

    # type=Vmem, kM=0, N=20 or -20, cell=whichever, ion=X, cell2=unused

    # By default, we have D_Na = D_K = 1e-18. Make sure "0" cells are more
    # negative than "1" cells are positive.
    #sim.Dm_array[Cl,:] = 2e-18
    sim.Dm_array[K, :] = 10e-18

    # Cell 0 & 1 are the inputs.
    # Make cell #0 +Vm, cell #1 -Vm -> (+42mV, -63mV)
    # THIS IS WHERE YOU SET THE INPUTS TO YOUR FAVORITE VALUES.
    sim.Dm_array[K,0] = 20e-18	# +Vm
    sim.Dm_array[Na,1]= 20e-18	# +Vm

    # Now build cells #2 and #3 as inversions of cells #0 and #1 respectively.
    # Do it by having their Na and K ion channels depend on cell #0,1 Vmem.
    # Remember the Na has Vnernst>0, and K has Vnernst<0.
    # So to build an inverter, we want to scale D_Na=0 when Vin>0 and D_K=0 when
    # Vin<0.
    # Cell 2 = invert (cell #0); Cell 3 = invert (cell #1)
    gNa.change_Vmem_gate (2, kM=0, N= 300, kVMax=1)	# Scale=0 when Vmem>0
    gK.change_Vmem_gate  (2, kM=0, N=-300, kVMax=1)	# Scale=0 when Vmem<0
    gNa.change_Vmem_gate (3, kM=0, N= 300, kVMax=1)
    gK.change_Vmem_gate  (3, kM=0, N=-300, kVMax=1)
    sim.Dm_array[Na,2:4]= 20e-18	# Start with high Dm so we can
    sim.Dm_array[K, 2:4]= 20e-18	# scale down easily.

    # Cell #4 = mix(#0, #1) using GJ #0, #1
    # We're just getting the average of two cells' Vmem with GJs.
    sim.GJ_connects[0] = (0, 4)
    sim.GJ_connects[1] = (1, 4)
    sim.Dm_array[:,4]= 0	# No ion channels of its own, so it senses well

    # Cell #5 = mix(#2, #3) using GJ #2, #3
    sim.GJ_connects[2] = (2, 5)
    sim.GJ_connects[3] = (3, 5)
    sim.Dm_array[:,5]= 0	# No ion channels of its own, so it senses well

    # Cell #6 = gain-inverter (#4)
    # Again, we have cell 6's Na and K channels depend on cell 4's Vmem.
    gNa.change_Vmem_gate (6, kM=0, N= 300, kVMax=1)	# Scale=0 when Vmem>0
    gK.change_Vmem_gate  (6, kM=0, N=-300, kVMax=1)	# Scale=0 when Vmem<0
    sim.Dm_array[Na,6]= 20e-18	# Start with high Dm so we can
    sim.Dm_array[K, 6]= 20e-18	# scale down easily.

    # Cell #7 = gain-inverter (#5)
    # Cell 7's Na and K channels depend on cell 5's Vmem.
    gNa.change_Vmem_gate (7, kM=0, N= 300, kVMax=1)	# Scale=0 when Vmem>0
    gK.change_Vmem_gate  (7, kM=0, N=-300, kVMax=1)	# Scale=0 when Vmem<0
    sim.Dm_array[Na,7]= 20e-18	# Start with high Dm so we can
    sim.Dm_array[K, 7]= 20e-18	# scale down easily.

    # Cell #8 = mix(#6, #7) using GJ #4, #5
    sim.GJ_connects[4] = (6, 8)
    sim.GJ_connects[5] = (7, 8)
    sim.Dm_array[:,8]= 0	# No ion channels of its own, so it senses well

    # Cell #9 = gain-buf (#8)
    # Cell 9's Na and K channels depend on cell 8's Vmem.
    gNa.change_Vmem_gate (9, kM=0, N=-300, kVMax=1)	# Scale=0 when Vmem<0
    gK.change_Vmem_gate  (9, kM=0, N= 300, kVMax=1)	# Scale=0 when Vmem>0
    sim.Dm_array[Na,9]= 20e-18	# Start with high Dm so we can
    sim.Dm_array[K, 9]= 20e-18	# scale down easily.

# Observe how gated ion channels produce deltaV in a worm.
# Given:
# - head & tail [M]
# - gating-K-channel Hill-model parameters kM and N
# Then:
# - manually evaluate the Hill model for the given [M], kM and N and compute
#   the resulting Dm_array[K, head & tail]. Note that actual K channels in the
#   sim will not be gated, but will be hard coded to the values we compute.
# - simulate
# Result: observe the resultant head and tail Vmem. Note that M is mostly
# irrelevant in the sim itself; it mostly just affects the just-described
# computation of Dm_array[K, head & tail].
# For that matter, so is GJ_scale -- since we're looking at Vmem
# after just a few seconds
def setup_worm_gating (p):
    M_head=1.64; M_tail=.492; num_cells=5
    kM=1; N=10; GJ_scale=.1	# Lab sim #1, and benchmark
    # kM=1; N =2; GJ_scale=.1	# Lab sim #2

    # The ligand-gated K channel has diff = 1.7 e-17 m2/s when fully on.
    head_K = 1.7e-17 / (1. + (M_head/kM)**N)
    tail_K = 1.7e-17 / (1. + (M_tail/kM)**N)
    print ("Scale at head={:.2f}, tail={:.2f}".format (1/(1.+(M_head/kM)**N),
                                               1/(1.+(M_tail/kM)**N)))

    # gen_decay = False.
    mode_dict = {"full_body":False,"gen_decay":False}

    # N and kM are for the Hill model, where [M] controls the K channels.
    # 'GJ_scale' controls how strong the gap junctions are
    param_dict = default_worm_params()
    param_dict["GJ_scale"]=GJ_scale; param_dict["num_cells"]=num_cells
    param_dict["M_min"]=M_tail; param_dict["M_max"]=M_head
    setup_one_worm(p, mode_dict, param_dict)

    # setup_one_worm() has created K-feedback ion channels at the head & tail
    # cells. Remove the feedback, and set Dm_array[K] to what we want.
    K=sim.ion_i['K']
    sim.Dm_array[K,-1] = head_K
    sim.Dm_array[K,0]  = tail_K
    sim.IC_gates = []

    p.use_implicit = True
def post_worm_gating (GP, t_shots, cc_shots):
    eplt.plot_Vmem (t_shots, cc_shots)


# The worm used for our worm lab:
# - It uses Alexis' parameters and chemicals.
# - We can switch it between having ion channels in the head/tail only, or in
#   the entire body.
# Results:
# - see lab_worm_results.txt for details.
def setup_worm_lab (p):
    p.use_implicit = True
    p.sim_dump_interval = 1000	# Not much debug printout during the sim

    # gen_decay = False.
    mode_dict = {"full_body":False,"gen_decay":False}

    # N and kM are for the Hill model, where [M] controls the K channels.
    # 'GJ_scale' controls how strong the gap junctions are
    kM=1; N=10; GJ_scale=.1	# Lab sim #1
    #kM=1;  N=2; GJ_scale=.1	# Lab sim #2
    #kM=.2; N=5; GJ_scale=.1	# Lab sim #3
    kM=.2; N=5; GJ_scale=.02	# benchmark
    param_dict = default_worm_params()
    param_dict["kM"]=kM; param_dict["N"]=N; param_dict["GJ_scale"]=GJ_scale
    setup_one_worm(p, mode_dict, param_dict)
def post_worm_lab (GP, t_shots, cc_shots):
    #eplt.plot_Vmem (t_shots, cc_shots)
    #eplt.plot_ion (t_shots, cc_shots, 'M')
    pass

# This particular parameter set is a two-tailed worm for a while (and then it
# flips back to one head & one tail!). The trick is that kM=.2 is low enough
# that both ends have [M]>kM for a while.
def setup_worm_2tailed (p):
    mode_dict = {"full_body":False,"gen_decay":False}
    param_dict = default_worm_params()
    setup_one_worm(p, mode_dict, param_dict)
    
# The default set of worm parameters. Individual worm setup_...() functions
# start with these defaults, and then modify params as desired.
def default_worm_params ():
    param_dict = { \
        "kM":.2, "N":5, "GJ_scale":.02, "num_cells":5,
        "gd_tau":0, "gd_val":0,
	"M_min":0, "M_max":2, "M_valence":-1,
    }
    return (param_dict)

# One worm to rule them all. This is a parameterized worm that you can program
# to do most anything.
# - It uses Alexis' parameters and chemicals.
# - see lab_worm_results.txt for details.
# MD (the mode dictionary):
#	full_body: switches between having ion channels in the head/tail only,
#		or in every cell of the entire body.
#	gen_decay: if true, then M is subject to generation & decay, and the
#		parameters gd_tau, gd_val become relevant.
# PD (the parameter dictionary): all of the various physical parameters.
# Note that cell #0 is the tail and cell #-1 is the head.
def setup_one_worm(p, MD, PD):
    p.sim_dump_interval = 1000	# Not much debug printout during the sim

    num_cells = PD["num_cells"]
    n_GJs = num_cells-1
    sim.init_big_arrays (num_cells, n_GJs, p, ['M'])
    Na=sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']
    P=sim.ion_i['P']; M=sim.ion_i['M']

    # The ligand-gated K channel has diff = 1.7 e-17 m2/s when fully on.
    fb_cells = range(num_cells) if MD["full_body"] else [0,-1]
    sim.Dm_array[K, fb_cells] = 1.7e-17
    if (not MD["full_body"]):
        sim.Dm_array[:,1:-1]= 0

    # Ligand-gated K channels where requested: might be everywhere, nowhere,
    # or at the head & tail. Pick the default params so gain=1.
    g = sim.Hill_gate (sim.Gate.GATE_IC, out_ion=K, in_ion=M)
    for i in fb_cells:
        g.change_Hill_gate (i, inv=True, kM=PD["kM"],N=PD["N"])

    # Straight-line GJ connections along the worm. All are simple & ungated.
    sim.GJ_connects['from']  = range(n_GJs)
    sim.GJ_connects['to']    = sim.GJ_connects['from'] + 1

    # Physical parameters of the 'mystery' ion.
    sim.z_array[M] = PD["M_valence"]	# fixed ion valence

    # Initial concentrations: external {Na=145 mM, K=5mM, P=10mM}
    sim.cc_env[Na] = 145; sim.cc_env[K]=5; sim.cc_env[P]=10
    sim.cc_env[Cl] = 140	# To reach charge neutrality.

    # Initial concentrations: internal {Na=12, K=139, Cl=16, P=135}
    sim.cc_cells[Na] = 12; sim.cc_cells[K]=139; sim.cc_cells[P]=135
    sim.cc_cells[Cl] = 16	# To reach charge neutrality

    # Ligand creation/decay/gating: creation at .1uM/s, and decaying at
    # (.1/s)*(its concentration)). This yields concentration=1uM. However, we'll
    # spread it from tail to head to seed the gradient. Note that 1uM is 1
    # umole/liter, or 1 mmole/m3. We're actually doing 1000 times that big!
    if (MD["gen_decay"]):
        sim.gen_cells[M,:] = PD["gd_val"]/PD["gd_tau"]	# moles/m3 per s
        sim.decay_rates[M] = 1/PD["gd_tau"]		# 1/s
    spread = np.linspace (PD["M_min"], PD["M_max"], num_cells)
    sim.cc_cells[M, :]   = spread
    sim.cc_cells[Na, :] -= spread * PD["M_valence"]

    # Ion-channel diffusion coef: Na=K=M = 1e-18 m2/s, and P=0. Already done.

    # Change the gap-junction length from 100nm to 15nm.
    p.GJ_len = 15e-9

    # GJ scaled diff: Na=1.33e-17 m2/s, K=1.96e-17, M=1e-14, P=5e-17
    sim.GJ_diffusion[Na,:] = 1.33e-17
    sim.GJ_diffusion[K,:] = 1.96e-17
    sim.GJ_diffusion[M,:] = 1e-14
    sim.GJ_diffusion *= PD["GJ_scale"]

    #Na/K-ATPase pump with a maximum rate of 1.0x10-7 mol/(m^2 s)

    print ('Head D_K={}, tail D_K={}, GJ_scale={}'.format(sim.Dm_array[K,-1],
                sim.Dm_array[K,0],PD["GJ_scale"]))
    print ('Head kM={}'.format (sim.IC_gates[0].params[0,-1]))
    print ('Tail kM={}'.format (sim.IC_gates[0].params[0,0]))

# This is the function we're using for lab #1
# It uses the baseline values from setup_test1() cell #7:
#    sim.Dm_array[K,7] = 10.0e-18	# -57mV
def setup_lab1 (p):
    p.adaptive_timestep = True	# Slow but accurate mode
    p.sim_dump_interval = 1000	# Not much debug printout during the sim

    num_cells=4		# 4 cells

    # Gap junctions connect cells (we'll learn more soon).
    # No gap junctions means that the 4 cells are independent.
    n_GJs = 0

    # We've declared how many cells and GJs the simulation has. Create them
    # all, with default values.
    sim.init_big_arrays (num_cells, n_GJs, p)

    # To make it easier to set per-ion values below.
    Na = sim.ion_i['Na']; K = sim.ion_i['K']; Cl = sim.ion_i['Cl']

    # We want this lab to use a little different default values than the usual
    # ones. Specifically, a different diffusion constant for the K ion channels.
    # Note that this is the baseline value; simulation #3 will double it.
    sim.Dm_array[K,:] = 10.0e-18

    return
    # Simulation #2 asks you to change the initial cell-internal concentrations.
    # Here's where you can do that.
    # The defaults (set in sim.init_big_arrays) are [Na+]=10, [K+]=125,
    # [Cl-]=55, [other-]=80 (units are all moles/m^3). Note that they add up to
    # charge neutral. Make sure to keep the cell interior pretty close to charge
    # neutral, or else the simulation will explode!
    # For example, if we want to double [Na+], it would go from 10 to 20. We
    # will also increase [Cl] by 10 moles/m^3 to preserve charge neutrality.
    # You should do similar things for the other cells, as per the instructions.
    sim.cc_cells[Na,1] += 10	# Cell #1: [Na] is 20 rather than 10
    sim.cc_cells[Cl,1] += 10	#	And change [Cl] for charge neutrality.
    sim.cc_cells[K, 2] = 250	# Cell #2: [K] is 250 rather than 125
    sim.cc_cells[Cl,2] += 125
    sim.cc_cells[Cl,3] = 110	# Cell #3: [Cl] is 110 rather than 55
    sim.cc_cells[K, 3] += 55	#	And change [K] for charge neutrality.
    return

    # Simulation #3 asks you to alter the diffusion constants D_Na, D_K and
    # D_Cl. By default, sim.init_big_arrays() sets all three to 1e-18, and then
    # we overrode D_K above to 10e-18. Here's where we override them for
    # simulation #3.
    sim.Dm_array[Na,1] *= 2	 # Cell #1 doubles D_Na
    sim.Dm_array[K, 2] *= 2	 # Cell #1 doubles D_K
    sim.Dm_array[Cl,3] *= 2	 # Cell #1 doubles D_Cl

def post_lab1 (GP, t_shots, cc_shots):
    eplt.plot_Vmem (t_shots, cc_shots)
    eplt.plot_ion  (t_shots, cc_shots, 'Na')
    eplt.plot_ion  (t_shots, cc_shots, 'K')
    eplt.plot_ion  (t_shots, cc_shots, 'Cl')

# Setup_lab2() instantiates the same cells as does setup_lab1(), but this time
# we only simulate for 1 second (i.e., to QSS rather than SS).
def setup_lab2 (p):
    p.adaptive_timestep = False	# Slow but accurate mode
    p.sim_dump_interval = 10		# Not much debug printout during the sim
    p.sim_long_dump_interval = 10	# Not much debug printout during the sim

    num_cells=4
    n_GJs = 0
    sim.init_big_arrays (num_cells, n_GJs, p)

    # By default, we have D_Na = D_K = 1e-18
    Na = sim.ion_i['Na']; K = sim.ion_i['K']
    sim.Dm_array[Na,0]= 20.0e-18	# +44 mV (reversal potential of Na+)
    sim.Dm_array[Na,1]=  5.0e-18	# +13mV
    sim.Dm_array[K,2] = 10.0e-18	# -57mV
    sim.Dm_array[K,3] = 160.0e-18	# -82mV (reversal potential of K+)

def post_lab2 (GP, t_shots, cc_shots):
    edb.analyze_equiv_network (cc_shots[-1],GP)
    eplt.plot_Vmem (t_shots, cc_shots)
    return
    eplt.plot_ion  (t_shots, cc_shots, 'Na')
    eplt.plot_ion  (t_shots, cc_shots, 'K')
    eplt.plot_ion  (t_shots, cc_shots, 'Cl')

# This function shows that the QSS Vmem is insensitive to initial Vmem.
# 4 cells. Each one has Dm_Na set so that QSS Vmem will be -57mV.
# Tweak initial [Na] in cells 1-3 so that each starts out 5mV higher than
# the previous one. Nonetheless, they all converge to the same -57mV very
# quickly.
def setup_lab2b (p):
    p.adaptive_timestep = False	# Slow but accurate mode
    p.sim_dump_interval = 1000	# Not much debug printout during the sim

    num_cells=4
    n_GJs = 0
    sim.init_big_arrays (num_cells, n_GJs, p)

    # By default, we have D_Na = D_K = 1e-18
    # Pick the -57mV case from test1. So first, set everyone's D_Na for 30mV
    Na=sim.ion_i['Na']; K=sim.ion_i['K']
    sim.Dm_array[K,:]= 10.0e-18	# +30 mV

    # Next, change the initial charge on various cells.
    # Adding .005 moles/m3 to Na increases Vmem by about 15mV
    for i in range(0,num_cells):
        sim.cc_cells[Na,i] += .005*(i-5)
    #print ("Original cc_cells is ", sim.cc_cells)
    #import pdb; pdb.set_trace()

def post_lab2b (GP, t_shots, cc_shots):
    eplt.plot_Vmem (t_shots, cc_shots)

# This is a *very* simple NN. There is only one layer, and no activation
# function. It merely validates that we can in fact compute a weighted sum.
# Cells 0, 1 and 2 are the input layer, each with a different Vmem.
# Cells 3, 4 and 5 are the output layer: each one weights a different
# combination of inputs. The weights are the GJ permeabilities; the inputs are
# the input-layer Vmem. It all works in QSS.
def setup_lab_QSS_weighted_sum(p):
    p.adaptive_timestep = False	# Slow but accurate mode
    num_cells=6
    n_GJs = 7
    sim.init_big_arrays (num_cells, n_GJs, p)
    eplt.set_network_shape ([2, 3]) # For future pretty plotting.
    GJ_scale = 1		# experiment with making all GJs bigger/smaller

    # Cells 0, 1 and 2 are the input layer, each with a different Vmem.
    Na = sim.ion_i['Na']; K=sim.ion_i['K']; P=sim.ion_i['P']
    sim.Dm_array[Na, 0]= 20e-18 		# Vm =  44mV
    sim.Dm_array[[Na,K], 1]= 20e-18 		# Vm =  -1mV
    sim.Dm_array[K,  2]= 20e-18 		# Vm = -66mV

    # Cell 3 = mix (0, 2) using GJ #0, #1. Result= -11mV = (44-66)/2
    # We get it correct.
    sim.GJ_connects[0] = (0, 3)
    sim.GJ_connects[1] = (2, 3)
    sim.Dm_array[:,3]= 0	# No ion channels of its own, so it senses well
    sim.GJ_diffusion[P] = 1e-18			# For historical reasons.
    sim.GJ_diffusion[:,0] *= GJ_scale
    sim.GJ_diffusion[:,1] *= GJ_scale

    # Cell 4 = mix (0, 2) using GJ #2, #3. (44-V)*1=(V+66)*.2
    # Analytical solution= 25.7mV; we get 25mV.
    sim.GJ_connects[2] = (0, 4)
    sim.GJ_connects[3] = (2, 4)
    sim.Dm_array[:,4]= 0	# No ion channels of its own, so it senses well
    sim.GJ_diffusion[:,2] *= GJ_scale
    sim.GJ_diffusion[:,3] *= .2*GJ_scale # GJ 3 is extra small.

    # Cell 5 = mix (0, 1, 2) using GJ #4, #5 and #6
    # Equation is (V-44)*1 + (V+1)*.2 + (V+63)*.2 = 0,
    # or 1V+.2V+.2V = 44-.2-12.6, or V=22. Actual=21mV
    sim.GJ_connects[4] = (0, 5)
    sim.GJ_connects[5] = (1, 5)
    sim.GJ_connects[6] = (2, 5)
    sim.Dm_array[:,5]= 0	# No ion channels of its own, so it senses well
    sim.GJ_diffusion[:,4] *= GJ_scale
    sim.GJ_diffusion[:,5] *= .2*GJ_scale # GJs #4 and #5 are extra small.
    sim.GJ_diffusion[:,6] *= .2*GJ_scale

# This is an XOR gate (just like setup_magic_xor). However, it is substantially
# simpler than that one, needing only 7 cells instead of 10. The trick is that
# we take advantage of three-valued gates in two places (see below for details).
def setup_lab_magic_xor (p):
    p.sim_integ_max_delt_Vm = .0001	# Volts/step
    num_cells=7
    n_GJs = 4
    sim.init_big_arrays (num_cells, n_GJs, p)

    # With the default ion concentrations, we have Vn = +72mV(Na), -86mV(K)
    # and -25mV(Cl).
    # By default, we have D_Na = D_K = 1e-18. With the overrides here, we get
    # cells #0, #1 of +18mV, -91mV with GJs removed; or about +15mV, -82mV with
    # cell #2 connected normally.
    # Cells #2 and #5 are blends of two other cells. If the other cells are both
    # +15mV or both -82mV, then the blend cells are the same. If one input is
    # +15mV and the other is -82mV, then the blend cell is about -31mV.
    Na = sim.ion_i['Na']; K=sim.ion_i['K']; Cl=sim.ion_i['Cl']; P=sim.ion_i['P']
    sim.GJ_diffusion[P] = 1e-18			# For historical reasons.
    GJ_scale = 1			# Cell[2] settles by t=10s; Vin=-81,+15
    #GJ_scale = .1			# Settle by t=50s; Vin=-94,+24mV
    #GJ_scale = 10			# Settle by t=4s; Vin=-43,+4mV
    sim.Dm_array[Cl,:]= 1e-18
    sim.Dm_array[Na,:]= 4e-18

    # The plan:
    # Cells #0 and #1 are the inputs.
    # Cell #2 = mix(#0, #1) using GJ #0, #1. It is thus +40mV for A=B=1,
    #		-80mV for A=B=0, and -20mV for A^B=1.
    # Cell #3 = (Cell #2 > -50mV) -- so (in==00? -82mV: +15mV)
    # Cell #4 = (Cell #2 <  10mV) -- so (in==11? -82mV: +15mV)
    # Cell #5 = mix(#3, #4) using GJ #2, #3.
    #		So it's 00,11->-31mV; 01,10-> +15mV
    # Cell #6 = gain-buf (#5) = A ^ B

    Na=sim.ion_i['Na']; K=sim.ion_i['K']
    gNa= sim.Vmem_gate (sim.Gate.GATE_IC, out_ion=Na)
    gK = sim.Vmem_gate (sim.Gate.GATE_IC, out_ion=K)

    # Cells 0,1,2,5 don't look at any Vmem
    # Cells 3 and 4 look at the Vmem from cell 2
    # Cell  6 looks at the Vmem from cell 5
    in_cells = [0, 0, 0, 2, 2, 0, 5]
    gNa.Vmem_gate_unphysical_inputs (in_cells)
    gK.Vmem_gate_unphysical_inputs  (in_cells)

    # Cell 0 & 1 are the inputs.
    # Make cell #0 +Vm, cell #1 -Vm -> (+42mV, -63mV)
    # THIS IS WHERE YOU SET THE INPUTS TO YOUR FAVORITE VALUES.
    sim.Dm_array[Na,0] = 0	# +18mV for Dm[K]=0
    sim.Dm_array[K,1]= 0	# -91mV for Dm[Na]=0

    # Cell #2 = mix(#0, #1) using GJ #0, #1
    # It is -32mV for the XOR; or (as expected) -90mV for 00 and +18mv for 11.
    sim.GJ_connects[0] = (0, 2)
    sim.GJ_connects[1] = (1, 2)
    sim.Dm_array[:,2]= 0	# No ion channels of its own, so it senses well

    # Cell #3 = high when (cell #2 < -5mV)
    # Remember the Na has Vnernst>0, and K has Vnernst<0.
    # So to drive Vmem high, we scale Na=1 and K=0.
    gNa.change_Vmem_gate (3, kM=-.005, N= 300) # Scale=1 when Vmem[2] <  -5mV
    gK.change_Vmem_gate  (3, kM=-.005, N=-300) # Scale=1 when Vmem[2] >  -5mV

    # Cell #4 = (cell #2 > -60mV)
    gNa.change_Vmem_gate (4, kM=-.06, N=-300)  # Scale=1 when Vmem[2] > -60mV
    gK.change_Vmem_gate  ( 4, kM=-.06, N= 300)  # Scale=1 when Vmem[2] < -60mV

    # Cell #5 = mix(#3, #4) using GJ #2, #3.
    # It goes to about +18mV for 01 or 10, and about -31mV for 00 or 11.
    sim.GJ_connects[2] = (3, 5)
    sim.GJ_connects[3] = (4, 5)
    sim.Dm_array[:,5]= 0	# No ion channels of its own, so it senses well

    # Cell #6 = (cell #5 > -6mV)
    gNa.change_Vmem_gate (6, kM=-.006, N=-300) # Scale=1 when Vmem[2] > -6mV
    gK.change_Vmem_gate  (6, kM=-.006, N= 300) # Scale=1 when Vmem[2] < -6mV


######################################################################
######################################################################
# 4 independent cells; each has proteins A and B.
# Hill inverter:
#	Generation gating: B = Hill inverter ([A])
#	Decay rate for B = 1
# [A] is 10, 20, 50, 100 in cells 0-3 respectively.
# We also set it up as a transfer curve by driving [A] in cell #0, plotting [B]
# in cell #0, and ignoring everything else.
# This is *not* in the QA benchmark suite.
def setup_grn_Hill_invs (p):
    num_cells=4
    n_GJs = 0

    # By default, new ions have D_ic=0; D_gj=1e-18; z=0, c_in=c_out=0.
    sim.init_big_arrays (num_cells, n_GJs, p, ['A','B'])
    A = sim.ion_i['A']; B = sim.ion_i['B']

    kVmax = 100; N=5; kM=50; kDP=1
    sim.cc_cells[A,:] = [10,20,50,100]	# moles/m3 per s

    # Build the Hill inverter
    #sim.gen_cells[B,:] = kVmax
    #sim.gen_gating[B,:] = [sim.Hill_inv_gate(A,N,kM,cell) for cell in range(4)]
    #sim.decay_rates[B] = kDP

def post_grn_Hill_invs (GP, t_shots, cc_shots):
    eplt.plot_ion (t_shots, cc_shots, 'A')
    eplt.plot_ion (t_shots, cc_shots, 'B')

######################################################################
######################################################################
# This function also does a Hill inverter or buffer.
# However, it has clever postprocess plotting to do a transfer function.
# 4 cells. Two new proteins A and B
# Each cell:
#	- generates A for a very slow ramp from [A]=0 to 100.
#	- defines a Hill inverter B = !A; each cell has a different Hill N.
# Run the sim for 400 seconds in order to get [A] up to 97.
# This is *not* in the QA benchmark suite.
def setup_grn_Hill_xfer (p):
    num_cells=4
    n_GJs = 0

    build_inverter = False
    # By default, new ions have D_ic=0; D_gj=1e-18; z=0, c_in=c_out=0.
    sim.init_big_arrays (num_cells, n_GJs, p, ['A','B'])
    A = sim.ion_i['A']; B = sim.ion_i['B']

    # Make [A] slowly ramp from 0 to 100
    sim.decay_rates[A] = .01	# Compared to kDP=1 for B
    sim.gen_cells[A,:] = 1	# Must be 100x bigger than decay so [A]max=100

    # Build the Hill inverter
    global N			# So post_grn_Hill_invs2() can use it.
    kVmax = 100; kM=50; kDP=1; N=[1,2,3,8]
    sim.gen_cells[B,:] = kVmax

    # Make it consistent at t=0
    sim.cc_cells[B,:]=(100 if build_inverter else 0)

    for cell in range(4):
        func = (sim.Hill_inv_gate if build_inverter else sim.Hill_buf_gate)
        sim.gen_gating[B,cell] = func (A,N[cell],kM,cell)

    sim.decay_rates[B] = kDP

def post_grn_Hill_xfer (GP, t_shots, cc_shots):
    plot_ion (t_shots, cc_shots, 'A')
    plot_ion (t_shots, cc_shots, 'B')

    import matplotlib.pyplot as plt
    plt.figure()
    plt.xlabel('[A]', fontsize = 20)
    plt.ylabel('[B]', fontsize = 20)
    global N
    A = sim.ion_i['A']; B = sim.ion_i['B']
    A_shots = [s[A,0] for s in cc_shots]
    for cell in range(4):
        B_shots = [s[B,cell] for s in cc_shots]
        plt.plot(A_shots, B_shots, linewidth=2.0)
    plt.legend (['cell'+str(i)+'(N='+str(N[i])+')' for i in range(4)])
    plt.show()

######################################################################
######################################################################

def run_one_test (name, end_time, regress_mode):
    # Run whatever test got chosen on the command line.
    GP = sim.Params()
    eval ('setup_'+name+'(GP)')

    if (regress_mode):	# Works even if setup_...() overrules these params!
        GP.adaptive_timestep = False	# Force regression
        GP.use_implicit = False
        GP.sim_dump_interval = 1
        GP.sim_long_dump_interval = 10

    # Initialize Vmem -- typically 0, or close to that.
    Vm = sim.compute_Vm (sim.cc_cells)
    assert (np.abs(Vm)<.5).all(), \
            "Your initial voltages are too far from charge neutral"
    np.set_printoptions (formatter={'float': '{: 6.3g}'.format}, linewidth=90)
    print ('Initial Vm   ={}mV\n'.format(1000*Vm))

    print ('Starting main simulation loop')
    t_shots, cc_shots = sim.sim (end_time)
    print ('Simulation is finished.')

    # We often want a printed dump of the final simulation results.
    np.set_printoptions (formatter={'float': '{:.6g}'.format}, linewidth=90)
    edb.dump (end_time, sim.cc_cells, edb.Units.mol_per_m3s, True)
    #edb.dump (end_time, sim.cc_cells, edb.Units.mV_per_s, True)

    # Run a post-simulating hook for plotting, if the user has made one.
    import sys
    if ('post_'+name in dir(sys.modules[__name__])) \
          and (not regress_mode):
        eval ('post_'+name+'(GP, t_shots, cc_shots)')

def command_line ():
    # If no command-line arguments are given, prompt for them.
    import sys
    if (len (sys.argv) <= 1):
        args = 'main.py ' + input('Arguments: ')
        sys.argv = re.split (' ', args)

    regress_mode = (len(sys.argv) == 4) and (sys.argv[3]=="--regress")
    if (regress_mode):
        sys.argv = sys.argv[0:3]

    if (len(sys.argv) != 3):
       raise SyntaxError('Usage: python3 main.py test-name-to-run sim-end-time')

    end_time = float (sys.argv[2])

    run_one_test (sys.argv[1], end_time, regress_mode)

command_line()
#import cProfile
#cProfile.run ('run_one_test("worm_lab", 300.0, True)')
